/*
    File:    popcount.hpp
    Created: 28 June 2020 at 09:29 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef POPCNT_H
#define POPCNT_H
#   include <cstdint>
std::size_t popcnt(std::uint64_t z);
#endif