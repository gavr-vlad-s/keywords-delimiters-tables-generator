/*
    File:    type_with_size_modifiers_node.hpp
    Created: 25 August 2021 at 14:13 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TYPE_WITH_SIZE_MODIFIERS_NODE_H
#define TYPE_WITH_SIZE_MODIFIERS_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/type_value_node.hpp"
namespace ast{
    struct Type_with_size_modifiers_node : public Type_value_node{
        enum class Modifier_kind{
            Long, Short
        };

        enum class Type_kind{
            Char,     String,  Bool,
            Ordering, Uint,    Int,
            Float,    Complex, Quat,
        };

        Type_with_size_modifiers_node()                                     = default;
        Type_with_size_modifiers_node(const Type_with_size_modifiers_node&) = default;
        virtual ~Type_with_size_modifiers_node()                            = default;

        Type_with_size_modifiers_node(Type_kind                         kind,
                                      const std::vector<Modifier_kind>& modifiers)
            : kind_{kind}
            , modifiers_{modifiers}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Type_kind                  kind_;
        std::vector<Modifier_kind> modifiers_;
    };
};
#endif