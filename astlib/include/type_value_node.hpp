/*
    File:    type_value_node.hpp
    Created: 25 August 2021 at 11:38 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TYPE_VALUE_NODE_H
#define TYPE_VALUE_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Type_value_node : public Expr_node{
        Type_value_node()                       = default;
        Type_value_node(const Type_value_node&) = default;
        virtual ~Type_value_node()              = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const = 0;
    };
};
#endif