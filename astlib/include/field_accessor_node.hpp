/*
    File:    field_accessor_node.hpp
    Created: 23 August 2021 at 13:00 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef FIELD_ACCESSOR_NODE_H
#define FIELD_ACCESSOR_NODE_H
#   include <cstddef>
#   include "../include/composite_name_component_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast{
    struct Field_accessor_node : public Composite_name_component_node{
        Field_accessor_node()                           = default;
        Field_accessor_node(const Field_accessor_node&) = default;
        virtual ~Field_accessor_node()                  = default;

        explicit Field_accessor_node(const fst_level_scanner::Fst_lvl_token& field_name)
            : field_name_{field_name}
        {
        }

        NodeInfo get_info() const override;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const override;

        fst_level_scanner::Fst_lvl_token field_name_;
    };
};
#endif