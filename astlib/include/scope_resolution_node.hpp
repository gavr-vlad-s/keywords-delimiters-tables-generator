/*
    File:    scope_resolution_node.hpp
    Created: 23 August 2021 at 11:04 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SCOPE_RESOLUTION_NODE_H
#define SCOPE_RESOLUTION_NODE_H
#   include <cstddef>
#   include "../include/composite_name_component_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast{
    struct Scope_resolution_node : public Composite_name_component_node{
        Scope_resolution_node()                             = default;
        Scope_resolution_node(const Scope_resolution_node&) = default;
        virtual ~Scope_resolution_node()                    = default;

        explicit Scope_resolution_node(const fst_level_scanner::Fst_lvl_token& scoped_elem)
            : scoped_elem_{scoped_elem}
        {
        }

        NodeInfo get_info() const override;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const override;

        fst_level_scanner::Fst_lvl_token scoped_elem_;
    };
};
#endif