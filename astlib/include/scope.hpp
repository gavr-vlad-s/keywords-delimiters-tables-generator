/*
    File:    scope.hpp
    Created: 21 August 2021 at 09:25 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SCOPE_H
#define SCOPE_H
#   include <cstddef>
#   include <map>
#   include <memory>
#   include <string>
#   include <unordered_set>
#   include "../include/ast_node.hpp"
namespace symbol_table{
    enum class Identifier_kind{
        Unknown,                        Variable_name,                   Const_name,
        Type_name,                      Func_name,                       Package_func_name,
        Meta_func_name,                 Package_meta_func_name,          Meta_variable_name,
        Meta_const_name,                Field_name,                      Enum_name,
        Struct_name,                    Enum_elem,                       Label_name,
        Category_name,                  Func_argument_name,              Package_func_argument_name,
        Meta_func_argument_name,        Package_meta_func_argument_name, Category_argument_name,
        Package_category_argument_name,
    };

    struct Identifier_info{
        Identifier_kind kind_;
    };

    bool operator==(const Identifier_info& lhs, const Identifier_info& rhs);
    bool operator<(const Identifier_info& lhs, const Identifier_info& rhs);

    using Identifier_info_set = std::map<Identifier_info, std::unordered_set<std::shared_ptr<ast::Node>>>;

    struct Scope final{
        Scope()             = default;
        Scope(const Scope&) = default;
        ~Scope()            = default;

        Scope(bool in_function, const std::map<std::size_t, Identifier_info_set>& ids)
            : in_function_{in_function}
            , ids_{ids}
        {
        }

        Identifier_info_set find_id(std::size_t id) const;

        bool                                        in_function_ = false;
        std::map<std::size_t, Identifier_info_set>  ids_;
    };
};
#endif