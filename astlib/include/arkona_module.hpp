/*
    File:    arkona_module.hpp
    Created: 18 July 2021 at 14:28 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ARKONA_MODULE_H
#define ARKONA_MODULE_H
#   include <memory>
#   include <string>
#   include "../../tries/include/char_trie.hpp"
#   include "../include/module_node.hpp"
#   include "../include/symbol_table.hpp"
namespace ast{
    struct Module final{
        Module()              = default;
        Module(const Module&) = default;
        ~Module()             = default;

        Module(const std::shared_ptr<Char_trie>&                  strs_trie,
               const std::shared_ptr<Char_trie>&                  ids_trie,
               const std::shared_ptr<Module_node>&                module_node,
               const std::shared_ptr<symbol_table::Symbol_table>& sym_table)
            : strs_trie_{strs_trie}
            , ids_trie_{ids_trie}
            , module_node_{module_node}
            , sym_table_{sym_table}
        {
        }

        std::string to_string() const;

        std::shared_ptr<Char_trie>                  strs_trie_;
        std::shared_ptr<Char_trie>                  ids_trie_;
        std::shared_ptr<Module_node>                module_node_;
        std::shared_ptr<symbol_table::Symbol_table> sym_table_;
    };
};
#endif