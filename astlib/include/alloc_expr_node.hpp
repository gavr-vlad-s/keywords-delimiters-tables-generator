/*
    File:    alloc_expr_node.hpp
    Created: 24 August 2021 at 17:04 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ALLOC_EXPR_NODE_H
#define ALLOC_EXPR_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Alloc_expr_node : public Expr_node{
        Alloc_expr_node()                       = default;
        Alloc_expr_node(const Alloc_expr_node&) = default;
        virtual ~Alloc_expr_node()              = default;

        explicit Alloc_expr_node(const std::shared_ptr<Expr_node>& operand)
            : operand_{operand}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::shared_ptr<Expr_node> operand_;
    };
};
#endif