/*
    File:    sizeof_node.hpp
    Created: 22 August 2021 at 17:43 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SIZEOF_NODE_H
#define SIZEOF_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Sizeof_node : public Expr_node{
        enum class Sizeof_kind{
            Cardinality, Size, Datasize
        };

        Sizeof_node()                   = default;
        Sizeof_node(const Sizeof_node&) = default;
        virtual ~Sizeof_node()          = default;

        Sizeof_node(Sizeof_kind                       kind,
                    const std::shared_ptr<Expr_node>& operand)
            : kind_{kind}
            , operand_{operand}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Sizeof_kind                kind_;
        std::shared_ptr<Expr_node> operand_;
    };
};
#endif