/*
    File:    address_node.hpp
    Created: 22 August 2021 at 21:33 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ADDRESS_NODE_H
#define ADDRESS_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Address_node : public Expr_node{
        enum class Address_kind{
            Address, Data_address, Expr_type
        };

        Address_node()                    = default;
        Address_node(const Address_node&) = default;
        virtual ~Address_node()           = default;

        Address_node(Address_kind                      kind,
                     const std::shared_ptr<Expr_node>& operand)
            : kind_{kind}
            , operand_{operand}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Address_kind               kind_;
        std::shared_ptr<Expr_node> operand_;
    };
};
#endif