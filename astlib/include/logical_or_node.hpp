/*
    File:    logical_or_node.hpp
    Created: 21 August 2021 at 15:23 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef LOGICAL_OR_NODE_H
#define LOGICAL_OR_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Logical_or_node : public Expr_node{
        enum class Logical_or_kind{
            Logical_or,          Logical_or_not, Logical_or_full,
            Logical_or_not_full, Logical_xor
        };

        Logical_or_node()                       = default;
        Logical_or_node(const Logical_or_node&) = default;
        virtual ~Logical_or_node()              = default;

        Logical_or_node(Logical_or_kind                   kind,
                        const std::shared_ptr<Expr_node>& lhs,
                        const std::shared_ptr<Expr_node>& rhs)
            : kind_{kind}
            , lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Logical_or_kind            kind_;
        std::shared_ptr<Expr_node> lhs_;
        std::shared_ptr<Expr_node> rhs_;
    };
};
#endif