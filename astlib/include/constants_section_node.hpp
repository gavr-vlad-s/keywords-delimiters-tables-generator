/*
    File:    constants_section_node.hpp
    Created: 28 August 2021 at 15:58 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef CONSTANTS_SECTION_NODE_H
#define CONSTANTS_SECTION_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/stmt_node.hpp"
#   include "../include/constant_node.hpp"
#   include "../include/expr_node.hpp"
namespace ast{
    struct Constants_section_node : public Stmt_node{
        Constants_section_node()                              = default;
        Constants_section_node(const Constants_section_node&) = default;
        virtual ~Constants_section_node()                     = default;

        explicit Constants_section_node(const std::vector<std::shared_ptr<Constant_node>>& constants)
            : constants_{constants}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::vector<std::shared_ptr<Constant_node>> constants_;
    };
};
#endif