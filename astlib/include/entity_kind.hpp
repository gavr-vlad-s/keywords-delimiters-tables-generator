/*
    File:    entity_kind.hpp
    Created: 01 January 2022 at 10:48 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ENTITY_KIND_H
#define ENTITY_KIND_H
namespace symbol_table{
    enum class Entity_kind{
        Unknown, Variable_name, Constant_name, Type_name
    };
};
#endif