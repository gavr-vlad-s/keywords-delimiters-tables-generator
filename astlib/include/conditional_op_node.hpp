/*
    File:    conditional_op_node.hpp
    Created: 21 August 2021 at 13:33 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef CONDITIONAL_OP_NODE_H
#define CONDITIONAL_OP_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Conditional_op_node : public Expr_node{
        enum class Conditional_kind{
            Ordinary, Full
        };

        Conditional_op_node()                           = default;
        Conditional_op_node(const Conditional_op_node&) = default;
        virtual ~Conditional_op_node()                  = default;

        Conditional_op_node(Conditional_kind                  kind,
                            const std::shared_ptr<Expr_node>& condition,
                            const std::shared_ptr<Expr_node>& true_branch,
                            const std::shared_ptr<Expr_node>& false_branch)
            : kind_{kind}
            , condition_{condition}
            , true_branch_{true_branch}
            , false_branch_{false_branch}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Conditional_kind           kind_;
        std::shared_ptr<Expr_node> condition_;
        std::shared_ptr<Expr_node> true_branch_;
        std::shared_ptr<Expr_node> false_branch_;
    };
};
#endif