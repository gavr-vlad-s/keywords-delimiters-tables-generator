/*
    File:    bitwise_and_node.hpp
    Created: 22 August 2021 at 10:43 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef BITWISE_AND_NODE_H
#define BITWISE_AND_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Bitwise_and_node : public Expr_node{
        enum class Bitwise_and_kind{
            Bitwise_and, Bitwise_and_not,   Left_shift,
            Right_shift, Cyclic_left_shift, Cyclic_right_shift
        };

        Bitwise_and_node()                        = default;
        Bitwise_and_node(const Bitwise_and_node&) = default;
        virtual ~Bitwise_and_node()               = default;

        Bitwise_and_node(Bitwise_and_kind                  kind,
                         const std::shared_ptr<Expr_node>& lhs,
                         const std::shared_ptr<Expr_node>& rhs)
            : kind_{kind}
            , lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Bitwise_and_kind           kind_;
        std::shared_ptr<Expr_node> lhs_;
        std::shared_ptr<Expr_node> rhs_;
    };
};
#endif