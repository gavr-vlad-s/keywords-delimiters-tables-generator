/*
    File:    block_node.hpp
    Created: 29 July 2021 at 23:16 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef BLOCK_NODE_H
#define BLOCK_NODE_H
#   include <cstddef>
#   include "../include/ast_node.hpp"
#   include "../include/stmt_node.hpp"
namespace ast{
    struct Block_node : public Node{
        Block_node()                  = default;
        Block_node(const Block_node&) = default;
        virtual ~Block_node()         = default;

        explicit Block_node(const std::vector<std::shared_ptr<Stmt_node>>& elements)
            : elements_{elements}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                              const std::shared_ptr<Char_trie>& ids_trie,
                              const Indents&                    indents) const override;

        std::vector<std::shared_ptr<Stmt_node>> elements_;
    };
};
#endif