/*
    File:    cast_node.hpp
    Created: 24 August 2021 at 12:32 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef CAST_NODE_H
#define CAST_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Cast_node : public Expr_node{
        enum class Cast_kind{
            Reinterpret, Convert
        };

        Cast_node()                 = default;
        Cast_node(const Cast_node&) = default;
        virtual ~Cast_node()        = default;

        Cast_node(Cast_kind                         kind,
                  const std::shared_ptr<Expr_node>& value_to_cast,
                  const std::shared_ptr<Expr_node>& dst_type)
            : kind_{kind}
            , value_to_cast_{value_to_cast}
            , dst_type_{dst_type}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Cast_kind                  kind_;
        std::shared_ptr<Expr_node> value_to_cast_;
        std::shared_ptr<Expr_node> dst_type_;
    };
};
#endif