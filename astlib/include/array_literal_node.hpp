/*
    File:    array_literal_node.hpp
    Created: 24 August 2021 at 14:25 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ARRAY_LITERAL_NODE_H
#define ARRAY_LITERAL_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Array_literal_node : public Expr_node{
        Array_literal_node()                          = default;
        Array_literal_node(const Array_literal_node&) = default;
        virtual ~Array_literal_node()                 = default;

        explicit Array_literal_node(const std::vector<std::shared_ptr<Expr_node>>& components)
            : components_{components}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::vector<std::shared_ptr<Expr_node>> components_;
    };
};
#endif