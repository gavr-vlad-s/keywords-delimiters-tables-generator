/*
    File:    package_func_parametrized_package_with_common_type_group.hpp
    Created: 07 January 2022 at 10:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PACKAGE_FUNC_PARAMETRIZED_PACKAGE_WITH_COMMON_TYPE_GROUP_H
#define PACKAGE_FUNC_PARAMETRIZED_PACKAGE_WITH_COMMON_TYPE_GROUP_H
#   include "../include/package_func_parameter_group.hpp"
#   include "../include/expr_node.hpp"
#   include "../include/ast_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
#   include <memory>
#   include <vector>
namespace ast::package_func{
    struct Parametrized_package_with_common_type_group : public Parameter_group{
        Parametrized_package_with_common_type_group()                                                   = default;
        Parametrized_package_with_common_type_group(const Parametrized_package_with_common_type_group&) = default;
        virtual ~Parametrized_package_with_common_type_group()                                          = default;

        Parametrized_package_with_common_type_group(const fst_level_scanner::Fst_lvl_token& args,
                                                    const std::shared_ptr<Expr_node>&       args_type)
            : args_{args}
            , args_type_{args_type}
        {
        }

        Parameter_group_kind get_kind() const override
        {
            return Parameter_group_kind::Parametrized_package_with_common_type;
        }

        std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                              const std::shared_ptr<Char_trie>& ids_trie,
                              const ast::Indents&               indents) const override;

        fst_level_scanner::Fst_lvl_token args_;
        std::shared_ptr<Expr_node>       args_type_;
    };
};
#endif