/*
    File:    enum_set_node.hpp
    Created: 25 August 2021 at 20:18 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ENUM_SET_NODE_H
#define ENUM_SET_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
#   include "../include/type_value_node.hpp"
namespace ast{
    struct Enum_set_node : public Type_value_node{
        Enum_set_node()                     = default;
        Enum_set_node(const Enum_set_node&) = default;
        virtual ~Enum_set_node()            = default;

        explicit Enum_set_node(const std::shared_ptr<Expr_node>& elem_type)
            : elem_type_{elem_type}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::shared_ptr<Expr_node> elem_type_;
    };
};
#endif