/*
    File:    id_expr_node.hpp
    Created: 11 December 2021 at 16:55 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ID_EXPR_NODE_H
#define ID_EXPR_NODE_H
#   include <cstddef>
#   include "../include/ast_node.hpp"
#   include "../include/expr_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast{
    struct Id_expr_node : public Expr_node{
        Id_expr_node()                    = default;
        Id_expr_node(const Id_expr_node&) = default;
        virtual ~Id_expr_node()           = default;

        explicit Id_expr_node(const fst_level_scanner::Fst_lvl_token& id) : id_{id} {}

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                              const std::shared_ptr<Char_trie>& ids_trie,
                              const Indents&                    indents) const override;

        fst_level_scanner::Fst_lvl_token id_;
    };
};
#endif