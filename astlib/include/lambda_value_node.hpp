/*
    File:    lambda_value_node.hpp
    Created: 27 August 2021 at 13:09 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef LAMBDA_VALUE_NODE_H
#define LAMBDA_VALUE_NODE_H
#   include <cstddef>
#   include "../include/func_signature_node.hpp"
#   include "../include/expr_node.hpp"
#   include "../include/block_node.hpp"
namespace ast{
    struct Lambda_value_node : public Expr_node{
        Lambda_value_node()                              = default;
        Lambda_value_node(const Lambda_value_node&) = default;
        virtual ~Lambda_value_node()                     = default;

        Lambda_value_node(const std::shared_ptr<Func_signature_node>& signature,
                          const std::shared_ptr<Block_node>& body)
            : signature_{signature}
            , body_{body}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::shared_ptr<Func_signature_node> signature_;
        std::shared_ptr<Block_node>          body_;
    };
};
#endif