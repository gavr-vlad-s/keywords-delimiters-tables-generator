/*
    File:    auto_type_node.hpp
    Created: 26 August 2021 at 09:13 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef AUTO_TYPE_NODE_H
#define AUTO_TYPE_NODE_H
#   include <cstddef>
#   include "../include/type_value_node.hpp"
namespace ast{
    struct Auto_type_node : public Type_value_node{
        Auto_type_node()                      = default;
        Auto_type_node(const Auto_type_node&) = default;
        virtual ~Auto_type_node()             = default;

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;
    };
};
#endif