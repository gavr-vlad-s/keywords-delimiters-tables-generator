/*
    File:    symbol_table.hpp
    Created: 18 July 2021 at 11:24 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H
#   include <list>
#   include <memory>
#   include <set>
#   include <string>
#   include "../../tries/include/char_trie.hpp"
#   include "../include/scope.hpp"
namespace symbol_table{
    class Symbol_table final{
    public:
        Symbol_table()                    = default;
        Symbol_table(const Symbol_table&) = default;
        ~Symbol_table()                   = default;

        Symbol_table(const std::shared_ptr<Char_trie>& strs_trie,
                     const std::shared_ptr<Char_trie>& ids_trie)
            : strs_trie_{strs_trie}
            , ids_trie_{ids_trie}
            , scopes_{}
        {
        }

        void set_current_module_name(const std::string& module_name)
        {
            current_module_name_ = module_name;
        }

        std::string get_current_module_name() const
        {
            return current_module_name_;
        }

        void add_used_module_name(const std::string& name)
        {
            used_modules_names_.insert(name);
        }

        std::set<std::string> get_used_modules_names() const
        {
            return used_modules_names_;
        }

        bool is_current_module_name(const std::string& name)
        {
            return current_module_name_ == name;
        }

        bool is_used_module(const std::string& name)
        {
            return used_modules_names_.find(name) != used_modules_names_.end();
        }

        void add_scope(const std::shared_ptr<Scope>& scope)
        {
            scopes_.push_front(scope);
        }

        std::shared_ptr<Scope> get_current_scope()
        {
            if(scopes_.empty()){
                scopes_.push_front(std::make_shared<Scope>());
                return scopes_.front();
            }
            return scopes_.front();
        }

        void close_scope()
        {
            if(scopes_.empty()){
                return;
            }
            scopes_.pop_front();
        }

        Identifier_info_set find_id(std::size_t id) const
        {
            for(const auto& scope : scopes_)
            {
                if(!scope){
                    continue;
                }
                const auto s = scope->find_id(id);
                if(s.empty()){
                    continue;
                }
                return s;
            }
            return {};
        }
    private:
        std::shared_ptr<Char_trie>        strs_trie_ = nullptr;
        std::shared_ptr<Char_trie>        ids_trie_  = nullptr;

        std::list<std::shared_ptr<Scope>> scopes_;

        std::string                current_module_name_;
        std::set<std::string>      used_modules_names_;
    };
};
#endif