/*
    File:    logical_multiand_node.hpp
    Created: 21 August 2021 at 19:31 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef LOGICAL_MULTIAND_NODE_H
#define LOGICAL_MULTIAND_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Logical_multiand_node : public Expr_node{
        Logical_multiand_node()                             = default;
        Logical_multiand_node(const Logical_multiand_node&) = default;
        virtual ~Logical_multiand_node()                    = default;

        explicit Logical_multiand_node(const std::vector<std::shared_ptr<Expr_node>>& operands)
            : operands_{operands}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::vector<std::shared_ptr<Expr_node>> operands_;
    };
};
#endif