/*
    File:    bitwise_or_node.hpp
    Created: 22 August 2021 at 10:28 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef BITWISE_OR_NODE_H
#define BITWISE_OR_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Bitwise_or_node : public Expr_node{
        enum class Bitwise_or_kind{
            Bitwise_or, Bitwise_or_not, Bitwise_xor
        };

        Bitwise_or_node()                       = default;
        Bitwise_or_node(const Bitwise_or_node&) = default;
        virtual ~Bitwise_or_node()              = default;

        Bitwise_or_node(Bitwise_or_kind                   kind,
                        const std::shared_ptr<Expr_node>& lhs,
                        const std::shared_ptr<Expr_node>& rhs)
            : kind_{kind}
            , lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Bitwise_or_kind            kind_;
        std::shared_ptr<Expr_node> lhs_;
        std::shared_ptr<Expr_node> rhs_;
    };
};
#endif