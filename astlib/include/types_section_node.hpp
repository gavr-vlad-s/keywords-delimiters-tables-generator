/*
    File:    types_section_node.hpp
    Created: 28 August 2021 at 16:32 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TYPES_SECTION_NODE_H
#define TYPES_SECTION_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/stmt_node.hpp"
#   include "../include/type_definition_node.hpp"
namespace ast{
    struct Types_section_node : public Stmt_node{
        Types_section_node()                          = default;
        Types_section_node(const Types_section_node&) = default;
        virtual ~Types_section_node()                 = default;

        explicit Types_section_node(const std::vector<std::shared_ptr<Type_definition_node>>& types)
            : types_{types}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::vector<std::shared_ptr<Type_definition_node>> types_;
    };
};
#endif