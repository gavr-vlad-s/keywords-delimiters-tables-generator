/*
    File:    composite_name_component_node.hpp
    Created: 23 August 2021 at 09:38 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef COMPOSITE_NAME_COMPONENT_NODE_H
#define COMPOSITE_NAME_COMPONENT_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Composite_name_component_node : public Expr_node{
        Composite_name_component_node()                                     = default;
        Composite_name_component_node(const Composite_name_component_node&) = default;
        virtual ~Composite_name_component_node()                            = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const = 0;
    };
};
#endif