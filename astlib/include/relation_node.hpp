/*
    File:    relation_node.hpp
    Created: 21 August 2021 at 20:24 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef RELATION_NODE_H
#define RELATION_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Relation_node : public Expr_node{
        enum class Relation_kind{
            Equiv, LT, GT, LEQ, GEQ, EQ, NEQ, Element
        };

        Relation_node()                     = default;
        Relation_node(const Relation_node&) = default;
        virtual ~Relation_node()            = default;

        Relation_node(Relation_kind                     kind,
                      const std::shared_ptr<Expr_node>& lhs,
                      const std::shared_ptr<Expr_node>& rhs)
            : kind_{kind}
            , lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Relation_kind              kind_;
        std::shared_ptr<Expr_node> lhs_;
        std::shared_ptr<Expr_node> rhs_;
    };
};
#endif