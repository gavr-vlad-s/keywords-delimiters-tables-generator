/*
    File:    bitscale_type_node.hpp
    Created: 25 August 2021 at 20:02 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef BITSCALE_TYPE_NODE_H
#define BITSCALE_TYPE_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/expr_node.hpp"
#   include "../include/type_value_node.hpp"
namespace ast{
    struct Bitscale_type_node : public Type_value_node{
        Bitscale_type_node()                          = default;
        Bitscale_type_node(const Bitscale_type_node&) = default;
        virtual ~Bitscale_type_node()                 = default;

        explicit Bitscale_type_node(const std::vector<std::shared_ptr<Expr_node>>& dimensions)
            : dimensions_{dimensions}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::vector<std::shared_ptr<Expr_node>> dimensions_;
    };
};
#endif