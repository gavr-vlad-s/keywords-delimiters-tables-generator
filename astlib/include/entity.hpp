/*
    File:    entity.hpp
    Created: 02 January 2022 at 14:55 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ENTITY_H
#define ENTITY_H
#   include <cstddef>
#   include <map>
#   include <memory>
#   include <string>
#   include <unordered_set>
#   include "../include/entity_kind.hpp"
#   include "../include/ast_node.hpp"
namespace symbol_table{
    struct Entity{
        Entity_kind                kind_;
        std::shared_ptr<ast::Node> pnode_;
    };

    bool operator==(const Entity& lhs, const Entity& rhs);

    struct Entity_hash{
        std::size_t operator()(const Entity& e) const;
    };

    using Entity_set = std::unordered_set<Entity, Entity_hash>;

    using Entity_map = std::map<std::u32string, Entity_set>;
};
#endif