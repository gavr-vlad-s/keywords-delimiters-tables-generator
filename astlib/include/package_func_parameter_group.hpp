/*
    File:    package_func_parameter_group.hpp
    Created: 06 January 2022 at 14:08 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PACKAGE_FUNC_PARAMETER_GROUP_H
#define PACKAGE_FUNC_PARAMETER_GROUP_H
#   include <string>
#   include "../include/ast_node.hpp"
#   include "../../tries/include/char_trie.hpp"
namespace ast::package_func{
    enum class Parameter_group_kind{
        Ordinary,                                       Non_parametrized_package,
        Parametrized_package_with_common_type,          Parametrized_package_with_different_types,
        Non_parametrized_tuple_package,                 Parametrized_tuple_package_with_common_type,
        Parametrized_tuple_package_with_different_types
    };

    struct Parameter_group{
        Parameter_group()                       = default;
        Parameter_group(const Parameter_group&) = default;
        virtual ~Parameter_group()              = default;

        virtual Parameter_group_kind get_kind() const = 0;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const ast::Indents&               indents) const = 0;
    };
};
#endif