/*
    File:    logical_and_node.hpp
    Created: 21 August 2021 at 15:36 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef LOGICAL_AND_NODE_H
#define LOGICAL_AND_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Logical_and_node : public Expr_node{
        enum class Logical_and_kind{
            Logical_and,      Logical_and_not,
            Logical_and_full, Logical_and_not_full
        };

        Logical_and_node()                        = default;
        Logical_and_node(const Logical_and_node&) = default;
        virtual ~Logical_and_node()               = default;

        Logical_and_node(Logical_and_kind                  kind,
                         const std::shared_ptr<Expr_node>& lhs,
                         const std::shared_ptr<Expr_node>& rhs)
            : kind_{kind}
            , lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Logical_and_kind           kind_;
        std::shared_ptr<Expr_node> lhs_;
        std::shared_ptr<Expr_node> rhs_;
    };
};
#endif