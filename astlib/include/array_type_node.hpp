/*
    File:    array_type_node.hpp
    Created: 25 August 2021 at 19:33 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ARRAY_TYPE_NODE_H
#define ARRAY_TYPE_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/expr_node.hpp"
#   include "../include/type_value_node.hpp"
namespace ast{
    struct Array_type_node : public Type_value_node{
        Array_type_node()                                = default;
        Array_type_node(const Array_type_node&) = default;
        virtual ~Array_type_node()                       = default;

        explicit Array_type_node(const std::vector<std::shared_ptr<Expr_node>>& dimensions,
                                 const std::shared_ptr<Expr_node>& elem_type)
            : dimensions_{dimensions}
            , elem_type_{elem_type}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::vector<std::shared_ptr<Expr_node>> dimensions_;
        std::shared_ptr<Expr_node>              elem_type_;
    };
};
#endif