/*
    File:    power_node.hpp
    Created: 22 August 2021 at 12:53 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef POWER_NODE_H
#define POWER_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Power_node : public Expr_node{
        enum class Power_kind{
            Power, Float_power
        };

        Power_node()                  = default;
        Power_node(const Power_node&) = default;
        virtual ~Power_node()         = default;

        Power_node(Power_kind                        kind,
                   const std::shared_ptr<Expr_node>& lhs,
                   const std::shared_ptr<Expr_node>& rhs)
            : kind_{kind}
            , lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Power_kind                 kind_;
        std::shared_ptr<Expr_node> lhs_;
        std::shared_ptr<Expr_node> rhs_;
    };
};
#endif