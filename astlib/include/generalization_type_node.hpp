/*
    File:    generalization_type_node.hpp
    Created: 26 August 2021 at 09:33 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef GENERALIZATION_TYPE_NODE_H
#define GENERALIZATION_TYPE_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/expr_node.hpp"
#   include "../include/type_value_node.hpp"
namespace ast{
    struct Generalization_type_node : public Type_value_node{
        Generalization_type_node()                                = default;
        Generalization_type_node(const Generalization_type_node&) = default;
        virtual ~Generalization_type_node()                       = default;

        explicit Generalization_type_node(const std::vector<std::shared_ptr<Expr_node>>& generalized_types)
            : generalized_types_{generalized_types}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::vector<std::shared_ptr<Expr_node>> generalized_types_;
    };
};
#endif