/*
    File:    reference_type_node.hpp
    Created: 25 August 2021 at 20:35 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef REFERENCE_TYPE_NODE_H
#define REFERENCE_TYPE_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
#   include "../include/type_value_node.hpp"
namespace ast{
    struct Reference_type_node : public Type_value_node{
        Reference_type_node()                           = default;
        Reference_type_node(const Reference_type_node&) = default;
        virtual ~Reference_type_node()                  = default;

        Reference_type_node(bool is_const, const std::shared_ptr<Expr_node>& value_type)
            : is_const_{is_const}
            , value_type_{value_type}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        bool                       is_const_ = false;
        std::shared_ptr<Expr_node> value_type_;
    };
};
#endif