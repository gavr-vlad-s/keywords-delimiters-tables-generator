/*
    File:    constant_size_type_node.hpp
    Created: 25 August 2021 at 11:42 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef CONSTANT_SIZE_TYPE_NODE_H
#define CONSTANT_SIZE_TYPE_NODE_H
#   include <cstddef>
#   include "../include/type_value_node.hpp"
namespace ast{
    struct Constant_size_type_node : public Type_value_node{
        enum class Type_kind{
            Char8,      Char16,     Char32,    String8,
            String16,   String32,   Bool8,     Bool16,
            Bool32,     Bool64,     Ordering8, Ordering16,
            Ordering32, Ordering64, Uint8,     Uint16,
            Uint32,     Uint64,     Uint128,   Int8,
            Int16,      Int32,      Int64,     Int128,
            Float32,    Float64,    Float80,   Float128,
            Complex32,  Complex64,  Complex80, Complex128,
            Quat32,     Quat64,     Quat80,    Quat128,
            Void
        };

        Constant_size_type_node()                               = default;
        Constant_size_type_node(const Constant_size_type_node&) = default;
        virtual ~Constant_size_type_node()                      = default;

        explicit Constant_size_type_node(Type_kind kind)
            : kind_{kind}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Type_kind kind_;
    };
};
#endif