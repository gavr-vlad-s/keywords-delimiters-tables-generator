/*
    File:    function_ptr_type_node.hpp
    Created: 27 August 2021 at 12:41 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef FUNCTION_PTR_TYPE_NODE_H
#define FUNCTION_PTR_TYPE_NODE_H
#   include <cstddef>
#   include "../include/func_signature_node.hpp"
#   include "../include/type_value_node.hpp"
namespace ast{
    struct Function_ptr_type_node : public Type_value_node{
        Function_ptr_type_node()                              = default;
        Function_ptr_type_node(const Function_ptr_type_node&) = default;
        virtual ~Function_ptr_type_node()                     = default;

        Function_ptr_type_node(bool is_pure, const std::shared_ptr<Func_signature_node>& signature)
            : is_pure_{is_pure}
            , signature_{signature}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        bool                                 is_pure_ = false;
        std::shared_ptr<Func_signature_node> signature_;
    };
};
#endif