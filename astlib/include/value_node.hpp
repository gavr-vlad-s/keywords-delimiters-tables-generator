/*
    File:    value_node.hpp
    Created: 14 July 2021 at 19:07 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef VALUE_NODE_H
#define VALUE_NODE_H
#   include <cstdint>
#   include <quadmath.h>
#   include "../../numbers/include/quaternion.hpp"
#   include "../include/expr_node.hpp"
namespace ast{
    enum class Value_kind : std::uint16_t {
        Int_value,  Float_value,    Complex_value,
        Quat_value, Char_value,     String_value,
        Bool_value, Ordering_value
    };

    enum class Float_kind : std::uint16_t {
        Float32, Float64, Float80, Float128
    };

    enum class Complex_kind : std::uint16_t {
        Complex32, Complex64, Complex80, Complex128
    };

    enum class Quat_kind : std::uint16_t {
        Quat32, Quat64, Quat80, Quat128
    };

    enum class Char_kind : std::uint16_t {
        Char8, Char16, Char32
    };

    enum class String_kind : std::uint16_t {
        String8, String16, String32
    };

    enum class Ordering{
        Less, Equal, Greater
    };

    struct Value_info{
        Value_kind    val_kind_;
        std::uint16_t val_subkind_;
        union{
            unsigned __int128        int_val_;
            __float128               float_val_;
            __complex128             complex_val_;
            quat::quat_t<__float128> quat_val_;
            char32_t                 char_val_;
            std::size_t              str_index_;
            bool                     bool_val_;
            Ordering                 ordering_val_;
        };
    };

    struct Value_node : public Expr_node{
        Value_node()                  = default;
        Value_node(const Value_node&) = default;
        virtual ~Value_node()         = default;

        explicit Value_node(const Value_info& info) : value_{info} {}

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                              const std::shared_ptr<Char_trie>& ids_trie,
                              const Indents&                    indents) const override;

        Value_info value_;
    };

    Value_info make_int_value(unsigned __int128 int_val);

    Value_info make_float_value(__float128 float_val,
                                Float_kind float_kind = Float_kind::Float64);

    Value_info make_complex_value(const __complex128& complex_val,
                                  Complex_kind        complex_kind = Complex_kind::Complex64);

    Value_info make_quat_value(const quat::quat_t<__float128>& quat_val,
                               Quat_kind                       quat_kind = Quat_kind::Quat64);

    Value_info make_char_value(char32_t   char_val,
                                Char_kind char_kind = Char_kind::Char32);

    Value_info make_str_value(std::size_t str_index,
                              String_kind str_kind = String_kind::String32);

    Value_info make_bool_value(bool bool_val);

    Value_info make_ordering_value(Ordering ordering_val);
};
#endif