/*
    File:    package_func_signature_node.hpp
    Created: 06 January 2022 at 13:49 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PACKAGE_FUNC_SIGNATURE_NODE_H
#define PACKAGE_FUNC_SIGNATURE_NODE_H
#   include <cstddef>
#   include <memory>
#   include <vector>
#   include "../include/expr_node.hpp"
#   include "../include/ast_node.hpp"
#   include "../include/package_func_parameter_group.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast{
    struct Package_func_signature_node : public Node{
        Package_func_signature_node()                                   = default;
        Package_func_signature_node(const Package_func_signature_node&) = default;
        virtual ~Package_func_signature_node()                          = default;

        Package_func_signature_node(const std::vector<std::shared_ptr<ast::package_func::Parameter_group>>& formal_args,
                                    const std::shared_ptr<Expr_node>&                                       result_type)
            : formal_args_{formal_args}
            , result_type_{result_type}
        {
        }

        NodeInfo get_info() const override;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const override;

        std::vector<std::shared_ptr<ast::package_func::Parameter_group>> formal_args_;
        std::shared_ptr<Expr_node>                                       result_type_;
    };
};
#endif