/*
    File:    package_func_parameter_group_variant.hpp
    Created: 07 January 2022 at 10:26 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PACKAGE_FUNC_PARAMETER_GROUP_VARIANT_H
#define PACKAGE_FUNC_PARAMETER_GROUP_VARIANT_H
#   include "../include/package_func_non_parametrized_package_group.hpp"
#   include "../include/package_func_non_parametrized_tuple_package_group.hpp"
#   include "../include/package_func_ordinary_parameter_group.hpp"
#   include "../include/package_func_parametrized_package_with_common_type_group.hpp"
#   include "../include/package_func_parametrized_package_with_different_types_group.hpp"
#   include "../include/package_func_parametrized_tuple_package_with_common_type_group.hpp"
#   include "../include/package_func_parametrized_tuple_package_with_different_types_group.hpp"
#endif