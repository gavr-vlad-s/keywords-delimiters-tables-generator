/*
    File:    alloc_bitscale_expr_node.hpp
    Created: 24 August 2021 at 18:12 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ALLOC_BITSCALE_EXPR_NODE_H
#define ALLOC_BITSCALE_EXPR_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Alloc_bitscale_expr_node : public Expr_node{
        Alloc_bitscale_expr_node()                                = default;
        Alloc_bitscale_expr_node(const Alloc_bitscale_expr_node&) = default;
        virtual ~Alloc_bitscale_expr_node()                       = default;

        explicit Alloc_bitscale_expr_node(const std::vector<std::shared_ptr<Expr_node>>& operands)
            : operands_{operands}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::vector<std::shared_ptr<Expr_node>> operands_;
    };
};
#endif