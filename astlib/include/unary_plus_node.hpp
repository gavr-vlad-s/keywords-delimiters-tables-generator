/*
    File:    unary_plus_node.hpp
    Created: 22 August 2021 at 17:25 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef UNARY_PLUS_NODE_H
#define UNARY_PLUS_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Unary_plus_node : public Expr_node{
        enum class Unary_plus_kind{
            Plus,                    Minus,                   Elem_type,
            Min_value_in_collection, Max_value_in_collection, Min_value_of_type,
            Max_value_of_type
        };

        Unary_plus_node()                       = default;
        Unary_plus_node(const Unary_plus_node&) = default;
        virtual ~Unary_plus_node()              = default;

        Unary_plus_node(Unary_plus_kind                   kind,
                        const std::shared_ptr<Expr_node>& operand)
            : kind_{kind}
            , operand_{operand}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Unary_plus_kind            kind_;
        std::shared_ptr<Expr_node> operand_;
    };
};
#endif