/*
    File:    used_modules_node.hpp
    Created: 15 July 2021 at 21:14 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef USED_MODULES_NODE_H
#define USED_MODULES_NODE_H
#   include <cstddef>
#   include <memory>
#   include <vector>
#   include "../include/ast_node.hpp"
#   include "../include/qualified_id_node.hpp"
namespace ast{
    struct Used_modules_node : public Node{
        Used_modules_node()                         = default;
        Used_modules_node(const Used_modules_node&) = default;
        virtual ~Used_modules_node()                = default;

        explicit Used_modules_node(const std::vector<std::shared_ptr<Qualified_id_node>>& names)
            : names_{names}
        {
        }

        NodeInfo get_info() const override;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const override;

        std::vector<std::shared_ptr<Qualified_id_node>> names_;
    };
};
#endif