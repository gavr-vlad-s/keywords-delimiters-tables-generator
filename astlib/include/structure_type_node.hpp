/*
    File:    structure_type_node.hpp
    Created: 26 August 2021 at 10:40 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef STRUCTURE_TYPE_NODE_H
#define STRUCTURE_TYPE_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/expr_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast{
    struct Fields_group_def{
        std::vector<fst_level_scanner::Fst_lvl_token> fields_;
        std::shared_ptr<Expr_node>                    fields_type_;
    };

    struct Structure_type_node : public Expr_node{
        Structure_type_node()                           = default;
        Structure_type_node(const Structure_type_node&) = default;
        virtual ~Structure_type_node()                  = default;

        Structure_type_node(const fst_level_scanner::Fst_lvl_token& name,
                            const std::vector<Fields_group_def>&    fields_def)
            : name_{name}
            , fields_def_{fields_def}
        {
        }

        NodeInfo get_info() const override;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const override;

        fst_level_scanner::Fst_lvl_token name_;
        std::vector<Fields_group_def>    fields_def_;
    };
};
#endif