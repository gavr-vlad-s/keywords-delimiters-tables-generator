/*
    File:    alias_node.hpp
    Created: 05 February 2022 at 16:21 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ALIAS_NODE_H
#define ALIAS_NODE_H
#   include <cstddef>
#   include "../include/stmt_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
#   include "../include/expr_node.hpp"
namespace ast{
    struct Alias_node : public Stmt_node{
        Alias_node()                  = default;
        Alias_node(const Alias_node&) = default;
        virtual ~Alias_node()         = default;

        Alias_node(bool                                    is_exported,
                   const fst_level_scanner::Fst_lvl_token& name,
                   const std::shared_ptr<Expr_node>&       definition)
            : is_exported_{is_exported}
            , name_{name}
            , definition_{definition}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        bool                             is_exported_ = false;
        fst_level_scanner::Fst_lvl_token name_;
        std::shared_ptr<Expr_node>       definition_;
    };
};
#endif