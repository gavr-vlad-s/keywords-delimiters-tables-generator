/*
    File:    expr_node.hpp
    Created: 21 August 2021 at 10:06 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef EXPR_NODE_H
#define EXPR_NODE_H
#   include <cstddef>
#   include "../include/stmt_node.hpp"
namespace ast{
    struct Expr_node : public Stmt_node{
        Expr_node()                 = default;
        Expr_node(const Expr_node&) = default;
        virtual ~Expr_node()        = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const = 0;
    };
};
#endif