/*
    File:    ast_node.hpp
    Created: 12 July 2021 at 20:26 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef AST_NODE_H
#define AST_NODE_H
#   include <cstddef>
#   include <cstdint>
#   include <memory>
#   include <string>
#   include "../../tries/include/char_trie.hpp"
namespace ast{
    enum class Node_kind : std::uint16_t{
        Unknown,             Module,                        Id,
        Value,               Type_def,                      Func_def,
        Proto_def,           Qualified_id,                  Used_modules,
        Block,               Assignment,                    Conditional_op,
        Logical_or,          Logical_and,                   Logical_not,
        Logical_multiand,    Compare_with,                  Relation,
        Range,               Bitwise_or,                    Bitwise_and,
        Bitwise_not,         Add,                           Mul,
        Power,               Postfix_inc,                   Prefix_inc,
        Unary_plus,          Sizeof_op,                     Index_sizeof,
        Dereference,         Ptr_def,                       Address,
        Scoped_elem,         Field_accessor,                Call_op,
        Indexing_op,         Metacall_op,                   Composite_name,
        Structure_value,     Cast_op,                       Tuple_expr,
        Set_expr,            Array_literal,                 Bitscale_literal,
        Free_expr,           Alloc_expr,                    Alloc_bitscale_expr,
        Constant_size_type,  Base_type_with_type_modifiers, Enum_type,
        Array_type,          Bitscale_type,                 Enum_set_type,
        Reference_type,      Auto_type,                     Type_type,
        Generalization_type, Structure_type,                Func_signature,
        Func_ptr_type,       Lambda_value,                  Variables_section,
        Constant,            Constants_section,             Types_section,
        Func_header,         Package_func_signature,        Package_func_header,
        Alias
    };

    struct NodeInfo{
        Node_kind     kind_;
        std::uint16_t subkind_;
    };

    struct Indents{
        std::size_t indent_;
        std::size_t indent_inc_;
    };

    struct Node{
        Node()            = default;
        Node(const Node&) = default;
        virtual ~Node()   = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const = 0;
    };
};
#endif