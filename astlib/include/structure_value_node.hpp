/*
    File:    structure_value_node.hpp
    Created: 23 August 2021 at 15:55 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef STRUCTURE_VALUE_NODE_H
#define STRUCTURE_VALUE_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/expr_node.hpp"
#   include "../include/composite_name_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast{
    struct Field_initializer{
        fst_level_scanner::Fst_lvl_token field_name_;
        std::shared_ptr<Expr_node>       field_value_;
    };

    struct Structure_value_node : public Expr_node{
        Structure_value_node()                           = default;
        Structure_value_node(const Structure_value_node&) = default;
        virtual ~Structure_value_node()                  = default;

        Structure_value_node(const std::shared_ptr<Composite_name_node>& name,
                             const std::vector<Field_initializer>&       initializers)
            : name_{name}
            , initializers_{initializers}
        {
        }

        NodeInfo get_info() const override;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const override;

        std::shared_ptr<Composite_name_node> name_;
        std::vector<Field_initializer>       initializers_;
    };
};
#endif