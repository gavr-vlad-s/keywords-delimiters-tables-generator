/*
    File:    type_definition_node.hpp
    Created: 28 August 2021 at 16:18 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TYPE_DEFINITION_NODE_H
#define TYPE_DEFINITION_NODE_H
#   include <cstddef>
#   include "../include/stmt_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
#   include "../include/expr_node.hpp"
namespace ast{
    struct Type_definition_node : public Stmt_node{
        Type_definition_node()                            = default;
        Type_definition_node(const Type_definition_node&) = default;
        virtual ~Type_definition_node()                   = default;

        Type_definition_node(bool                                    is_exported,
                             const fst_level_scanner::Fst_lvl_token& name,
                             const std::shared_ptr<Expr_node>&       definition)
            : is_exported_{is_exported}
            , name_{name}
            , definition_{definition}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        bool                             is_exported_ = false;
        fst_level_scanner::Fst_lvl_token name_;
        std::shared_ptr<Expr_node>       definition_;
    };
};
#endif