/*
    File:    assignment_node.hpp
    Created: 21 August 2021 at 10:19 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ASSIGNMENT_NODE_H
#define ASSIGNMENT_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Assignment_node : public Expr_node{
        enum class Assignment_kind{
            Assign,                      Copy,                      Logical_and_assign,
            Logical_and_full_assign,     Logical_and_not_assign,    Logical_and_not_full_assign,
            Logical_or_assign,           Logical_or_full_assign,    Logical_or_not_assign,
            Logical_or_not_full_assign,  Logical_xor_assign,        Bitwise_and_assign,
            Bitwise_and_not_assign,      Left_shift_assign,         Right_shift_assign,
            Cyclic_left_shift_assign,    Cyclic_right_shift_assign, Bitwise_or_assign,
            Bitwise_or_not_assign,       Bitwise_xor_assign,        Plus_assign,
            Minus_assign,                Type_add_assign,           Algebraic_or_assign,
            Mul_assign,                  Div_assign,                Set_minus_assign,
            Symmetric_difference_assign, Remainder_assign,          Float_remainder_assign,
            Power_assign,                Float_power_assign
        };

        Assignment_node()                       = default;
        Assignment_node(const Assignment_node&) = default;
        virtual ~Assignment_node()              = default;

        Assignment_node(Assignment_kind                   kind,
                        const std::shared_ptr<Expr_node>& lhs,
                        const std::shared_ptr<Expr_node>& rhs)
            : kind_{kind}
            , lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Assignment_kind            kind_;
        std::shared_ptr<Expr_node> lhs_;
        std::shared_ptr<Expr_node> rhs_;
    };
};
#endif