/*
    File:    index_sizeof_node.hpp
    Created: 22 August 2021 at 20:40 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef INDEX_SIZEOF_NODE_H
#define INDEX_SIZEOF_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Index_sizeof_node : public Expr_node{
        Index_sizeof_node()                         = default;
        Index_sizeof_node(const Index_sizeof_node&) = default;
        virtual ~Index_sizeof_node()                = default;

        Index_sizeof_node(const std::shared_ptr<Expr_node>& lhs,
                          const std::shared_ptr<Expr_node>& rhs)
            : lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::shared_ptr<Expr_node> lhs_;
        std::shared_ptr<Expr_node> rhs_;
    };
};
#endif