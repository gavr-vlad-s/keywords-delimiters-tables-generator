/*
    File:    create_standard_modules.hpp
    Created: 21 November 2021 at 13:28 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef CREATE_STANDARD_MODULES_H
#define CREATE_STANDARD_MODULES_H
#   include <map>
#   include <string>
#   include "../include/arkona_module.hpp"
#   include "../../tries/include/char_trie.hpp"
#   include "../include/module_node.hpp"
#   include "../include/symbol_table.hpp"

namespace ast{
    class Create_standard_modules{
    public:
        Create_standard_modules()                               = default;
        Create_standard_modules(const Create_standard_modules&) = default;
        virtual ~Create_standard_modules()                      = default;

        Create_standard_modules(const std::shared_ptr<Char_trie>&                  strs_trie,
                                const std::shared_ptr<Char_trie>&                  ids_trie,
                                const std::shared_ptr<symbol_table::Symbol_table>& sym_table)
            : strs_trie_{strs_trie}
            , ids_trie_{ids_trie}
            , sym_table_{sym_table}
        {
        }

        std::map<std::u32string, std::shared_ptr<ast::Module>> operator()();
    private:
        std::shared_ptr<Char_trie>                  strs_trie_;
        std::shared_ptr<Char_trie>                  ids_trie_;
        std::shared_ptr<symbol_table::Symbol_table> sym_table_;

        std::shared_ptr<ast::Module> strings_encodings_module_create();
        std::shared_ptr<ast::Module> strings_formatting_module_create();
        std::shared_ptr<ast::Module> system_io_module_create();
        std::shared_ptr<ast::Module> system_io_files_module_create();
        std::shared_ptr<ast::Module> prelude_module_create();

        std::map<std::u32string, std::size_t> indices_of_standard_identifiers_;

        void create_indices_for_standard_identifiers();
    };
};
#endif