/*
    File:    prefix_inc_node.hpp
    Created: 22 August 2021 at 13:57 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PREFIX_INC_NODE_H
#define PREFIX_INC_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Prefix_inc_node : public Expr_node{
        enum class Prefix_inc_kind{
            Inc, Dec, Wrapping_inc, Wrapping_dec
        };

        Prefix_inc_node()                       = default;
        Prefix_inc_node(const Prefix_inc_node&) = default;
        virtual ~Prefix_inc_node()              = default;

        Prefix_inc_node(Prefix_inc_kind                   kind,
                        const std::shared_ptr<Expr_node>& operand)
            : kind_{kind}
            , operand_{operand}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Prefix_inc_kind            kind_;
        std::shared_ptr<Expr_node> operand_;
    };
};
#endif