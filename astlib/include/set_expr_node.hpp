/*
    File:    set_expr_node.hpp
    Created: 24 August 2021 at 14:21 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SET_EXPR_NODE_H
#define SET_EXPR_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Set_expr_node : public Expr_node{
        Set_expr_node()                     = default;
        Set_expr_node(const Set_expr_node&) = default;
        virtual ~Set_expr_node()            = default;

        explicit Set_expr_node(const std::vector<std::shared_ptr<Expr_node>>& components)
            : components_{components}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::vector<std::shared_ptr<Expr_node>> components_;
    };
};
#endif