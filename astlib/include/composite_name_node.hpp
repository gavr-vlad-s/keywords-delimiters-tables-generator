/*
    File:    composite_name_node.hpp
    Created: 23 August 2021 at 15:23 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef COMPOSITE_NAME_NODE_H
#define COMPOSITE_NAME_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/composite_name_component_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast{
    struct Composite_name_node : public Composite_name_component_node{
        Composite_name_node()                           = default;
        Composite_name_node(const Composite_name_node&) = default;
        virtual ~Composite_name_node()                  = default;

        Composite_name_node(const fst_level_scanner::Fst_lvl_token&        start_name,
                            const std::vector<std::shared_ptr<Expr_node>>& components)
            : start_name_{start_name}
            , components_{components}
        {
        }

        NodeInfo get_info() const override;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const override;

        fst_level_scanner::Fst_lvl_token        start_name_;
        std::vector<std::shared_ptr<Expr_node>> components_;
    };
};
#endif