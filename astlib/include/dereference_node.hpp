/*
    File:    dereference_node.hpp
    Created: 22 August 2021 at 20:52 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef DEREFERENCE_NODE_H
#define DEREFERENCE_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Dereference_node : public Expr_node{
        Dereference_node()                        = default;
        Dereference_node(const Dereference_node&) = default;
        virtual ~Dereference_node()               = default;

        Dereference_node(std::size_t                       num_of_ops,
                         const std::shared_ptr<Expr_node>& operand)
            : num_of_ops_{num_of_ops}
            , operand_{operand}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::size_t                num_of_ops_ = 0;
        std::shared_ptr<Expr_node> operand_;
    };
};
#endif