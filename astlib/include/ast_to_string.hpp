/*
    File:    ast_to_string.hpp
    Created: 17 July 2021 at 21:49 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef AST_TO_STRING_H
#define AST_TO_STRING_H
#   include <memory>
#   include <string>
#   include "../include/ast_node.hpp"
#   include "../../tries/include/char_trie.hpp"
#   include "../include/module_node.hpp"
namespace ast{
    class AST_to_string final{
    public:
        AST_to_string()                     = default;
        AST_to_string(const AST_to_string&) = default;
        ~AST_to_string()                    = default;

        AST_to_string(const std::shared_ptr<Char_trie>& strs_trie,
                      const std::shared_ptr<Char_trie>& ids_trie)
            : strs_trie_{strs_trie}
            , ids_trie_{ids_trie}
        {
        }

        std::string operator()(const std::shared_ptr<Module_node>& module_node,
                               const Indents&                      indents) const;
    private:
        std::shared_ptr<Char_trie> strs_trie_ = nullptr;
        std::shared_ptr<Char_trie> ids_trie_ = nullptr;
    };
};
#endif