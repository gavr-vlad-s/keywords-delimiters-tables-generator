/*
    File:    qualified_id_node.hpp
    Created: 15 July 2021 at 21:01 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef QUALIFIED_ID_NODE_H
#define QUALIFIED_ID_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/ast_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast{
    struct Qualified_id_node : public Node{
        Qualified_id_node()                         = default;
        Qualified_id_node(const Qualified_id_node&) = default;
        virtual ~Qualified_id_node()                = default;

        explicit Qualified_id_node(const std::vector<fst_level_scanner::Fst_lvl_token>& components)
            : components_{components}
        {
        }

        NodeInfo get_info() const override;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const override;

        std::vector<fst_level_scanner::Fst_lvl_token> components_;
    };
};
#endif