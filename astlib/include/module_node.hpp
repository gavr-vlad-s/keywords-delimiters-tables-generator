/*
    File:    module_node.hpp
    Created: 15 July 2021 at 21:24 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef MODULE_NODE_H
#define MODULE_NODE_H
#   include <memory>
#   include "../include/ast_node.hpp"
#   include "../include/qualified_id_node.hpp"
#   include "../include/used_modules_node.hpp"
#   include "../include/block_node.hpp"
namespace ast{
    struct Module_node : public Node{
        Module_node()                   = default;
        Module_node(const Module_node&) = default;
        virtual ~Module_node()          = default;

        Module_node(const std::shared_ptr<Qualified_id_node>& name,
                    const std::shared_ptr<Used_modules_node>& used_modules,
                    const std::shared_ptr<Block_node>&        body)
            : name_{name}, used_modules_{used_modules}, body_{body}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                              const std::shared_ptr<Char_trie>& ids_trie,
                              const Indents&                    indents) const override;

        std::shared_ptr<Qualified_id_node> name_;
        std::shared_ptr<Used_modules_node> used_modules_;
        std::shared_ptr<Block_node>        body_;
    };
};
#endif