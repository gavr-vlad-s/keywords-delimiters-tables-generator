/*
    File:    indexing_op_node.hpp
    Created: 23 August 2021 at 13:23 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef INDEXING_OP_NODE_H
#define INDEXING_OP_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/composite_name_component_node.hpp"
namespace ast{
    struct Indexing_op_node : public Composite_name_component_node{
        Indexing_op_node()                        = default;
        Indexing_op_node(const Indexing_op_node&) = default;
        virtual ~Indexing_op_node()               = default;

        explicit Indexing_op_node(const std::vector<std::shared_ptr<Expr_node>>& operands)
            : operands_{operands}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::vector<std::shared_ptr<Expr_node>> operands_;
    };
};
#endif