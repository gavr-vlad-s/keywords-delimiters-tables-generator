/*
    File:    mul_node.hpp
    Created: 22 August 2021 at 12:42 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef MUL_NODE_H
#define MUL_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Mul_node : public Expr_node{
        enum class Mul_kind{
            Mul,                  Div,       Set_minus,
            Symmetric_difference, Remainder, Float_remainder
        };

        Mul_node()                = default;
        Mul_node(const Mul_node&) = default;
        virtual ~Mul_node()       = default;

        Mul_node(Mul_kind                  kind,
                const std::shared_ptr<Expr_node>& lhs,
                const std::shared_ptr<Expr_node>& rhs)
            : kind_{kind}
            , lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Mul_kind                   kind_;
        std::shared_ptr<Expr_node> lhs_;
        std::shared_ptr<Expr_node> rhs_;
    };
};
#endif