/*
    File:    enum_type_node.hpp
    Created: 25 July 2021 at 18:41 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ENUM_TYPE_NODE_H
#define ENUM_TYPE_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/type_value_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast{
    struct Enum_type_node : public Type_value_node{
        Enum_type_node()                      = default;
        Enum_type_node(const Enum_type_node&) = default;
        virtual ~Enum_type_node()             = default;

        Enum_type_node(const fst_level_scanner::Fst_lvl_token&              name,
                       const std::vector<fst_level_scanner::Fst_lvl_token>& components)
            : name_{name}
            , components_{components}
        {
        }

        NodeInfo get_info() const override;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const override;

        fst_level_scanner::Fst_lvl_token              name_;
        std::vector<fst_level_scanner::Fst_lvl_token> components_;
    };
};
#endif