/*
    File:    range_node.hpp
    Created: 22 August 2021 at 09:50 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef RANGE_NODE_H
#define RANGE_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Range_node : public Expr_node{
        enum class Range_kind{
            Segment, Semisegment
        };

        Range_node()                  = default;
        Range_node(const Range_node&) = default;
        virtual ~Range_node()         = default;

        Range_node(Range_kind                        kind,
                   const std::shared_ptr<Expr_node>& start_value,
                   const std::shared_ptr<Expr_node>& end_value,
                   const std::shared_ptr<Expr_node>& step_value)
            : kind_{kind}
            , start_value_{start_value}
            , end_value_{end_value}
            , step_value_{step_value}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Range_kind                 kind_;
        std::shared_ptr<Expr_node> start_value_;
        std::shared_ptr<Expr_node> end_value_;
        std::shared_ptr<Expr_node> step_value_;
    };
};
#endif