/*
    File:    func_signature_node.hpp
    Created: 27 August 2021 at 09:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef FUNC_SIGNATURE_NODE_H
#define FUNC_SIGNATURE_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/expr_node.hpp"
#   include "../include/ast_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast{
    struct Group_of_func_args{
        std::vector<fst_level_scanner::Fst_lvl_token> args_;
        std::shared_ptr<Expr_node>                    args_type_;
    };

    struct Func_signature_node : public Node{
        Func_signature_node()                           = default;
        Func_signature_node(const Func_signature_node&) = default;
        virtual ~Func_signature_node()                  = default;

        Func_signature_node(const std::vector<Group_of_func_args>& formal_args,
                            const std::shared_ptr<Expr_node>&      result_type)
            : formal_args_{formal_args}
            , result_type_{result_type}
        {
        }

        NodeInfo get_info() const override;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const override;

        std::vector<Group_of_func_args> formal_args_;
        std::shared_ptr<Expr_node>      result_type_;
    };
};
#endif