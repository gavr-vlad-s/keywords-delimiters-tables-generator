/*
    File:    postfix_inc_node.hpp
    Created: 22 August 2021 at 13:40 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef POSTFIX_INC_NODE_H
#define POSTFIX_INC_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Postfix_inc_node : public Expr_node{
        enum class Postfix_inc_kind{
            Inc, Dec, Wrapping_inc, Wrapping_dec
        };

        Postfix_inc_node()                        = default;
        Postfix_inc_node(const Postfix_inc_node&) = default;
        virtual ~Postfix_inc_node()               = default;

        Postfix_inc_node(Postfix_inc_kind                  kind,
                         const std::shared_ptr<Expr_node>& operand)
            : kind_{kind}
            , operand_{operand}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        Postfix_inc_kind           kind_;
        std::shared_ptr<Expr_node> operand_;
    };
};
#endif