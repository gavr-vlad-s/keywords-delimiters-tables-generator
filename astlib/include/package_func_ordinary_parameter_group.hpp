/*
    File:    package_func_ordinary_parameter_group.hpp
    Created: 07 January 2022 at 10:16 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PACKAGE_FUNC_ORDINARY_PARAMETER_GROUP_H
#define PACKAGE_FUNC_ORDINARY_PARAMETER_GROUP_H
#   include <memory>
#   include <vector>
#   include "../include/package_func_parameter_group.hpp"
#   include "../include/expr_node.hpp"
#   include "../include/ast_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast::package_func{
    struct Ordinary_parameter_group : public Parameter_group{
        Ordinary_parameter_group()                                = default;
        Ordinary_parameter_group(const Ordinary_parameter_group&) = default;
        virtual ~Ordinary_parameter_group()                       = default;

        Ordinary_parameter_group(const std::vector<fst_level_scanner::Fst_lvl_token>& args,
                                 const std::shared_ptr<ast::Expr_node>&               args_type)
            : args_{args}
            , args_type_{args_type}
        {
        }

        Parameter_group_kind get_kind() const override
        {
            return Parameter_group_kind::Ordinary;
        }

        std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                              const std::shared_ptr<Char_trie>& ids_trie,
                              const ast::Indents&               indents) const override;

        std::vector<fst_level_scanner::Fst_lvl_token> args_;
        std::shared_ptr<ast::Expr_node>               args_type_;
    };
};
#endif