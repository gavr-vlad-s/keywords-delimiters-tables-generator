/*
    File:    constant_node.hpp
    Created: 28 August 2021 at 15:26 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef CONSTANT_NODE_H
#define CONSTANT_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/stmt_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
#   include "../include/expr_node.hpp"
namespace ast{
    struct Constant_node : public Stmt_node{
        Constant_node()                              = default;
        Constant_node(const Constant_node&) = default;
        virtual ~Constant_node()                     = default;

        Constant_node(bool                                    is_exported,
                      const fst_level_scanner::Fst_lvl_token& name,
                      const std::shared_ptr<Expr_node>&       type,
                      const std::shared_ptr<Expr_node>&       value)
            : is_exported_{is_exported}
            , name_{name}
            , type_{type}
            , value_{value}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        bool                             is_exported_ = false;
        fst_level_scanner::Fst_lvl_token name_;
        std::shared_ptr<Expr_node>       type_;
        std::shared_ptr<Expr_node>       value_;
    };
};
#endif