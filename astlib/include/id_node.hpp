/*
    File:    id_node.hpp
    Created: 14 July 2021 at 19:52 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ID_NODE_H
#define ID_NODE_H
#   include <cstddef>
#   include "../include/ast_node.hpp"
namespace ast{
    struct Id_node : public Node{
        Id_node()               = default;
        Id_node(const Id_node&) = default;
        virtual ~Id_node()      = default;

        explicit Id_node(std::size_t id_idx) : id_idx_{id_idx} {}

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                              const std::shared_ptr<Char_trie>& ids_trie,
                              const Indents&                    indents) const override;

        std::size_t id_idx_;
    };
};
#endif