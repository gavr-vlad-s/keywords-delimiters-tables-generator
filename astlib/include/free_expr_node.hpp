/*
    File:    free_expr_node.hpp
    Created: 24 August 2021 at 16:48 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef FREE_EXPR_NODE_H
#define FREE_EXPR_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Free_expr_node : public Expr_node{
        Free_expr_node()                      = default;
        Free_expr_node(const Free_expr_node&) = default;
        virtual ~Free_expr_node()             = default;

        Free_expr_node(bool is_scale, const std::shared_ptr<Expr_node>& operand)
            : is_scale_{is_scale}
            , operand_{operand}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        bool                       is_scale_ = false;
        std::shared_ptr<Expr_node> operand_;
    };
};
#endif