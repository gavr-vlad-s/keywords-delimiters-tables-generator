/*
    File:    system_io_module_create.hpp
    Created: 17 January 2022 at 19:54 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SYSTEM_IO_MODULE_CREATE_H
#define SYSTEM_IO_MODULE_CREATE_H
#   include <cstddef>
#   include <map>
#   include <memory>
#   include <string>
#   include "../include/arkona_module.hpp"
#   include "../../tries/include/char_trie.hpp"
namespace standard_modules::sistema::vvod_vyvod{
    std::shared_ptr<ast::Module> create_module(const std::shared_ptr<Char_trie>&      strs_trie,
                                               const std::shared_ptr<Char_trie>&      ids_trie,
                                               std::map<std::u32string, std::size_t>& indices_of_standard_identifiers);
};
#endif