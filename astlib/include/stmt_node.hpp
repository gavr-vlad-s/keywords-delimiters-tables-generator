/*
    File:    stmt_node.hpp
    Created: 21 August 2021 at 10:02 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef STMT_NODE_H
#define STMT_NODE_H
#   include <cstddef>
#   include "../include/ast_node.hpp"
namespace ast{
    struct Stmt_node : public Node{
        Stmt_node()                 = default;
        Stmt_node(const Stmt_node&) = default;
        virtual ~Stmt_node()        = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const = 0;
    };
};
#endif