/*
    File:    compare_with_node.hpp
    Created: 21 August 2021 at 20:10 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef COMPARE_WITH_NODE_H
#define COMPARE_WITH_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Compare_with_node : public Expr_node{
        Compare_with_node()                         = default;
        Compare_with_node(const Compare_with_node&) = default;
        virtual ~Compare_with_node()                = default;

        Compare_with_node(const std::shared_ptr<Expr_node>& lhs,
                          const std::shared_ptr<Expr_node>& rhs)
            : lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::shared_ptr<Expr_node> lhs_;
        std::shared_ptr<Expr_node> rhs_;
    };
};
#endif