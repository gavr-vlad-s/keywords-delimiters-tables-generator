/*
    File:    variables_section_node.hpp
    Created: 27 August 2021 at 14:17 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef VARIABLES_SECTION_NODE_H
#define VARIABLES_SECTION_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/stmt_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
#   include "../include/expr_node.hpp"
namespace ast{
    struct Variables_section_node : public Stmt_node{
        Variables_section_node()                              = default;
        Variables_section_node(const Variables_section_node&) = default;
        virtual ~Variables_section_node()                     = default;

        Variables_section_node(bool                                                 is_exported,
                               const std::vector<fst_level_scanner::Fst_lvl_token>& names,
                               const std::shared_ptr<Expr_node>&                    type,
                               const std::shared_ptr<Expr_node>&                    value)
            : is_exported_{is_exported}
            , names_{names}
            , type_{type}
            , value_{value}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        bool                                          is_exported_ = false;
        std::vector<fst_level_scanner::Fst_lvl_token> names_;
        std::shared_ptr<Expr_node>                    type_;
        std::shared_ptr<Expr_node>                    value_;
    };
};
#endif