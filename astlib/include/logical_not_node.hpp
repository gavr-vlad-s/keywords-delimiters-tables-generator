/*
    File:    logical_not_node.hpp
    Created: 21 August 2021 at 15:46 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef LOGICAL_NOT_NODE_H
#define LOGICAL_NOT_NODE_H
#   include <cstddef>
#   include "../include/expr_node.hpp"
namespace ast{
    struct Logical_not_node : public Expr_node{
        Logical_not_node()                        = default;
        Logical_not_node(const Logical_not_node&) = default;
        virtual ~Logical_not_node()               = default;

        Logical_not_node(std::size_t                       num_of_ops,
                         const std::shared_ptr<Expr_node>& operand)
            : num_of_ops_{num_of_ops}
            , operand_{operand}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        std::size_t                num_of_ops_ = 0;
        std::shared_ptr<Expr_node> operand_;
    };
};
#endif