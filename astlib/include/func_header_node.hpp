/*
    File:    func_header_node.hpp
    Created: 06 December 2021 at 19:34 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef FUNC_HEADER_NODE_H
#define FUNC_HEADER_NODE_H
#   include <cstddef>
#   include <vector>
#   include "../include/expr_node.hpp"
#   include "../include/ast_node.hpp"
#   include "../include/stmt_node.hpp"
#   include "../include/func_signature_node.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace ast{
    struct Func_header_node : public Stmt_node{
        Func_header_node()                        = default;
        Func_header_node(const Func_header_node&) = default;
        virtual ~Func_header_node()               = default;

        Func_header_node(bool                                        is_exported,
                         bool                                        is_pure,
                         const fst_level_scanner::Fst_lvl_token&     name,
                         const std::shared_ptr<Func_signature_node>& signature)
            : is_exported_{is_exported}
            , is_pure_{is_pure}
            , name_{name}
            , signature_{signature}
        {
        }

        virtual NodeInfo get_info() const;

        virtual std::string to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const;

        bool                                 is_exported_ = false;
        bool                                 is_pure_     = false;
        fst_level_scanner::Fst_lvl_token     name_;
        std::shared_ptr<Func_signature_node> signature_;
    };
};
#endif