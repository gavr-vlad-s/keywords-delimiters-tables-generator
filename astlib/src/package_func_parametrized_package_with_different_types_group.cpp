/*
    File:    package_func_parametrized_package_with_different_types_group.cpp
    Created: 08 January 2022 at 10:57 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/package_func_parametrized_package_with_different_types_group.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../tries/include/idx_to_string.hpp"
#include "../../strings/include/join.hpp"

namespace ast::package_func{
    namespace{
        const char* parametrized_group_fmt = R"~({0}PARAMETRIZED_ARGUMENTS_GROUP_WITH_DIFFERENT_TYPES
{0}{{
{1}ARGUMENTS_NAME
{1}{{
{2}{4}
{1}}}
{1}TYPE
{1}{{
{3}
{1}}}
{0}}})~";
    };

    std::string Parametrized_package_with_different_types_group::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                                                           const std::shared_ptr<Char_trie>& ids_trie,
                                                                           const ast::Indents&               indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');
        const auto  indent2_str = std::string(indents.indent_ + 2 * indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        std::string type_str;
        if(args_type_){
            type_str = args_type_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(parametrized_group_fmt,
                             indent0_str,
                             indent1_str,
                             indent2_str,
                             type_str,
                             idx_to_string(ids_trie, args_.lexeme_.id_index_));

        return result;
    }
}