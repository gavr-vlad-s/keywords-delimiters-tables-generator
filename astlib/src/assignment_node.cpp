/*
    File:    assignment_node.cpp
    Created: 21 August 2021 at 12:53 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/assignment_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* assignment_fmt = R"~({0}ASSIGN [{1}]
{0}{{
{2}LHS:
{2}{{
{3}
{2}}}
{2}RHS:
{2}{{
{4}
{2}}}
{0}}})~";

    const char* kinds[] = {
        "Assign",                      "Copy",                      "Logical_and_assign",
        "Logical_and_full_assign",     "Logical_and_not_assign",    "Logical_and_not_full_assign",
        "Logical_or_assign",           "Logical_or_full_assign",    "Logical_or_not_assign",
        "Logical_or_not_full_assign",  "Logical_xor_assign",        "Bitwise_and_assign",
        "Bitwise_and_not_assign",      "Left_shift_assign",         "Right_shift_assign",
        "Cyclic_left_shift_assign",    "Cyclic_right_shift_assign", "Bitwise_or_assign",
        "Bitwise_or_not_assign",       "Bitwise_xor_assign",        "Plus_assign",
        "Minus_assign",                "Type_add_assign",           "Algebraic_or_assign",
        "Mul_assign",                  "Div_assign",                "Set_minus_assign",
        "Symmetric_difference_assign", "Remainder_assign",          "Float_remainder_assign",
        "Power_assign",                "Float_power_assign"
    };
};

namespace ast{
    NodeInfo Assignment_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Assignment;
        ninfo.subkind_ = static_cast<std::uint16_t>(kind_);

        return ninfo;
    }

    std::string Assignment_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                           const std::shared_ptr<Char_trie>& ids_trie,
                                           const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        const auto  kind_str    = std::string{kinds[static_cast<unsigned>(kind_)]};

        std::string lhs_str;
        std::string rhs_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        if(lhs_){
            lhs_str = lhs_->to_string(strs_trie, ids_trie, nested_indent);
        }
        if(rhs_){
            rhs_str = rhs_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(assignment_fmt,
                             indent0_str,
                             kind_str,
                             indent1_str,
                             lhs_str,
                             rhs_str);

        return result;
    }
};