/*
    File:    alloc_expr_node.cpp
    Created: 24 August 2021 at 18:06 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/alloc_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* alloc_expr_fmt = R"~({0}ALLOC_EXPR
{0}{{
{1}OPERAND:
{1}{{
{2}
{1}}}
{0}}})~";
};

namespace ast{
    NodeInfo Alloc_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Alloc_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Alloc_expr_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                           const std::shared_ptr<Char_trie>& ids_trie,
                                           const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        std::string operand_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        if(operand_){
            operand_str = operand_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(alloc_expr_fmt,
                             indent0_str,
                             indent1_str,
                             operand_str);

        return result;
    }
};