/*
    File:    structure_value_node.cpp
    Created: 23 August 2021 at 16:34 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/structure_value_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../tries/include/idx_to_string.hpp"
#include "../../strings/include/join.hpp"

namespace{
    const char* structure_value_fmt = R"~({0}STRUCTURE_VALUE
{0}{{
{1}NAME:
{1}{{
{2}
{1}}}
{1}FIELDS_INITIALIZERS:
{1}{{
{3}
{1}}}
{0}}})~";

    const char* field_initializer_fmt = R"~({0}FIELD_INITIALIZER
{0}{{
{1}NAME:
{1}{{
{2}
{1}}}
{1}VALUE:
{1}{{
{3}
{1}}}
{0}}})~";
};

namespace ast{
    NodeInfo Structure_value_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Structure_value;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Structure_value_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                                const std::shared_ptr<Char_trie>& ids_trie,
                                                const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');
        const auto  indent2_str = std::string(indents.indent_ + 2 * indents.indent_inc_, ' ');
        const auto  indent3_str = std::string(indents.indent_ + 3 * indents.indent_inc_, ' ');
        const auto  indent4_str = std::string(indents.indent_ + 4 * indents.indent_inc_, ' ');

        std::string name_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 4 * indents.indent_inc_, indents.indent_inc_};

        if(name_){
            name_str = name_->to_string(strs_trie, ids_trie, nested_indent);
        }

        const auto initializer_to_str = [&ids_trie,
                                         &strs_trie,
                                         &nested_indent2,
                                         &indent2_str,
                                         &indent3_str,
                                         &indent4_str](const auto& initializer) -> std::string
                                        {
                                            const auto& [iname, value] = initializer;
                                            const auto  iname_str      = indent4_str +
                                                                         idx_to_string(ids_trie, iname.lexeme_.id_index_);
                                            std::string value_str;

                                            if(value){
                                                value_str = value->to_string(strs_trie, ids_trie, nested_indent2);
                                            }

                                            return fmt::format(field_initializer_fmt,
                                                               indent2_str,
                                                               indent3_str,
                                                               iname_str,
                                                               value_str);
                                        };

        const auto initializers_str = join(initializer_to_str,
                                           initializers_.begin(),
                                           initializers_.end(),
                                           std::string{"\n"});

        result = fmt::format(structure_value_fmt,
                             indent0_str,
                             indent1_str,
                             name_str,
                             initializers_str);

        return result;
    }
};