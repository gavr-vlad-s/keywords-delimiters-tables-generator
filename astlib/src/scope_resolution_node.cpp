/*
    File:    scope_resolution_node.cpp
    Created: 23 August 2021 at 12:08 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <vector>
#include "../include/scope_resolution_node.hpp"
#include "../../tries/include/idx_to_string.hpp"
#include "../../strings/include/join.hpp"

namespace ast{
    NodeInfo Scope_resolution_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Scoped_elem;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Scope_resolution_node::to_string(const std::shared_ptr<Char_trie>&,
                                                 const std::shared_ptr<Char_trie>& ids_trie,
                                                 const Indents&                    indents) const
    {
        std::string result;

        const auto id_str = idx_to_string(ids_trie, scoped_elem_.lexeme_.id_index_);

        result = std::string(indents.indent_, ' ') + "SCOPE_RESOLUTION_NODE " + id_str;

        return result;
    }
};