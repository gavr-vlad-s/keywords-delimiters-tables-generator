/*
    File:    bitwise_or_node.cpp
    Created: 22 August 2021 at 10:36 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/bitwise_or_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* bitwise_or_fmt = R"~({0}BITWISE_OR [{1}]
{0}{{
{2}LHS:
{2}{{
{3}
{2}}}
{2}RHS:
{2}{{
{4}
{2}}}
{0}}})~";

    const char* kinds[] = {
        "Bitwise_or", "Bitwise_or_not", "Bitwise_xor"
    };
};

namespace ast{
    NodeInfo Bitwise_or_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Bitwise_or;
        ninfo.subkind_ = static_cast<std::uint16_t>(kind_);

        return ninfo;
    }

    std::string Bitwise_or_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                           const std::shared_ptr<Char_trie>& ids_trie,
                                           const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        const auto  kind_str    = std::string{kinds[static_cast<unsigned>(kind_)]};

        std::string lhs_str;
        std::string rhs_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        if(lhs_){
            lhs_str = lhs_->to_string(strs_trie, ids_trie, nested_indent);
        }
        if(rhs_){
            rhs_str = rhs_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(bitwise_or_fmt,
                             indent0_str,
                             kind_str,
                             indent1_str,
                             lhs_str,
                             rhs_str);

        return result;
    }
};