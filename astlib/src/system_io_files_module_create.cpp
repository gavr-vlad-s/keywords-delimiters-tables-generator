/*
    File:    system_io_files_module_create.cpp
    Created: 30 January 2022 at 16:21 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/system_io_files_module_create.hpp"
#include "../include/all_nodes.hpp"
#include "../include/symbol_table.hpp"
#include "../include/package_func_parameter_group_variant.hpp"
#include "../../fst-lvl-scanner/include/lexeme.hpp"

namespace standard_modules::sistema::vvod_vyvod::faily{
    namespace details{
        using Lexeme         = fst_level_scanner::LexemeInfo;

        //! Function that return identifier.
        const Lexeme id_lexeme(std::size_t idx)
        {
            Lexeme li;
            li.code_.kind_    = fst_level_scanner::Lexem_kind::Id;
            li.code_.subkind_ = 0;
            li.id_index_      = idx;
            return li;
        }

        std::shared_ptr<ast::Alias_node> build_encoding_alias(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            const std::size_t                      kodirovka_id = indices_of_standard_identifiers[U"Кодировка"];
            const std::size_t                      stroki_id    = indices_of_standard_identifiers[U"Строки"];
            const std::size_t                      kodirovki_id = indices_of_standard_identifiers[U"Кодировки"];
            const fst_level_scanner::Fst_lvl_token start_name   = {{{4,25 }, {4,33 }}, id_lexeme(kodirovka_id)};

            const std::vector<fst_level_scanner::Fst_lvl_token> scoped_name_components = {
                {{{4,37 }, {4,42 }}, id_lexeme(stroki_id)},
                {{{4,45 }, {4,53 }}, id_lexeme(kodirovki_id)},
                {{{4,56 }, {4,64 }}, id_lexeme(kodirovka_id)},
            };

            const auto scoped_name = std::make_shared<ast::Scope_resolution_node>(scoped_name_components);

            return std::make_shared<ast::Alias_node>(true, start_name, scoped_name);
        }

        std::shared_ptr<ast::Enum_type_node> build_mode_enum(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            std::shared_ptr<ast::Enum_type_node> result;
            return result;
        }

        std::shared_ptr<ast::Func_header_node> build_openf_function(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            std::shared_ptr<ast::Func_header_node> result;
            return result;
        }

        std::shared_ptr<ast::Func_header_node> build_sizef_function(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            std::shared_ptr<ast::Func_header_node> result;
            return result;
        }

        std::shared_ptr<ast::Func_header_node> build_closef_function(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            std::shared_ptr<ast::Func_header_node> result;
            return result;
        }

        std::shared_ptr<ast::Func_header_node> build_readf_function(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            std::shared_ptr<ast::Func_header_node> result;
            return result;
        }

        std::shared_ptr<ast::Func_header_node> build_writef_function(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            std::shared_ptr<ast::Func_header_node> result;
            return result;
        }
    };

    std::shared_ptr<ast::Module> create_module(const std::shared_ptr<Char_trie>&      strs_trie,
                                               const std::shared_ptr<Char_trie>&      ids_trie,
                                               std::map<std::u32string, std::size_t>& indices_of_standard_identifiers)
    {
        std::shared_ptr<ast::Module> result;

        auto sym_table = std::make_shared<symbol_table::Symbol_table>(strs_trie, ids_trie);
        auto scope     = sym_table->get_current_scope();

        auto alias_for_encoding = details::build_encoding_alias(indices_of_standard_identifiers, scope);
        auto enum_mode          = details::build_mode_enum(indices_of_standard_identifiers, scope);
        auto openf_func         = details::build_openf_function(indices_of_standard_identifiers, scope);
        auto sizef_func         = details::build_sizef_function(indices_of_standard_identifiers, scope);
        auto closef_func        = details::build_closef_function(indices_of_standard_identifiers, scope);
        auto readf_func         = details::build_readf_function(indices_of_standard_identifiers, scope);
        auto writef_func        = details::build_writef_function(indices_of_standard_identifiers, scope);

        return result;
    }
};
