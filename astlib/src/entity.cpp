/*
    File:    entity.cpp
    Created: 02 January 2022 at 15:06 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <tuple>
#include "../include/entity.hpp"
#include "../../thirdparty/hashing/include/tuple_hash.hpp"

namespace symbol_table{
    bool operator==(const Entity& lhs, const Entity& rhs)
    {
        return std::tie(lhs.kind_, lhs.pnode_) == std::tie(rhs.kind_, rhs.pnode_);
    }

    std::size_t Entity_hash::operator()(const Entity& e) const
    {
        std::tuple<Entity_kind, std::shared_ptr<ast::Node>> t{e.kind_, e.pnode_};
        return hashing::calc_hash(t);
    }
};
