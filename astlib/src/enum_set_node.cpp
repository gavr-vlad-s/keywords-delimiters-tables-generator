/*
    File:    enum_set_node.cpp
    Created: 25 August 2021 at 20:26 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/enum_set_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"

namespace{
    const char* enum_set_fmt = R"~({0}ENUM_SET_TYPE
{0}{{
{1}ELEM_TYPE
{1}{{
{2}
{1}}}
{0}}})~";
};

namespace ast{
    NodeInfo Enum_set_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Enum_set_type;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Enum_set_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                         const std::shared_ptr<Char_trie>& ids_trie,
                                         const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        std::string elem_type_str;
        if(elem_type_){
            elem_type_str = elem_type_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(enum_set_fmt, indent0_str, indent1_str, elem_type_str);

        return result;
    }
};