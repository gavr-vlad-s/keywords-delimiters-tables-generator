/*
    File:    index_sizeof_node.cpp
    Created: 22 August 2021 at 20:49 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/index_sizeof_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* index_sizeof_fmt = R"~({0}INDEX_SIZEOF
{0}{{
{1}LHS:
{1}{{
{2}
{1}}}
{1}RHS:
{1}{{
{3}
{1}}}
{0}}})~";
};

namespace ast{
    NodeInfo Index_sizeof_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Index_sizeof;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Index_sizeof_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                             const std::shared_ptr<Char_trie>& ids_trie,
                                             const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        std::string lhs_str;
        std::string rhs_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        if(lhs_){
            lhs_str = lhs_->to_string(strs_trie, ids_trie, nested_indent);
        }
        if(rhs_){
            rhs_str = rhs_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(index_sizeof_fmt,
                             indent0_str,
                             indent1_str,
                             lhs_str,
                             rhs_str);

        return result;
    }
};