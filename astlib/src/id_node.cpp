/*
    File:    id_node.cpp
    Created: 14 July 2021 at 19:57 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/id_node.hpp"
#include "../../tries/include/idx_to_string.hpp"

namespace ast{
    NodeInfo Id_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Id;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Id_node::to_string(const std::shared_ptr<Char_trie>&,
                                   const std::shared_ptr<Char_trie>& ids_trie,
                                   const Indents&                    indents) const
    {
        std::string result;

        result = std::string(indents.indent_, ' ') + "ID " + idx_to_string(ids_trie, id_idx_);

        return result;
    }
};