/*
    File:    alias_node.cpp
    Created: 05 February 2021 at 16:29 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/alias_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"
#include "../../tries/include/idx_to_string.hpp"

namespace{
    const char* alias_node_fmt = R"~({0}{1}ALIAS_DEFINITION
{0}{{
{2}NAME : {3}
{2}DEFINITION
{2}{{
{4}
{2}}}
{0}}})~";
};

namespace ast{
    NodeInfo Alias_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Alias;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Alias_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        std::string is_exported_str;
        std::string definition_str;
        if(is_exported_){
            is_exported_str = "EXPORTED ";
        }
        if(definition_){
            definition_str = definition_->to_string(strs_trie, ids_trie, nested_indent);
        }


        const auto name_str = idx_to_string(ids_trie, name_.lexeme_.id_index_);

        result = fmt::format(alias_node_fmt,
                             indent0_str,
                             is_exported_str,
                             indent1_str,
                             name_str,
                             definition_str);

        return result;
    }
};