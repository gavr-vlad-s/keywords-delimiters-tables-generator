/*
    File:    reference_type_node.cpp
    Created: 25 August 2021 at 20:41 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/reference_type_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"

namespace{
    const char* reference_type_fmt = R"~({0}{3}
{0}{{
{1}VALUE_TYPE
{1}{{
{2}
{1}}}
{0}}})~";
};

namespace ast{
    NodeInfo Reference_type_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Reference_type;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Reference_type_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                         const std::shared_ptr<Char_trie>& ids_trie,
                                         const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        std::string value_type_str;
        if(value_type_){
            value_type_str = value_type_->to_string(strs_trie, ids_trie, nested_indent);
        }

        const std::string title = is_const_ ? "CONST REFERENCE_TYPE" : "REFERENCE_TYPE";

        result = fmt::format(reference_type_fmt,
                             indent0_str,
                             indent1_str,
                             value_type_str,
                             title);

        return result;
    }
};