/*
    File:    generalization_type_node.cpp
    Created: 26 August 2021 at 09:41 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/generalization_type_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"

namespace{
    const char* generalization_type_fmt = R"~({0}GENERALIZATION_TYPE
{0}{{
{1}GENERALIZED_TYPES
{1}{{
{2}
{1}}}
{0}}})~";

    const char* generalized_type_fmt = R"~({0}GENERALIZED_TYPE {1}
{0}{{
{2}
{0}}})~";
};

namespace ast{
    NodeInfo Generalization_type_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Generalization_type;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Generalization_type_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                                    const std::shared_ptr<Char_trie>& ids_trie,
                                                    const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');
        const auto indent2_str = std::string(indents.indent_ + 2 * indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};

        std::vector<std::pair<std::size_t, std::string>> generalized_types_strs;

        std::size_t i = 0;

        for(const auto& generalized_type : generalized_types_){
            if(generalized_type){
                generalized_types_strs.push_back({i, generalized_type->to_string(strs_trie, ids_trie, nested_indent)});
            }else{
                generalized_types_strs.push_back({i, std::string{}});
            }

            i++;
        }

        const auto generalized_types_str = join([&indent2_str](const auto& p) -> std::string
                                           {
                                               const auto& [idx, dim_str] = p;
                                               return fmt::format(generalized_type_fmt, indent2_str, idx, dim_str);
                                           },
                                           generalized_types_strs.begin(),
                                           generalized_types_strs.end(),
                                           std::string{"\n"});

        result = fmt::format(generalization_type_fmt, indent0_str, indent1_str, generalized_types_str);

        return result;
    }
};