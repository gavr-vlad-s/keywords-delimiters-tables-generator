/*
    File:    conditional_op_node.cpp
    Created: 21 August 2021 at 13:40 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/conditional_op_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* conditional_op_fmt = R"~({0}CONDITIONAL [{1}]
{0}{{
{2}CONDITION:
{2}{{
{3}
{2}}}
{2}TRUE_BRANCH:
{2}{{
{4}
{2}}}
{2}FALSE_BRANCH:
{2}{{
{5}
{2}}}
{0}}})~";

    const char* kinds[] = {
        "Ordinary", "Full"
    };
};

namespace ast{
    NodeInfo Conditional_op_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Conditional_op;
        ninfo.subkind_ = static_cast<std::uint16_t>(kind_);

        return ninfo;
    }

    std::string Conditional_op_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                               const std::shared_ptr<Char_trie>& ids_trie,
                                               const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        const auto  kind_str    = std::string{kinds[static_cast<unsigned>(kind_)]};

        std::string condition_str;
        std::string true_branch_str;
        std::string false_branch_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        if(condition_){
            condition_str = condition_->to_string(strs_trie, ids_trie, nested_indent);
        }
        if(true_branch_){
            true_branch_str = true_branch_->to_string(strs_trie, ids_trie, nested_indent);
        }
        if(false_branch_){
            false_branch_str = false_branch_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(conditional_op_fmt,
                             indent0_str,
                             kind_str,
                             indent1_str,
                             condition_str,
                             true_branch_str,
                             false_branch_str);

        return result;
    }
};