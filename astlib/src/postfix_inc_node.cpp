/*
    File:    postfix_inc_node.cpp
    Created: 22 August 2021 at 13:50 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/postfix_inc_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* postfix_inc_fmt = R"~({0}POSTFIX_INC [{1}]
{0}{{
{2}OPERAND:
{2}{{
{3}
{2}}}
{0}}})~";

    const char* kinds[] = {
        "Inc", "Dec", "Wrapping_inc", "Wrapping_dec"
    };
};

namespace ast{
    NodeInfo Postfix_inc_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Postfix_inc;
        ninfo.subkind_ = static_cast<std::uint16_t>(kind_);

        return ninfo;
    }

    std::string Postfix_inc_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                            const std::shared_ptr<Char_trie>& ids_trie,
                                            const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        const auto  kind_str    = std::string{kinds[static_cast<unsigned>(kind_)]};

        std::string operand_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        if(operand_){
            operand_str = operand_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(postfix_inc_fmt,
                             indent0_str,
                             kind_str,
                             indent1_str,
                             operand_str);

        return result;
    }
};