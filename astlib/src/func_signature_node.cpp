/*
    File:    func_signature_node.cpp
    Created: 27 August 2021 at 09:13 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/func_signature_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../tries/include/idx_to_string.hpp"
#include "../../strings/include/join.hpp"

namespace{
    const char* func_signature_fmt = R"~({0}FUNC_SIGNATURE
{0}{{
{1}ARGUMENTS_GROUPS:
{1}{{
{2}
{1}}}
{1}RESULT_TYPE:
{1}{{
{3}
{1}}}
{0}}})~";

    const char* arguments_group_fmt = R"~({0}ARGUMENTS_GROUP
{0}{{
{1}ARGUMENTS_NAMES
{1}{{
{2}
{1}}}
{1}TYPE
{1}{{
{3}
{1}}}
{0}}})~";

    const char* argument_fmt = R"~({0}ARGUMENT_NAME: {1})~";

    using Token = fst_level_scanner::Fst_lvl_token;
};

namespace ast{
    NodeInfo Func_signature_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Func_signature;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Func_signature_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                               const std::shared_ptr<Char_trie>& ids_trie,
                                               const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');
        const auto  indent2_str = std::string(indents.indent_ + 2 * indents.indent_inc_, ' ');
        const auto  indent3_str = std::string(indents.indent_ + 3 * indents.indent_inc_, ' ');
        const auto  indent4_str = std::string(indents.indent_ + 4 * indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 4 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto argument_name_to_str = [&indent4_str, &ids_trie](const Token& token) -> std::string
                                          {
                                              return fmt::format(argument_fmt,
                                                                 indent4_str,
                                                                 idx_to_string(ids_trie, token.lexeme_.id_index_));
                                          };

        const auto arguments_names_to_str = [&argument_name_to_str](const std::vector<Token>& names) -> std::string
                                            {
                                                return join(argument_name_to_str,
                                                            names.begin(),
                                                            names.end(),
                                                            std::string{"\n"});
                                            };

        const auto arguments_group_to_str = [&ids_trie,
                                             &strs_trie,
                                             &indent2_str,
                                             &indent3_str,
                                             &nested_indent,
                                             &arguments_names_to_str](const Group_of_func_args& group) -> std::string
                                            {
                                                const auto names_str = arguments_names_to_str(group.args_);

                                                std::string type_str;
                                                if(group.args_type_){
                                                    type_str = group.args_type_->to_string(strs_trie, ids_trie, nested_indent);
                                                }

                                                return fmt::format(arguments_group_fmt,
                                                                   indent2_str,
                                                                   indent3_str,
                                                                   names_str,
                                                                   type_str);
                                            };

        std::string result_type_str;
        if(result_type_){
            result_type_str = result_type_->to_string(strs_trie, ids_trie, nested_indent2);
        }

        const auto groups_str = join(arguments_group_to_str,
                                     formal_args_.begin(),
                                     formal_args_.end(),
                                     std::string{"\n"});

        result = fmt::format(func_signature_fmt, indent0_str, indent1_str, groups_str, result_type_str);

        return result;
    }
};