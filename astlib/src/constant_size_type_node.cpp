/*
    File:    constant_size_type_node.cpp
    Created: 25 August 2021 at 13:30 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/constant_size_type_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* add_fmt = R"~({0}CONSTANT_SIZE_TYPE [{1}]
{0}{{
{0}}})~";

    const char* kinds[] = {
        "Char8",      "Char16",     "Char32",    "String8",
        "String16",   "String32",   "Bool8",     "Bool16",
        "Bool32",     "Bool64",     "Ordering8", "Ordering16",
        "Ordering32", "Ordering64", "Uint8",     "Uint16",
        "Uint32",     "Uint64",     "Uint128",   "Int8",
        "Int16",      "Int32",      "Int64",     "Int128",
        "Float32",    "Float64",    "Float80",   "Float128",
        "Complex32",  "Complex64",  "Complex80", "Complex128",
        "Quat32",     "Quat64",     "Quat80",    "Quat128",
        "Void"
    };
};

namespace ast{
    NodeInfo Constant_size_type_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Constant_size_type;
        ninfo.subkind_ = static_cast<std::uint16_t>(kind_);

        return ninfo;
    }

    std::string Constant_size_type_node::to_string(const std::shared_ptr<Char_trie>&,
                                                   const std::shared_ptr<Char_trie>&,
                                                   const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  kind_str    = std::string{kinds[static_cast<unsigned>(kind_)]};

        result = fmt::format(add_fmt,
                             indent0_str,
                             kind_str);

        return result;
    }
};