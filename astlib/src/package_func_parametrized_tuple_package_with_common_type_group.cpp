/*
    File:    package_func_parametrized_tuple_package_with_common_type_group.cpp
    Created: 08 January 2022 at 10:25 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/package_func_parametrized_tuple_package_with_common_type_group.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../tries/include/idx_to_string.hpp"
#include "../../strings/include/join.hpp"

namespace ast::package_func{
    namespace{
        const char* parametrized_tuple_group_fmt = R"~({0}PARAMETRIZED_TUPLE_ARGUMENTS_GROUP
{0}{{
{1}ARGUMENTS_NAMES
{1}{{
{2}
{1}}}
{1}TYPE
{1}{{
{3}
{1}}}
{0}}})~";

        const char* argument_fmt = R"~({0}ARGUMENT_NAME: {1})~";

        using Token = fst_level_scanner::Fst_lvl_token;
    };

    std::string Parametrized_tuple_package_with_common_type_group::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                                                             const std::shared_ptr<Char_trie>& ids_trie,
                                                                             const ast::Indents&               indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');
        const auto  indent2_str = std::string(indents.indent_ + 2 * indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto argument_name_to_str = [&indent2_str, &ids_trie](const Token& token) -> std::string
                                          {
                                              return fmt::format(argument_fmt,
                                                                 indent2_str,
                                                                 idx_to_string(ids_trie, token.lexeme_.id_index_));
                                          };

        const auto arguments_names_to_str = [&argument_name_to_str](const std::vector<Token>& names) -> std::string
                                            {
                                                return join(argument_name_to_str,
                                                            names.begin(),
                                                            names.end(),
                                                            std::string{"\n"});
                                            };

        const auto names_str = arguments_names_to_str(args_);
        std::string type_str;
        if(args_type_){
            type_str = args_type_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(parametrized_tuple_group_fmt, indent0_str, indent1_str, names_str, type_str);

        return result;
    }
}