/*
    File:    field_accessor_node.cpp
    Created: 23 August 2021 at 13:06 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/field_accessor_node.hpp"
#include "../../tries/include/idx_to_string.hpp"
#include "../../strings/include/join.hpp"

namespace ast{
    NodeInfo Field_accessor_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Field_accessor;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Field_accessor_node::to_string(const std::shared_ptr<Char_trie>&,
                                               const std::shared_ptr<Char_trie>& ids_trie,
                                               const Indents&                    indents) const
    {
        std::string result;

        const auto id_str = idx_to_string(ids_trie, field_name_.lexeme_.id_index_);

        result = std::string(indents.indent_, ' ') + "FIELD_ACCESSOR " + id_str;

        return result;
    }
};