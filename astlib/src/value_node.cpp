/*
    File:    value_node.cpp
    Created: 14 July 2021 at 19:24 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/value_node.hpp"
#include "../../numbers/include/int128_to_str.hpp"
#include "../../numbers/include/float128_to_string.hpp"
#include "../../char-conv/include/print_char32.hpp"
#include "../../tries/include/idx_to_string.hpp"

namespace{
    const char* kind_strs[] = {
        "Int_value ",  "Float_value ",    "Complex_value ",
        "Quat_value ", "Char_value ",     "String_value ",
        "Bool_value ", "Ordering_value "
    };

    const char* float_kinds[] = {
        "[Float32] ", "[Float64] ", "[Float80] ", "[Float128] "
    };

    const char* complex_kinds[] = {
        "[Complex32] ", "[Complex64] ", "[Complex80] ", "[Complex128] "
    };

    const char* quat_kinds[] = {
        "[Quat32] ", "[Quat64] ", "[Quat80] ", "[Quat128] "
    };

    const char* char_kinds[] = {
        "[Char8] ", "[Char16] ", "[Char32] "
    };

    const char* string_kinds[] = {
        "[String8] ", "[String16] ", "[String32] "
    };

    const char* ordering_vals[] = {
        "Less", "Equal", "Greater"
    };
};

namespace ast{
    NodeInfo Value_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Value;
        ninfo.subkind_ = 0;

        return ninfo;
    }

     std::string Value_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                       const std::shared_ptr<Char_trie>&,
                                       const Indents&                    indents) const
    {
        const auto indent_str = std::string(indents.indent_, ' ');

        std::string result = indent_str + kind_strs[static_cast<unsigned>(value_.val_kind_)];

        switch(value_.val_kind_){
            case Value_kind::Int_value:
                result += ::to_string(value_.int_val_);
                break;
            case Value_kind::Float_value:
                result += float_kinds[value_.val_subkind_] + ::to_string(value_.float_val_);
                break;
            case Value_kind::Complex_value:
                result += complex_kinds[value_.val_subkind_]       +
                          ::to_string(crealq(value_.complex_val_)) +
                          " + "                                    +
                          ::to_string(cimagq(value_.complex_val_)) +
                          "i";
                break;
            case Value_kind::Quat_value:
                result += quat_kinds[value_.val_subkind_]  +
                          ::to_string(value_.quat_val_[0]) +
                          ::to_string(value_.quat_val_[1]) + "i" +
                          ::to_string(value_.quat_val_[2]) + "j" +
                          ::to_string(value_.quat_val_[3]) + "k";
                break;
            case Value_kind::Char_value:
                result += char_kinds[value_.val_subkind_] + show_char32(value_.char_val_);
                break;
            case Value_kind::String_value:
                result += string_kinds[value_.val_subkind_] + idx_to_string(strs_trie, value_.str_index_);
                break;
            case Value_kind::Bool_value:
                result += value_.bool_val_ ? "true" : "false";
                break;
            case Value_kind::Ordering_value:
                result += ordering_vals[static_cast<unsigned>(value_.ordering_val_)];
                break;
            default:
                ;
        }

        return result;
    }



    Value_info make_int_value(unsigned __int128 int_val)
    {
        Value_info result;

        result.val_kind_    = Value_kind::Int_value;
        result.val_subkind_ = 0;
        result.int_val_     = int_val;

        return result;
    }

    Value_info make_float_value(__float128 float_val, Float_kind float_kind)
    {
        Value_info result;

        result.val_kind_    = Value_kind::Float_value;
        result.val_subkind_ = static_cast<std::uint16_t>(float_kind);
        result.float_val_   = float_val;

        return result;
    }

    Value_info make_complex_value(const __complex128& complex_val, Complex_kind complex_kind)
    {
        Value_info result;

        result.val_kind_     = Value_kind::Complex_value;
        result.val_subkind_  = static_cast<std::uint16_t>(complex_kind);
        result.complex_val_  = complex_val;

        return result;
    }

    Value_info make_quat_value(const quat::quat_t<__float128>& quat_val, Quat_kind quat_kind)
    {
        Value_info result;

        result.val_kind_     = Value_kind::Quat_value;
        result.val_subkind_  = static_cast<std::uint16_t>(quat_kind);
        result.quat_val_     = quat_val;

        return result;
    }

    Value_info make_char_value(char32_t char_val, Char_kind char_kind)
    {
        Value_info result;

        result.val_kind_     = Value_kind::Char_value;
        result.val_subkind_  = static_cast<std::uint16_t>(char_kind);
        result.char_val_     = char_val;

        return result;
    }

    Value_info make_str_value(std::size_t str_index, String_kind str_kind)
    {
        Value_info result;

        result.val_kind_     = Value_kind::String_value;
        result.val_subkind_  = static_cast<std::uint16_t>(str_kind);
        result.str_index_    = str_index;

        return result;
    }

    Value_info make_bool_value(bool bool_val)
    {
        Value_info result;

        result.val_kind_    = Value_kind::Bool_value;
        result.val_subkind_ = 0;
        result.bool_val_    = bool_val;

        return result;
    }

    Value_info make_ordering_value(Ordering ordering_val)
    {
        Value_info result;

        result.val_kind_     = Value_kind::Ordering_value;
        result.val_subkind_  = 0;
        result.ordering_val_ = ordering_val;

        return result;
    }
};