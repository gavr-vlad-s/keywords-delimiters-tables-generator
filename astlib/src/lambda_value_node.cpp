/*
    File:    lambda_value_node.cpp
    Created: 27 August 2021 at 13:16 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/lambda_value_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* lambda_value_fmt = R"~({0}LAMBDA_VALUE
{0}{{
{1}SIGNATURE:
{1}{{
{2}
{1}}}
{1}BODY:
{1}{{
{3}
{1}}}
{0}}})~";
};

namespace ast{
    NodeInfo Lambda_value_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Lambda_value;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Lambda_value_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                            const std::shared_ptr<Char_trie>& ids_trie,
                                            const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        std::string signature_str;
        std::string body_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        if(signature_){
            signature_str = signature_->to_string(strs_trie, ids_trie, nested_indent);
        }
        if(body_){
            body_str = body_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(lambda_value_fmt,
                             indent0_str,
                             indent1_str,
                             signature_str,
                             body_str);

        return result;
    }
};