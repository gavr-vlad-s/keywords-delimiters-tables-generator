/*
    File:    variables_section_node.cpp
    Created: 27 August 2021 at 19:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/variables_section_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"
#include "../../tries/include/idx_to_string.hpp"

namespace{
    const char* variables_section_fmt = R"~({0}{1}VARIABLES_SECTION
{0}{{
{2}VARIABLES_NAMES
(2){{
{3}
{2}}}
{2}TYPE
{2}{{
{4}
{2}}}
{2}VALUE
{2}{{
{5}
{2}}}
{0}}})~";

    const char* name_fmt = R"~({0}NAME: {1})~";
};

namespace ast{
    NodeInfo Variables_section_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Variables_section;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Variables_section_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                          const std::shared_ptr<Char_trie>&         ids_trie,
                                          const Indents&                            indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');
        const auto indent2_str = std::string(indents.indent_ + 2 * indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        std::string is_exported_str;
        std::string type_str;
        std::string value_str;
        if(is_exported_){
            is_exported_str = "EXPORTED ";
        }
        if(type_){
            type_str = type_->to_string(strs_trie, ids_trie, nested_indent);
        }
        if(value_){
            value_str = value_->to_string(strs_trie, ids_trie, nested_indent);
        }


        const auto name_to_str = [&ids_trie, &indent2_str](const auto& name) -> std::string
                                 {
                                     return fmt::format(name_fmt,
                                                        indent2_str,
                                                        idx_to_string(ids_trie, name.lexeme_.id_index_));
                                 };

        const auto names_str = join(name_to_str, names_.begin(), names_.end(), std::string{"\n"});

        result = fmt::format(variables_section_fmt,
                             indent0_str,
                             is_exported_str,
                             indent1_str,
                             names_str,
                             type_str,
                             value_str);

        return result;
    }
};