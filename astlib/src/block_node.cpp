/*
    File:    block_node.cpp
    Created: 29 July 2021 at 23:19 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/block_node.hpp"

namespace ast{
    NodeInfo Block_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Block;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Block_node::to_string(const std::shared_ptr<Char_trie>&,
                                      const std::shared_ptr<Char_trie>&,
                                      const Indents&                    indents) const
    {
        std::string result;

        result = std::string(indents.indent_, ' ') + "BLOCK {}";

        return result;
    }
};