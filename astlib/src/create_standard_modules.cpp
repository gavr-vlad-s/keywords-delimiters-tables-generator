/*
    File:    create_standard_modules.cpp
    Created: 21 November 2021 at 14:45 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <memory>
#include "../include/create_standard_modules.hpp"
#include "../../astlib/include/all_nodes.hpp"
#include "../../fst-lvl-scanner/include/lexeme.hpp"
#include "../include/strings_encodings_module_create.hpp"
#include "../include/strings_formatting_module_create.hpp"
#include "../include/system_io_module_create.hpp"
#include "../include/system_io_files_module_create.hpp"

namespace{
    const std::vector<std::u32string> standard_identifiers = {
        // Components of names of standard modules:
        U"Ввод_вывод", U"Кодировки", U"Преамбула", U"Система", U"Строки", U"Файлы", U"Форматирование",

        // Identifiers from the module Строки::Кодировки:
        U"CP1251",      U"CP866",       U"KOI8R",       U"UCS2",       U"UCS4",        U"UTF8",          U"s",
        U"Кодировка",   U"Кодировка16", U"Кодировка32", U"Кодировка8", U"в_кодировку", U"из_кодировки",  U"кодировка",

        // Identifiers from the module Строки::Форматирование:
        U"fmt", U"аргументы", U"формат",

        // Identifiers from the module Система::Ввод_вывод:
        U"fmt", U"аргументы", U"ввод", U"печать",

        // Identifiers from the module Система::Ввод_вывод::Файлы:
        U"дескриптор",   U"дописывание", U"закрытьф", U"запись",        U"имя_файла", U"кодировка", U"открытьф",
        U"писать_текст", U"размерф",     U"режим",    U"режим_доступа", U"текст",     U"усечение",  U"читать_текст",
        U"читатьф",      U"чтение",

        // Identifiers from the module Преамбула:
        U"CIm",             U"CRe",             U"E",                 U"Im",            U"Lg",                       U"Ln",
        U"Log",             U"Log2",            U"R",                 U"Re",            U"T",                        U"V",
        U"abs",             U"arccos",          U"arccosh",           U"arcctg",        U"arcctgh",                  U"arcsin",
        U"arcsinh",         U"arctg",           U"arctgh",            U"cos",           U"cosh",                     U"ctg",
        U"ctgh",            U"exp",             U"max",               U"min",           U"p",                        U"sign",
        U"sin",             U"sinh",            U"sqrt",              U"tg",            U"tgh",                      U"x",
        U"y",               U"z",               U"аддитивный_моноид", U"аргументы",     U"большц",                   U"в_строку",
        U"вариант",         U"возможно",        U"вызов",             U"вызов_функции", U"единица",                  U"значение",
        U"значение",        U"индексация",      U"код_ошибки",        U"меньшц",        U"мультипликативный_моноид", U"нет_значения",
        U"нуль",            U"общий_тип",       U"округл",            U"ошибка",        U"пи",                       U"получить_значение",
        U"получить_ссылку", U"преобр_в_строку", U"равенство",         U"результат",     U"сопр",                     U"типы_пакета",
        U"упорядочение",    U"усеч",
    };
};

namespace ast{
    std::shared_ptr<ast::Module> Create_standard_modules::strings_encodings_module_create()
    {
        return standard_modules::strings::encodings::create_module(strs_trie_, ids_trie_, indices_of_standard_identifiers_);
    }

    std::shared_ptr<ast::Module> Create_standard_modules::strings_formatting_module_create()
    {
        return standard_modules::strings::formatting::create_module(strs_trie_, ids_trie_, indices_of_standard_identifiers_);
    }

    std::shared_ptr<ast::Module> Create_standard_modules::system_io_module_create()
    {
        return standard_modules::sistema::vvod_vyvod::create_module(strs_trie_, ids_trie_, indices_of_standard_identifiers_);
    }

    std::shared_ptr<ast::Module> Create_standard_modules::system_io_files_module_create()
    {
        return standard_modules::sistema::vvod_vyvod::faily::create_module(strs_trie_, ids_trie_, indices_of_standard_identifiers_);
    }

    std::shared_ptr<ast::Module> Create_standard_modules::prelude_module_create()
    {
        std::shared_ptr<ast::Module> result;
        return result;
    }

    std::map<std::u32string, std::shared_ptr<ast::Module>> Create_standard_modules::operator()()
    {
        create_indices_for_standard_identifiers();

        const auto strings_encoding_module   = strings_encodings_module_create();
        const auto strings_formatting_module = strings_formatting_module_create();
        const auto system_io_module          = system_io_module_create();
        const auto system_io_files_module    = system_io_files_module_create();
        const auto prelude_module            = prelude_module_create();

        std::map<std::u32string, std::shared_ptr<ast::Module>> result = {
            {U"Строки::Кодировки",          strings_encoding_module  },
            {U"Строки::Форматирование",     strings_formatting_module},
            {U"Система::Ввод_вывод",        system_io_module         },
            {U"Система::Ввод_вывод::Файлы", system_io_files_module   },
            {U"Преамбула",                  prelude_module           },
        };

        return result;
    }

    void Create_standard_modules::create_indices_for_standard_identifiers()
    {
        for(const auto& id : standard_identifiers)
        {
            indices_of_standard_identifiers_[id] = ids_trie_->insert(id);
        }
    }
};