/*
    File:    composite_name_node.cpp
    Created: 23 August 2021 at 15:32 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/composite_name_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"
#include "../../tries/include/idx_to_string.hpp"

namespace{
    const char* composite_name_fmt = R"~({0}COMPOSITE_NAME
{0}{{
{2}
{1}
{0}}})~";

    const char* operand_fmt = R"~({0}COMPOSITE_NAME_COMPONENT
{0}{{
{1}
{0}}})~";
};

namespace ast{
    NodeInfo Composite_name_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Composite_name;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Composite_name_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                               const std::shared_ptr<Char_trie>& ids_trie,
                                               const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto operand_to_str = [&indent1_str,
                                     &strs_trie,
                                     &ids_trie,
                                     &nested_indent](const auto& p) -> std::string
                                    {
                                       if(!p){
                                           return std::string{};
                                       }
                                       const auto op_str = p->to_string(strs_trie, ids_trie, nested_indent);
                                       return fmt::format(operand_fmt, indent1_str, op_str);
                                    };

        const auto components_str = join(operand_to_str, components_.begin(), components_.end(), std::string{"\n"});

        const auto start_name_str = idx_to_string(ids_trie, start_name_.lexeme_.id_index_);

        result = fmt::format(composite_name_fmt, indent0_str, components_str, start_name_str);
        return result;
    }
};