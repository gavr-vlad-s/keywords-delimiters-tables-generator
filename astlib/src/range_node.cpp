/*
    File:    range_node.cpp
    Created: 22 August 2021 at 09:58 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/range_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* range_op_fmt = R"~({0}RANGE [{1}]
{0}{{
{2}START:
{2}{{
{3}
{2}}}
{2}END:
{2}{{
{4}
{2}}}
{2}STEP:
{2}{{
{5}
{2}}}
{0}}})~";

    const char* kinds[] = {
        "Segment", "Semisegment"
    };
};

namespace ast{
    NodeInfo Range_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Range;
        ninfo.subkind_ = static_cast<std::uint16_t>(kind_);

        return ninfo;
    }

    std::string Range_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                      const std::shared_ptr<Char_trie>& ids_trie,
                                      const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        const auto  kind_str    = std::string{kinds[static_cast<unsigned>(kind_)]};

        std::string start_value_str;
        std::string end_value_str;
        std::string step_value_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        if(start_value_){
            start_value_str = start_value_->to_string(strs_trie, ids_trie, nested_indent);
        }
        if(end_value_){
            end_value_str = end_value_->to_string(strs_trie, ids_trie, nested_indent);
        }
        if(step_value_){
            step_value_str = step_value_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(range_op_fmt,
                             indent0_str,
                             kind_str,
                             indent1_str,
                             start_value_str,
                             end_value_str,
                             step_value_str);

        return result;
    }
};