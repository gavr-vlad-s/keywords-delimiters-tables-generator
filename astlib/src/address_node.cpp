/*
    File:    address_node.cpp
    Created: 22 August 2021 at 21:40 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/address_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* address_fmt = R"~({0}ADDRESS [{1}]
{0}{{
{2}OPERAND:
{2}{{
{3}
{2}}}
{0}}})~";

    const char* kinds[] = {
        "Address", "Data_address", "Expr_type"
    };
};

namespace ast{
    NodeInfo Address_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Address;
        ninfo.subkind_ = static_cast<std::uint16_t>(kind_);

        return ninfo;
    }

    std::string Address_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                        const std::shared_ptr<Char_trie>& ids_trie,
                                        const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        const auto  kind_str    = std::string{kinds[static_cast<unsigned>(kind_)]};

        std::string operand_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        if(operand_){
            operand_str = operand_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(address_fmt,
                             indent0_str,
                             kind_str,
                             indent1_str,
                             operand_str);

        return result;
    }
};