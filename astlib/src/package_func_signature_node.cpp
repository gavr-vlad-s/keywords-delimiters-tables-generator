/*
    File:    package_func_signature_node.cpp
    Created: 07 January 2022 at 10:08 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/package_func_signature_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../tries/include/idx_to_string.hpp"
#include "../../strings/include/join.hpp"

namespace{
    const char* package_func_signature_fmt = R"~({0}PACKAGE_FUNC_SIGNATURE
{0}{{
{1}ARGUMENTS_GROUPS:
{1}{{
{2}
{1}}}
{1}RESULT_TYPE:
{1}{{
{3}
{1}}}
{0}}})~";

};

namespace ast{
    NodeInfo Package_func_signature_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Package_func_signature;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Package_func_signature_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                                       const std::shared_ptr<Char_trie>& ids_trie,
                                                       const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');
        const auto  indent2_str = std::string(indents.indent_ + 2 * indents.indent_inc_, ' ');

        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto arguments_group_to_str = [&ids_trie,
                                             &strs_trie,
                                             &nested_indent2,
                                             &indent2_str](const std::shared_ptr<ast::package_func::Parameter_group>& group) -> std::string
                                            {
                                                if(!group){
                                                    return indent2_str + "ARGUMENT_GROUPS";
                                                }
                                                return group->to_string(strs_trie, ids_trie, nested_indent2);
                                            };

        std::string result_type_str;
        if(result_type_){
            result_type_str = result_type_->to_string(strs_trie, ids_trie, nested_indent2);
        }

        const auto groups_str = join(arguments_group_to_str, formal_args_.begin(), formal_args_.end(), std::string{"\n"});

        result = fmt::format(package_func_signature_fmt, indent0_str, indent1_str, groups_str, result_type_str);

        return result;
    }
};