/*
    File:    ast_to_string.cpp
    Created: 17 July 2021 at 22:06 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/ast_to_string.hpp"

namespace ast{
    std::string AST_to_string::operator()(const std::shared_ptr<Module_node>& module_node,
                                          const Indents&                      indents) const
    {
        return module_node->to_string(strs_trie_, ids_trie_, indents);
    }
};