/*
    File:    qualified_id_node.cpp
    Created: 15 July 2021 at 21:07 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <vector>
#include "../include/qualified_id_node.hpp"
#include "../../tries/include/idx_to_string.hpp"
#include "../../strings/include/join.hpp"

namespace ast{
    NodeInfo Qualified_id_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Qualified_id;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Qualified_id_node::to_string(const std::shared_ptr<Char_trie>&,
                                             const std::shared_ptr<Char_trie>& ids_trie,
                                             const Indents&                    indents) const
    {
        std::string result;

        std::vector<std::string> components;
        for(const auto& component : components_)
        {
            components.push_back(idx_to_string(ids_trie, component.lexeme_.id_index_));
        }

        const auto q_id_str = join(components.begin(), components.end(), std::string{"::"});

        result = std::string(indents.indent_, ' ') + "QUALIFIED_ID " + q_id_str;

        return result;
    }
};