/*
    File:    function_ptr_type_node.cpp
    Created: 27 August 2021 at 12:46 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/function_ptr_type_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"

namespace{
    const char* func_ptr_type_fmt = R"~({0}{3}
{0}{{
{1}SIGNATURE
{1}{{
{2}
{1}}}
{0}}})~";
};

namespace ast{
    NodeInfo Function_ptr_type_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Func_ptr_type;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Function_ptr_type_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                                  const std::shared_ptr<Char_trie>& ids_trie,
                                                  const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        std::string signature_str;
        if(signature_){
            signature_str = signature_->to_string(strs_trie, ids_trie, nested_indent);
        }

        const std::string title = is_pure_ ? "PURE_FUNCTION_TYPE" : "FUNCTION_TYPE";

        result = fmt::format(func_ptr_type_fmt,
                             indent0_str,
                             indent1_str,
                             signature_str,
                             title);

        return result;
    }
};