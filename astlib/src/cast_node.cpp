/*
    File:    cast_node.cpp
    Created: 24 August 2021 at 12:38 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/cast_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* cast_fmt = R"~({0}CAST [{1}]
{0}{{
{2}VALUE_TO_CAST:
{2}{{
{3}
{2}}}
{2}DST_TYPE:
{2}{{
{4}
{2}}}
{0}}})~";

    const char* kinds[] = {
        "Reinterpret", "Convert"
    };
};

namespace ast{
    NodeInfo Cast_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Cast_op;
        ninfo.subkind_ = static_cast<std::uint16_t>(kind_);

        return ninfo;
    }

    std::string Cast_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                            const std::shared_ptr<Char_trie>& ids_trie,
                                            const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        const auto  kind_str    = std::string{kinds[static_cast<unsigned>(kind_)]};

        std::string value_to_cast_str;
        std::string dst_type_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        if(value_to_cast_){
            value_to_cast_str = value_to_cast_->to_string(strs_trie, ids_trie, nested_indent);
        }
        if(dst_type_){
            dst_type_str = dst_type_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(cast_fmt,
                             indent0_str,
                             kind_str,
                             indent1_str,
                             value_to_cast_str,
                             dst_type_str);

        return result;
    }
};