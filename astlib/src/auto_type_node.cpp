/*
    File:    auto_type_node.cpp
    Created: 26 August 2021 at 09:17 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/auto_type_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* auto_type_fmt = R"~({0}AUTO_TYPE
{0}{{
{0}}})~";
};

namespace ast{
    NodeInfo Auto_type_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Auto_type;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Auto_type_node::to_string(const std::shared_ptr<Char_trie>&,
                                          const std::shared_ptr<Char_trie>&,
                                          const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');

        result = fmt::format(auto_type_fmt, indent0_str);

        return result;
    }
};