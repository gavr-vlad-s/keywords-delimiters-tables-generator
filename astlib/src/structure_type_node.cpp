/*
    File:    structure_type_node.cpp
    Created: 26 August 2021 at 10:59 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/structure_type_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../tries/include/idx_to_string.hpp"
#include "../../strings/include/join.hpp"

namespace{
    const char* structure_type_fmt = R"~({0}STRUCTURE_TYPE
{0}{{
{1}NAME:
{1}{{
{2}
{1}}}
{1}FIELDS_GROUPS:
{1}{{
{3}
{1}}}
{0}}})~";

    const char* fields_group_fmt = R"~({0}FIELDS_GROUP
{0}{{
{1}FIELDS_NAMES
{1}{{
{2}
{1}}}
{1}TYPE
{1}{{
{3}
{1}}}
{0}}})~";

    const char* field_name_fmt = R"~({0}FIELD_NAME: {1})~";

    using Token = fst_level_scanner::Fst_lvl_token;
};

namespace ast{
    NodeInfo Structure_type_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Structure_type;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Structure_type_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                               const std::shared_ptr<Char_trie>& ids_trie,
                                               const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');
        const auto  indent2_str = std::string(indents.indent_ + 2 * indents.indent_inc_, ' ');
        const auto  indent3_str = std::string(indents.indent_ + 3 * indents.indent_inc_, ' ');
        const auto  indent4_str = std::string(indents.indent_ + 4 * indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 4 * indents.indent_inc_, indents.indent_inc_};

        const auto field_name_to_str = [&indent4_str, &ids_trie](const Token& token) -> std::string
                                       {
                                           return fmt::format(field_name_fmt,
                                                              indent4_str,
                                                              idx_to_string(ids_trie, token.lexeme_.id_index_));
                                       };

        const auto fields_names_to_str = [&field_name_to_str](const std::vector<Token>& names) -> std::string
                                         {
                                             return join(field_name_to_str,
                                                         names.begin(),
                                                         names.end(),
                                                         std::string{"\n"});
                                         };

        const auto fields_group_to_str = [&ids_trie,
                                          &strs_trie,
                                          &indent2_str,
                                          &indent3_str,
                                          &nested_indent,
                                          &fields_names_to_str](const Fields_group_def& group) -> std::string
                                         {
                                             const auto names_str = fields_names_to_str(group.fields_);

                                             std::string type_str;
                                             if(group.fields_type_){
                                                 type_str = group.fields_type_->to_string(strs_trie, ids_trie, nested_indent);
                                             }

                                             return fmt::format(fields_group_fmt,
                                                                indent2_str,
                                                                indent3_str,
                                                                names_str,
                                                                type_str);
                                         };

        const auto structure_type_name_str = idx_to_string(ids_trie, name_.lexeme_.id_index_);
        const auto groups_str              = join(fields_group_to_str,
                                                  fields_def_.begin(),
                                                  fields_def_.end(),
                                                  std::string{"\n"});

        result = fmt::format(structure_type_fmt, indent0_str, indent1_str, structure_type_name_str, groups_str);

        return result;
    }
};