/*
    File:    arkona_module.cpp
    Created: 04 August 2021 at 19:54 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/arkona_module.hpp"
#include "../include/ast_to_string.hpp"

namespace ast{
    std::string Module::to_string() const
    {
        AST_to_string converter{strs_trie_, ids_trie_};
        Indents       indent{4, 4};

        auto result = converter(module_node_, indent);

        return result;
    }
};