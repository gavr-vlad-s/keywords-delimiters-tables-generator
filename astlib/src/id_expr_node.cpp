/*
    File:    id_expr_node.cpp
    Created: 11 December 2021 at 17:12 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/id_expr_node.hpp"
#include "../../tries/include/idx_to_string.hpp"

namespace ast{
    NodeInfo Id_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Id;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Id_expr_node::to_string(const std::shared_ptr<Char_trie>&,
                                        const std::shared_ptr<Char_trie>& ids_trie,
                                        const Indents&                    indents) const
    {
        std::string result;
        result = std::string(indents.indent_, ' ') + "ID " + idx_to_string(ids_trie, id_.lexeme_.id_index_);
        return result;
    }
};