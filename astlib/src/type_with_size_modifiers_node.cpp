/*
    File:    type_with_size_modifiers_node.cpp
    Created: 25 August 2021 at 18:15 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/type_with_size_modifiers_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"

namespace{
    const char* type_with_modifiers_fmt = R"~({0}TYPE_WITH_SIZE_MODIFIERS
{0}{{
{1}KIND: {2}
{1}MODIFIERS:
{1}{{
{3}
{1}}}
{0}}})~";

    const char* kinds[] = {
        "Char",     "String",  "Bool",
        "Ordering", "Uint",    "Int",
        "Float",    "Complex", "Quat",
    };

    const char* modifier_kinds[] = {
        "Long", "Short"
    };
};

namespace ast{
    NodeInfo Type_with_size_modifiers_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Base_type_with_type_modifiers;
        ninfo.subkind_ = static_cast<std::uint16_t>(kind_);

        return ninfo;
    }

    std::string Type_with_size_modifiers_node::to_string(const std::shared_ptr<Char_trie>&,
                                                         const std::shared_ptr<Char_trie>&,
                                                         const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');
        const auto indent2_str = std::string(indents.indent_ + 2 * indents.indent_inc_, ' ');

        const auto kind_str      = std::string{kinds[static_cast<unsigned>(kind_)]};
        const auto mkind_to_str  = [&indent2_str](Modifier_kind mkind) -> std::string
                                   {
                                       return indent2_str + std::string{modifier_kinds[static_cast<unsigned>(mkind)]};
                                   };
        const auto modifiers_str = join(mkind_to_str,
                                        modifiers_.begin(),
                                        modifiers_.end(),
                                        std::string{"\n"});

        result = fmt::format(type_with_modifiers_fmt,
                             indent0_str,
                             indent1_str,
                             kind_str,
                             modifiers_str);
        return result;
    }
};