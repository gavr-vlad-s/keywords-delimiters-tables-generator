/*
    File:    func_header_node.cpp
    Created: 06 December 2021 at 19:47 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../../thirdparty/fmtlib/include/core.h"
#include "../../tries/include/idx_to_string.hpp"
#include "../include/func_header_node.hpp"

namespace{
    const char* func_header_fmt = R"~({0}FUNCTION_HEADER
{0}{{
{1}{4}{5}NAME: {2}
{1}SIGNATURE:
{1}{{
{3}
{1}}}
{0}}})~";
};


namespace ast{
    NodeInfo Func_header_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Func_header;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Func_header_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                            const std::shared_ptr<Char_trie>& ids_trie,
                                            const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        const Indents indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto name_str    = idx_to_string(ids_trie, name_.lexeme_.id_index_);

        std::string signature_str;
        if(signature_){
            signature_str = signature_->to_string(strs_trie, ids_trie, indent2);
        }

        std::string is_exported_str;
        if(is_exported_){
            is_exported_str = "EXPORTED ";
        }

        std::string is_pure_str;
        if(is_pure_){
            is_pure_str = "PURE ";
        }

        result = fmt::format(func_header_fmt,
                             indent0_str,
                             indent1_str,
                             name_str,
                             signature_str,
                             is_exported_str,
                             is_pure_str);

        return result;
    }
};