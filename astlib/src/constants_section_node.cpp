/*
    File:    constants_section_node.cpp
    Created: 28 August 2021 at 16:03 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/constants_section_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"

namespace{
    const char* constants_section_node_fmt = R"~({0}CONSTANTS_SECTION
{0}{{
{1}
{0}}})~";
};

namespace ast{
    NodeInfo Constants_section_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Constants_section;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Constants_section_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                                  const std::shared_ptr<Char_trie>& ids_trie,
                                                  const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');

        Indents nested_indent{indents.indent_ + indents.indent_inc_, indents.indent_inc_};

        const auto constant_node_to_string = [&strs_trie, &ids_trie, &nested_indent](const auto& c)
                                             {
                                                 if(!c){
                                                     return std::string{};
                                                 }

                                                 return c->to_string(strs_trie, ids_trie, nested_indent);
                                             };

        const auto section_body_str = join(constant_node_to_string,
                                           constants_.begin(),
                                           constants_.end(),
                                           std::string{"\n"});

        result = fmt::format(constants_section_node_fmt, indent0_str, section_body_str);

        return result;
    }
};