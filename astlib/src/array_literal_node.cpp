/*
    File:    array_literal_node.cpp
    Created: 24 August 2021 at 14:56 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/array_literal_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"

namespace{
    const char* array_literal_fmt = R"~({0}ARRAY_LITERAL
{0}{{
{1}
{0}}})~";

    const char* element_fmt = R"~({0}ELEMENT_VALUE {1}
{0}{{
{2}
{0}}})~";
};

namespace ast{
    NodeInfo Array_literal_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Array_literal;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Array_literal_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                              const std::shared_ptr<Char_trie>& ids_trie,
                                              const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        std::vector<std::pair<std::size_t, std::string>> components_strs;

        std::size_t i = 0;

        for(const auto& component : components_){
            if(component){
                components_strs.push_back({i, component->to_string(strs_trie, ids_trie, nested_indent)});
            }else{
                components_strs.push_back({i, std::string{}});
            }

            i++;
        }

        const auto components_str = join([&indent1_str](const auto& p) -> std::string
                                         {
                                             const auto& [idx, op_str] = p;
                                             return fmt::format(element_fmt, indent1_str, idx, op_str);
                                         },
                                         components_strs.begin(),
                                         components_strs.end(),
                                         std::string{"\n"});

        result = fmt::format(array_literal_fmt, indent0_str, components_str);

        return result;
    }
};