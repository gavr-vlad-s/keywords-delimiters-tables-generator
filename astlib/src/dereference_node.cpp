/*
    File:    dereference_node.cpp
    Created: 22 August 2021 at 20:58 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/dereference_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* dereference_fmt = R"~({0}DEREFERENCE [{1}]
{0}{{
{2}OPERAND:
{2}{{
{3}
{2}}}
{0}}})~";
};

namespace ast{
    NodeInfo Dereference_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Dereference;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Dereference_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                            const std::shared_ptr<Char_trie>& ids_trie,
                                            const Indents&                    indents) const
    {
        std::string result;

        const auto  indent0_str = std::string(indents.indent_, ' ');
        const auto  indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        std::string operand_str;

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        if(operand_){
            operand_str = operand_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(dereference_fmt,
                             indent0_str,
                             num_of_ops_,
                             indent1_str,
                             operand_str);

        return result;
    }
};