/*
    File:    scope.cpp
    Created: 03 January 2022 at 09:45 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <tuple>
#include "../include/scope.hpp"
#include "../../thirdparty/hashing/include/tuple_hash.hpp"

namespace symbol_table{
    bool operator==(const Identifier_info& lhs, const Identifier_info& rhs)
    {
        return lhs.kind_ == rhs.kind_;
    }

    bool operator<(const Identifier_info& lhs, const Identifier_info& rhs)
    {
        return lhs.kind_ < rhs.kind_;
    }

    Identifier_info_set Scope::find_id(std::size_t id) const
    {
        const auto it = ids_.find(id);
        if(it == ids_.end()){
            return {};
        }
        const auto [i, s] = *it;
        return s;
    }
};
