/*
    File:    strings_formatting_module_create.cpp
    Created: 08 January 2022 at 15:46 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/strings_formatting_module_create.hpp"
#include "../include/all_nodes.hpp"
#include "../include/symbol_table.hpp"
#include "../include/package_func_parameter_group_variant.hpp"
#include "../../fst-lvl-scanner/include/lexeme.hpp"

namespace standard_modules::strings::formatting{
    namespace details{
        using Lexeme         = fst_level_scanner::LexemeInfo;

        //! Function that return identifier.
        const Lexeme id_lexeme(std::size_t idx)
        {
            Lexeme li;
            li.code_.kind_    = fst_level_scanner::Lexem_kind::Id;
            li.code_.subkind_ = 0;
            li.id_index_      = idx;
            return li;
        }

        auto build_format32_func(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            // экспорт : пакетная функция формат(fmt : конст ссылка строка,
            //                                   пакет аргументы <!!> T) : строка;
            const std::size_t                      format_id              = indices_of_standard_identifiers[U"формат"];
            const std::size_t                      fmt_id                 = indices_of_standard_identifiers[U"fmt"];
            const std::size_t                      t_id                   = indices_of_standard_identifiers[U"T"];
            const std::size_t                      argumenty_id           = indices_of_standard_identifiers[U"аргументы"];
            const fst_level_scanner::Fst_lvl_token format32               = {{{3,31 }, {3,36 }}, id_lexeme(format_id)};
            const fst_level_scanner::Fst_lvl_token fmt_arg                = {{{3,38 }, {3,40 }}, id_lexeme(fmt_id)};
            const fst_level_scanner::Fst_lvl_token t_arg                  = {{{4,59 }, {4,59 }}, id_lexeme(t_id)};
            const fst_level_scanner::Fst_lvl_token argumenty              = {{{4,44 }, {4,52 }}, id_lexeme(argumenty_id)};

            std::vector<fst_level_scanner::Fst_lvl_token> vec_for_fmt     = {fmt_arg};

            const auto                             type_string            = std::make_shared<ast::Type_with_size_modifiers_node>(ast::Type_with_size_modifiers_node::Type_kind::String,
                                                                                                                                 std::vector<ast::Type_with_size_modifiers_node::Modifier_kind>{});
            const auto                             type_for_fmt           = std::make_shared<ast::Reference_type_node>(true, type_string);
            const auto                             group_for_fmt          = std::make_shared<ast::package_func::Ordinary_parameter_group>(vec_for_fmt, type_for_fmt);

            const auto                             type_for_argumenty     = std::make_shared<ast::Id_expr_node>(t_arg);
            const auto                             group_for_argumenty    = std::make_shared<ast::package_func::Parametrized_package_with_different_types_group>(argumenty, type_for_argumenty);

            std::vector<std::shared_ptr<ast::package_func::Parameter_group>> package_func_arguments {group_for_fmt, group_for_argumenty};

            const auto                             type_of_returned_value = std::make_shared<ast::Type_with_size_modifiers_node>(ast::Type_with_size_modifiers_node::Type_kind::String,
                                                                                                                                 std::vector<ast::Type_with_size_modifiers_node::Modifier_kind>{});

            const auto                             signature              = std::make_shared<ast::Package_func_signature_node>(package_func_arguments, type_of_returned_value);
            const auto                             node                   = std::make_shared<ast::Package_func_header_node>(true, false, format32, signature);
            const auto                             format32_func_name     = symbol_table::Identifier_info{symbol_table::Identifier_kind::Package_func_name};

            scope->ids_[format_id][format32_func_name].insert(node);

            return node;
        }

        auto build_format8_func(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            // экспорт : пакетная функция формат(fmt : конст ссылка строка8,
            //                                   пакет аргументы <!!> T) : строка8;
            const std::size_t                      format_id              = indices_of_standard_identifiers[U"формат"];
            const std::size_t                      fmt_id                 = indices_of_standard_identifiers[U"fmt"];
            const std::size_t                      t_id                   = indices_of_standard_identifiers[U"T"];
            const std::size_t                      argumenty_id           = indices_of_standard_identifiers[U"аргументы"];
            const fst_level_scanner::Fst_lvl_token format8                = {{{6,31 }, {6,36 }}, id_lexeme(format_id)};
            const fst_level_scanner::Fst_lvl_token fmt_arg                = {{{6,38 }, {6,40 }}, id_lexeme(fmt_id)};
            const fst_level_scanner::Fst_lvl_token t_arg                  = {{{7,59 }, {7,59 }}, id_lexeme(t_id)};
            const fst_level_scanner::Fst_lvl_token argumenty              = {{{7,44 }, {7,52 }}, id_lexeme(argumenty_id)};

            std::vector<fst_level_scanner::Fst_lvl_token> vec_for_fmt     = {fmt_arg};

            const auto                             type_string            = std::make_shared<ast::Constant_size_type_node>(ast::Constant_size_type_node::Type_kind::String8);
            const auto                             type_for_fmt           = std::make_shared<ast::Reference_type_node>(true, type_string);
            const auto                             group_for_fmt          = std::make_shared<ast::package_func::Ordinary_parameter_group>(vec_for_fmt, type_for_fmt);

            const auto                             type_for_argumenty     = std::make_shared<ast::Id_expr_node>(t_arg);
            const auto                             group_for_argumenty    = std::make_shared<ast::package_func::Parametrized_package_with_different_types_group>(argumenty, type_for_argumenty);

            std::vector<std::shared_ptr<ast::package_func::Parameter_group>> package_func_arguments {group_for_fmt, group_for_argumenty};

            const auto                             type_of_returned_value = std::make_shared<ast::Constant_size_type_node>(ast::Constant_size_type_node::Type_kind::String8);

            const auto                             signature              = std::make_shared<ast::Package_func_signature_node>(package_func_arguments, type_of_returned_value);
            const auto                             node                   = std::make_shared<ast::Package_func_header_node>(true, false, format8, signature);
            const auto                             format8_func_name      = symbol_table::Identifier_info{symbol_table::Identifier_kind::Package_func_name};

            scope->ids_[format_id][format8_func_name].insert(node);

            return node;
        }

        auto build_format16_func(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            // экспорт : пакетная функция формат(fmt : конст ссылка строка16,
            //                                   пакет аргументы <!!> T) : строка16;
            const std::size_t                      format_id              = indices_of_standard_identifiers[U"формат"];
            const std::size_t                      fmt_id                 = indices_of_standard_identifiers[U"fmt"];
            const std::size_t                      t_id                   = indices_of_standard_identifiers[U"T"];
            const std::size_t                      argumenty_id           = indices_of_standard_identifiers[U"аргументы"];
            const fst_level_scanner::Fst_lvl_token format16               = {{{9,31 }, {9,36 }}, id_lexeme(format_id)};
            const fst_level_scanner::Fst_lvl_token fmt_arg                = {{{9,38 }, {9,40 }}, id_lexeme(fmt_id)};
            const fst_level_scanner::Fst_lvl_token t_arg                  = {{{10,59 }, {10,59 }}, id_lexeme(t_id)};
            const fst_level_scanner::Fst_lvl_token argumenty              = {{{10,44 }, {10,52 }}, id_lexeme(argumenty_id)};

            std::vector<fst_level_scanner::Fst_lvl_token> vec_for_fmt     = {fmt_arg};

            const auto                             type_string            = std::make_shared<ast::Constant_size_type_node>(ast::Constant_size_type_node::Type_kind::String16);
            const auto                             type_for_fmt           = std::make_shared<ast::Reference_type_node>(true, type_string);
            const auto                             group_for_fmt          = std::make_shared<ast::package_func::Ordinary_parameter_group>(vec_for_fmt, type_for_fmt);

            const auto                             type_for_argumenty     = std::make_shared<ast::Id_expr_node>(t_arg);
            const auto                             group_for_argumenty    = std::make_shared<ast::package_func::Parametrized_package_with_different_types_group>(argumenty, type_for_argumenty);

            std::vector<std::shared_ptr<ast::package_func::Parameter_group>> package_func_arguments {group_for_fmt, group_for_argumenty};

            const auto                             type_of_returned_value = std::make_shared<ast::Constant_size_type_node>(ast::Constant_size_type_node::Type_kind::String16);

            const auto                             signature              = std::make_shared<ast::Package_func_signature_node>(package_func_arguments, type_of_returned_value);
            const auto                             node                   = std::make_shared<ast::Package_func_header_node>(true, false, format16, signature);
            const auto                             format16_func_name     = symbol_table::Identifier_info{symbol_table::Identifier_kind::Package_func_name};

            scope->ids_[format_id][format16_func_name].insert(node);

            return node;
        }

        auto build_module_name(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers)
        {
            const fst_level_scanner::Fst_lvl_token strings_id    = {{{1, 7 }, {1, 12 }},
                                                                    id_lexeme(indices_of_standard_identifiers[U"Строки"])};
            const fst_level_scanner::Fst_lvl_token formatting_id = {{{1, 15 }, {1, 28 }},
                                                                    id_lexeme(indices_of_standard_identifiers[U"Форматирование"])};

            std::vector<fst_level_scanner::Fst_lvl_token> components {strings_id, formatting_id};

            return std::make_shared<ast::Qualified_id_node>(components);
        }
    }

    std::shared_ptr<ast::Module> create_module(const std::shared_ptr<Char_trie>&      strs_trie,
                                               const std::shared_ptr<Char_trie>&      ids_trie,
                                               std::map<std::u32string, std::size_t>& indices_of_standard_identifiers)
    {
        std::shared_ptr<ast::Module> result;

        auto sym_table = std::make_shared<symbol_table::Symbol_table>(strs_trie, ids_trie);
        auto scope     = sym_table->get_current_scope();

        const auto format32_func = details::build_format32_func(indices_of_standard_identifiers, scope);
        const auto format8_func  = details::build_format8_func(indices_of_standard_identifiers, scope);
        const auto format16_func = details::build_format16_func(indices_of_standard_identifiers, scope);

        std::vector<std::shared_ptr<ast::Stmt_node>> module_body {format32_func, format8_func, format16_func};

        const auto body                    = std::make_shared<ast::Block_node>(module_body);
        const auto used_modules            = std::make_shared<ast::Used_modules_node>();
        const auto name                    = details::build_module_name(indices_of_standard_identifiers);

        const auto ast_root                = std::make_shared<ast::Module_node>(name, used_modules, body);

        result = std::make_shared<ast::Module>(strs_trie, ids_trie, ast_root, sym_table);

        return result;
    }
};
