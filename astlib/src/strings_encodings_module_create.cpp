/*
    File:    strings_encodings_module_create.cpp
    Created: 05 January 2022 at 09:16 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/strings_encodings_module_create.hpp"
#include "../include/all_nodes.hpp"
#include "../include/symbol_table.hpp"
#include "../../fst-lvl-scanner/include/lexeme.hpp"

namespace standard_modules::strings::encodings{
    namespace details{
        using Lexeme         = fst_level_scanner::LexemeInfo;

        //! Function that return identifier.
        const Lexeme id_lexeme(std::size_t idx)
        {
            Lexeme li;
            li.code_.kind_    = fst_level_scanner::Lexem_kind::Id;
            li.code_.subkind_ = 0;
            li.id_index_      = idx;
            return li;
        }

        auto build_kodirovka32_enum(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            const std::size_t                                   kodirovka32_id             = indices_of_standard_identifiers[U"Кодировка32"];
            const std::size_t                                   ucs4_id                    = indices_of_standard_identifiers[U"UCS4"];
            const fst_level_scanner::Fst_lvl_token              kodirovka32_enum_name      = {{{3,46 }, {3,56 }}, id_lexeme(kodirovka32_id)};
            const std::vector<fst_level_scanner::Fst_lvl_token> kodirovka32_enum_members   = {
                {{{3,58 }, {3,61 }}, id_lexeme(ucs4_id)},
            };

            const auto                                          node                       = std::make_shared<ast::Enum_type_node>(kodirovka32_enum_name,
                                                                                                                                   kodirovka32_enum_members);
            symbol_table::Identifier_info_set kodirovka32_id_infos = {
                {symbol_table::Identifier_info{symbol_table::Identifier_kind::Enum_name}, {node}}
            };
            scope->ids_[kodirovka32_id] = kodirovka32_id_infos;

            symbol_table::Identifier_info_set ucs4_id_infos = {
                {symbol_table::Identifier_info{symbol_table::Identifier_kind::Enum_elem}, {node}}
            };
            scope->ids_[ucs4_id] = ucs4_id_infos;

            return node;
        }

        auto build_kodirovka16_enum(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            const std::size_t                                   kodirovka16_id             = indices_of_standard_identifiers[U"Кодировка16"];
            const std::size_t                                   ucs2_id                    = indices_of_standard_identifiers[U"UCS2"];
            const fst_level_scanner::Fst_lvl_token              kodirovka16_enum_name      = {{{4,46 }, {4,56 }}, id_lexeme(kodirovka16_id)};
            const std::vector<fst_level_scanner::Fst_lvl_token> kodirovka16_enum_members   = {
                {{{4,58 }, {4,61 }}, id_lexeme(ucs2_id)},
            };

            const auto                                          node                       = std::make_shared<ast::Enum_type_node>(kodirovka16_enum_name,
                                                                                                                                   kodirovka16_enum_members);
            symbol_table::Identifier_info_set kodirovka16_id_infos = {
                {symbol_table::Identifier_info{symbol_table::Identifier_kind::Enum_name}, {node}}
            };
            scope->ids_[kodirovka16_id] = kodirovka16_id_infos;

            symbol_table::Identifier_info_set ucs2_id_infos = {
                {symbol_table::Identifier_info{symbol_table::Identifier_kind::Enum_elem}, {node}}
            };
            scope->ids_[ucs2_id] = ucs2_id_infos;

            return node;
        }

        auto build_kodirovka8_enum(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            const std::size_t                                   kodirovka8_id             = indices_of_standard_identifiers[U"Кодировка8"];
            const std::size_t                                   cp866_id                  = indices_of_standard_identifiers[U"CP866"];
            const std::size_t                                   cp1251_id                 = indices_of_standard_identifiers[U"CP1251"];
            const std::size_t                                   koi8r_id                  = indices_of_standard_identifiers[U"KOI8R"];
            const std::size_t                                   utf8_id                   = indices_of_standard_identifiers[U"UTF8"];
            const fst_level_scanner::Fst_lvl_token              kodirovka8_enum_name      = {{{5,46 }, {5,55 }}, id_lexeme(kodirovka8_id)};
            const std::vector<fst_level_scanner::Fst_lvl_token> kodirovka8_enum_members   = {
                {{{5,57 }, {5,61 }}, id_lexeme(cp866_id)},
                {{{5,64 }, {5,69 }}, id_lexeme(cp1251_id)},
                {{{5,72 }, {5,76 }}, id_lexeme(koi8r_id)},
                {{{5,79 }, {5,82 }}, id_lexeme(utf8_id)},
            };

            const auto                                          node                      = std::make_shared<ast::Enum_type_node>(kodirovka8_enum_name,
                                                                                                                                  kodirovka8_enum_members);
            symbol_table::Identifier_info_set kodirovka8_id_infos = {
                {symbol_table::Identifier_info{symbol_table::Identifier_kind::Enum_name}, {node}}
            };
            scope->ids_[kodirovka8_id] = kodirovka8_id_infos;

            symbol_table::Identifier_info_set cp866_id_infos = {
                {symbol_table::Identifier_info{symbol_table::Identifier_kind::Enum_elem}, {node}}
            };
            scope->ids_[cp866_id] = cp866_id_infos;

            symbol_table::Identifier_info_set cp1251_id_infos = {
                {symbol_table::Identifier_info{symbol_table::Identifier_kind::Enum_elem}, {node}}
            };
            scope->ids_[cp1251_id] = cp1251_id_infos;

            symbol_table::Identifier_info_set koi8r_id_infos = {
                {symbol_table::Identifier_info{symbol_table::Identifier_kind::Enum_elem}, {node}}
            };
            scope->ids_[koi8r_id] = koi8r_id_infos;

            symbol_table::Identifier_info_set utf8_id_infos = {
                {symbol_table::Identifier_info{symbol_table::Identifier_kind::Enum_elem}, {node}}
            };
            scope->ids_[utf8_id] = utf8_id_infos;

            return node;
        }

        auto build_types_section(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            // Кодировка32 = перечисление Кодировка32{UCS4},
            const auto                             kodirovka32_enum           = build_kodirovka32_enum(indices_of_standard_identifiers, scope);
            const std::size_t                      kodirovka32_id             = indices_of_standard_identifiers[U"Кодировка32"];
            const fst_level_scanner::Fst_lvl_token kodirovka32_type_node_name = {{{3,19 }, {3,29 }}, id_lexeme(kodirovka32_id)};
            const auto                             kodirovka32_type_node      = std::make_shared<ast::Type_definition_node>(true,
                                                                                                                            kodirovka32_type_node_name,
                                                                                                                            kodirovka32_enum);
            const auto                             kodirovka32_type_name      = symbol_table::Identifier_info{symbol_table::Identifier_kind::Type_name};

            scope->ids_[kodirovka32_id][kodirovka32_type_name] = {kodirovka32_type_node};

            // Кодировка16 = перечисление Кодировка16{UCS2},
            const auto                             kodirovka16_enum           = build_kodirovka16_enum(indices_of_standard_identifiers, scope);
            const std::size_t                      kodirovka16_id             = indices_of_standard_identifiers[U"Кодировка16"];
            const fst_level_scanner::Fst_lvl_token kodirovka16_type_node_name = {{{4,19 }, {4,29 }}, id_lexeme(kodirovka16_id)};
            const auto                             kodirovka16_type_node      = std::make_shared<ast::Type_definition_node>(true,
                                                                                                                            kodirovka16_type_node_name,
                                                                                                                            kodirovka16_enum);
            const auto                             kodirovka16_type_name      = symbol_table::Identifier_info{symbol_table::Identifier_kind::Type_name};

            scope->ids_[kodirovka16_id][kodirovka16_type_name] = {kodirovka16_type_node};

            // Кодировка8  = перечисление Кодировка8{CP866, CP1251, KOI8R, UTF8},
            const auto                             kodirovka8_enum           = build_kodirovka8_enum(indices_of_standard_identifiers, scope);
            const std::size_t                      kodirovka8_id             = indices_of_standard_identifiers[U"Кодировка8"];
            const fst_level_scanner::Fst_lvl_token kodirovka8_type_node_name = {{{5,19 }, {5,28 }}, id_lexeme(kodirovka8_id)};
            const auto                             kodirovka8_type_node      = std::make_shared<ast::Type_definition_node>(true,
                                                                                                                        kodirovka8_type_node_name,
                                                                                                                        kodirovka8_enum);
            const auto                             kodirovka8_type_name      = symbol_table::Identifier_info{symbol_table::Identifier_kind::Type_name};

            scope->ids_[kodirovka8_id][kodirovka8_type_name] = {kodirovka8_type_node};

            // Кодировка   = обобщение{Кодировка8, Кодировка16, Кодировка32};
            const std::size_t                      kodirovka_id              = indices_of_standard_identifiers[U"Кодировка"];
            const fst_level_scanner::Fst_lvl_token kodirovka_type_node_name  = {{{6,19 }, {5,27 }}, id_lexeme(kodirovka_id)};

            const std::vector<std::shared_ptr<ast::Expr_node>>       kodirovka_generalized_types = {
                kodirovka32_enum, kodirovka16_enum, kodirovka8_enum
            };

            const auto                             kodirovka_generalization  = std::make_shared<ast::Generalization_type_node>(kodirovka_generalized_types);
            const auto                             kodirovka_type_node       = std::make_shared<ast::Type_definition_node>(true,
                                                                                                                        kodirovka_type_node_name,
                                                                                                                        kodirovka_generalization);
            const auto                             kodirovka_type_name       = symbol_table::Identifier_info{symbol_table::Identifier_kind::Type_name};

            scope->ids_[kodirovka_id][kodirovka_type_name] = {kodirovka_type_node};

            // Section типы for all above:
            const std::vector<std::shared_ptr<ast::Type_definition_node>> types_section = {
                kodirovka32_type_node, kodirovka16_type_node, kodirovka8_type_node, kodirovka_type_node
            };
            return std::make_shared<ast::Types_section_node>(types_section);
        }

        auto build_iz_kodirovki8_function(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            // экспорт : чистая функция из_кодировки(s         : конст ссылка строка8,
            //                                       кодировка : Кодировка8) : строка;
            const std::size_t                      iz_kodirovki8_id = indices_of_standard_identifiers[U"из_кодировки"];
            const std::size_t                      sarg_id          = indices_of_standard_identifiers[U"s"];
            const std::size_t                      kodirovka_id     = indices_of_standard_identifiers[U"кодировка"];
            const std::size_t                      kodirovka8_id    = indices_of_standard_identifiers[U"Кодировка8"];
            const fst_level_scanner::Fst_lvl_token iz_kodirovki8    = {{{8,29 }, {8,40 }}, id_lexeme(iz_kodirovki8_id)};
            const fst_level_scanner::Fst_lvl_token s                = {{{8,42 }, {8,42 }}, id_lexeme(sarg_id)};
            const fst_level_scanner::Fst_lvl_token kodirovka        = {{{9,42 }, {9,50 }}, id_lexeme(kodirovka_id)};
            const fst_level_scanner::Fst_lvl_token kodirovka8       = {{{9,54 }, {9,63 }}, id_lexeme(kodirovka8_id)};

            const auto                             type_string8     = std::make_shared<ast::Constant_size_type_node>(ast::Constant_size_type_node::Type_kind::String8);
            const auto                             type_for_s       = std::make_shared<ast::Reference_type_node>(true, type_string8);
            const ast::Group_of_func_args          group_for_s{{s}, type_for_s};

            const auto                             type_for_kodirovka = std::make_shared<ast::Id_expr_node>(kodirovka8);
            const ast::Group_of_func_args          group_for_kodirovka{{kodirovka}, type_for_kodirovka};

            const auto                             type_of_returned_value = std::make_shared<ast::Type_with_size_modifiers_node>(ast::Type_with_size_modifiers_node::Type_kind::String,
                                                                                                                                 std::vector<ast::Type_with_size_modifiers_node::Modifier_kind>{});

            std::vector<ast::Group_of_func_args>   arguments {group_for_s, group_for_kodirovka};

            const auto                             signature               = std::make_shared<ast::Func_signature_node>(arguments, type_of_returned_value);
            const auto                             node                    = std::make_shared<ast::Func_header_node>(true, true, iz_kodirovki8, signature);
            const auto                             iz_kodirovki8_func_name = symbol_table::Identifier_info{symbol_table::Identifier_kind::Func_name};

            scope->ids_[iz_kodirovki8_id][iz_kodirovki8_func_name].insert(node);
            return node;
        }

        auto build_iz_kodirovki16_function(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            // экспорт : чистая функция из_кодировки(s         : конст ссылка строка16,
            //                                       кодировка : Кодировка16) : строка;
            const std::size_t                      iz_kodirovki16_id = indices_of_standard_identifiers[U"из_кодировки"];
            const std::size_t                      sarg_id           = indices_of_standard_identifiers[U"s"];
            const std::size_t                      kodirovka_id      = indices_of_standard_identifiers[U"кодировка"];
            const std::size_t                      kodirovka16_id    = indices_of_standard_identifiers[U"Кодировка16"];
            const fst_level_scanner::Fst_lvl_token iz_kodirovki16    = {{{11,29 }, {11,40 }}, id_lexeme(iz_kodirovki16_id)};
            const fst_level_scanner::Fst_lvl_token s                 = {{{11,42 }, {11,42 }}, id_lexeme(sarg_id)};
            const fst_level_scanner::Fst_lvl_token kodirovka         = {{{12,42 }, {12,50 }}, id_lexeme(kodirovka_id)};
            const fst_level_scanner::Fst_lvl_token kodirovka16       = {{{12,54 }, {12,64 }}, id_lexeme(kodirovka16_id)};

            const auto                             type_string16     = std::make_shared<ast::Constant_size_type_node>(ast::Constant_size_type_node::Type_kind::String16);
            const auto                             type_for_s        = std::make_shared<ast::Reference_type_node>(true, type_string16);
            const ast::Group_of_func_args          group_for_s{{s}, type_for_s};

            const auto                             type_for_kodirovka = std::make_shared<ast::Id_expr_node>(kodirovka16);
            const ast::Group_of_func_args          group_for_kodirovka{{kodirovka}, type_for_kodirovka};

            const auto                             type_of_returned_value = std::make_shared<ast::Type_with_size_modifiers_node>(ast::Type_with_size_modifiers_node::Type_kind::String,
                                                                                                                                 std::vector<ast::Type_with_size_modifiers_node::Modifier_kind>{});

            std::vector<ast::Group_of_func_args>   arguments {group_for_s, group_for_kodirovka};

            const auto                             signature                = std::make_shared<ast::Func_signature_node>(arguments, type_of_returned_value);
            const auto                             node                     = std::make_shared<ast::Func_header_node>(true, true, iz_kodirovki16, signature);
            const auto                             iz_kodirovki16_func_name = symbol_table::Identifier_info{symbol_table::Identifier_kind::Func_name};

            scope->ids_[iz_kodirovki16_id][iz_kodirovki16_func_name].insert(node);

            return node;
        }

        auto build_v_kodirovku8_function(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            // экспорт : чистая функция в_кодировку(s         : конст ссылка строка,
            //                                      кодировка : Кодировка8) : строка8;
            const std::size_t                      v_kodirovku8_id = indices_of_standard_identifiers[U"в_кодировку"];
            const std::size_t                      sarg_id         = indices_of_standard_identifiers[U"s"];
            const std::size_t                      kodirovka_id    = indices_of_standard_identifiers[U"кодировка"];
            const std::size_t                      kodirovka8_id   = indices_of_standard_identifiers[U"Кодировка8"];
            const fst_level_scanner::Fst_lvl_token v_kodirovku8    = {{{14,29 }, {14,39 }}, id_lexeme(v_kodirovku8_id)};
            const fst_level_scanner::Fst_lvl_token s               = {{{14,41 }, {14,41 }}, id_lexeme(sarg_id)};
            const fst_level_scanner::Fst_lvl_token kodirovka       = {{{15,41 }, {15,49 }}, id_lexeme(kodirovka_id)};
            const fst_level_scanner::Fst_lvl_token kodirovka8      = {{{15,53 }, {15,62 }}, id_lexeme(kodirovka8_id)};

            const auto                             type_string     = std::make_shared<ast::Type_with_size_modifiers_node>(ast::Type_with_size_modifiers_node::Type_kind::String,
                                                                                                                          std::vector<ast::Type_with_size_modifiers_node::Modifier_kind>{});
            const auto                             type_for_s      = std::make_shared<ast::Reference_type_node>(true, type_string);
            const ast::Group_of_func_args          group_for_s{{s}, type_for_s};

            const auto                             type_for_kodirovka = std::make_shared<ast::Id_expr_node>(kodirovka8);
            const ast::Group_of_func_args          group_for_kodirovka{{kodirovka}, type_for_kodirovka};

            const auto                             type_of_returned_value = std::make_shared<ast::Constant_size_type_node>(ast::Constant_size_type_node::Type_kind::String8);

            std::vector<ast::Group_of_func_args>   arguments {group_for_s, group_for_kodirovka};

            const auto                             signature              = std::make_shared<ast::Func_signature_node>(arguments, type_of_returned_value);
            const auto                             node                   = std::make_shared<ast::Func_header_node>(true, true, v_kodirovku8, signature);
            const auto                             v_kodirovku8_func_name = symbol_table::Identifier_info{symbol_table::Identifier_kind::Func_name};

            scope->ids_[v_kodirovku8_id][v_kodirovku8_func_name].insert(node);

            return node;
        }

        auto build_v_kodirovku16_function(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers, std::shared_ptr<symbol_table::Scope>& scope)
        {
            // экспорт : чистая функция в_кодировку(s         : конст ссылка строка,
            //                                      кодировка : Кодировка16) : строка8;
            const std::size_t                      v_kodirovku16_id = indices_of_standard_identifiers[U"в_кодировку"];
            const std::size_t                      sarg_id          = indices_of_standard_identifiers[U"s"];
            const std::size_t                      kodirovka_id     = indices_of_standard_identifiers[U"кодировка"];
            const std::size_t                      kodirovka16_id   = indices_of_standard_identifiers[U"Кодировка16"];
            const fst_level_scanner::Fst_lvl_token v_kodirovku16    = {{{17,29 }, {17,39 }}, id_lexeme(v_kodirovku16_id)};
            const fst_level_scanner::Fst_lvl_token s                = {{{17,41 }, {17,41 }}, id_lexeme(sarg_id)};
            const fst_level_scanner::Fst_lvl_token kodirovka        = {{{18,41 }, {18,49 }}, id_lexeme(kodirovka_id)};
            const fst_level_scanner::Fst_lvl_token kodirovka16      = {{{18,53 }, {18,63 }}, id_lexeme(kodirovka16_id)};

            const auto                             type_string      = std::make_shared<ast::Type_with_size_modifiers_node>(ast::Type_with_size_modifiers_node::Type_kind::String,
                                                                                                                           std::vector<ast::Type_with_size_modifiers_node::Modifier_kind>{});
            const auto                             type_for_s       = std::make_shared<ast::Reference_type_node>(true, type_string);
            const ast::Group_of_func_args          group_for_s{{s}, type_for_s};

            const auto                             type_for_kodirovka = std::make_shared<ast::Id_expr_node>(kodirovka16);
            const ast::Group_of_func_args          group_for_kodirovka{{kodirovka}, type_for_kodirovka};

            const auto                             type_of_returned_value = std::make_shared<ast::Constant_size_type_node>(ast::Constant_size_type_node::Type_kind::String8);

            std::vector<ast::Group_of_func_args>   arguments {group_for_s, group_for_kodirovka};

            const auto                             signature               = std::make_shared<ast::Func_signature_node>(arguments, type_of_returned_value);
            const auto                             node                    = std::make_shared<ast::Func_header_node>(true, true, v_kodirovku16, signature);
            const auto                             v_kodirovku16_func_name = symbol_table::Identifier_info{symbol_table::Identifier_kind::Func_name};

            scope->ids_[v_kodirovku16_id][v_kodirovku16_func_name].insert(node);

            return node;
        }

        auto build_module_name(std::map<std::u32string, std::size_t>& indices_of_standard_identifiers)
        {
            const fst_level_scanner::Fst_lvl_token strings_id   = {{{1, 7 }, {1, 12 }},
                                                                   id_lexeme(indices_of_standard_identifiers[U"Строки"])};
            const fst_level_scanner::Fst_lvl_token encodings_id = {{{1, 15 }, {1, 23 }},
                                                                   id_lexeme(indices_of_standard_identifiers[U"Кодировки"])};

            std::vector<fst_level_scanner::Fst_lvl_token> components {strings_id, encodings_id};

            return std::make_shared<ast::Qualified_id_node>(components);
        }
    };

    std::shared_ptr<ast::Module> create_module(const std::shared_ptr<Char_trie>&      strs_trie,
                                               const std::shared_ptr<Char_trie>&      ids_trie,
                                               std::map<std::u32string, std::size_t>& indices_of_standard_identifiers)
    {
        std::shared_ptr<ast::Module> result;

        auto sym_table = std::make_shared<symbol_table::Symbol_table>(strs_trie, ids_trie);
        auto scope     = sym_table->get_current_scope();

        const auto kodirovki_types_section = details::build_types_section(indices_of_standard_identifiers, scope);
        const auto iz_kodirovki8_func      = details::build_iz_kodirovki8_function(indices_of_standard_identifiers, scope);
        const auto iz_kodirovki16_func     = details::build_iz_kodirovki16_function(indices_of_standard_identifiers, scope);
        const auto v_kodirovku8_func       = details::build_v_kodirovku8_function(indices_of_standard_identifiers, scope);
        const auto v_kodirovku16_func      = details::build_v_kodirovku16_function(indices_of_standard_identifiers, scope);

        std::vector<std::shared_ptr<ast::Stmt_node>> module_body {kodirovki_types_section, iz_kodirovki8_func, iz_kodirovki16_func, v_kodirovku8_func, v_kodirovku16_func};

        const auto body                    = std::make_shared<ast::Block_node>(module_body);
        const auto used_modules            = std::make_shared<ast::Used_modules_node>();
        const auto name                    = details::build_module_name(indices_of_standard_identifiers);

        const auto ast_root                = std::make_shared<ast::Module_node>(name, used_modules, body);

        result = std::make_shared<ast::Module>(strs_trie, ids_trie, ast_root, sym_table);

        return result;
    }
};
