/*
    File:    enum_type_node.cpp
    Created: 25 August 2021 at 18:50 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/enum_type_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"
#include "../../tries/include/idx_to_string.hpp"

namespace{
    const char* enum_type_fmt = R"~({0}ENUM_TYPE
{0}{{
{1}NAME: {2}
{1}COMPONENTS:
{1}{{
{3}
{1}}}
{0}}})~";
};

namespace ast{
    NodeInfo Enum_type_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Enum_type;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Enum_type_node::to_string(const std::shared_ptr<Char_trie>&,
                                          const std::shared_ptr<Char_trie>& ids_trie,
                                          const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');
        const auto indent2_str = std::string(indents.indent_ + 2 * indents.indent_inc_, ' ');

        const auto name_str         = idx_to_string(ids_trie, name_.lexeme_.id_index_);
        const auto component_to_str = [&ids_trie, &indent2_str](const auto& component) -> std::string
                                      {
                                          return indent2_str + idx_to_string(ids_trie, component.lexeme_.id_index_);
                                      };

        const auto components_str = join(component_to_str,
                                         components_.begin(),
                                         components_.end(),
                                         std::string{"\n"});

        result = fmt::format(enum_type_fmt,
                             indent0_str,
                             indent1_str,
                             name_str,
                             components_str);
        return result;
    }
};