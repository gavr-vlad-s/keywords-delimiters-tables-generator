/*
    File:    bitscale_type_node.cpp
    Created: 25 August 2021 at 20:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/bitscale_type_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"

namespace{
    const char* array_type_fmt = R"~({0}BITSCALE_TYPE
{0}{{
{1}DIMENSIONS
{1}{{
{2}
{1}}}
{0}}})~";

    const char* dimension_fmt = R"~({0}DIMENSION {1}
{0}{{
{2}
{0}}})~";
};

namespace ast{
    NodeInfo Bitscale_type_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Bitscale_type;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Bitscale_type_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                              const std::shared_ptr<Char_trie>& ids_trie,
                                              const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');
        const auto indent2_str = std::string(indents.indent_ + 2 * indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};

        std::vector<std::pair<std::size_t, std::string>> dimensions_strs;

        std::size_t i = 0;

        for(const auto& dimension : dimensions_){
            if(dimension){
                dimensions_strs.push_back({i, dimension->to_string(strs_trie, ids_trie, nested_indent)});
            }else{
                dimensions_strs.push_back({i, std::string{}});
            }

            i++;
        }

        const auto dimensions_str = join([&indent2_str](const auto& p) -> std::string
                                    {
                                        const auto& [idx, dim_str] = p;
                                        return fmt::format(dimension_fmt, indent2_str, idx, dim_str);
                                    },
                                    dimensions_strs.begin(),
                                    dimensions_strs.end(),
                                    std::string{"\n"});

        result = fmt::format(array_type_fmt, indent0_str, indent1_str, dimensions_str);

        return result;
    }
};