/*
    File:    alloc_bitscale_expr_node.cpp
    Created: 24 August 2021 at 18:15 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <utility>
#include "../include/alloc_bitscale_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings/include/join.hpp"

namespace{
    const char* alloc_bitscale_fmt = R"~({0}ALLOC_BITSCALE
{0}{{
{1}
{0}}})~";

    const char* dimension_fmt = R"~({0}DIMENSION {1}
{0}{{
{2}
{0}}})~";
};

namespace ast{
    NodeInfo Alloc_bitscale_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Alloc_bitscale_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Alloc_bitscale_expr_node::to_string(const std::shared_ptr<Char_trie>& strs_trie,
                                                    const std::shared_ptr<Char_trie>& ids_trie,
                                                    const Indents&                    indents) const
    {
        std::string result;

        const auto indent0_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(indents.indent_ + indents.indent_inc_, ' ');

        Indents nested_indent{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        std::vector<std::pair<std::size_t, std::string>> args_strs;

        std::size_t i = 0;

        for(const auto& operand : operands_){
            if(operand){
                args_strs.push_back({i, operand->to_string(strs_trie, ids_trie, nested_indent)});
            }else{
                args_strs.push_back({i, std::string{}});
            }

            i++;
        }

        const auto operands_str = join([&indent1_str](const auto& p) -> std::string
                                       {
                                           const auto& [idx, op_str] = p;
                                           return fmt::format(dimension_fmt, indent1_str, idx, op_str);
                                       },
                                       args_strs.begin(),
                                       args_strs.end(),
                                       std::string{"\n"});

        result = fmt::format(alloc_bitscale_fmt, indent0_str, operands_str);

        return result;
    }
};