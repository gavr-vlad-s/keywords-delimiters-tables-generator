/*
    File:    usage.hpp
    Created: 11 June 2021 at 21:32 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef USAGE_H
#define USAGE_H
void usage(const char* program_name);
#endif