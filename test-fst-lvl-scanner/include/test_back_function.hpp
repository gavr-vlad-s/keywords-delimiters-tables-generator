/*
    File:    test_back_function.hpp
    Created: 22 June 2021 at 22:10 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef TEST_BACK_FUNCTION_H
#define TEST_BACK_FUNCTION_H
namespace testing_scanner{
    void test_back_function();
};
#endif