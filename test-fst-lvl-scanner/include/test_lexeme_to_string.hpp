/*
    File:    test_lexeme_to_string.hpp
    Created: 13 June 2021 at 10:53 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef TEST_LEXEME_TO_STRING_H
#define TEST_LEXEME_TO_STRING_H
namespace testing_scanner{
    void test_lexeme_to_string();
};
#endif