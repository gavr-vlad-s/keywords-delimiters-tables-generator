/*
    File:    testing-func.hpp
    Created: 14 June 2021 at 09:01 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TESTING_FUNC_H
#define TESTING_FUNC_H
#   include <memory>
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace testing_scanner{
    void test_func(const std::shared_ptr<fst_level_scanner::Scanner>& arkonasc);
};
#endif