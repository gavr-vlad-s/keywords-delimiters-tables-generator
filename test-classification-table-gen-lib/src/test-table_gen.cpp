/*
    File:    test-table_gen.cpp
    Created: 30 May 2021 at 12:45 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../../classification-table-gen-lib/include/table_gen.hpp"
#include <cstdint>
#include <iostream>
#include <map>
#include <string>

static std::u32string spaces_str()
{
    std::u32string s;
    for(char32_t c = 1; c <= U' '; c++){
        s += c;
    }
    return s;
}

static const char32_t* id_begin_chars          =
    U"_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    U"ЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуф"
    U"хцчшщъыьэюяѐёђѓєѕіїјљњћќўџѠѡѢѣѤѥѦѧѨѩѪѫѬѭѮѯѰѱѲѳѴѵѶѷѸѹѺѻѼѽѾѿҀҁҊҋҌҍҎҏҐґ"
    U"ҒғҔҕҖҗҘҙҚқҜҝҞҟҠҡҢңҤҥҦҧҨҩҪҫҬҭҮүҰұҲҳҴҵҶҷҸҹҺһҼҽҾҿӀӁӂӀӃӄӅӆӇӈӉӊӋӌӍӎӏӐӑӒӓӔӕ"
    U"ӖӗӘәӚӛӜӝӞӟӠӡӢӣӤӥӦӧӨөӪӫӬӭӮӯӰӱӲӳӴӵӶӷӸӹӺӻӼӽӾӿ";
static const char32_t* id_body_chars           =
    U"_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    U"ЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуф"
    U"хцчшщъыьэюяѐёђѓєѕіїјљњћќўџѠѡѢѣѤѥѦѧѨѩѪѫѬѭѮѯѰѱѲѳѴѵѶѷѸѹѺѻѼѽѾѿҀҁҊҋҌҍҎҏҐґ"
    U"ҒғҔҕҖҗҘҙҚқҜҝҞҟҠҡҢңҤҥҦҧҨҩҪҫҬҭҮүҰұҲҳҴҵҶҷҸҹҺһҼҽҾҿӀӁӂӀӃӄӅӆӇӈӉӊӋӌӍӎӏӐӑӒӓӔӕ"
    U"ӖӗӘәӚӛӜӝӞӟӠӡӢӣӤӥӦӧӨөӪӫӬӭӮӯӰӱӲӳӴӵӶӷӸӹӺӻӼӽӾӿ";
static const char32_t* keyword_begin_chars     =
    U"бвгдезиклмнопрстфцчшэ";
static const char32_t* delim_begin_chars       =
    U"[]{}()!~^@.:;,=#<>+-*/%?|&\\";
static const char32_t* double_quote_chars      =
    U"\"";
static const char32_t* single_quote_chars      =
    U"\'";
static const char32_t* letters_Xx_chars        =
    U"Xx";
static const char32_t* letters_Bb_chars        =
    U"Bb";
static const char32_t* letters_Oo_chars        =
    U"Oo";
static const char32_t* dollar_chars            =
    U"$";
static const char32_t* hex_digits_chars        =
    U"0123456789ABCDEFabcdef";
static const char32_t* oct_digits_chars        =
    U"01234567";
static const char32_t* bin_digits_chars        =
    U"01";
static const char32_t* dec_digits_chars        =
    U"0123456789";
static const char32_t* zero_chars              =
    U"0";
static const char32_t* letters_Ee_chars        =
    U"Ee";
static const char32_t* plus_minus_chars        =
    U"+-";
static const char32_t* precision_letters_chars =
    U"fdxq";
static const char32_t* digits_1_to_9_chars     =
    U"123456789";
static const char32_t* point_chars             =
    U".";
static const char32_t* quat_suffix_chars       =
    U"ijk";




enum class MyCategory : uint64_t{
    Spaces,           Id_begin,        Id_body,
    Keyword_begin,    Delimiter_begin, Double_quote,
    Letters_Xx,       Letters_Bb,      Letters_Oo,
    Single_quote,     Dollar,          Hex_digit,
    Oct_digit,        Bin_digit,       Dec_digit,
    Zero,             Letters_Ee,      Plus_minus,
    Precision_letter, Digits_1_to_9,   Point,
    Quat_suffix,      Other
};


static const std::map<std::u32string, Category_info> categories = {
    {U"Spaces",           {static_cast<uint64_t>(MyCategory::Spaces),           spaces_str()            }},
    {U"Id_begin",         {static_cast<uint64_t>(MyCategory::Id_begin),         id_begin_chars          }},
    {U"Id_body",          {static_cast<uint64_t>(MyCategory::Id_body),          id_body_chars           }},
    {U"Keyword_begin",    {static_cast<uint64_t>(MyCategory::Keyword_begin),    keyword_begin_chars     }},
    {U"Delimiter_begin",  {static_cast<uint64_t>(MyCategory::Delimiter_begin),  delim_begin_chars       }},
    {U"Double_quote",     {static_cast<uint64_t>(MyCategory::Double_quote),     double_quote_chars      }},
    {U"Letters_Xx",       {static_cast<uint64_t>(MyCategory::Letters_Xx),       letters_Xx_chars        }},
    {U"Letters_Bb",       {static_cast<uint64_t>(MyCategory::Letters_Bb),       letters_Bb_chars        }},
    {U"Letters_Oo",       {static_cast<uint64_t>(MyCategory::Letters_Oo),       letters_Oo_chars        }},
    {U"Single_quote",     {static_cast<uint64_t>(MyCategory::Single_quote),     single_quote_chars      }},
    {U"Dollar",           {static_cast<uint64_t>(MyCategory::Dollar),           dollar_chars            }},
    {U"Hex_digit",        {static_cast<uint64_t>(MyCategory::Hex_digit),        hex_digits_chars        }},
    {U"Oct_digit",        {static_cast<uint64_t>(MyCategory::Oct_digit),        oct_digits_chars        }},
    {U"Bin_digit",        {static_cast<uint64_t>(MyCategory::Bin_digit),        bin_digits_chars        }},
    {U"Dec_digit",        {static_cast<uint64_t>(MyCategory::Dec_digit),        dec_digits_chars        }},
    {U"Zero",             {static_cast<uint64_t>(MyCategory::Zero),             zero_chars              }},
    {U"Letters_Ee",       {static_cast<uint64_t>(MyCategory::Letters_Ee),       letters_Ee_chars        }},
    {U"Plus_minus",       {static_cast<uint64_t>(MyCategory::Plus_minus),       plus_minus_chars        }},
    {U"Precision_letter", {static_cast<uint64_t>(MyCategory::Precision_letter), precision_letters_chars}},
    {U"Digits_1_to_9",    {static_cast<uint64_t>(MyCategory::Digits_1_to_9),    digits_1_to_9_chars    }},
    {U"Point",            {static_cast<uint64_t>(MyCategory::Point),            point_chars            }},
    {U"Quat_suffix",      {static_cast<uint64_t>(MyCategory::Quat_suffix),      quat_suffix_chars      }},
};

static const Info_for_category_header header_info = {
    U"fst_level_scanner",
    U"Гаврилов Владимир Сергеевич",
    {
        U"vladimir.s.gavrilov@gmail.com",
        U"gavrilov.vladimir.s@mail.ru",
        U"gavvs1977@yandex.ru"
    },
    U"GPLv3"
};


int main()
{
    const auto table      = build_category_table(categories);
    std::cout << "Categories table:\n" << table << "\n\n";
    const auto cat_header = build_category_include(categories, header_info);
    std::cout << "Categories header:\n" << cat_header << "\n\n";
    return 0;
}