/*
    File:    delim_trans_table.hpp
    Created: 23 February 2022 at 12:49 UTC
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef DELIM_TRANS_TABLE_H
#define DELIM_TRANS_TABLE_H
#include "../include/lexeme.hpp"
namespace fst_level_scanner {
    class Delim_trans_table {
    public:
        static constexpr int there_is_no_transition = -1;

        int init_state(char32_t c) const;
        int next_state(int state, char32_t c) const;
        LexemeInfo lexeme_for_state(int state) const;
    };
};
#endif