/*
    File:    lexeme_to_str.hpp
    Created: 06 June 2021 at 12:41 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef LEXEME_TO_STR_H
#define LEXEME_TO_STR_H
#   include <string>
#   include <memory>
#   include "../include/lexeme.hpp"
#   include "../../tries/include/char_trie.hpp"
namespace fst_level_scanner{
    std::string to_string(const LexemeInfo&                 li,
                          const std::shared_ptr<Char_trie>& ids_trie,
                          const std::shared_ptr<Char_trie>& strs_trie);
};
#endif