/*
    File:    keyword_kind.hpp
    Created: 23 February 2022 at 12:49 UTC
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef KEYWORD_CODE_H
#define KEYWORD_CODE_H
#   include <cstdint>
namespace fst_level_scanner {
    enum Keyword_kind : uint8_t{
        Kw_abstraktnaja, Kw_bajt,         Kw_bezzn,           Kw_bezzn8,          
        Kw_bezzn16,      Kw_bezzn32,      Kw_bezzn64,         Kw_bezzn128,        
        Kw_bolshe,       Kw_bolshoe,      Kw_v,               Kw_vechno,          
        Kw_veshch,       Kw_veshch32,     Kw_veshch64,        Kw_veshch80,        
        Kw_veshch128,    Kw_vozvrat,      Kw_vyberi,          Kw_vydeli,          
        Kw_vyjdi,        Kw_golovnaya,    Kw_diapazon,        Kw_dlya,            
        Kw_esli,         Kw_iz,           Kw_inache,          Kw_ines,            
        Kw_ispolzuet,    Kw_istina,       Kw_kak,             Kw_kategorija,      
        Kw_kvat,         Kw_kvat32,       Kw_kvat64,          Kw_kvat80,          
        Kw_kvat128,      Kw_kompl,        Kw_kompl32,         Kw_kompl64,         
        Kw_kompl80,      Kw_kompl128,     Kw_konst,           Kw_konstanty,       
        Kw_log,          Kw_log8,         Kw_log16,           Kw_log32,           
        Kw_log64,        Kw_lozh,         Kw_ljambda,         Kw_malenkoe,        
        Kw_massiv,       Kw_menshe,       Kw_meta,            Kw_modul,           
        Kw_nastrojka,    Kw_nichto,       Kw_obobshchenie,    Kw_obobshchjonnaja, 
        Kw_operatsija,   Kw_osvobodi,     Kw_paket,           Kw_paketnaja,       
        Kw_pauk,         Kw_perem,        Kw_perechisl_mnozh, Kw_perechislenie,   
        Kw_povtorjaj,    Kw_poka,         Kw_pokuda,          Kw_porjadok,        
        Kw_porjadok8,    Kw_porjadok16,   Kw_porjadok32,      Kw_porjadok64,      
        Kw_postfiksnaja, Kw_preobrazuj,   Kw_prefiksnaja,     Kw_prodolzhi,       
        Kw_psevdonim,    Kw_pusto,        Kw_ravno,           Kw_razbor,          
        Kw_rassmatrivaj, Kw_realizatsija, Kw_realizuj,        Kw_samo,            
        Kw_sbros,        Kw_simv,         Kw_simv8,           Kw_simv16,          
        Kw_simv32,       Kw_sravni_s,     Kw_ssylka,          Kw_stroka,          
        Kw_stroka8,      Kw_stroka16,     Kw_stroka32,        Kw_struktura,       
        Kw_tip,          Kw_tipy,         Kw_to,              Kw_funktsija,       
        Kw_chistaja,     Kw_tsel,         Kw_tsel8,           Kw_tsel16,          
        Kw_tsel32,       Kw_tsel64,       Kw_tsel128,         Kw_shkala,          
        Kw_shkalu,       Kw_ekviv,        Kw_eksport,         Kw_element
    };
};
#endif
