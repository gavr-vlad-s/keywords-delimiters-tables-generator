/*
    File:    category.hpp
    Created: 23 February 2022 at 12:49 UTC
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef CATEGORY_H
#define CATEGORY_H
#   include <cstdint>
namespace fst_level_scanner {
    enum class Category : std::uint64_t {
        Spaces,            Id_begin,         Id_body,       
        Keyword_begin,     Delimiter_begin,  Double_quote,  
        Letters_Xx,        Letters_Bb,       Letters_Oo,    
        Single_quote,      Dollar,           Hex_digit,     
        Oct_digit,         Bin_digit,        Dec_digit,     
        Zero,              Letters_Ee,       Plus_minus,    
        Precision_letter,  Digits_1_to_9,    Point,         
        Quat_suffix,       Other
    };
};
#endif