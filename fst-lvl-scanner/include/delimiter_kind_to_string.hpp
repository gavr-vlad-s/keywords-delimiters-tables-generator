/*
    File:    delimiter_kind_to_string.hpp
    Created: 23 February 2022 at 12:49 UTC
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef DELIMITER_KIND_TO_STRING_CONVERSION_H
#define DELIMITER_KIND_TO_STRING_CONVERSION_H
#   include <string>
#   include "../include/delimiter_kind.hpp"
namespace fst_level_scanner {
    std::string delimiter_kind_to_string(Delimiter_kind code);
};
#endif