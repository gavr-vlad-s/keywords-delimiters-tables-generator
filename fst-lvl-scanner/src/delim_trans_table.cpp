/*
    File:    delim_trans_table.cpp
    Created: 23 February 2022 at 12:49 UTC
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstddef>
#include <cstdint>
#include <iterator>
#include "../include/delim_trans_table.hpp"
#include "../include/get_init_state.hpp"
#include "../include/state_for_char.hpp"
#include "../include/trans-elem.hpp"
#include "../include/search_char.hpp"

namespace fst_level_scanner {
    static const State_for_char init_table[] = {
        {0  , U'!'},  {12 , U'#'},  {16 , U'%'}, {20 , U'&'}, 
        {26 , U'('},  {28 , U')'},  {29 , U'*'}, {36 , U'+'}, 
        {40 , U','},  {41 , U'-'},  {46 , U'.'}, {52 , U'/'}, 
        {57 , U':'},  {65 , U';'},  {66 , U'<'}, {93 , U'='}, 
        {95 , U'>'},  {101, U'\?'}, {103, U'@'}, {104, U'['}, 
        {106, U'\\'}, {108, U']'},  {109, U'^'}, {113, U'{'}, 
        {118, U'|'},  {127, U'}'},  {128, U'~'}
    };

    static constexpr std::size_t init_table_size = std::size(init_table);

    static const char32_t trans_strs[] = {
        U'&', U'=', U'|', U'\0',
        U'&', U'\0',
        U'\0',
        U'|', U'\0',
        U'.', U'=', U'\0',
        U'=', U'\0',
        U'#', U'>', U'\0',
        U'>', U'\0',
        U'&', U'=', U'\0',
        U':', U'\0',
        U'*', U'/', U'=', U'\0',
        U'+', U'=', U'\0',
        U'<', U'\0',
        U'-', U'=', U'>', U'\0',
        U'.', U'|', U'\0',
        U'.', U'\0',
        U'*', U'=', U'\\', U'\0',
        U')', U':', U'=', U'>', U']', U'|', U'}', U'\0',
        U'!', U'#', U'&', U'+', U'-', U':', U'<', U'=', U'\?', U'|', U'\0',
        U'!', U'>', U'\0',
        U'&', U'>', U'\0',
        U'#', U'<', U'=', U'\0',
        U'>', U'\?', U'\0',
        U'=', U'>', U'\0',
        U'=', U'^', U'\0',
        U'.', U':', U'\0',
        U'}', U'\0',
        U'#', U':', U'=', U'|', U'\0',
        U'&', U'|', U'\0'
    };

    static const trans_table::Trans_elem trans_table[] = {
        {0, {Lexem_kind::Delimiter, Delimiter_kind::Logical_not}, 1},
        {4, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Logical_and_not}, 4},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::NEQ}, 0},
        {7, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Logical_or_not}, 5},
        {9, {Lexem_kind::Delimiter, Delimiter_kind::Logical_and_not}, 6},
        {9, {Lexem_kind::Delimiter, Delimiter_kind::Logical_or_not}, 8},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Logical_and_not_full}, 10},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Logical_and_not_assign}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Logical_or_not_full}, 11},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Logical_or_not_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Logical_and_not_full_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Logical_or_not_full_assign}, 0},
        {14, {Lexem_kind::Delimiter, Delimiter_kind::Sharp}, 13},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Data_size}, 0},
        {17, {Lexem_kind::Delimiter, Delimiter_kind::Maximal_value_in_collection}, 15},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Maximal_value_of_type}, 0},
        {9, {Lexem_kind::Delimiter, Delimiter_kind::Remainder}, 17},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Float_remainder}, 19},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Remainder_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Float_remainder_assign}, 0},
        {19, {Lexem_kind::Delimiter, Delimiter_kind::Bitwise_and}, 21},
        {9, {Lexem_kind::Delimiter, Delimiter_kind::Logical_and}, 23},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Bitwise_and_assign}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Logical_and_full}, 25},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Logical_and_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Logical_and_full_assign}, 0},
        {22, {Lexem_kind::Delimiter, Delimiter_kind::Round_bracket_opened}, 27},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Tuple_begin}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Round_bracket_closed}, 0},
        {24, {Lexem_kind::Delimiter, Delimiter_kind::Mul}, 30},
        {9, {Lexem_kind::Delimiter, Delimiter_kind::Power}, 33},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Comment_end}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Mul_assign}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Float_power}, 35},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Power_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Float_power_assign}, 0},
        {28, {Lexem_kind::Delimiter, Delimiter_kind::Plus}, 37},
        {31, {Lexem_kind::Delimiter, Delimiter_kind::Inc}, 39},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Plus_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Inc_with_wrap}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Comma}, 0},
        {33, {Lexem_kind::Delimiter, Delimiter_kind::Minus}, 42},
        {31, {Lexem_kind::Delimiter, Delimiter_kind::Dec}, 45},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Minus_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Right_arrow}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Dec_with_wrap}, 0},
        {37, {Lexem_kind::Delimiter, Delimiter_kind::Dot}, 47},
        {40, {Lexem_kind::Delimiter, Delimiter_kind::Range}, 49},
        {40, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Algebraic_separator}, 50},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Range_excluded_end}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Algebraic_separator}, 51},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Algebraic_separator_assign}, 0},
        {42, {Lexem_kind::Delimiter, Delimiter_kind::Div}, 53},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Comment_begin}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Div_assign}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Symmetric_difference}, 56},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Symmetric_difference_assign}, 0},
        {46, {Lexem_kind::Delimiter, Delimiter_kind::Colon}, 58},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Tuple_end}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Scope_resolution}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Copy}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Meta_bracket_closed}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Array_literal_end}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Bitscale_literal_end}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Set_literal_end}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Semicolon}, 0},
        {54, {Lexem_kind::Delimiter, Delimiter_kind::LT}, 67},
        {65, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Common_template_type}, 77},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Minimal_value_in_collection}, 0},
        {68, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Data_address}, 79},
        {17, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Type_add}, 81},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Left_arrow}, 0},
        {17, {Lexem_kind::Delimiter, Delimiter_kind::Meta_bracket_opened}, 82},
        {71, {Lexem_kind::Delimiter, Delimiter_kind::Left_shift}, 83},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::LEQ}, 0},
        {75, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Get_expr_type}, 86},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Label_prefix}, 0},
        {17, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Common_template_type}, 88},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Template_type}, 0},
        {17, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Data_address}, 89},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Address}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Type_add}, 90},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Common_type}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Minimal_value_of_type}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Cyclic_left_shift}, 91},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Left_shift_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Get_expr_type}, 0},
        {17, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Get_elem_type}, 92},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Common_template_type}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Data_address}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Type_add_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Cyclic_left_shift_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Get_elem_type}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Assign}, 94},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::EQ}, 0},
        {78, {Lexem_kind::Delimiter, Delimiter_kind::GT}, 96},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::GEQ}, 0},
        {78, {Lexem_kind::Delimiter, Delimiter_kind::Right_shift}, 98},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Right_shift_assign}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Cyclic_right_shift}, 100},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Cyclic_right_shift_assign}, 0},
        {40, {Lexem_kind::Delimiter, Delimiter_kind::Cond_op}, 102},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Cond_op_full}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::At}, 0},
        {22, {Lexem_kind::Delimiter, Delimiter_kind::Square_bracket_opened}, 105},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Array_literal_begin}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Set_difference}, 107},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Set_difference_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Square_bracket_closed}, 0},
        {81, {Lexem_kind::Delimiter, Delimiter_kind::Bitwise_xor}, 110},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Bitwise_xor_assign}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Logical_xor}, 112},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Logical_xor_assign}, 0},
        {84, {Lexem_kind::Delimiter, Delimiter_kind::Figure_bracket_opened}, 114},
        {40, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Pattern}, 116},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Set_literal_begin}, 0},
        {87, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Pattern}, 117},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Pattern}, 0},
        {89, {Lexem_kind::Delimiter, Delimiter_kind::Bitwise_or}, 119},
        {7, {Lexem_kind::Delimiter, Delimiter_kind::Maybe_Cardinality}, 123},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Bitscale_literal_begin}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Bitwise_or_assign}, 0},
        {9, {Lexem_kind::Delimiter, Delimiter_kind::Logical_or}, 124},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Cardinality}, 0},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Logical_or_full}, 126},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Logical_or_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Logical_or_full_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Figure_bracket_closed}, 0},
        {94, {Lexem_kind::Delimiter, Delimiter_kind::Bitwise_not}, 129},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Bitwise_and_not}, 131},
        {12, {Lexem_kind::Delimiter, Delimiter_kind::Bitwise_or_not}, 132},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Bitwise_and_not_assign}, 0},
        {6, {Lexem_kind::Delimiter, Delimiter_kind::Bitwise_or_not_assign}, 0}
    };

    int Delim_trans_table::init_state(char32_t c) const
    {
        return get_init_state(c, init_table, init_table_size);
    }

    int Delim_trans_table::next_state(int state, char32_t c) const
    {
        int             result           = Delim_trans_table::there_is_no_transition;
        auto            elem             = trans_table[state];
        const char32_t* transition_chars = trans_strs + elem.offset_of_first_transition_char_;
        int             y                = search_char(c, transition_chars);
        if(y != THERE_IS_NO_CHAR){
            result = elem.first_state_ + y;
        }
        return result;
    }

    LexemeInfo Delim_trans_table::lexeme_for_state(int state) const
    {
        LexemeInfo result;
        result.code_     = trans_table[state].code_;
        result.id_index_ = 0;
        return result;
    }
};