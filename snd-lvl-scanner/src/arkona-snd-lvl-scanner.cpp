/*
    File:    arkona-snd-lvl-scanner.cpp
    Created: 03 July 2021 at 20:12 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include <map>
#include "../include/arkona-snd-lvl-scanner.hpp"
#include "../include/lexeme_to_str.hpp"

namespace{
    using namespace snd_level_scanner;

    Snd_lvl_token convert_token(const Low_lvl_token& low_lvl_token)
    {
        Snd_lvl_token result;

        auto          low_lvl_lexeme  = low_lvl_token.lexeme_;
        auto          low_lvl_code    = low_lvl_lexeme.code_;
        uint8_t       low_lvl_subkind = low_lvl_code.subkind_;

        result.range_= low_lvl_token.range_;
        switch(low_lvl_code.kind_){
            case fst_level_scanner::Lexem_kind::Nothing:
                result.lexeme_.code_.kind_                    = Lexem_kind::Nothing;
                break;
            case fst_level_scanner::Lexem_kind::UnknownLexem:
                result.lexeme_.code_.kind_                    = Lexem_kind::UnknownLexem;
                break;
            case fst_level_scanner::Lexem_kind::Keyword:
                result.lexeme_.code_.kind_                    = Lexem_kind::Keyword;
                result.lexeme_.code_.subkind_                 = low_lvl_subkind;
                break;
            case fst_level_scanner::Lexem_kind::Delimiter:
                result.lexeme_.code_.kind_                    = Lexem_kind::Keyword;
                result.lexeme_.code_.subkind_                 = low_lvl_subkind;
                switch(static_cast<fst_level_scanner::Delimiter_kind>(low_lvl_subkind)){
                    case fst_level_scanner::Delimiter_kind::Maybe_Algebraic_separator:
                        result.lexeme_.code_.subkind_ = static_cast<uint8_t>(Delimiter_kind::Algebraic_separator);
                        break;
                    case fst_level_scanner::Delimiter_kind::Maybe_Cardinality:
                        result.lexeme_.code_.subkind_ = static_cast<uint8_t>(Delimiter_kind::Cardinality);
                        break;
                    case fst_level_scanner::Delimiter_kind::Maybe_Common_template_type:
                        result.lexeme_.code_.subkind_ = static_cast<uint8_t>(Delimiter_kind::Common_template_type);
                        break;
                    case fst_level_scanner::Delimiter_kind::Maybe_Data_address:
                        result.lexeme_.code_.subkind_ = static_cast<uint8_t>(Delimiter_kind::Data_address);
                        break;
                    case fst_level_scanner::Delimiter_kind::Maybe_Get_elem_type:
                        result.lexeme_.code_.subkind_ = static_cast<uint8_t>(Delimiter_kind::Get_elem_type);
                        break;
                    case fst_level_scanner::Delimiter_kind::Maybe_Get_expr_type:
                        result.lexeme_.code_.subkind_ = static_cast<uint8_t>(Delimiter_kind::Get_expr_type);
                        break;
                    case fst_level_scanner::Delimiter_kind::Maybe_Logical_and_not:
                        result.lexeme_.code_.subkind_ = static_cast<uint8_t>(Delimiter_kind::Logical_and_not);
                        break;
                    case fst_level_scanner::Delimiter_kind::Maybe_Logical_or_not:
                        result.lexeme_.code_.subkind_ = static_cast<uint8_t>(Delimiter_kind::Logical_or_not);
                        break;
                    case fst_level_scanner::Delimiter_kind::Maybe_Pattern:
                        result.lexeme_.code_.subkind_ = static_cast<uint8_t>(Delimiter_kind::Pattern);
                        break;
                    case fst_level_scanner::Delimiter_kind::Maybe_Type_add:
                        result.lexeme_.code_.subkind_ = static_cast<uint8_t>(Delimiter_kind::Type_add);
                        break;
                    default:
                        ;
                }
                break;
            case fst_level_scanner::Lexem_kind::Integer:
                result.lexeme_.code_.kind_                    = Lexem_kind::Integer;
                result.lexeme_.int_val_                       = low_lvl_lexeme.int_val_;
                break;
            case fst_level_scanner::Lexem_kind::Float:
                result.lexeme_.code_.kind_                    = Lexem_kind::Float;
                result.lexeme_.code_.subkind_                 = low_lvl_subkind;
                result.lexeme_.float_val_                     = low_lvl_lexeme.float_val_;
                break;
            case fst_level_scanner::Lexem_kind::Complex:
                result.lexeme_.code_.kind_                    = Lexem_kind::Complex;
                result.lexeme_.code_.subkind_                 = low_lvl_subkind;
                result.lexeme_.complex_val_                   = low_lvl_lexeme.complex_val_;
                break;
            case fst_level_scanner::Lexem_kind::Quat:
                result.lexeme_.code_.kind_                    = Lexem_kind::Quat;
                result.lexeme_.code_.subkind_                 = low_lvl_subkind;
                result.lexeme_.quat_val_                      = low_lvl_lexeme.quat_val_;
                break;
            case fst_level_scanner::Lexem_kind::Char:
                result.lexeme_.code_.kind_                    = Lexem_kind::Char;
                result.lexeme_.code_.subkind_                 = static_cast<uint8_t>(Char_kind::Char32);
                result.lexeme_.char_val_                      = low_lvl_lexeme.char_val_;
                break;
            default:
                ;
        }
        return result;
    }

    struct Encodings{
        Char_kind   char_encoding_;
        String_kind str_encoding_;
    };

    const std::map<std::u32string, Encodings> encodings = {
        {U"u8",  {Char_kind::Char8,  String_kind::String8 }},
        {U"u16", {Char_kind::Char16, String_kind::String16}},
        {U"u32", {Char_kind::Char32, String_kind::String32}}
    };

    template<typename... T>
    void print_diagnostic(const char* msg, T... args)
    {
        printf(msg, args...);
    }
};

namespace snd_level_scanner{
    void Scanner::get_id_or_encoded_string()
    {
        auto id_str                = ids_->get_string(low_lvl_token_.lexeme_.id_index_);
        auto it                    = encodings.find(id_str);
        token_.lexeme_.code_.kind_ = Lexem_kind::Id;
        token_.lexeme_.id_index_   = low_lvl_token_.lexeme_.id_index_;

        if(it == encodings.end()){
            return;
        }

        auto enc = it->second;
        ch_      = *(loc_->pcurrent_char_);
        if (ch_ == U'\'')
        {
            low_lvl_token_                = low_level_scanner_->current_lexeme();
            token_.lexeme_.char_val_      = low_lvl_token_.lexeme_.char_val_;
            token_.lexeme_.code_.kind_    = Lexem_kind::Char;
            token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(enc.char_encoding_);
            token_.range_.end_pos_        = low_lvl_token_.range_.end_pos_;
            lexeme_pos_                   = token_.range_;
            return;
        }

        if(ch_ != U'\"' && ch_ != U'$'){
            return;
        }

        get_compound_string();

        if (token_.lexeme_.code_.kind_ == Lexem_kind::String)
        {
            token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(enc.str_encoding_);
        }else{
            token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(enc.char_encoding_);
        }
    }

    Scanner::Next_state_info Scanner::initial_state_proc(){
        Next_state_info result{State::Initial_state, true};
        switch(low_lvl_lexem_kind_){
            case Low_level_lexem_kind::Encoded_char:
                compound_string_buffer_ += low_lvl_token_.lexeme_.char_val_;
                last_pos_               =  low_lvl_token_.range_.end_pos_;
                break;
            case Low_level_lexem_kind::String:
                compound_string_buffer_ += strs_->get_string(low_lvl_token_.lexeme_.str_index_);
                result.next_state_      =  State::Quoted_string;
                last_pos_               =  low_lvl_token_.range_.end_pos_;
                break;
            default:
                low_level_scanner_->back();
                token_.range_.end_pos_ = last_pos_;
                if(compound_string_buffer_.length() == 1){
                    token_.lexeme_.code_.kind_    = Lexem_kind::Char;
                    token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(Char_kind::Char32);
                }else{
                    token_.lexeme_.str_index_ = strs_->insert(compound_string_buffer_);
                }
                result.work_should_be_continued_ = false;
        }
        return result;
    }

    Scanner::Next_state_info Scanner::quoted_string_proc(){
        Next_state_info result{State::Quoted_string, true};
        if(low_lvl_lexem_kind_ == Low_level_lexem_kind::Encoded_char){
            compound_string_buffer_ += low_lvl_token_.lexeme_.char_val_;
            last_pos_               =  low_lvl_token_.range_.end_pos_;
            result.next_state_      =  State::Intermediate_encoded_char;
        }else{
            low_level_scanner_->back();
            token_.range_.end_pos_           = last_pos_;
            token_.lexeme_.str_index_        = strs_->insert(compound_string_buffer_);
            result.work_should_be_continued_ = false;
        }
        return result;
    }

    Scanner::Next_state_info Scanner::intermediate_encoded_char_proc(){
        Next_state_info result{State::Intermediate_encoded_char, true};
        switch(low_lvl_lexem_kind_){
            case Low_level_lexem_kind::Encoded_char:
                compound_string_buffer_ += low_lvl_token_.lexeme_.char_val_;
                last_pos_               =  low_lvl_token_.range_.end_pos_;
                break;
            case Low_level_lexem_kind::String:
                compound_string_buffer_ += strs_->get_string(low_lvl_token_.lexeme_.str_index_);
                result.next_state_      =  State::Quoted_string;
                last_pos_               =  low_lvl_token_.range_.end_pos_;
                break;
            default:
                low_level_scanner_->back();
                token_.range_.end_pos_           = last_pos_;
                token_.lexeme_.str_index_        = strs_->insert(compound_string_buffer_);
                result.work_should_be_continued_ = false;
        }
        return result;
    }


    Scanner::State_proc Scanner::procs[] = {
        &Scanner::initial_state_proc,
        &Scanner::quoted_string_proc,
        &Scanner::intermediate_encoded_char_proc
    };


    void Scanner::get_compound_string()
    {
        compound_string_buffer_.clear();

        State current_state = State::Initial_state;

        token_.lexeme_.code_.kind_    = Lexem_kind::String;
        token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(String_kind::String32);

        while((low_lvl_lexem_kind_ = (low_lvl_lexem_code_ = (low_lvl_token_  = low_level_scanner_->current_lexeme()).lexeme_.code_).kind_) != Low_level_lexem_kind::Nothing)
        {
            auto [next_state, work_should_be_continued] = (this->*procs[static_cast<unsigned>(current_state)])();
            if(!work_should_be_continued){
                return;
            }
            current_state = next_state;
        }

        switch(current_state){
            case State::Initial_state:
                low_level_scanner_->back();
                token_.range_.end_pos_ = last_pos_;
                if(compound_string_buffer_.length() == 1){
                    token_.lexeme_.code_.kind_    = Lexem_kind::Char;
                    token_.lexeme_.code_.subkind_ = static_cast<uint8_t>(Char_kind::Char32);
                }else{
                    token_.lexeme_.str_index_ = strs_->insert(compound_string_buffer_);
                }
                break;
            case State::Quoted_string:
                low_level_scanner_->back();
                token_.range_.end_pos_            = last_pos_;
                token_.lexeme_.str_index_         = strs_->insert(compound_string_buffer_);
                break;
            case State::Intermediate_encoded_char:
                low_level_scanner_->back();
                token_.range_.end_pos_            = last_pos_;
                token_.lexeme_.str_index_         = strs_->insert(compound_string_buffer_);
                break;
            default:
                ;
        }
    }

    Snd_lvl_token Scanner::current_lexeme()
    {
        low_lvl_token_ = low_level_scanner_->current_lexeme();
        lexeme_begin_  = low_level_scanner_->lexeme_begin_ptr();

        token_.range_  = low_lvl_token_.range_;
        lexeme_pos_    = low_lvl_token_.range_;

        switch(low_lvl_token_.lexeme_.code_.kind_){
            case fst_level_scanner::Lexem_kind::Nothing:
            case fst_level_scanner::Lexem_kind::UnknownLexem:
            case fst_level_scanner::Lexem_kind::Integer:
            case fst_level_scanner::Lexem_kind::Float:
            case fst_level_scanner::Lexem_kind::Complex:
            case fst_level_scanner::Lexem_kind::Quat:
            case fst_level_scanner::Lexem_kind::Keyword:
            case fst_level_scanner::Lexem_kind::Char:
            case fst_level_scanner::Lexem_kind::Delimiter:
                token_ = convert_token(low_lvl_token_);
                break;
            case fst_level_scanner::Lexem_kind::Id:
                get_id_or_encoded_string();
                break;
            case fst_level_scanner::Lexem_kind::Encoded_char:
            case fst_level_scanner::Lexem_kind::String:
                low_level_scanner_->back();
                get_compound_string();
                break;
            default:
                ;
        }

        return token_;
    }

    std::string Scanner::lexeme_to_string(const snd_level_scanner::LexemeInfo& li)
    {
        return snd_level_scanner::to_string(li, ids_, strs_);
    }
};