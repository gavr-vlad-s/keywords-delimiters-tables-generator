/*
    File:    lexeme.cpp
    Created: 27 June 2021 at 17:08 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/


#include <cstdio>
#include <quadmath.h>
#include <string>
#include "../include/lexeme.hpp"

namespace{
    const char* kind_strs[] = {
        "Nothing ",   "UnknownLexem ", "Keyword ", "Id ",
        "Delimiter ", "Char ",         "String ",  "Integer ",
        "Float ",     "Complex ",      "Quat "
    };
};

namespace snd_level_scanner {
    bool operator==(const LexemeInfo& lhs, const LexemeInfo& rhs)
    {
        bool result = true;
        if(lhs.code_.kind_ != rhs.code_.kind_){
            printf("Kinds are not equal. Actual kinds are: lhs kind is %s and rhs kind is %s\n",
                   kind_strs[static_cast<unsigned>(lhs.code_.kind_)],
                   kind_strs[static_cast<unsigned>(rhs.code_.kind_)]);
            return false;
        }
        switch(lhs.code_.kind_){
            case Lexem_kind::Nothing: case Lexem_kind::UnknownLexem:
                break;
            case Lexem_kind::Keyword: case Lexem_kind::Delimiter:
                return lhs.code_.subkind_ == rhs.code_.subkind_;
                break;
            case Lexem_kind::Id:
                return lhs.id_index_ == rhs.id_index_;
                break;
            case Lexem_kind::String:
                return lhs.str_index_ == rhs.str_index_;
                break;
            case Lexem_kind::Integer:
                return lhs.int_val_ == rhs.int_val_;
                break;
            case Lexem_kind::Float:
                return (lhs.code_.subkind_ == rhs.code_.subkind_)       &&
                       fabsq(lhs.float_val_ - rhs.float_val_) < 1e-22q;
                break;
            case Lexem_kind::Complex:
                return (lhs.code_.subkind_ == rhs.code_.subkind_) &&
                       cabsq(lhs.complex_val_ - rhs.complex_val_ ) < FLT128_EPSILON;
                break;
            case Lexem_kind::Quat:
                return (lhs.code_.subkind_ == rhs.code_.subkind_)                           &&
                       sqrtq(quat::norm_in_square(lhs.quat_val_ - rhs.quat_val_)) < 1e-22q;
                break;
            case Lexem_kind::Char:
                return lhs.char_val_ == rhs.char_val_;
                break;
        }
        return result;
    }
};