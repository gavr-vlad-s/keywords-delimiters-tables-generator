/*
    File:    lexeme_to_str.cpp
    Created: 03 July 2021 at 12:20 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/lexeme_to_str.hpp"
#include "../../numbers/include/float128_to_string.hpp"
#include "../../numbers/include/int128_to_str.hpp"
#include "../../tries/include/idx_to_string.hpp"
#include "../../char-conv/include/char_conv.hpp"
#include "../../char-conv/include/print_char32.hpp"

namespace{
    const std::string kind_strs[] = {
        "Nothing",    "UnknownLexem",  "Keyword ", "Id ",
        "Delimiter ", "Char ",         "String ",  "Integer ",
        "Float ",     "Complex ",      "Quat "
    };

    const std::string float_subkind_strs[] = {
        "[Float32] ", "[Float64] ", "[Float80] ", "[Float128] "
    };

    const std::string complex_subkind_strs[] = {
        "[Complex32] ", "[Complex64] ", "[Complex80] ", "[Complex128] "
    };

    const std::string quat_subkind_strs[] = {
        "[Quat32] ", "[Quat64] ", "[Quat80] ", "[Quat128] "
    };

    const std::string char_kind_strs[] = {
        "[Char8] ", "[Char16] ", "[Char32] "
    };

    const std::string string_kind_strs[] = {
        "[String8] ", "[String16] ", "[String32] "
    };

    const std::string keyword_kind_strs[] = {
        "Kw_bajt",          "Kw_bezzn",        "Kw_bezzn8",       "Kw_bezzn16",
        "Kw_bezzn32",       "Kw_bezzn64",      "Kw_bezzn128",     "Kw_bolshe",
        "Kw_bolshoe",       "Kw_v",            "Kw_vechno",       "Kw_veshch",
        "Kw_veshch32",      "Kw_veshch64",     "Kw_veshch80",     "Kw_veshch128",
        "Kw_vozvrat",       "Kw_vyberi",       "Kw_vydeli",       "Kw_vyjdi",
        "Kw_golovnaya",     "Kw_diapazon",     "Kw_dlya",         "Kw_esli",
        "Kw_iz",            "Kw_inache",       "Kw_ines",         "Kw_ispolzuet",
        "Kw_istina",        "Kw_kak",          "Kw_kategorija",   "Kw_kvat",
        "Kw_kvat32",        "Kw_kvat64",       "Kw_kvat80",       "Kw_kvat128",
        "Kw_kompl",         "Kw_kompl32",      "Kw_kompl64",      "Kw_kompl80",
        "Kw_kompl128",      "Kw_konst",        "Kw_log",          "Kw_log8",
        "Kw_log16",         "Kw_log32",        "Kw_log64",        "Kw_lozh",
        "Kw_ljambda",       "Kw_malenkoe",     "Kw_massiv",       "Kw_menshe",
        "Kw_meta",          "Kw_modul",        "Kw_nastrojka",    "Kw_nichto",
        "Kw_obobshchenie",  "Kw_operatsija",   "Kw_osvobodi",     "Kw_paket",
        "Kw_paketnaja",     "Kw_pauk",         "Kw_perem",        "Kw_perechisl_mnozh",
        "Kw_perechislenie", "Kw_povtorjaj",    "Kw_poka",         "Kw_pokuda",
        "Kw_porjadok",      "Kw_porjadok8",    "Kw_porjadok16",   "Kw_porjadok32",
        "Kw_porjadok64",    "Kw_postfiksnaja", "Kw_preobrazuj",   "Kw_prefiksnaja",
        "Kw_prodolzhi",     "Kw_psevdonim",    "Kw_pusto",        "Kw_ravno",
        "Kw_razbor",        "Kw_rassmatrivaj", "Kw_realizatsija", "Kw_realizuj",
        "Kw_samo",          "Kw_sbros",        "Kw_simv",         "Kw_simv8",
        "Kw_simv16",        "Kw_simv32",       "Kw_sravni_s",     "Kw_ssylka",
        "Kw_stroka",        "Kw_stroka8",      "Kw_stroka16",     "Kw_stroka32",
        "Kw_struktura",     "Kw_tip",          "Kw_to",           "Kw_funktsija",
        "Kw_chistaja",      "Kw_tsel",         "Kw_tsel8",        "Kw_tsel16",
        "Kw_tsel32",        "Kw_tsel64",       "Kw_tsel128",      "Kw_shkala",
        "Kw_shkalu",        "Kw_ekviv",        "Kw_eksport",      "Kw_element"
    };

    const std::string delim_subkind_strs[] = {
        "Logical_not",                "Logical_and_not",           "Logical_and_not_full",       "Logical_and_not_full_assign",
        "Logical_and_not_assign",     "NEQ",                       "Logical_or_not",             "Logical_or_not_full",
        "Logical_or_not_full_assign", "Logical_or_not_assign",     "Sharp",                      "Data_size",
        "Remainder",                  "Float_remainder",           "Float_remainder_assign",     "Remainder_assign",
        "Bitwise_and",                "Logical_and",               "Logical_and_full",           "Logical_and_full_assign",
        "Logical_and_assign",         "Bitwise_and_assign",        "Round_bracket_opened",       "Tuple_begin",
        "Round_bracket_closed",       "Mul",                       "Power",                      "Float_power",
        "Float_power_assign",         "Power_assign",              "Comment_end",                "Mul_assign",
        "Plus",                       "Inc",                       "Inc_with_wrap",              "Plus_assign",
        "Comma",                      "Minus",                     "Dec",                        "Dec_with_wrap",
        "Minus_assign",               "Right_arrow",               "Dot",                        "Range",
        "Range_excluded_end",         "Algebraic_separator",       "Algebraic_separator_assign", "Div",
        "Comment_begin",              "Div_assign",                "Symmetric_difference",       "Symmetric_difference_assign",
        "Colon",                      "Tuple_end",                 "Scope_resolution",           "Copy",
        "Meta_bracket_closed",        "Array_literal_end",         "Set_literal_end",            "Semicolon",
        "LT",                         "Common_template_type",      "Template_type",              "Data_address",
        "Address",                    "Type_add",                  "Type_add_assign",            "Left_arrow",
        "Meta_bracket_opened",        "Common_type",               "Left_shift",                 "Cyclic_left_shift",
        "Cyclic_left_shift_assign",   "Left_shift_assign",         "LEQ",                        "Get_expr_type",
        "Get_elem_type",              "Label_prefix",              "Assign",                     "EQ",
        "GT",                         "GEQ",                       "Right_shift",                "Right_shift_assign",
        "Cyclic_right_shift",         "Cyclic_right_shift_assign", "Cond_op",                    "Cond_op_full",
        "At",                         "Square_bracket_opened",     "Array_literal_begin",        "Set_difference",
        "Set_difference_assign",      "Square_bracket_closed",     "Bitwise_xor",                "Bitwise_xor_assign",
        "Logical_xor",                "Logical_xor_assign",        "Figure_bracket_opened",      "Pattern",
        "Set_literal_begin",          "Bitwise_or",                "Cardinality",                "Bitwise_or_assign",
        "Logical_or",                 "Logical_or_full",           "Logical_or_full_assign",     "Logical_or_assign",
        "Figure_bracket_closed",      "Bitwise_not",               "Bitwise_and_not",            "Bitwise_and_not_assign",
        "Bitwise_or_not",             "Bitwise_or_not_assign",
    };
};

namespace snd_level_scanner{
    std::string to_string(const LexemeInfo&                 li,
                          const std::shared_ptr<Char_trie>& ids_trie,
                          const std::shared_ptr<Char_trie>& strs_trie)
    {
        std::string result;

        auto        lc     = li.code_;
        auto        lk     = lc.kind_;
        auto        lsk    = lc.subkind_;
        result             = kind_strs[static_cast<unsigned>(lk)];

        switch(lk){
            case Lexem_kind::Nothing: case Lexem_kind::UnknownLexem:
                break;
            case Lexem_kind::Keyword:
                result += keyword_kind_strs[lsk];
                break;
            case Lexem_kind::Id:
                result += idx_to_string(ids_trie, li.id_index_);
                break;
            case Lexem_kind::Delimiter:
                result += delim_subkind_strs[lsk];
                break;
            case Lexem_kind::Char:
                result += char_kind_strs[lsk] + show_char32(li.char_val_);
                break;
            case Lexem_kind::String:
                result += string_kind_strs[lsk] + idx_to_string(strs_trie, li.str_index_);
                break;
            case Lexem_kind::Integer:
                result += ::to_string(li.int_val_);
                break;
            case Lexem_kind::Float:
                result += float_subkind_strs[lsk] + ::to_string(li.float_val_);
                break;
            case Lexem_kind::Complex:
                result += complex_subkind_strs[lsk]            +
                          ::to_string(crealq(li.complex_val_)) +
                          " + "                                +
                          ::to_string(cimagq(li.complex_val_)) +
                          "i";
                break;
            case Lexem_kind::Quat:
                result += quat_subkind_strs[lsk]       +
                          ::to_string(li.quat_val_[0]) +
                          ::to_string(li.quat_val_[1]) + "i" +
                          ::to_string(li.quat_val_[2]) + "j" +
                          ::to_string(li.quat_val_[3]) + "k";
                break;
            default:
                ;
        }

        return result;
    }
};
