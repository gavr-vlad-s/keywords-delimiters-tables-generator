/*
    File:    lexeme.hpp
    Created: 27 June 2021 at 17:04 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SND_LVL_LEXEME_H
#define SND_LVL_LEXEME_H
#   include <cstddef>
#   include <cstdint>
#   include <quadmath.h>
#   include "../../numbers/include/quaternion.hpp"
#   include "../include/keyword_kind.hpp"
#   include "../include/delimiter_kind.hpp"
namespace snd_level_scanner {
    enum class Float_kind : uint8_t{
        Float32, Float64, Float80, Float128
    };

    enum class Complex_kind : uint8_t{
        Complex32, Complex64, Complex80, Complex128
    };

    enum class Quat_kind : uint8_t{
        Quat32, Quat64, Quat80, Quat128
    };

    enum class Char_kind : uint8_t{
        Char8, Char16, Char32
    };

    enum class String_kind : uint8_t{
        String8, String16, String32
    };

    enum class Lexem_kind : uint8_t{
        Nothing,   UnknownLexem, Keyword, Id,
        Delimiter, Char,         String,  Integer,
        Float,     Complex,      Quat
    };

    struct Lexem_code{
        Lexem_kind kind_;
        uint8_t    subkind_;
    };

    struct LexemeInfo{
        union{
            unsigned __int128        int_val_;
            __float128               float_val_;
            __complex128             complex_val_;
            quat::quat_t<__float128> quat_val_;
            char32_t                 char_val_;
            std::size_t              str_index_;
            std::size_t              id_index_;
        };
        Lexem_code code_;
    };

    bool operator==(const LexemeInfo& lhs, const LexemeInfo& rhs);
};
#endif