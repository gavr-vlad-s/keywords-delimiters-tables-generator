/*
    File:    arkona-snd-lvl-scanner.hpp
    Created: 29 June 2021 at 20:38 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ARKONA_SND_LVL_SCANER_H
#define ARKONA_SND_LVL_SCANER_H
#   include <cstddef>
#   include <cstdint>
#   include <string>
#   include <memory>
#   include "../include/lexeme.hpp"
#   include "../../iscanner/include/iscanner.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"

namespace snd_level_scanner {
    using Snd_lvl_token        = iscaner::Token<snd_level_scanner::LexemeInfo>;
    using Low_lvl_token        = fst_level_scanner::Fst_lvl_token;
    using Low_level_lexem_code = fst_level_scanner::Lexem_code;
    using Low_level_lexem_kind = fst_level_scanner::Lexem_kind;

    class Scanner : public iscaner::IScaner<snd_level_scanner::LexemeInfo>{
    public:
        Scanner()               = default;
        Scanner(const Scanner&) = default;
        virtual ~Scanner()      = default;

        Scanner(const iscaner::Location_ptr& location, const Errors_and_tries& et) :
            iscaner::IScaner<snd_level_scanner::LexemeInfo>(location, et)
        {
            low_level_scanner_ = std::make_shared<fst_level_scanner::Scanner>(location, et);
        };

        iscaner::Token<snd_level_scanner::LexemeInfo> current_lexeme() override;

        std::string lexeme_to_string(const snd_level_scanner::LexemeInfo& li) override;
    private:
        std::shared_ptr<fst_level_scanner::Scanner>   low_level_scanner_;

        char32_t                                      ch_;           /* current character */

        Low_lvl_token                                 low_lvl_token_;
        Low_level_lexem_code                          low_lvl_lexem_code_;
        uint8_t                                       low_lvl_lexem_subkind_;
        Low_level_lexem_kind                          low_lvl_lexem_kind_;

        iscaner::Position                             last_pos_;

        void get_id_or_encoded_string();
        void get_compound_string();

        /**
         * A compound string of the programming language Аркона can be described using the following
         * regular expression:
         *     e*s(e+s)*e* (1)
         * where
         *     e is an encoded character
         *     s is a double quoted string literal
         *
         * If we construct the minimal DFA for this regular expression, we get the following
         * transition table:
         *
         * |-------|---|---|------------------------------|
         * | State | e | s |            Remark            |
         * |-------|---|---|------------------------------|
         * |   A   | A | C | Begin state.                 |
         * |-------|---|---|------------------------------|
         * |   C   | D |   | Final state.                 |
         * |-------|---|---|------------------------------|
         * |   D   | D | C | Final state.                 |
         * |-------|---|---|------------------------------|
         *
         * More meanigful names for states:
         *
         * |-------|--------------------------------------|
         * | State |          Meaningful name             |
         * |-------|--------------------------------------|
         * |   A   | Initial_state                        |
         * |-------|--------------------------------------|
         * |   C   | Quoted_string                        |
         * |-------|--------------------------------------|
         * |   D   | Intermediate_encoded_char            |
         * |-------|--------------------------------------|
         */

        std::u32string compound_string_buffer_;

        enum State{
            Initial_state, Quoted_string, Intermediate_encoded_char
        };

        struct Next_state_info{
            State next_state_;
            bool  work_should_be_continued_;
        };

        using State_proc = Next_state_info (Scanner::*)();

        Next_state_info initial_state_proc();
        Next_state_info quoted_string_proc();
        Next_state_info intermediate_encoded_char_proc();

        static State_proc procs[];
    };
};
#endif