/*
    File:    delimiter_kind.hpp
    Created: 27 June 2021 at 17:01 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SND_LVL_DELIMITER_CODE_H
#define SND_LVL_DELIMITER_CODE_H
#   include <cstdint>
namespace snd_level_scanner {
    enum Delimiter_kind : uint8_t{
        Logical_not,                Logical_and_not,           Logical_and_not_full,       Logical_and_not_full_assign,
        Logical_and_not_assign,     NEQ,                       Logical_or_not,             Logical_or_not_full,
        Logical_or_not_full_assign, Logical_or_not_assign,     Sharp,                      Data_size,
        Remainder,                  Float_remainder,           Float_remainder_assign,     Remainder_assign,
        Bitwise_and,                Logical_and,               Logical_and_full,           Logical_and_full_assign,
        Logical_and_assign,         Bitwise_and_assign,        Round_bracket_opened,       Tuple_begin,
        Round_bracket_closed,       Mul,                       Power,                      Float_power,
        Float_power_assign,         Power_assign,              Comment_end,                Mul_assign,
        Plus,                       Inc,                       Inc_with_wrap,              Plus_assign,
        Comma,                      Minus,                     Dec,                        Dec_with_wrap,
        Minus_assign,               Right_arrow,               Dot,                        Range,
        Range_excluded_end,         Algebraic_separator,       Algebraic_separator_assign, Div,
        Comment_begin,              Div_assign,                Symmetric_difference,       Symmetric_difference_assign,
        Colon,                      Tuple_end,                 Scope_resolution,           Copy,
        Meta_bracket_closed,        Array_literal_end,         Set_literal_end,            Semicolon,
        LT,                         Common_template_type,      Template_type,              Data_address,
        Address,                    Type_add,                  Type_add_assign,            Left_arrow,
        Meta_bracket_opened,        Common_type,               Left_shift,                 Cyclic_left_shift,
        Cyclic_left_shift_assign,   Left_shift_assign,         LEQ,                        Get_expr_type,
        Get_elem_type,              Label_prefix,              Assign,                     EQ,
        GT,                         GEQ,                       Right_shift,                Right_shift_assign,
        Cyclic_right_shift,         Cyclic_right_shift_assign, Cond_op,                    Cond_op_full,
        At,                         Square_bracket_opened,     Array_literal_begin,        Set_difference,
        Set_difference_assign,      Square_bracket_closed,     Bitwise_xor,                Bitwise_xor_assign,
        Logical_xor,                Logical_xor_assign,        Figure_bracket_opened,      Pattern,
        Set_literal_begin,          Bitwise_or,                Cardinality,                Bitwise_or_assign,
        Logical_or,                 Logical_or_full,           Logical_or_full_assign,     Logical_or_assign,
        Figure_bracket_closed,      Bitwise_not,               Bitwise_and_not,            Bitwise_and_not_assign,
        Bitwise_or_not,             Bitwise_or_not_assign
    };
};
#endif
