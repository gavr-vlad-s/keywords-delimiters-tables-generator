/*
    File:    lexeme_to_str.hpp
    Created: 03 July 2021 at 12:12 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef SND_LVL_LEXEME_TO_STR_H
#define SND_LVL_LEXEME_TO_STR_H
#   include <memory>
#   include <string>
#   include "../include/lexeme.hpp"
#   include "../../tries/include/char_trie.hpp"
namespace snd_level_scanner{
    std::string to_string(const LexemeInfo&                 li,
                          const std::shared_ptr<Char_trie>& ids_trie,
                          const std::shared_ptr<Char_trie>& strs_trie);
};
#endif