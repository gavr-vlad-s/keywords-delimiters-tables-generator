/*
    File:    atoken.hpp
    Created: 26 July 2021 at 20:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ATOKEN_H
#define ATOKEN_H
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
namespace arkona_parser{
    using Token = fst_level_scanner::Fst_lvl_token;
};
#endif