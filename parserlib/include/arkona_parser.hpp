/*
    File:    arkona_parser.hpp
    Created: 20 July 2021 at 19:46 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ARKONA_PARSER_H
#define ARKONA_PARSER_H
#   include <memory>
#   include "../../astlib/include/arkona_module.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
#   include "../../tries/include/errors_and_tries.hpp"
#   include "../../astlib/include/symbol_table.hpp"
namespace arkona_parser{
    class Parser final{
    public:
        Parser();
        ~Parser();

        Parser(const std::shared_ptr<fst_level_scanner::Scanner>& scanner,
               const Errors_and_tries&                            et,
               const std::shared_ptr<symbol_table::Symbol_table>& table);

        std::shared_ptr<ast::Module> parse();
    private:
        struct Impl;
        std::shared_ptr<Impl> impl_;
    };
};
#endif