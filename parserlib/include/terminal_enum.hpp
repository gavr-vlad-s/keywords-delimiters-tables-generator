/*
    File:    terminal_enum.hpp
    Created: 24 July 2021 at 21:56 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TERMINAL_ENUM_H
#define TERMINAL_ENUM_H
#   include <cstdint>
#   include <string>
namespace arkona_parser{
    enum class Terminal_category : std::uint16_t{
        End_of_text, Module,                Uses,                  Scope_resolution,
        Comma,       Figure_bracket_opened, Figure_bracket_closed, Id,
        Semicolon,   Literal
    };

    std::string to_string(Terminal_category t);
};
#endif