/*
    File:    token_to_terminal_set.hpp
    Created: 26 July 2021 at 20:46 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TOKEN_TO_TERMINAL_SET_H
#define TOKEN_TO_TERMINAL_SET_H
#   include "../../fst-lvl-scanner/include/lexeme.hpp"
#   include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
#   include "../../bitset/include/static_bitset.hpp"
#   include "../include/terminal_enum.hpp"
namespace arkona_parser{
    using Token        = fst_level_scanner::Fst_lvl_token;
    using Terminal_set = pascal_set::set<Terminal_category,
                                         Terminal_category::End_of_text,
                                         Terminal_category::Literal>;

    Terminal_set token_to_terminal_set(const Token& token);
};
#endif