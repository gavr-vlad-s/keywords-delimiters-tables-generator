/*
    File:    arkona_parser.cpp
    Created: 20 July 2021 at 20:00 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include <iterator>
#include <string>
#include <utility>
#include <vector>
#include "../include/arkona_parser.hpp"
#include "../include/token_to_terminal_set.hpp"
#include "../include/terminal_enum.hpp"
#include "../../astlib/include/all_nodes.hpp"
#include "../../strings/include/join.hpp"
#include "../../tries/include/idx_to_string.hpp"

namespace arkona_parser{
    template<typename T>
    using proc_result_t = std::pair<std::shared_ptr<T>, bool>;


    struct Parser::Impl final{
    public:
        Impl()  = default;
        ~Impl() = default;

        Impl(const std::shared_ptr<fst_level_scanner::Scanner>& scanner,
             const Errors_and_tries&                            et,
             const std::shared_ptr<symbol_table::Symbol_table>& table)
        : scanner_{scanner}
        , et_{et}
        , table_(table)
        {
        }

        std::shared_ptr<ast::Module> parse();
    private:
        std::shared_ptr<fst_level_scanner::Scanner> scanner_;
        Errors_and_tries                            et_;
        std::shared_ptr<symbol_table::Symbol_table> table_;

        Terminal_set get_current(Token& token);

        proc_result_t<ast::Block_node>        block_node_proc();
        proc_result_t<ast::Qualified_id_node> qualified_id_proc();
        proc_result_t<ast::Used_modules_node> uses_stmt_proc();
        proc_result_t<ast::Used_modules_node> used_modules_proc();
        proc_result_t<ast::Module_node>       module_node_proc();

        std::string qualified_id_components_to_string(const std::vector<Token>& components) const;
    };

    Terminal_set Parser::Impl::get_current(Token& token)
    {
        token = scanner_->current_lexeme();
        return token_to_terminal_set(token);
    }

    std::string Parser::Impl::qualified_id_components_to_string(const std::vector<Token>& components) const
    {
        std::string result;

        result = join([this](const auto& component)
                      {
                          return idx_to_string(et_.ids_trie_, component.lexeme_.id_index_);
                      },
                      std::begin(components),
                      std::end(components),
                      std::string{"::"});

        return result;
    }

    proc_result_t<ast::Module_node> Parser::Impl::module_node_proc()
    {
        /**
         * This module handles the rule
         *      module -> модуль qualified_id used_modules? block
         * Using the notation
         *      m       модуль
         *      q       qualified_id
         *      u       used_modules
         *      b       block
         * we can rewrite this rule in the form
         *      module -> mqu?b
         * Minimal DFA for the regexp
         *      mqu?b
         * has the following transition table:
         *
         * |-------|---|---|---|---|------------------|
         * | State | m | q | u | b | Remark           |
         * |-------|---|---|---|---|------------------|
         * |   A   | B |   |   |   | Start state      |
         * |-------|---|---|---|---|------------------|
         * |   B   |   | C |   |   |                  |
         * |-------|---|---|---|---|------------------|
         * |   C   |   |   |   | D |                  |
         * |-------|---|---|---|---|------------------|
         * |   D   |   |   | E |   | Final state      |
         * |-------|---|---|---|---|------------------|
         * |   E   |   |   |   | D |                  |
         * |-------|---|---|---|---|------------------|
         */

        proc_result_t<ast::Module_node> result{std::make_shared<ast::Module_node>(), true};

        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B, C, D, E
        };

        State state = State::A;

        printf("Starting while loop of start non-terminal processing...\n");

        const char* states_str[] = {
            "State::A", "State::B", "State::C", "State::D", "State::E",
        };

        while(!((set_of_terminals = get_current(current_token))[Terminal_category::End_of_text]))
        {
            printf("Current state of module_node_proc: %s\n", states_str[static_cast<unsigned>(state)]);
            auto bitset_str = pascal_set::to_string([](Terminal_category c) -> std::string
                                                    {
                                                        return arkona_parser::to_string(c);
                                                    },
                                                    set_of_terminals);
            printf("Current set of terminal categories: %s\n", bitset_str.c_str());
            switch(state){
                case State::A:
                    if(set_of_terminals[Terminal_category::Module]){
                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected keyword модуль\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        if(set_of_terminals[Terminal_category::Id]){
                            state = State::B;
                        }else{
                            result.second = false;
                            return result;
                        }
                    }
                    break;
                case State::B:
                    if(set_of_terminals[Terminal_category::Id]){
                        scanner_->back();
                        auto [q_id_ptr, flag]     = qualified_id_proc();
                        printf("q_id_ptr: %p, flag: %s\n", q_id_ptr.get(), flag ? "true" : "false");
                        result.first->name_       = q_id_ptr;
                        auto compiled_module_name = qualified_id_components_to_string(q_id_ptr->components_);

                        table_->set_current_module_name(compiled_module_name);
                        if(!flag)
                        {
                            result.second = false;
                            return result;
                        }
                        state = State::C;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected (qualified) identifier.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::C:
                    if(set_of_terminals[Terminal_category::Uses]){
                        scanner_->back();
                        auto [used_modules_ptr, flag] = used_modules_proc();
                        result.first->used_modules_   = used_modules_ptr;
                        if(!flag)
                        {
                            result.second = false;
                            return result;
                        }
                        state = State::E;
                    }else if(set_of_terminals[Terminal_category::Figure_bracket_opened]){
                        scanner_->back();
                        auto [block_ptr, flag] = block_node_proc();
                        result.first->body_    = block_ptr;
                        if(!flag)
                        {
                            result.second = false;
                            return result;
                        }
                        state = State::D;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected lists of used modules or the start of a block.\n",
                               pos.line_no_,
                               pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::D:
                    scanner_->back();
                    return result;
                    break;
                case State::E:
                    if(set_of_terminals[Terminal_category::Figure_bracket_opened]){
                        scanner_->back();
                        auto [block_ptr, flag] = block_node_proc();
                        result.first->body_    = block_ptr;
                        if(!flag)
                        {
                            result.second = false;
                            return result;
                        }
                        state = State::D;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected the start of a block.\n",
                               pos.line_no_,
                               pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                default:
                    ;
            }
        }

        if(state != State::D){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            et_.ec_->increment_number_of_errors();
            scanner_->back();
            result.second = false;
        }

        return result;
    }

    proc_result_t<ast::Block_node> Parser::Impl::block_node_proc()
    {
        /**
         * This method handles the rule
         *      block -> { }
         * Using the notation
         *      {   l
         *      }   r
         * we can rewrite this rule in the form
         *      block -> lr
         * Minimal DFA for the regexp
         *      lr
         * has the following transition table:
         *
         * |-------|---|---|------------------|
         * | State | l | r | Remark           |
         * |-------|---|---|------------------|
         * |   A   | B |   | Start state      |
         * |-------|---|---|------------------|
         * |   B   |   | C |                  |
         * |-------|---|---|------------------|
         * |   C   |   |   | Final state      |
         * |-------|---|---|------------------|
         */

        proc_result_t<ast::Block_node> result{std::make_shared<ast::Block_node>(), true};

        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B, C
        };

        State state = State::A;

        while(!((set_of_terminals = get_current(current_token))[Terminal_category::End_of_text]))
        {
            switch(state){
                case State::A:
                    if(set_of_terminals[Terminal_category::Figure_bracket_opened]){
                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected {.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::B:
                    if(set_of_terminals[Terminal_category::Figure_bracket_closed]){
                        state = State::C;
                        return result;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected }.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::C:
                    scanner_->back();
                    return result;
                    break;
                default:
                    ;
            }
        }

        if(state != State::C){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            et_.ec_->increment_number_of_errors();
            scanner_->back();
            result.second = false;
        }

        return result;
    }

    proc_result_t<ast::Used_modules_node> Parser::Impl::used_modules_proc()
    {
        /**
         * This method handles the rule
         *      used_modules -> uses_stmt(; uses_stmt)
         * Using the notation
         *      u   uses_stmt
         *      s   ;
         * we can rewrite this rule in the form
         *      used_modules -> u(su)*
         * Minimal DFA for the regexp
         *      u(su)*
         * has the following transition table:
         *
         * |-------|---|---|------------------|
         * | State | u | s | Remark           |
         * |-------|---|---|------------------|
         * |   A   | B |   | Start state      |
         * |-------|---|---|------------------|
         * |   B   |   | A | Final state      |
         * |-------|---|---|------------------|
         */

        proc_result_t<ast::Used_modules_node> result{std::make_shared<ast::Used_modules_node>(), true};

        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B
        };

        State state = State::A;

        std::vector<std::shared_ptr<ast::Qualified_id_node>> names;

        while(!((set_of_terminals = get_current(current_token))[Terminal_category::End_of_text]))
        {
            switch(state){
                case State::A:
                    if(set_of_terminals[Terminal_category::Uses]){
                        scanner_->back();
                        auto [uses_stmt_ptr, flag] = uses_stmt_proc();

                        names.insert(names.end(), std::begin(uses_stmt_ptr->names_), std::end(uses_stmt_ptr->names_));

                        if(!flag){
                            result.first->names_ = names;
                            result.second        = false;
                            return result;
                        }

                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected keyword использует.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::B:
                    if(set_of_terminals[Terminal_category::Semicolon]){
                        state = State::A;
                    }else if(set_of_terminals[Terminal_category::Uses]){
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: missing semicolon.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        state = State::A;
                    }else{
                        scanner_->back();
                        result.first->names_ = names;
                        return result;
                    }
                    break;
                default:
                    ;
            }
        }

        if(state != State::B){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            et_.ec_->increment_number_of_errors();
            scanner_->back();
            result.second = false;
        }

        return result;
    }

    proc_result_t<ast::Used_modules_node> Parser::Impl::uses_stmt_proc()
    {
        /**
         * This method handles the rule
         *      uses_stmt -> использует qualified_id(, qualified_id)*
         * Using the notation
         *      u   использует
         *      q   qualified_id
         *      c   ,
         * we can rewrite this rule in the form
         *      uses_stmt -> uq(cq)*
         * Minimal DFA for the regexp
         *      uq(cq)*
         * has the the following transition table:
         *
         * |-------|---|---|---|------------------|
         * | State | u | q | c | Remark           |
         * |-------|---|---|---|------------------|
         * |   A   | B |   |   | Start state      |
         * |-------|---|---|---|------------------|
         * |   B   |   | C |   |                  |
         * |-------|---|---|---|------------------|
         * |   C   |   |   | B | Final state      |
         * |-------|---|---|---|------------------|
         */

        proc_result_t<ast::Used_modules_node> result{std::make_shared<ast::Used_modules_node>(), true};

        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B, C
        };

        State state = State::A;

        std::vector<std::shared_ptr<ast::Qualified_id_node>> names;

        while(!((set_of_terminals = get_current(current_token))[Terminal_category::End_of_text]))
        {
            switch(state){
                case State::A:
                    if(set_of_terminals[Terminal_category::Uses]){
                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected keyword использует.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::B:
                    if(set_of_terminals[Terminal_category::Id]){
                        scanner_->back();
                        auto [q_id_ptr, flag]         = qualified_id_proc();
                        auto current_used_module_name = qualified_id_components_to_string(q_id_ptr->components_);

                        if(table_->is_current_module_name(current_used_module_name)){
                            const auto& pos = current_token.range_.begin_pos_;
                            printf("Error at %zu:%zu: imported module name %s is the current compiled module name.\n",
                                   pos.line_no_,
                                   pos.line_pos_,
                                   current_used_module_name.c_str());
                            et_.ec_->increment_number_of_errors();
                        }else if(table_->is_used_module(current_used_module_name)){
                            const auto& pos = current_token.range_.begin_pos_;
                            printf("Error at %zu:%zu: imported module name %s is already defined.\n",
                                   pos.line_no_,
                                   pos.line_pos_,
                                   current_used_module_name.c_str());
                            et_.ec_->increment_number_of_errors();
                        }else{
                            names.push_back(q_id_ptr);
                            table_->add_used_module_name(current_used_module_name);
                        }

                        if(!flag){
                            result.first->names_ = names;
                            result.second        = false;
                            return result;
                        }

                        state = State::C;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected (qualified) identifier.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.first->names_ = names;
                        result.second        = false;
                        return result;
                    }
                    break;
                case State::C:
                    if(set_of_terminals[Terminal_category::Comma]){
                        state = State::B;
                    }else if(set_of_terminals[Terminal_category::Id]){
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: missing comma.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        state = State::B;
                    }else{
                        scanner_->back();
                        result.first->names_ = names;
                        return result;
                    }
                    break;
                default:
                    ;
            }
        }

        if(state != State::C){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            et_.ec_->increment_number_of_errors();
            scanner_->back();
            result.second = false;
        }

        return result;
    }

    proc_result_t<ast::Qualified_id_node> Parser::Impl::qualified_id_proc()
    {
        /**
         * The method handles the rule
         *     qualified_id -> ид(::ид)*
         * Using the notation
         *     i    ид
         *     s    ::
         * we can rewrite this rule in the form
         *     qualified_id -> i(si)*
         * Mininal DFA for the regexp
         *     i(si)*
         * has the following transition table:
         *
         * |-------|---|---|------------------|
         * | State | i | s | Remark           |
         * |-------|---|---|------------------|
         * |   A   | B |   | Start state      |
         * |-------|---|---|------------------|
         * |   B   |   | A | Final state      |
         * |-------|---|---|------------------|
         */

        proc_result_t<ast::Qualified_id_node> result{std::make_shared<ast::Qualified_id_node>(), true};

        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B
        };

        State state = State::A;

        std::vector<Token> components;

        while(!((set_of_terminals = get_current(current_token))[Terminal_category::End_of_text]))
        {
            switch(state){
                case State::A:
                    if(set_of_terminals[Terminal_category::Id]){
                        components.push_back(current_token);
                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected identifier\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        result.second             = false;
                        result.first->components_ = components;
                        scanner_->back();
                        return result;
                    }
                    break;
                case State::B:
                    if(set_of_terminals[Terminal_category::Scope_resolution]){
                        state = State::A;
                    }else{
                        result.first->components_ = components;
                        scanner_->back();
                        return result;
                    }
                    break;
                default:
                    ;
            }
        }

        if(state != State::B){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            et_.ec_->increment_number_of_errors();
            scanner_->back();
            result.second = false;
        }

        return result;
    }


    Parser::~Parser() = default;

    Parser::Parser(const std::shared_ptr<fst_level_scanner::Scanner>& scanner,
                   const Errors_and_tries&                            et,
                   const std::shared_ptr<symbol_table::Symbol_table>& table)
    : impl_(std::make_shared<Impl>(scanner, et, table))
    {
    }

    Parser::Parser() : impl_{std::make_shared<Impl>()} {}

    std::shared_ptr<ast::Module> Parser::parse()
    {
        printf("Starting Parser::parse()...\n");
        return impl_->parse();
    }

    std::shared_ptr<ast::Module> Parser::Impl::parse()
    {
        printf("Starting Parser::Impl::parse()...\n");

        printf("Pointer to error counter:    %p\n", et_.ec_.get());
        printf("Pointer to warning counter:  %p\n", et_.wc_.get());
        printf("Pointer to identifiers trie: %p\n", et_.ids_trie_.get());
        printf("Pointer to strings trie:     %p\n", et_.strs_trie_.get());
        printf("Pointer to scanner:          %p\n", scanner_.get());
        printf("Pointer to symbol table:     %p\n", table_.get());

        auto [module_node_ptr, flag] = module_node_proc();
        printf("module_node_ptr: %p, flag: %s\n",
               module_node_ptr.get(),
               flag ? "true" : "false");
        printf("Parsing is done.\n");
        auto result                  = std::make_shared<ast::Module>(et_.strs_trie_,
                                                                     et_.ids_trie_,
                                                                     module_node_ptr,
                                                                     table_);
        return result;
    }
};