/*
    File:    token_to_terminal_set.cpp
    Created: 27 July 2021 at 20:13 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <map>
#include "../include/token_to_terminal_set.hpp"

namespace{
    using Lexem_kind     = fst_level_scanner::Lexem_kind;
    using Keyword_kind   = fst_level_scanner::Keyword_kind;
    using Delimiter_kind = fst_level_scanner::Delimiter_kind;

    using namespace arkona_parser;

    const Terminal_set default_set = {Terminal_category::End_of_text};

    const std::map<Keyword_kind, Terminal_set> keywords_mapping = {
        {Keyword_kind::Kw_modul,     {Terminal_category::Module}},
        {Keyword_kind::Kw_ispolzuet, {Terminal_category::Uses  }},
    };

    Terminal_set keyword_conversion(const Token& token)
    {
        Terminal_set result = default_set;

        Keyword_kind k  = static_cast<Keyword_kind>(token.lexeme_.code_.subkind_);
        auto         it = keywords_mapping.find(k);

        if(it != keywords_mapping.end()){
            result = it->second;
        }

        return result;
    }


    const std::map<Delimiter_kind, Terminal_set> delimiters_mapping = {
        {Delimiter_kind::Scope_resolution,      {Terminal_category::Scope_resolution     }},
        {Delimiter_kind::Semicolon,             {Terminal_category::Semicolon            }},
        {Delimiter_kind::Comma,                 {Terminal_category::Comma                }},
        {Delimiter_kind::Figure_bracket_opened, {Terminal_category::Figure_bracket_opened}},
        {Delimiter_kind::Figure_bracket_closed, {Terminal_category::Figure_bracket_closed}},
    };

    Terminal_set delimiter_conversion(const Token& token)
    {
        Terminal_set result = default_set;

        Delimiter_kind k  = static_cast<Delimiter_kind>(token.lexeme_.code_.subkind_);
        auto           it = delimiters_mapping.find(k);

        if(it != delimiters_mapping.end()){
            result = it->second;
        }

        return result;
    }
};

namespace arkona_parser{
    Terminal_set token_to_terminal_set(const Token& token)
    {
        Terminal_set result;

        const auto& lexeme = token.lexeme_;
        const auto& code   = lexeme.code_;

        switch(code.kind_){
            case Lexem_kind::Nothing:
                result[Terminal_category::End_of_text] = true;
                break;
            case Lexem_kind::UnknownLexem:
                result[Terminal_category::End_of_text] = true;
                break;
            case Lexem_kind::Keyword:
                result = keyword_conversion(token);
                break;
            case Lexem_kind::Id:
                result[Terminal_category::Id]                = true;
//                 result[Terminal_category::Block_entry_begin] = true;
                break;
            case Lexem_kind::Delimiter:
                result = delimiter_conversion(token);
                break;
            case Lexem_kind::Char:  case Lexem_kind::String:  case Lexem_kind::Integer:
            case Lexem_kind::Float: case Lexem_kind::Complex: case Lexem_kind::Quat:
                result[Terminal_category::Literal]           = true;
//                 result[Terminal_category::Block_entry_begin] = true;
                break;
            default:
                break;
        }

        return result;
    }
};
