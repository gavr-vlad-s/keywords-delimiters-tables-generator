/*
    File:    terminal_enum.cpp
    Created: 08 August 2021 at 14:38 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/terminal_enum.hpp"

namespace{
    const std::string terminal_category_strs[] = {
        "End_of_text", "Module",                "Uses",                  "Scope_resolution",
        "Comma",       "Figure_bracket_opened", "Figure_bracket_closed", "Id",
        "Semicolon",   "Literal"
    };
};

namespace arkona_parser{
    std::string to_string(Terminal_category t)
    {
        return terminal_category_strs[static_cast<unsigned>(t)];
    }
};