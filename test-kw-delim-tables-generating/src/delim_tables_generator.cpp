/*
    File:    delim_tables_generator.cpp
    Created: 29 April 2021 at 21:13 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/delim_tables_generator.hpp"


Delimiter_table_info create_delimiter_table(const std::vector<Lexeme_with_code>& lexemes_and_codes)
{
    Delimiter_table_info      result;
    Info_for_transition_table info;

    info.lexemes_and_codes_    = lexemes_and_codes;
    info.additions_for_header_ = U"../include/lexeme_info.hpp";
    info.additions_for_impl_   = U"";
    info.kind_                 = Table_kind::Delimiter;
    info.namespace_name_       = U"fst_level_scanner";
    info.author_               = U"Гаврилов Владимир Сергеевич";
    info.emails_               = {
                                    U"vladimir.s.gavrilov@gmail.com",
                                    U"gavrilov.vladimir.s@mail.ru",
                                    U"gavvs1977@yandex.ru"
                                 };
    info.licence_id_           = U"GPLv3";
    info.lexem_info_type_name_ = U"LexemeInfo";

    Transition_table_builder  table_builder;
    const auto                table = table_builder(info);
    const auto                aux_files = table_builder.build_auxiliary_files_content("fst_level_scanner");

    result.main_header_file_ = table.main_header_content_;
    result.main_impl_file_   = table.main_impl_content_;
    result.includes_         = aux_files.includes_;
    result.srcs_             = aux_files.srcs_;

    return result;
}