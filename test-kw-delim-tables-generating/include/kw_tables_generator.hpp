/*
    File:    kw_tables_generator.hpp
    Created: 17 April 2021 at 10:22 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef KW_TABLES_GENERATOR_H
#define KW_TABLES_GENERATOR_H
#   include <map>
#   include <string>
#   include <vector>
#   include "../../tries/include/keywords_or_delimiters_transition_table_builder.hpp"

struct Keyword_table_info
{
    std::string                        main_header_file_;
    std::string                        main_impl_file_;
    std::map<std::string, std::string> includes_;
    std::map<std::string, std::string> srcs_;
};

Keyword_table_info create_keyword_table(const std::vector<Lexeme_with_code>& lexemes_and_codes);
#endif