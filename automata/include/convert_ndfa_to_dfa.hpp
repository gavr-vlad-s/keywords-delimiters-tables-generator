/*
    File:    convert_ndfa_to_dfa.hpp
    Created: 04 September 2021 at 15:34 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef CONVERT_NDFA_TO_DFA_H
#define CONVERT_NDFA_TO_DFA_H
#   include <cstddef>
#   include <map>
#   include <set>
#   include <stack>
#   include <vector>
#   include "../include/generalized_char.hpp"
#   include "../include/ndfa.hpp"
#   include "../include/dfa.hpp"
#   include "../../tries/include/size_t_trie.hpp"
#   include "../../sets/include/operations_with_sets.hpp"

namespace details{
    using namespace operations_with_sets;

    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    std::set<std::size_t> epsilon_closure(const NDFA<T, A, Traits>& a, const std::set<std::size_t>& s)
    {
        std::stack<std::size_t> stack_of_states;
        std::set<std::size_t>   eps_clos = s;
        for(std::size_t x : s)
        {
            stack_of_states.push(x);
        }
        while(!stack_of_states.empty())
        {
            std::size_t t = stack_of_states.top();
            stack_of_states.pop();
            auto& t_jumps = a.jumps_[t];
            auto  iter = t_jumps.find(epsilon<T, Traits>());
            if (iter != t_jumps.end())
            {
                const auto& [_, states_and_action]     = *iter;
                const auto& [eps_jumps_states, action] = states_and_action;
                for(auto st : eps_jumps_states)
                {
                    auto it = eps_clos.find(st);
                    if(it == eps_clos.end())
                    {
                        eps_clos.insert(st);
                        stack_of_states.push(st);
                    }
                }
            }
        }
        return eps_clos;
    }


    /* Функция contains_final_state проверяет, содержит ли множество s состояний НКА a
    конечное состояние этого автомата. */
    template<typename T,
             typename A,
             typename Traits = Generalized_char_traits<T>>
    bool contains_final_state(const NDFA<T, A, Traits>& a, const std::set<std::size_t>& s)
    {
        return s.find(a.final_state_) != s.end();
    }


    /* Следующая функция по множеству s состояний НКА a строит множество символов,
     * по которым из хотя бы одного из состояний, принадлежащих множеству s,
     * есть переход. Эпсилон-переходы не учитываются. */
    template<typename T,
             typename A,
             typename Traits = Generalized_char_traits<T>>
    std::set<Generalized_char<T, Traits>> jump_chars_set(const NDFA<T, A, Traits>& a, const std::set<std::size_t>& s)
    {
        std::set<Generalized_char<T>> jump_chars;
        for(std::size_t st : s)
        {
            auto& st_jumps = a.jumps_[st];
            for(const auto& [gc, _] : st_jumps)
            {
                /* этот цикл --- по всем переходам для состояния st недетерминированного
                 * конечного автомата */
                if(gc.kind_ != Kind_of_char::Epsilon)
                {
                    jump_chars.insert(gc);
                }
            }
        }
        return jump_chars;
    }


    /* Следующая функция вычисляет множество состояний, в которое перейдёт множество
     * состояний states по символу (или классу символов) gc и возвращает получившееся
     * множество в виде контейнера std::set<size_t>. */
    template<typename T,
             typename A,
             typename Traits = Generalized_char_traits<T>>
    std::set<std::size_t> move(const NDFA<T, A, Traits>&          a,
                               const std::set<std::size_t>&       states,
                               const Generalized_char<T, Traits>& gc)
    {
        std::set<std::size_t> move_set;
        for(std::size_t st : states)
        {
            auto& st_jumps = a.jumps_[st];
            auto it = st_jumps.find(gc);
            if(it != st_jumps.end())
            {
                move_set = move_set + (it->second).first;
            }
        }
        return move_set;
    }

    /* Следующая функция вычисляет действие, которое нужно выполнить при переходе из состояния ДКА, представленного
     * множеством s состояний НКА a, по символу gc. */
    template<typename T,
             typename A,
             typename ATraits = Action_traits<A>,
             typename Traits = Generalized_char_traits<T>>
    A action_for_dfa_jump(const NDFA<T, A, Traits>&          a,
                          const std::set<std::size_t>&       s,
                          const Generalized_char<T, Traits>& gc)
    {
        A result = ATraits::neutral();
        for(std::size_t x : s)
        {
            auto&  x_jmp  = a.jumps_[x];
            auto   it     = x_jmp.find(gc);
            if(it != x_jmp.end())
            {
                auto target = it->second;
                A    act    = target.second;

                result = ATraits::common(result, act);
            }
        }
        return result;
    }

};

template<typename T,
         typename A,
         typename ATraits = Action_traits<A>,
         typename Traits = Generalized_char_traits<T>>
DFA<T, A, Traits> convert_NDFA_to_DFA(const NDFA<T, A, Traits>& ndfa)
{
    DFA<T, A, Traits> a;

    std::vector<std::size_t>           marked_states_of_dfa;
    std::vector<std::size_t>           unmarked_states_of_dfa;
    Size_t_trie                        sets_of_ndfa_states;
    std::map<std::size_t, std::size_t> states_nums; /* Это отображение индексов множеств имён
                                                       состояний НКА в номера состояний ДКА.
                                                       Нумерация последних начинается с нуля,
                                                       и нумеруются они в порядке появления
                                                       в вычислениях. */
    std::size_t current_nom_of_DFA_state = 0;

    // Вычисляем начальное состояние ДКА a.
    auto        begin_state     = details::epsilon_closure<T, A, Traits>(ndfa,  {ndfa.begin_state_});
    std::size_t begin_state_idx = sets_of_ndfa_states.insert(begin_state);

    states_nums[begin_state_idx] = current_nom_of_DFA_state;
    if(details::contains_final_state<T, A, Traits>(ndfa, begin_state))
    {
        a.final_states_.insert(current_nom_of_DFA_state);
    }
    current_nom_of_DFA_state++;

    unmarked_states_of_dfa.push_back(begin_state_idx);

    while(!unmarked_states_of_dfa.empty())
    {
        std::size_t t_idx = unmarked_states_of_dfa.back();
        marked_states_of_dfa.push_back(t_idx);
        unmarked_states_of_dfa.pop_back();
        // Обработка непомеченного состояния
        /* Для этого сначала получим список всех символов, по которым из
         * обрабатываемого состояния вообще возможен переход. */
        std::set<std::size_t>         t          = sets_of_ndfa_states.get_set(t_idx);
        std::set<Generalized_char<T>> jump_chars = details::jump_chars_set<T, A, Traits>(ndfa, t);
        /* А теперь вычислим переходы по этим символам из текущего состояния ДКА */
        for(const auto& gc : jump_chars)
        {
            auto        u     = details::epsilon_closure(ndfa, details::move(ndfa, t, gc));
            std::size_t u_idx = sets_of_ndfa_states.insert(u);
            if(!u.empty())
            {
                /* здесь оказываемся, если epsilon-замыкание не пусто,
                 * то есть переход из t по gc имеется */
                DFA_state_with_action<A> sa;
                sa.action_ = details::action_for_dfa_jump<T, A, ATraits, Traits>(ndfa, t, gc);
                auto it       = states_nums.find(u_idx);
                if(it == states_nums.end())
                {
                    unmarked_states_of_dfa.push_back(u_idx);
                    states_nums[u_idx] = current_nom_of_DFA_state;
                    if(details::contains_final_state<T, A, Traits>(ndfa, u)){
                        a.final_states_.insert(current_nom_of_DFA_state);
                    }
                    sa.st_ = current_nom_of_DFA_state;
                    current_nom_of_DFA_state++;
                }else{
                    sa.st_ = it->second;
                }
                // теперь добавляем запись о переходе
                a.jumps_[std::make_pair(states_nums[t_idx], gc)] = sa;
            }
        }
    }
    a.number_of_states_ = current_nom_of_DFA_state;

    return a;
}
#endif