/*
    File:    generalized_char.hpp
    Created: 29 August 2021 at 13:06 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef GENERALIZED_CHAR_H
#define GENERALIZED_CHAR_H
#   include <string>
enum class Kind_of_char {
    Epsilon, Char
};

template<typename T>
struct Generalized_char_traits
{
    static bool lt(T a, T b);
    static bool eq(T a, T b);
    static std::string to_string(T a);
};

template<typename T, typename Traits = Generalized_char_traits<T>>
struct Generalized_char {
    Kind_of_char kind_;
    T            ch_;
};

template<typename T, typename Traits = Generalized_char_traits<T>>
bool operator < (const Generalized_char<T, Traits>& a, const Generalized_char<T, Traits>& b)
{
    if(a.kind_ != b.kind_){
        return a.kind_ < b.kind_;
    }

    switch(a.kind_){
        case Kind_of_char::Epsilon:
            return false;
        case Kind_of_char::Char:
            return Traits::lt(a.ch_, b.ch_);
        default:
            ;
    }

    return false;
}

template<typename T, typename Traits = Generalized_char_traits<T>>
bool operator == (const Generalized_char<T, Traits>& a, const Generalized_char<T, Traits>& b)
{
    if(a.kind_ != b.kind_){
        return false;
    }

    switch(a.kind_){
        case Kind_of_char::Epsilon:
            return true;
        case Kind_of_char::Char:
            return Traits::eq(a.ch_, b.ch_);
        default:
            ;
    }

    return false;
}


template<typename T, typename Traits = Generalized_char_traits<T>>
std::string gen_char_to_string(const Generalized_char<T, Traits>& a)
{
    if(a.kind_ == Kind_of_char::Epsilon){
        return std::string{"epsilon"};
    }

    return std::string{"Generalized_char [Char] "} + Traits::to_string(a.ch_);
}


template<typename T, typename Traits = Generalized_char_traits<T>>
Generalized_char<T, Traits> epsilon()
{
    Generalized_char<T, Traits> gc;
    gc.kind_ = Kind_of_char::Epsilon;
    return gc;
}

template<typename T, typename Traits = Generalized_char_traits<T>>
Generalized_char<T, Traits> ordinary_char(T c)
{
    Generalized_char<T, Traits> gc;
    gc.kind_ = Kind_of_char::Char;
    gc.ch_   = c;
    return gc;
}
#endif