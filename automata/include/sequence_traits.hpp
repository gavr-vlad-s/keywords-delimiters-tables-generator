/*
    File:    sequence_traits.hpp
    Created: 23 October 2021 at 08:59 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef SEQUENCE_TRAITS_H
#define SEQUENCE_TRAITS_H
template<typename Container>
struct Sequence_traits
{
    using SequenceT  = typename Container::value_type;
    using CharacterT = typename SequenceT::value_type;
};

template<typename Container>
using SequenceT = typename Sequence_traits<Container>::SequenceT;

template<typename Container>
using CharacterT = typename Sequence_traits<Container>::CharacterT;
#endif