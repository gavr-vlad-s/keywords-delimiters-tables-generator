/*
    File:    minimal_dfa_for_lcp.hpp
    Created: 23 October 2021 at 09:06 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef MINIMAL_DFA_FOR_LCP_H
#define MINIMAL_DFA_FOR_LCP_H
#   include <cstddef>
#   include <set>
#   include <string>
#   include <tuple>
#   include "../include/sequence_traits.hpp"
#   include "../include/generalized_char.hpp"
#   include "../include/action_traits.hpp"
#   include "../include/ndfa.hpp"
#   include "../include/dfa.hpp"
#   include "../include/minimal_dfa.hpp"
#   include "../include/convert_ndfa_to_dfa.hpp"
#   include "../include/minimize_dfa.hpp"

using Action_type = std::size_t;

template<>
struct Action_traits<Action_type>
{
    static std::string to_string(Action_type a)
    {
        return std::to_string(a);
    }

    static Action_type common(Action_type a, Action_type b)
    {
        return std::min(a, b);
    }

    static Action_type neutral()
    {
        return std::numeric_limits<Action_type>::max();
    }
};

namespace min_dfa_for_lcp::details
{
    template<typename Container>
    using Char_type = CharacterT<Container>;

    template<typename Container>
    using Sequence_type = SequenceT<Container>;

    template<typename Container>
    using Char_type_traits = Generalized_char_traits<Char_type<Container>>;

    template<typename Container>
    using Generalized_char_type = Generalized_char<Char_type<Container>>;

    template<typename Container>
    using NDFA_type = NDFA<Char_type<Container>, Action_type, Generalized_char_traits<Char_type<Container>>>;

    template<typename Container>
    using DFA_type = DFA<Char_type<Container>, Action_type, Generalized_char_traits<Char_type<Container>>>;

    template<typename Container>
    using NDFA_state_jumps_type = NDFA_state_jumps<Char_type<Container>, Action_type, Char_type_traits<Container>>;

    template<typename Container>
    using NDFA_jumps_type = NDFA_jumps<Char_type<Container>, Action_type, Char_type_traits<Container>>;

    using States_with_action_type = States_with_action<Action_type>;


    States_with_action_type neutral_sa(const std::set<std::size_t>& target_states)
    {
        return States_with_action_type{target_states, Action_traits<Action_type>::neutral()};
    }

    template<typename Container>
    NDFA_type<Container> ndfa_from_container_of_sequences(const Container& container)
    {
        std::set<std::size_t>      states_with_incoming_eps_transitions;
        std::set<std::size_t>      states_with_outcoming_eps_transitions;

        std::size_t                offset_of_current_state = 1;

        NDFA_jumps_type<Container> jumps;

        Action_type                idx = Action_type{0};

        jumps.emplace_back(NDFA_state_jumps_type<Container>{});
        for(const auto& seq : container)
        {
            std::vector<std::tuple<Generalized_char_type<Container>, std::set<std::size_t>, Action_type>> for_jumps;
            std::size_t                                                                                   target_state_for_current_char = 1;

            states_with_incoming_eps_transitions.insert(offset_of_current_state);
            for(const auto& c : seq)
            {
                const auto gc                      = ordinary_char(c);
                const auto [target_states, action] = neutral_sa({target_state_for_current_char + offset_of_current_state});

                for_jumps.push_back({gc, target_states, action});
                target_state_for_current_char++;
            }
            const auto [last_gc, target_states, action] = for_jumps.back();
            for_jumps.back() = {last_gc, target_states, idx};

            NDFA_jumps_type<Container> added_jumps;
            for(const auto& [gc, states, act] : for_jumps)
            {
                NDFA_state_jumps_type<Container> current_jumps;
                current_jumps[gc] = States_with_action_type{states, act};
                added_jumps.emplace_back(current_jumps);
            }
            added_jumps.emplace_back(NDFA_state_jumps_type<Container>{});

            jumps.insert(jumps.end(), added_jumps.begin(), added_jumps.end());

            offset_of_current_state += target_state_for_current_char;

            states_with_outcoming_eps_transitions.insert(jumps.size() - 1);

            idx++;
        }

        jumps.emplace_back(NDFA_state_jumps_type<Container>{});

        std::size_t final_state = jumps.size()  - 1;

        jumps[0][epsilon<Char_type<Container>>()] = neutral_sa(states_with_incoming_eps_transitions);
        for(auto st : states_with_outcoming_eps_transitions)
        {
            NDFA_state_jumps_type<Container> j;
            jumps[st][epsilon<Char_type<Container>>()] = neutral_sa({final_state});
        }

        NDFA_type<Container> result;
        result.jumps_       = jumps;
        result.begin_state_ = 0;
        result.final_state_ = final_state;

        return result;
    }
};

template<typename Container>
auto minimal_dfa_for_lcp(const Container& container)
{
    const auto ndfa   = min_dfa_for_lcp::details::ndfa_from_container_of_sequences(container);
    const auto dfa    = convert_NDFA_to_DFA(ndfa);
    const auto result = minimize_DFA(dfa);

    return result;
}
#endif