/*
    File:    action_traits.hpp
    Created: 11 September 2021 at 15:59 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef ACTION_TRAITS_H
#define ACTION_TRAITS_H
template<typename A>
struct Action_traits{
    static std::string to_string(A a);
    static A           common(A a, A b);
    static A           neutral();
};
#endif