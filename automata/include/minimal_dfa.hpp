/*
    File:    minimal_dfa.hpp
    Created: 27 September 2021 at 19:14 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef MINIMAL_DFA_H
#define MINIMAL_DFA_H
#   include <cstddef>
#   include <map>
#   include <set>
#   include <string>
#   include <utility>
#   include <vector>
#   include "../include/generalized_char.hpp"
#   include "../include/dfa.hpp"
#   include "../include/action_traits.hpp"
#   include "../../sets/include/operations_with_sets.hpp"
#   include "../../thirdparty/fmtlib/include/core.h"

template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
using Min_DFA_state_jumps = std::map<Generalized_char<T, Traits>, DFA_state_with_action<A>>;

template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
using Min_DFA_jumps       = std::vector<Min_DFA_state_jumps<T, A, Traits>>;

template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
struct Min_DFA{
    Min_DFA_jumps<T, A, Traits> jumps_;
    size_t                      begin_state_ = 0;
    std::set<size_t>            final_states_;

    Min_DFA()                    = default;
    ~Min_DFA()                   = default;
    Min_DFA(const Min_DFA& orig) = default;
};

template<typename T,
         typename A,
         typename ATraits = Action_traits<A>,
         typename Traits = Generalized_char_traits<T>>
std::string minimal_dfa_to_string(const Min_DFA<T, A, Traits>& automaton)
{
    std::string result;

    const std::string title_fmt = R"~(Start state: {0}
Final states: {1}
Minimal DFA jumps:
)~";

    const std::string transition_fmt = R"~(delta({0}, {1}) = {2} with action {3})~";

    const auto final_states_str = operations_with_sets::set_to_string(automaton.final_states_,
                                                                      [](std::size_t e)->std::string
                                                                      {
                                                                          return std::to_string(e);
                                                                      });

    const auto title_str = fmt::format(title_fmt, automaton.begin_state_, final_states_str);

    result = title_str;

    const auto& jumps         = automaton.jumps_;
    std::size_t num_of_states = jumps.size();

    for(std::size_t state = 0; state < num_of_states; ++state)
    {
        const auto& current_state_jumps = jumps[state];
        for(const auto& [gc, target_sa] : current_state_jumps)
        {
            const auto& [target_state, action] = target_sa;

            const auto  transition_str = fmt::format(transition_fmt,
                                                     state,
                                                     gen_char_to_string(gc),
                                                     target_state,
                                                     ATraits::to_string(action));

            result += transition_str + "\n";
        }
    }
    result.pop_back();

    return result;
}
#endif