/*
    File:    execute_minimal_dfa.hpp
    Created: 07 November 2021 at 11:37 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef EXECUTE_MINIMAL_DFA_H
#define EXECUTE_MINIMAL_DFA_H
#   include "../include/generalized_char.hpp"
#   include "../include/action_traits.hpp"
#   include "../include/minimal_dfa.hpp"

template<typename Sequence>
using chararacter_t = typename Sequence::value_type;

template<typename Sequence>
using Chararacter_type_traits = Generalized_char_traits<chararacter_t<Sequence>>;

template<typename Sequence>
using generalized_char_t = Generalized_char<chararacter_t<Sequence>>;

template<typename Sequence, typename A, typename Traits = Chararacter_type_traits<Sequence>>
using min_dfa_t = Min_DFA<chararacter_t<Sequence>, A, Traits>;


template<typename Sequence, typename A, typename Traits = Chararacter_type_traits<Sequence>>
A execute_min_dfa(const Sequence& seq, const min_dfa_t<Sequence, A, Traits>& automaton)
{
    using ATraits = Action_traits<A>;

    A           result       = ATraits::neutral();

    const auto& jumps        = automaton.jumps_;
    const auto& final_states = automaton.final_states_;
    std::size_t state        = automaton.begin_state_;

    for(const auto& c : seq)
    {
        const auto& state_jumps = jumps[state];
        const auto  gc          = ordinary_char<chararacter_t<Sequence>, Traits>(c);
        auto        it          = state_jumps.find(gc);

        if(it == state_jumps.end())
        {
            return result;
        }

        const auto [_, sa]                = *it;
        const auto [target_state, action] = sa;

        state = target_state;

        if(final_states.count(target_state) != 0){
            result = action;
        }
    }

    return result;
}
#endif