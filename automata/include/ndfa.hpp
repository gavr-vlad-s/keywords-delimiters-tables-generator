/*
    File:    ndfa.hpp
    Created: 29 August 2021 at 14:10 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef NDFA_H
#define NDFA_H
#   include <cstddef>
#   include <map>
#   include <set>
#   include <string>
#   include <utility>
#   include <vector>
#   include "../include/generalized_char.hpp"
#   include "../include/action_traits.hpp"
#   include "../../sets/include/operations_with_sets.hpp"
#   include "../../thirdparty/fmtlib/include/core.h"

using Set_of_states = std::set<size_t>;

template<typename A>
using States_with_action = std::pair<Set_of_states, A>;

template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
using NDFA_state_jumps = std::map<Generalized_char<T, Traits>, States_with_action<A>>;

template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
using NDFA_jumps = std::vector<NDFA_state_jumps<T, A, Traits>>;

template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
struct NDFA{
    NDFA_jumps<T, A, Traits> jumps_;
    std::size_t              begin_state_;
    std::size_t              final_state_;

    NDFA()                 = default;
    ~NDFA()                = default;
    NDFA(const NDFA& orig) = default;
};

template<typename T,
         typename A,
         typename ATraits = Action_traits<A>,
         typename Traits = Generalized_char_traits<T>>
std::string ndfa_to_string(const NDFA<T, A, Traits>& automaton)
{
    std::string result;

    const std::string title_fmt = R"~(begin_state: {0}
final_state: {1}
transitions:
)~";

    result = fmt::format(title_fmt, automaton.begin_state_, automaton.final_state_);


    const std::string transition_fmt = R"~({0}delta({1}, {2}) = {3} with action {4})~";

    const auto indent = std::string(4, ' ');

    std::size_t state_idx = 0;
    for(const auto& j : automaton.jumps_)
    {
        for(const auto& [gc, sa] : j)
        {
            const auto& [target_states, action] = sa;

            const auto transition_str = fmt::format(transition_fmt,
                                                    indent,
                                                    state_idx,
                                                    gen_char_to_string(gc),
                                                    operations_with_sets::set_to_string(target_states,
                                                                                        [](std::size_t e)->std::string
                                                                                        {
                                                                                            return std::to_string(e);
                                                                                        }),
                                                    ATraits::to_string(action));

            result += transition_str + "\n";
        }
        state_idx++;
    }
    result.pop_back();

    return result;
}
#endif