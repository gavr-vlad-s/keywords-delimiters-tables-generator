/*
    File:    minimize_dfa.hpp
    Created: 09 October 2021 at 21:21 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef MINIMIZE_DFA_H
#define MINIMIZE_DFA_H
#   include <algorithm>
#   include <list>
#   include <set>
#   include <utility>
#   include <vector>
#   include "../include/dfa.hpp"
#   include "../include/minimal_dfa.hpp"
#   include "../../sets/include/operations_with_sets.hpp"

namespace details{
    using namespace operations_with_sets;

    /* Данная функция по таблице переходов ДКА a строит ту же таблицу, но в ином формате. */
    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Min_DFA_jumps<T, A, Traits> convert_jumps(const DFA<T, A, Traits>& a)
    {
        Min_DFA_jumps<T, A, Traits> conv_j(a.number_of_states_);
        for(const auto& [state_and_gc, target] : a.jumps_){
            const auto& [state, gc] = state_and_gc;
            conv_j[state][gc] = target;
        }
        return conv_j;
    }
    using Partition_as_vector = std::vector<size_t>;
    /* Этот тип описывает разбиение множества состояний ДКА на группы состояний. А именно,
       значение элемента с индексом i представляет собой номер группы, которой принадлежит
       состояние с номером i. Номера групп считаем неотрицательными целыми числами, причём
       группа состояний с номером 0 --- фиктивная, в следующем смысле: если из состояния i
       нет никакого перехода по некоторому символу gc, то считаем, что имеется переход из i
       по символу gc в эту фиктивную группу. */

    using Partition_as_sets = std::list<std::set<size_t>>;
    /* Этот тип тоже описывает разбиение множества состояний ДКА на группы состояний, но
       описывает несколько иначе. А именно, элемент с индексом i представляет собой
       группу состояний с номером i + 1. При этом фиктивная группа с номером 0 в этот список
       групп не входит. */

    using Group = std::set<size_t>;

    /* Функция build_initial_partition строит начальное разбиение состояний ДКА a:
       на состояния, являющиеся конечными, и на состояния, конечными не являющиеся. */
    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Partition_as_sets build_initial_partition(const DFA<T, A, Traits>& a)
    {
        Partition_as_sets initial;
        auto final_states_group = a.final_states_;
        std::set<size_t> non_final_states_group;
        for(size_t st = 0; st < a.number_of_states_; st++){
            if(!is_elem(st, final_states_group)){
                non_final_states_group.insert(st);
            }
        }
        initial.push_back(final_states_group);
        if(!non_final_states_group.empty()){
            initial.push_back(non_final_states_group);
        }
        return initial;
    }

    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Partition_as_vector convert_partition_form(const Partition_as_sets& ps, const DFA<T, A, Traits>& a)
    {
        Partition_as_vector pv(a.number_of_states_);
        size_t i = 1;
        for(const auto& s : ps){
            for(size_t x : s){
                pv[x] = i;
            }
            i++;
        }
        return pv;
    }

    using State_and_group_numb = std::pair<size_t, size_t>;
    /* первый элемент данной пары является состоянием, а второй --- номером группы состояний,
     * в которую переходит данное состояние */

    using States_classification = std::vector<State_and_group_numb>;

    /* Следующая функция классифицирует состояния, находящиеся в группе group.
     * А именно, возвращается вектор, состоящий из пар вида
     *        (состояние; номер группы состояний, в которую переходит состояние по gc)
     * При этом все первые элементы данных пар принадлежат группе group.
     */
    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    States_classification group_states_classificate(const Partition_as_vector&         pv,
                                                    const Min_DFA_jumps<T, A, Traits>& j,
                                                    const Group&                       group,
                                                    const Generalized_char<T, Traits>& gc)
    {
        States_classification sc;
        for(size_t state : group)
        {
            auto& state_jumps = j[state]; // получаем map, в котором хранятся
                                          // переходы состояния state, а ключи имеют
                                          // тип Generalized_char
            auto it = state_jumps.find(gc);
            if(it != state_jumps.end()){
                /* если переход по символу gc есть, то записываем пару
                 *  (состояние; номер группы состояний, в которую переходит состояние по gc)
                 */
                sc.push_back(std::make_pair(state, pv[(it->second).st_]));
            }else{
                /* иначе считаем, что переход по gc выполняется в
                 * фиктивную группу с номером 0*/
                sc.push_back(std::make_pair(state, 0));
            }
        }
        /* теперь отсортируем данный вектор по второму компоненту каждой пары,
           т.е. по номеру группы, в которую переходит состояние по gc */
        std::sort(sc.begin(), sc.end(),
                 [](const auto& sg1, const auto& sg2){
                    return sg1.second < sg2.second;
                 });
        return sc;
    }

    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Partition_as_sets split_group_by_gc(const Partition_as_vector&         pv,
                                        const Min_DFA_jumps<T, A, Traits>& j,
                                        const Group&                       group,
                                        const Generalized_char<T, Traits>& gc)
    {
        Partition_as_sets partition;
        auto states_classification = group_states_classificate(pv, j, group, gc);

        /* а теперь собственно и строим разбиение группы group, пользуясь для этого отсортированным вектором пар */
        std::pair<size_t, size_t> current_elem  = states_classification[0];
        Group                     current_group = {current_elem.first};
        for(const auto p : states_classification)
        {
            if(current_elem.second == p.second){
                current_group.insert(p.first);
            }else{
                partition.push_back(current_group);
                current_group = {p.first};
            }
            current_elem = p;
        }
        partition.push_back(current_group);
        return partition;
    }

    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Partition_as_sets split_group_partition_by_gc(const Partition_as_vector&         pv,
                                                  const Min_DFA_jumps<T, A, Traits>& j,
                                                  const Partition_as_sets&           group_partition,
                                                  const Generalized_char<T, Traits>& gc)
    {
        Partition_as_sets partition;
        for(const auto& g : group_partition)
        {
            auto splitted_g = split_group_by_gc(pv, j, g, gc);
            partition.insert(partition.end(), splitted_g.begin(), splitted_g.end());
        }
        return partition;
    }

    /* Следующая функция по множеству s состояний ДКА с таблицей переходов j строит множество
       символов, по которым из хотя бы одного из состояний, принадлежащих множеству s,
       есть переход. */
    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    std::set<Generalized_char<T, Traits>> jump_chars_for_group(const Min_DFA_jumps<T, A, Traits>& j, const Group& s)
    {
        std::set<Generalized_char<T, Traits>> jump_chars;
        for(size_t st : s)
        {
            auto& st_jumps = j[st];
            for(const auto& [gc, _] : st_jumps)
            {
                /* этот цикл --- по всем переходам для состояния st детерминированного конечного автомата */
                jump_chars.insert(gc);
            }
        }
        return jump_chars;
    }

    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Partition_as_sets split_group(const Partition_as_vector& pv, const Min_DFA_jumps<T, A, Traits>& j, const Group& group)
    {
        /* для группы состояний group получим символы и классы символов, по которым
        возможны переходы из состояний этой группы */
        auto jump_chars                      = jump_chars_for_group(j, group);
        Partition_as_sets partition_of_group = {group};
        /* Далее для каждого возможного символа перехода выясним,
        различимы ли какие--либо состояния по этому символу. */
        for(auto gc : jump_chars)
        {
            partition_of_group = split_group_partition_by_gc(pv, j, partition_of_group, gc);
        }
        return partition_of_group;
    }

    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Partition_as_sets split_partition(const Partition_as_sets& old, const Min_DFA_jumps<T, A, Traits>& j, const DFA<T, A, Traits>& a)
    {
        Partition_as_sets result;
        Partition_as_vector pv = convert_partition_form(old, a);
        for(const auto& g : old)
        {
            auto splitted_g = split_group(pv, j, g);
            result.insert(result.end(), splitted_g.begin(), splitted_g.end());
        }
        return result;
    }

    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Partition_as_sets split_states_into_equivalence_classes(const Min_DFA_jumps<T, A, Traits>& j,
                                                            const DFA<T, A, Traits>&          a)
    {
        Partition_as_sets old_partition, new_partition;
        new_partition = build_initial_partition(a);
        while(old_partition != new_partition)
        {
            old_partition = new_partition;
            new_partition = split_partition(old_partition, j, a);
        }
        return new_partition;
    }

    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    A action_for_group(const Min_DFA_jumps<T, A, Traits>& j, const Group& g, const Generalized_char<T, Traits>& gc)
    {
        using ATraits = Action_traits<A>;

        A resulting_action = ATraits::neutral();
        for(const size_t state : g)
        {
            auto& state_j          = j[state];
            auto  it               = state_j.find(gc);
            if(it != state_j.end())
            {
                auto target = it->second;
                resulting_action = ATraits::common(resulting_action, target.action_);
            }
        }
        return resulting_action;
    }

    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Min_DFA_jumps<T, A, Traits> minimal_DFA_jumps(const Partition_as_sets&           equivalence_classes,
                                                  const Min_DFA_jumps<T, A, Traits>& j,
                                                  const DFA<T, A, Traits>&           a)
    {
        Min_DFA_jumps<T, A, Traits> result;
        auto eq_classes_as_vector = convert_partition_form(equivalence_classes, a);
        for(const auto& eq_class : equivalence_classes)
        {
            size_t representative = *(eq_class.begin()); // выбрали представителя текущего
                                                         // класса эквивалентности состояний
            /* после склейки состояний, находящихся в одном классе эквивалентности, в одно
             * состояние нового автомата, в качестве символов перехода достаточно рассмотреть
             * лишь символы, по которым совершается переход из состояния--представителя */
            auto&                             representative_jumps = j[representative];
            Min_DFA_state_jumps<T, A, Traits> current_jumps;
            for(const auto& [jump_char, target] : representative_jumps)
            {
                A                        act = action_for_group(j, eq_class, jump_char);
                DFA_state_with_action<A> resulting_target{eq_classes_as_vector[target.st_] - 1, act};
                current_jumps[jump_char] = resulting_target;
            }
            result.push_back(current_jumps);
        }
        return result;
    }

    using Permutation = std::vector<size_t>;

    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Permutation build_state_permutation(const Min_DFA_jumps<T, A, Traits>& j, size_t begin_state)
    {
        size_t        state_idx        = 1;
        size_t        number_of_states = j.size();
        auto          permutation      = Permutation(number_of_states);
        permutation[begin_state]       = 0;
        for(size_t i = 0; i < number_of_states; i++)
        {
            if(i != begin_state){
                permutation[i] = state_idx++;
            }
        }
        return permutation;
    }

    /* По таблице переходов ДКА с минимальным количеством состояний строится
     * таблица переходов, в которой начальное состояние имеет номер 0. */
    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Min_DFA_jumps<T, A, Traits> reorder_states_in_jt(const Min_DFA_jumps<T, A, Traits>& j, const Permutation& p)
    {
        size_t                      number_of_states = j.size();
        Min_DFA_jumps<T, A, Traits> result(number_of_states);
        size_t                      state_idx        = 0;
        for(const auto& m : j)
        {
            Min_DFA_state_jumps<T, A, Traits> new_jumps_for_state;
            for(const auto& [jump_char, target] : m)
            {
                DFA_state_with_action<A> new_target = target;
                new_target.st_                      = p[target.st_];
                new_jumps_for_state[jump_char]      = new_target;
            }
            result[p[state_idx]] = new_jumps_for_state;
            state_idx++;
        }
        return result;
    }

    template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
    Min_DFA<T, A, Traits> reordered_DFA(const Min_DFA<T, A, Traits>& source)
    {
        Min_DFA<T, A, Traits> result;
        auto& sj          = source.jumps_;
        auto  permutation = build_state_permutation(sj, source.begin_state_);
        auto  new_jumps   = reorder_states_in_jt(sj, permutation);
        auto& sf          = source.final_states_;

        std::set<size_t> fs;
        for(const size_t s : sf){
            fs.insert(permutation[s]);
        }

        result.jumps_        = new_jumps;
        result.begin_state_  = 0;
        result.final_states_ = fs;

        return result;
    }

};

template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
Min_DFA<T, A, Traits> minimize_DFA(const DFA<T, A, Traits>& source)
{
    using namespace operations_with_sets;

    Min_DFA<T, A, Traits> result;
    auto sj                   = details::convert_jumps(source);
    auto equivalence_classes  = details::split_states_into_equivalence_classes(sj, source);
    auto minimal_jumps        = details::minimal_DFA_jumps(equivalence_classes, sj, source);
    auto eq_classes_as_vector = details::convert_partition_form(equivalence_classes, source);

    /* выясняем, какие состояния являются конечными, а какое состояние является начальным */
    size_t       mb = source.begin_state_;
    auto         mf = source.final_states_;
    size_t       bs = 0;
    decltype(mf) fs;
    for(const auto& g : equivalence_classes)
    {
        if(is_elem(mb, g)){
            bs = eq_classes_as_vector[mb] - 1;
        }
        auto temp = g * mf;
        if(!temp.empty()){
            size_t s = eq_classes_as_vector[*(g.begin())] - 1;
            fs.insert(s);
        }
    }

    result.jumps_        = minimal_jumps;
    result.begin_state_  = bs;
    result.final_states_ = fs;
    result               = details::reordered_DFA(result);

    return result;
}
#endif