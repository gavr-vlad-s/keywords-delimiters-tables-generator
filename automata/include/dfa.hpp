/*
    File:    dfa.hpp
    Created: 01 September 2021 at 20:06 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef DFA_H
#define DFA_H
#   include <cstddef>
#   include <map>
#   include <set>
#   include <string>
#   include <utility>
#   include <vector>
#   include "../include/generalized_char.hpp"
#   include "../include/action_traits.hpp"
#   include "../../sets/include/operations_with_sets.hpp"
#   include "../../thirdparty/fmtlib/include/core.h"

template<typename A>
struct DFA_state_with_action{
    std::size_t st_;
    A           action_;
};

template<typename T, typename Traits = Generalized_char_traits<T>>
using State_and_gen_char = std::pair<std::size_t, Generalized_char<T, Traits>>;

template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
using DFA_jumps = std::map<State_and_gen_char<T, Traits>, DFA_state_with_action<A>>;

template<typename T, typename A, typename Traits = Generalized_char_traits<T>>
struct DFA{
    DFA_jumps<T, A, Traits> jumps_;
    std::size_t             begin_state_      = 0;
    std::size_t             number_of_states_ = 0;
    std::set<std::size_t>   final_states_;

    DFA()                = default;
    ~DFA()               = default;
    DFA(const DFA& orig) = default;
};


template<typename T,
         typename A,
         typename ATraits = Action_traits<A>,
         typename Traits = Generalized_char_traits<T>>
std::string dfa_to_string(const DFA<T, A, Traits>& automaton)
{
    std::string result;

    const std::string title_fmt = R"~(Start state: {0}
Final states: {1}
Number of states: {2}
DFA jumps:
)~";

    const std::string transition_fmt = R"~(delta({0}, {1}) = {2} with action {3})~";

    const auto final_states_str = operations_with_sets::set_to_string(automaton.final_states_,
                                                                      [](std::size_t e)->std::string
                                                                      {
                                                                          return std::to_string(e);
                                                                      });

    const auto title_str = fmt::format(title_fmt, automaton.begin_state_, final_states_str, automaton.number_of_states_);

    result = title_str;

    for(const auto& [sgc, target_sa] : automaton.jumps_)
    {
        const auto& [state, gc]            = sgc;
        const auto& [target_state, action] = target_sa;

        const auto  transition_str = fmt::format(transition_fmt,
                                                 state,
                                                 gen_char_to_string(gc),
                                                 target_state,
                                                 ATraits::to_string(action));

        result += transition_str + "\n";
    }
    result.pop_back();

    return result;
}
#endif