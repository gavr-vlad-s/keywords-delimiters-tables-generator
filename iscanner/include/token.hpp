/*
    File:    token.hpp
    Created: 11 April 2021 at 17:36 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TOKEN_H
#define TOKEN_H
#   include "../include/position.hpp"
namespace iscaner{
    template<typename Lexeme_type>
    struct Token{
        Token() = default;
        Position_range range_;
        Lexeme_type    lexeme_;
    };

    template<typename Lexeme_type>
    bool operator==(const Token<Lexeme_type>& lhs, const Token<Lexeme_type>& rhs)
    {
        return (lhs.range_ == rhs.range_) && (lhs.lexeme_ == rhs.lexeme_);
    }
};
#endif