/*
     File:    testing-snd-lvl-sc.cpp
     Created: 10 July 2021 at 12:29 MSK
     Author:  Гаврилов Владимир Сергеевич
     E-mails: vladimir.s.gavrilov@gmail.com
              gavrilov.vladimir.s@mail.ru
              gavvs1977@yandex.ru
    License: GPLv3
*/

#include <memory>
#include "../include/usage.hpp"
#include "../../file_utils/include/get_processed_text.hpp"
#include "../../iscanner/include/location.hpp"
#include "../../tries/include/errors_and_tries.hpp"
#include "../../tries/include/error_count.hpp"
#include "../../tries/include/warning_count.hpp"
#include "../../tries/include/char_trie.hpp"
#include "../../snd-lvl-scanner/include/arkona-snd-lvl-scanner.hpp"
#include "../include/test_lexeme_to_string.hpp"
#include "../include/testing-func.hpp"


enum Exit_codes{
    Success, No_args, File_processing_error
};

int main(int argc, char* argv[])
{
    if(1 == argc){
        usage(argv[0]);
        return No_args;
    }

    auto              text      = file_utils::get_processed_text(argv[1]);
    if(text.empty()){
        return File_processing_error;
    }

    char32_t*         p                 = const_cast<char32_t*>(text.c_str());
    auto              loc               = std::make_shared<iscaner::Location>(p);
    Errors_and_tries  et;
    et.ec_                              = std::make_shared<Error_count>();
    et.wc_                              = std::make_shared<Warning_count>();
    et.ids_trie_                        = std::make_shared<Char_trie>();
    et.strs_trie_                       = std::make_shared<Char_trie>();
    auto              snd_lvl_scanner   = std::make_shared<snd_level_scanner::Scanner>(loc, et);

    snd_lvl_testing_scanner::test_lexeme_to_string();
    snd_lvl_testing_scanner::test_func(snd_lvl_scanner);
    et.print();

    return Success;
}