/*
    File:    testing-func.cpp
    Created: 11 July 2021 at 12:38 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/testing-func.hpp"

namespace snd_lvl_testing_scanner{
    void test_func(const std::shared_ptr<snd_level_scanner::Scanner>& arkonasc)
    {
        snd_level_scanner::Snd_lvl_token ati;
        snd_level_scanner::Lexem_kind    alk;
        do{
            ati    = arkonasc->current_lexeme();
            alk    = ati.lexeme_.code_.kind_;
            auto s = arkonasc->token_to_string(ati);
            puts(s.c_str());
        }while(alk != snd_level_scanner::Lexem_kind::Nothing);
    }
};