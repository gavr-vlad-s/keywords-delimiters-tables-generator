Testing of converting of a lexeme into the string...


Running tests for special lexemes (UnknownLexem and None)...
Total tests: 2
Passed: 2
Failed: 0

Running tests for keywords...
Total tests: 112
Passed: 112
Failed: 0

Running tests for identifiers...
Total tests: 2
Passed: 2
Failed: 0

Running tests for delimiters...
Total tests: 114
Passed: 114
Failed: 0

[line: 1, pos: 1]--[line: 1, pos: 12] lexeme: Id веский_довод
[line: 3, pos: 17]--[line: 3, pos: 17] lexeme: Keyword Kw_v
[line: 4, pos: 9]--[line: 4, pos: 13] lexeme: Keyword Kw_vyjdi
[line: 4, pos: 23]--[line: 4, pos: 25] lexeme: Id вон
[line: 4, pos: 34]--[line: 4, pos: 39] lexeme: Keyword Kw_vyberi
[line: 6, pos: 17]--[line: 6, pos: 27] lexeme: Id выбери_меня
[line: 6, pos: 37]--[line: 6, pos: 49] lexeme: Id выдели_цветом
[line: 8, pos: 5]--[line: 8, pos: 11] lexeme: Keyword Kw_vozvrat
[line: 8, pos: 20]--[line: 8, pos: 25] lexeme: Keyword Kw_vydeli
[line: 11, pos: 1]--[line: 11, pos: 7] lexeme: Id главный
[line: 11, pos: 9]--[line: 11, pos: 16] lexeme: Keyword Kw_golovnaya
[line: 11, pos: 24]--[line: 11, pos: 27] lexeme: Id грач
[line: 13, pos: 13]--[line: 13, pos: 35] lexeme: Id вещественных_чисел_поле
[line: 15, pos: 13]--[line: 15, pos: 15] lexeme: Keyword Kw_dlya
[line: 15, pos: 21]--[line: 15, pos: 30] lexeme: Id длительный
[line: 15, pos: 40]--[line: 15, pos: 54] lexeme: Id деловая_колбаса
[line: 17, pos: 41]--[line: 17, pos: 43] lexeme: Keyword Kw_veshch
[line: 17, pos: 48]--[line: 17, pos: 52] lexeme: Keyword Kw_vechno
[line: 19, pos: 26]--[line: 19, pos: 29] lexeme: Keyword Kw_esli
[line: 19, pos: 40]--[line: 19, pos: 43] lexeme: Id есть
[line: 19, pos: 54]--[line: 19, pos: 70] lexeme: Id ел_ем_и_буду_есть
[line: 21, pos: 9]--[line: 21, pos: 19] lexeme: Id вечный_кипр
[line: 21, pos: 23]--[line: 21, pos: 27] lexeme: Keyword Kw_veshch80
[line: 21, pos: 35]--[line: 21, pos: 40] lexeme: Keyword Kw_veshch128
[line: 21, pos: 46]--[line: 21, pos: 50] lexeme: Keyword Kw_veshch32
[line: 21, pos: 56]--[line: 21, pos: 60] lexeme: Keyword Kw_veshch64
[line: 21, pos: 65]--[line: 21, pos: 70] lexeme: Id вещ256
[line: 24, pos: 1]--[line: 24, pos: 2] lexeme: Keyword Kw_iz
[line: 24, pos: 7]--[line: 24, pos: 10] lexeme: Keyword Kw_ines
[line: 24, pos: 12]--[line: 24, pos: 16] lexeme: Keyword Kw_inache
[line: 27, pos: 13]--[line: 27, pos: 17] lexeme: Keyword Kw_konst
[line: 27, pos: 29]--[line: 27, pos: 31] lexeme: Keyword Kw_kak
[line: 27, pos: 44]--[line: 27, pos: 48] lexeme: Keyword Kw_kompl
[line: 29, pos: 5]--[line: 29, pos: 8] lexeme: Keyword Kw_kvat
[line: 29, pos: 16]--[line: 29, pos: 22] lexeme: Keyword Kw_kompl64
[line: 29, pos: 35]--[line: 29, pos: 40] lexeme: Keyword Kw_kvat32
[line: 29, pos: 51]--[line: 29, pos: 57] lexeme: Keyword Kw_kompl80
[line: 31, pos: 9]--[line: 31, pos: 14] lexeme: Keyword Kw_kvat64
[line: 31, pos: 27]--[line: 31, pos: 34] lexeme: Keyword Kw_kompl128
[line: 31, pos: 44]--[line: 31, pos: 49] lexeme: Keyword Kw_kvat80
[line: 31, pos: 63]--[line: 31, pos: 69] lexeme: Keyword Kw_kompl32
[line: 33, pos: 17]--[line: 33, pos: 23] lexeme: Keyword Kw_kvat128
[line: 35, pos: 3]--[line: 35, pos: 11] lexeme: Keyword Kw_kategorija
[line: 37, pos: 17]--[line: 37, pos: 36] lexeme: Id комплексное_значение
[line: 37, pos: 42]--[line: 37, pos: 51] lexeme: Id кватернион
[line: 37, pos: 62]--[line: 37, pos: 69] lexeme: Id колонист
[line: 39, pos: 9]--[line: 39, pos: 20] lexeme: Id исторический
[line: 39, pos: 29]--[line: 39, pos: 38] lexeme: Keyword Kw_ispolzuet
[line: 41, pos: 17]--[line: 41, pos: 24] lexeme: Id исконный
[line: 41, pos: 32]--[line: 41, pos: 37] lexeme: Keyword Kw_istina
[line: 41, pos: 49]--[line: 41, pos: 60] lexeme: Id используемый
[line: 41, pos: 66]--[line: 41, pos: 73] lexeme: Id истинный
[line: 43, pos: 13]--[line: 43, pos: 23] lexeme: Id иностранный
[line: 45, pos: 18]--[line: 45, pos: 25] lexeme: Keyword Kw_diapazon
[line: 47, pos: 5]--[line: 47, pos: 12] lexeme: Id логарифм
[line: 47, pos: 20]--[line: 47, pos: 22] lexeme: Keyword Kw_log
[line: 47, pos: 32]--[line: 47, pos: 35] lexeme: Keyword Kw_lozh
[line: 47, pos: 45]--[line: 47, pos: 50] lexeme: Id лживый
[line: 47, pos: 56]--[line: 47, pos: 65] lexeme: Id логический
[line: 49, pos: 9]--[line: 49, pos: 15] lexeme: Id лягушка
[line: 49, pos: 23]--[line: 49, pos: 28] lexeme: Keyword Kw_ljambda
[line: 49, pos: 37]--[line: 49, pos: 41] lexeme: Keyword Kw_log64
[line: 49, pos: 47]--[line: 49, pos: 54] lexeme: Id линейный
[line: 51, pos: 13]--[line: 51, pos: 16] lexeme: Keyword Kw_log8
[line: 51, pos: 31]--[line: 51, pos: 32] lexeme: Id ля
[line: 51, pos: 44]--[line: 51, pos: 48] lexeme: Keyword Kw_log32
[line: 51, pos: 59]--[line: 51, pos: 63] lexeme: Keyword Kw_log16
[line: 51, pos: 64]--[line: 51, pos: 64] lexeme: Nothing

Total errors: 0.

Total warnings: 0
