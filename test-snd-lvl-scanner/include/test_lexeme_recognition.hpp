/*
    File:    test_lexeme_recognition.hpp
    Created: 11 July 2021 at 10:10 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef TEST_LEXEME_RECOGNITION_H
#define TEST_LEXEME_RECOGNITION_H
namespace snd_lvl_testing_scanner{
    void test_lexeme_recognition();
};
#endif