/*
    File:    testing-func.hpp
    Created: 11 July 2021 at 12:35 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TESTING_FUNC_H
#define TESTING_FUNC_H
#   include <memory>
#   include "../../snd-lvl-scanner/include/arkona-snd-lvl-scanner.hpp"
namespace snd_lvl_testing_scanner{
    void test_func(const std::shared_ptr<snd_level_scanner::Scanner>& arkonasc);
};
#endif