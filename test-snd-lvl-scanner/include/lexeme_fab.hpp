/*
    File:    lexeme_fab.hpp
    Created: 10 July 2021 at 13:34 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef LEXEME_FAB_H
#define LEXEME_FAB_H
#   include <cstddef>
#   include "../../snd-lvl-scanner/include/lexeme.hpp"
namespace snd_lvl_testing_scanner{
    using Lexeme         = snd_level_scanner::LexemeInfo;
    using Keyword_kind   = snd_level_scanner::Keyword_kind;
    using Lexem_kind     = snd_level_scanner::Lexem_kind;
    using Float_kind     = snd_level_scanner::Float_kind;
    using Complex_kind   = snd_level_scanner::Complex_kind;
    using Quat_kind      = snd_level_scanner::Quat_kind;
    using Delimiter_kind = snd_level_scanner::Delimiter_kind;
    using quat128        = quat::quat_t<__float128>;
    using String_kind    = snd_level_scanner::String_kind;
    using Char_kind      = snd_level_scanner::Char_kind;

    //! Functions that return special lexemes.
    const Lexeme unknown_lexeme();
    const Lexeme nothing_lexeme();

    //! Function that return keyword.
    const Lexeme keyword_lexeme(Keyword_kind kw);

    //! Function that return identifier.
    const Lexeme id_lexeme(std::size_t idx);

    //! Function that return character lexeme.
    const Lexeme char_lexeme(char32_t c, Char_kind char_kind = Char_kind::Char32);

    //! Function that return string lexeme.
    const Lexeme string_lexeme(size_t      str_idx,
                               String_kind str_kind = String_kind::String32);

    //! Function that return integer lexeme.
    const Lexeme integer_lexeme(unsigned __int128 int_val);

    //! Function that return float lexeme.
    const Lexeme float_lexeme(__float128 float_val,
                              Float_kind precision = Float_kind::Float64);

    //! Function that return complex lexeme.
    const Lexeme complex_lexeme(__complex128 complex_val,
                                Complex_kind precision = Complex_kind::Complex64);

    //! Function that return quaternion lexeme.
    const Lexeme quat_lexeme(const quat128& quat_val,
                             Quat_kind      precision = Quat_kind::Quat64);

    //! Function that return delimiter lexeme.
    const Lexeme delim_lexeme(Delimiter_kind delim);
};
#endif