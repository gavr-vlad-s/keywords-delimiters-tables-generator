/*
    File:    test_lexeme_to_string.hpp
    Created: 10 July 2021 at 13:27 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef TEST_LEXEME_TO_STRING_H
#define TEST_LEXEME_TO_STRING_H
namespace snd_lvl_testing_scanner{
    void test_lexeme_to_string();
};
#endif