/*
 * It happens that in std::map<K,V> the key type is integer, and a lot of keys with the
 * same corresponding values. If such a map must be a generated constant, then this map
 * can be optimized. Namely, iterating through a map using range-based for, we will
 * build a std::vector<std::pair<K, V>>. Then we group pairs std::pair<K, V> in pairs
 * in the form (segment, a value of type V), where 'segment' is a struct consisting of
 * lower bound and upper bound. Next, we permute the grouped pair in the such way that
 * in order to search for in the array of the resulting values we can use the algorithm
 * from the answer to exercise 6.2.24 of the book
 * Knuth D.E. The art of computer programming. Volume 3. Sorting and search. ---
 * 2nd ed. --- Addison-Wesley, 1998.
*/

#define RandomAccessIterator typename
#define Callable             typename
#define Integral             typename
template<typename T>
struct Segment{
    T lower_bound;
    T upper_bound;

    Segment()               = default;
    Segment(const Segment&) = default;
    ~Segment()              = default;
};

template<typename T, typename V>
struct Segment_with_value{
    Segment<T> bounds;
    V          value;

    Segment_with_value()                          = default;
    Segment_with_value(const Segment_with_value&) = default;
    ~Segment_with_value()                         = default;
};

/* This function uses algorithm from the answer to the exercise 6.2.24 of the monography
 *  Knuth D.E. The art of computer programming. Volume 3. Sorting and search. --- 2nd ed.
 *  --- Addison-Wesley, 1998.
*/
template<RandomAccessIterator I, typename K>
std::pair<bool, size_t> knuth_find(I it_begin, I it_end, K key)
{
    std::pair<bool, size_t> result = {false, 0};
    size_t                  i      = 1;
    size_t                  n      = it_end - it_begin;
    while(i <= n)
    {
        const auto& curr        = it_begin[i - 1];
        const auto& curr_bounds = curr.bounds;
        if(key < curr_bounds.lower_bound){
            i = 2 * i;
        }else if(key > curr_bounds.upper_bound){
            i = 2 * i + 1;
        }else{
            result.first = true; result.second = i - 1;
            break;
        }
    }
    return result;
}

static const Segment_with_value<char32_t, uint64_t> categories_table[] = {
    {{U'b'   , U'b'   },  2182      },  {{U'2'   , U'7'   },  546820    },  
    {{U'а'   , U'е'   },  14        },  {{U'('   , U'*'   },  16        },  
    {{U'G'   , U'N'   },  6         },  {{U'o'   , U'o'   },  262       },  
    {{U'ц'   , U'ш'   },  14        },  {{U'#'   , U'#'   },  16        },  
    {{U'.'   , U'.'   },  1048592   },  {{U'B'   , U'B'   },  2182      },  
    {{U'Y'   , U'Z'   },  6         },  {{U'f'   , U'f'   },  264198    },  
    {{U'x'   , U'x'   },  262214    },  {{U'к'   , U'т'   },  14        },  
    {{U'ў'   , U'ҁ'   },  6         },  {{U'!'   , U'!'   },  16        },  
    {{U'%'   , U'&'   },  16        },  {{U','   , U','   },  16        },  
    {{U'0'   , U'0'   },  63492     },  {{U':'   , U'@'   },  16        },  
    {{U'E'   , U'E'   },  67590     },  {{U'P'   , U'W'   },  6         },  
    {{U'_'   , U'_'   },  6         },  {{U'd'   , U'd'   },  264198    },  
    {{U'i'   , U'k'   },  2097158   },  {{U'q'   , U'q'   },  262150    },  
    {{U'{'   , U'~'   },  16        },  {{U'и'   , U'и'   },  14        },  
    {{U'ф'   , U'ф'   },  14        },  {{U'э'   , U'э'   },  14        },  
    {{U'Ҋ'   , U'ӿ'   },  6         },  {{U'\x01', U' '   },  1         },  
    {{U'\"'  , U'\"'  },  32        },  {{U'$'   , U'$'   },  1024      },  
    {{U'\''  , U'\''  },  512       },  {{U'+'   , U'+'   },  131088    },  
    {{U'-'   , U'-'   },  131088    },  {{U'/'   , U'/'   },  16        },  
    {{U'1'   , U'1'   },  555012    },  {{U'8'   , U'9'   },  542724    },  
    {{U'A'   , U'A'   },  2054      },  {{U'C'   , U'D'   },  2054      },  
    {{U'F'   , U'F'   },  2054      },  {{U'O'   , U'O'   },  262       },  
    {{U'X'   , U'X'   },  70        },  {{U'['   , U'^'   },  16        },  
    {{U'a'   , U'a'   },  2054      },  {{U'c'   , U'c'   },  2054      },  
    {{U'e'   , U'e'   },  67590     },  {{U'g'   , U'h'   },  6         },  
    {{U'l'   , U'n'   },  6         },  {{U'p'   , U'p'   },  6         },  
    {{U'r'   , U'w'   },  6         },  {{U'y'   , U'z'   },  6         },  
    {{U'Ѐ'   , U'Я'   },  6         },  {{U'ж'   , U'з'   },  6         },  
    {{U'й'   , U'й'   },  6         },  {{U'у'   , U'у'   },  6         },  
    {{U'х'   , U'х'   },  6         },  {{U'щ'   , U'ь'   },  6         },  
    {{U'ю'   , U'ќ'   },  6         }
};

static constexpr size_t num_of_elems_in_categories_table = 61;

uint64_t get_categories_set(char32_t c)
{
    auto t = knuth_find(categories_table,
                        categories_table + num_of_elems_in_categories_table,
                        c);
    return t.first ?
           categories_table[t.second].value :
           (1ULL << static_cast<uint64_t>(Category::Other));
}
