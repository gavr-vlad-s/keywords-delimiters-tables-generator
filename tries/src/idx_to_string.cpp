/*
    File:    idx_to_string.cpp
    Created: 21 March 2021 at 15:16 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/idx_to_string.hpp"
#include "../../char-conv/include/char_conv.hpp"
std::string idx_to_string(const std::shared_ptr<Char_trie>& t,
                          size_t                            idx,
                          std::string                       default_value)
{
    auto        u32str = t->get_string(idx);
    std::string s      = u32string_to_utf8(u32str);
    return idx ? s : default_value;
}