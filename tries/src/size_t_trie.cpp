/*
    File:    size_t_trie.cpp
    Created: 04 September 2021 at 15:59 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#include "../include/size_t_trie.hpp"

std::set<std::size_t> Size_t_trie::get_set(std::size_t idx)
{
    std::set<std::size_t> s;
    std::size_t current = idx;
    for( ; current; current = node_buffer[current].parent){
        s.insert(node_buffer[current].c);
    }
    return s;
}

std::size_t Size_t_trie::insert(const std::set<std::size_t>& s)
{
    std::basic_string<std::size_t> str;
    for(std::size_t x : s){
        str.push_back(x);
    }
    std::size_t set_idx = Trie<std::size_t>::insert(str);
    return set_idx;
}