/*
    File:    errors_and_tries.hpp
    Created: 11 April 2021 at 17:44 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef ERRORS_AND_TRIES_H
#define ERRORS_AND_TRIES_H
#   include <memory>
#   include "../include/error_count.hpp"
#   include "../include/char_trie.hpp"
#   include "../include/warning_count.hpp"

struct Errors_and_tries{
    Errors_and_tries()                                    = default;
    ~Errors_and_tries()                                   = default;
    Errors_and_tries(const Errors_and_tries&)             = default;
    Errors_and_tries& operator=(const Errors_and_tries&)  = default;
    Errors_and_tries& operator=(Errors_and_tries&&)       = default;

    void print() const;

    std::shared_ptr<Error_count>   ec_;
    std::shared_ptr<Warning_count> wc_;
    std::shared_ptr<Char_trie>     ids_trie_;
    std::shared_ptr<Char_trie>     strs_trie_;
};
#endif