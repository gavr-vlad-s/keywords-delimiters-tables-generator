/*
    File:    idx_to_string.hpp
    Created: 21 March 2021 at 15:13 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef IDX_TO_STRING_H
#define IDX_TO_STRING_H

#include <memory>
#include <string>
#include "../include/char_trie.hpp"
/**
 *  \param [in] t    pointer to prefix tree
 *  \param [in] idx  index of string in the prefix tree t
 *
 *  \return          string corresponding to the index idx
 *  */
std::string idx_to_string(const std::shared_ptr<Char_trie>& t,
                          size_t                            idx,
                          std::string                       default_value = std::string());
#endif