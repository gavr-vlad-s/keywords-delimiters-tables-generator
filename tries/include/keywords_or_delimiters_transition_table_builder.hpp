/*
    File:    keywords_or_delimiters_transition_table_builder.hpp
    Created: 20 March 2021 at 12:21 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef KEYWORDS_OR_DELIMITERS_TRANSITION_TABLE_BUILDER_H
#define KEYWORDS_OR_DELIMITERS_TRANSITION_TABLE_BUILDER_H
#   include <cstddef>
#   include <map>
#   include <memory>
#   include <set>
#   include <string>
#   include <vector>
#   include "../include/char_trie.hpp"
#   include "../include/attributed_trie.hpp"

struct Lexeme_with_code
{
    std::u32string lexeme_;
    std::u32string code_;
};

enum class Table_kind
{
    Keyword, Delimiter
};

struct Info_for_transition_table
{
    std::vector<Lexeme_with_code> lexemes_and_codes_;
    std::u32string                additions_for_header_;
    std::u32string                additions_for_impl_;
    Table_kind                    kind_;
    std::u32string                namespace_name_;
    std::u32string                author_;
    std::vector<std::u32string>   emails_;
    std::u32string                lexem_info_type_name_;
    std::u32string                licence_id_;
};

struct Transition_table
{
    std::string              main_header_content_;
    std::string              main_header_name_;
    std::string              main_impl_content_;
    std::string              main_impl_name_;
    std::set<std::u32string> additional_codes_;
};

struct Auxiliary_files_content
{
    std::map<std::string, std::string> includes_;
    std::map<std::string, std::string> srcs_;
};

struct Info_for_enum_creating
{
    std::vector<std::u32string> codes_;
    Table_kind                  kind_;
    std::u32string              namespace_name_;
    std::u32string              author_;
    std::vector<std::u32string> emails_;
    std::u32string              licence_id_;
    std::u32string              underlying_type_name_;
};

struct To_string_info
{
    std::string header_content_;
    std::string header_name_;
    std::string impl_content_;
    std::string impl_name_;
};

class Transition_table_builder {
public:
    Transition_table_builder()
        : char_trie_{std::make_shared<Char_trie>()}
        , attributed_trie_{std::make_shared<Attributed_trie<char32_t, size_t>>()}
    {
    }

    Transition_table_builder(const Transition_table_builder& rhs) = default;
    ~Transition_table_builder()                                   = default;

    Transition_table operator()(const Info_for_transition_table& info);

    Auxiliary_files_content build_auxiliary_files_content(const std::string& namespace_name) const;

    std::string build_enum_for_codes(const Info_for_enum_creating& info);

    To_string_info build_enum_to_string_conversion_function(const Info_for_enum_creating& info) const;
private:
    std::shared_ptr<Char_trie>                         char_trie_;
    std::shared_ptr<Attributed_trie<char32_t, size_t>> attributed_trie_;
};
#endif