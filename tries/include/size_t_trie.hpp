/*
    File:    size_t_trie.hpp
    Created: 04 September 2021 at 15:53 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef SIZE_T_TRIE_H
#define SIZE_T_TRIE_H
#   include <cstddef>
#   include <set>
#   include "../include/trie.hpp"
/*! Данный класс предназначен для хранения множеств состояний недетерминированных конечных
 *  автоматов. Каждое такое множество в качестве внутреннего представления будет иметь
 *  строку из состояний, каждое из которых, в свой черёд, представляется своим номером,
 *  являющимся значением типа size_t. */
class Size_t_trie : public Trie<std::size_t> {
public:
    virtual ~Size_t_trie()               = default;
    Size_t_trie()                        = default;
    Size_t_trie(const Size_t_trie& orig) = default;

    /*! Функция get_set по индексу idx множества состояний строит это же множество, но уже как
     * std::set<std::size_t> . */
    std::set<std::size_t> get_set(std::size_t idx);

    /* Данная функция записывает множество, состоящее из элементов типа size_t,
     * в префиксное дерево таких множеств, и возвращает получившийся индекс. */
    std::size_t insert(const std::set<std::size_t>& s);
private:
};
#endif