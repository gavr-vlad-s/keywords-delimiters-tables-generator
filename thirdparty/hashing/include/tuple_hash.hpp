/*
    File:    tuple_hash.hpp
    Created: 02 January 2022 at 14:07 MSK
    Author:  see URI in this file
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef TUPLE_HASH_H
#define TUPLE_HASH_H
// This code was taken from https://stackoverflow.com/questions/7110301/generic-hash-for-tuples-in-unordered-map-unordered-set/7115547#7115547
#   include <cinttypes>
#   include <cstddef>
#   include <functional>
#   include <tuple>
namespace hashing{
    template <typename TT>
    struct hash
    {
        size_t operator()(TT const& tt) const
        {
            return std::hash<TT>()(tt);
        }
    };

    namespace{
        template <class T>
        inline void hash_combine(std::size_t& seed, T const& v)
        {
            seed ^= hashing::hash<T>()(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        }

        // Recursive template code derived from Matthieu M.
        template <class Tuple, size_t Index = std::tuple_size<Tuple>::value - 1>
        struct HashValueImpl
        {
            static void apply(size_t& seed, const Tuple& tuple)
            {
            HashValueImpl<Tuple, Index-1>::apply(seed, tuple);
            hash_combine(seed, std::get<Index>(tuple));
            }
        };

        template <class Tuple>
        struct HashValueImpl<Tuple,0>
        {
            static void apply(size_t& seed, const Tuple& tuple)
            {
                hash_combine(seed, std::get<0>(tuple));
            }
        };
    }

    template <typename ... TT>
    struct hash<std::tuple<TT...>>
    {
        size_t operator()(const std::tuple<TT...>& tt) const
        {
            size_t seed = 0;
            HashValueImpl<std::tuple<TT...> >::apply(seed, tt);
            return seed;
        }
    };

    template <typename ... TT>
    size_t calc_hash(const std::tuple<TT...>& tt)
    {
        return hash<std::tuple<TT...>>{}(tt);
    }
}
#endif