/*
    File:    pair_hash.hpp
    Created: 01 January 2022 at 10:32 MSK
    Author:  Unknown
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef PAIR_HASH_H
#define PAIR_HASH_H
#   include <cstddef>
#   include <functional>
#   include <utility>
// The code below was taken from https://stackoverflow.com/questions/28367913/how-to-stdhash-an-unordered-stdpair
template<typename T>
void hash_combine(std::size_t& seed, const T& key)
{
    std::hash<T> hasher;
    seed ^= hasher(key) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

namespace std {
    template<typename T1, typename T2>
    struct hash<std::pair<T1, T2>> {
        std::size_t operator()(const std::pair<T1, T2>& p) const
        {
            std::size_t seed(0);
            ::hash_combine(seed, p.first);
            ::hash_combine(seed, p.second);
            return seed;
        }
    };
}
#endif