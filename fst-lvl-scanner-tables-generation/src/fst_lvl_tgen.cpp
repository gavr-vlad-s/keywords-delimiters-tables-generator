/*
    File:    fst_lvl_tgen.cpp
    Created: 03 June 2021 at 10:46 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/fst_lvl_kw_delim_table_gen.hpp"
#include "../include/classification_table_gen.hpp"
#include <iostream>
#include <fstream>
#include <string>

static const std::string scanner_include_dir = "./fst-lvl-scanner/include/";
static const std::string scanner_src_dir     = "./fst-lvl-scanner/src/";

int main()
{
    const auto kw_delim_tables = build_kw_delim_tables();

    for(auto&& [name, content] : kw_delim_tables.includes_)
    {
        const auto file_name = scanner_include_dir + name;
        std::ofstream fout{file_name};
        fout << content;
    }

    for(auto&& [name, content] : kw_delim_tables.srcs_)
    {
        const auto file_name = scanner_src_dir + name;
        std::ofstream fout{file_name};
        fout << content;
    }

    const auto classification_table = build_classification_table(kw_delim_tables.delimiter_begin_chars_,
                                                                 kw_delim_tables.keyword_begin_chars_);
    {
        const auto file_name = scanner_include_dir + "category.hpp";
        std::ofstream fout{file_name};
        fout << classification_table.category_header_;
    }
    {
        const std::string file_name = "table.hpp";
        std::ofstream fout{file_name};
        fout << classification_table.table_;
    }
    return 0;
}