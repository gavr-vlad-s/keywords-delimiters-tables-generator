/*
    File:    fst_lvl_kw_delim_table_gen.cpp
    Created: 03 June 2021 at 11:54 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/fst_lvl_kw_delim_table_gen.hpp"
#include "../../tries/include/keywords_or_delimiters_transition_table_builder.hpp"
#include <vector>
#include <set>

namespace
{
    const std::vector<Lexeme_with_code> kw_info = {
        {U"абстрактная",   U"Kw_abstraktnaja"   },
        {U"байт",          U"Kw_bajt"           }, {U"беззн",         U"Kw_bezzn"          },
        {U"беззн8",        U"Kw_bezzn8"         }, {U"беззн16",       U"Kw_bezzn16"        },
        {U"беззн32",       U"Kw_bezzn32"        }, {U"беззн64",       U"Kw_bezzn64"        },
        {U"беззн128",      U"Kw_bezzn128"       }, {U"больше",        U"Kw_bolshe"         },
        {U"большое",       U"Kw_bolshoe"        }, {U"в",             U"Kw_v"              },
        {U"вечно",         U"Kw_vechno"         }, {U"вещ",           U"Kw_veshch"         },
        {U"вещ32",         U"Kw_veshch32"       }, {U"вещ64",         U"Kw_veshch64"       },
        {U"вещ80",         U"Kw_veshch80"       }, {U"вещ128",        U"Kw_veshch128"      },
        {U"возврат",       U"Kw_vozvrat"        }, {U"выбери",        U"Kw_vyberi"         },
        {U"выдели",        U"Kw_vydeli"         }, {U"выйди",         U"Kw_vyjdi"          },
        {U"головная",      U"Kw_golovnaya"      }, {U"диапазон",      U"Kw_diapazon"       },
        {U"для",           U"Kw_dlya"           }, {U"если",          U"Kw_esli"           },
        {U"из",            U"Kw_iz"             }, {U"иначе",         U"Kw_inache"         },
        {U"инес",          U"Kw_ines"           }, {U"использует",    U"Kw_ispolzuet"      },
        {U"истина",        U"Kw_istina"         }, {U"как",           U"Kw_kak"            },
        {U"категория",     U"Kw_kategorija"     }, {U"кват",          U"Kw_kvat"           },
        {U"кват32",        U"Kw_kvat32"         }, {U"кват64",        U"Kw_kvat64"         },
        {U"кват80",        U"Kw_kvat80"         }, {U"кват128",       U"Kw_kvat128"        },
        {U"компл",         U"Kw_kompl"          }, {U"компл32",       U"Kw_kompl32"        },
        {U"компл64",       U"Kw_kompl64"        }, {U"компл80",       U"Kw_kompl80"        },
        {U"компл128",      U"Kw_kompl128"       }, {U"конст",         U"Kw_konst"          },
        {U"константы",     U"Kw_konstanty"      }, {U"лог",           U"Kw_log"            },
        {U"лог8",          U"Kw_log8"           }, {U"лог16",         U"Kw_log16"          },
        {U"лог32",         U"Kw_log32"          }, {U"лог64",         U"Kw_log64"          },
        {U"ложь",          U"Kw_lozh"           }, {U"лямбда",        U"Kw_ljambda"        },
        {U"маленькое",     U"Kw_malenkoe"       }, {U"массив",        U"Kw_massiv"         },
        {U"меньше",        U"Kw_menshe"         }, {U"мета",          U"Kw_meta"           },
        {U"модуль",        U"Kw_modul"          }, {U"настройка",     U"Kw_nastrojka"      },
        {U"ничто",         U"Kw_nichto"         }, {U"обобщение",     U"Kw_obobshchenie"   },
        {U"обобщённая",    U"Kw_obobshchjonnaja"}, {U"операция",      U"Kw_operatsija"     },
        {U"освободи",      U"Kw_osvobodi"       }, {U"пакет",         U"Kw_paket"          },
        {U"пакетная",      U"Kw_paketnaja"      }, {U"паук",          U"Kw_pauk"           },
        {U"перем",         U"Kw_perem"          }, {U"перечисл_множ", U"Kw_perechisl_mnozh"},
        {U"перечисление",  U"Kw_perechislenie"  }, {U"повторяй",      U"Kw_povtorjaj"      },
        {U"пока",          U"Kw_poka"           }, {U"покуда",        U"Kw_pokuda"         },
        {U"порядок",       U"Kw_porjadok"       }, {U"порядок8",      U"Kw_porjadok8"      },
        {U"порядок16",     U"Kw_porjadok16"     }, {U"порядок32",     U"Kw_porjadok32"     },
        {U"порядок64",     U"Kw_porjadok64"     }, {U"постфиксная",   U"Kw_postfiksnaja"   },
        {U"преобразуй",    U"Kw_preobrazuj"     }, {U"префиксная",    U"Kw_prefiksnaja"    },
        {U"продолжи",      U"Kw_prodolzhi"      }, {U"псевдоним",     U"Kw_psevdonim"      },
        {U"пусто",         U"Kw_pusto"          }, {U"равно",         U"Kw_ravno"          },
        {U"разбор",        U"Kw_razbor"         }, {U"рассматривай",  U"Kw_rassmatrivaj"   },
        {U"реализация",    U"Kw_realizatsija"   }, {U"реализуй",      U"Kw_realizuj"       },
        {U"само",          U"Kw_samo"           }, {U"сброс",         U"Kw_sbros"          },
        {U"симв",          U"Kw_simv"           }, {U"симв8",         U"Kw_simv8"          },
        {U"симв16",        U"Kw_simv16"         }, {U"симв32",        U"Kw_simv32"         },
        {U"сравни_с",      U"Kw_sravni_s"       }, {U"ссылка",        U"Kw_ssylka"         },
        {U"строка",        U"Kw_stroka"         }, {U"строка8",       U"Kw_stroka8"        },
        {U"строка16",      U"Kw_stroka16"       }, {U"строка32",      U"Kw_stroka32"       },
        {U"структура",     U"Kw_struktura"      }, {U"тип",           U"Kw_tip"            },
        {U"типы",          U"Kw_tipy"           }, {U"то",            U"Kw_to"             },
        {U"функция",       U"Kw_funktsija"      }, {U"чистая",        U"Kw_chistaja"       },
        {U"цел",           U"Kw_tsel"           }, {U"цел8",          U"Kw_tsel8"          },
        {U"цел16",         U"Kw_tsel16"         }, {U"цел32",         U"Kw_tsel32"         },
        {U"цел64",         U"Kw_tsel64"         }, {U"цел128",        U"Kw_tsel128"        },
        {U"шкала",         U"Kw_shkala"         }, {U"шкалу",         U"Kw_shkalu"         },
        {U"эквив",         U"Kw_ekviv"          }, {U"экспорт",       U"Kw_eksport"        },
        {U"элемент",       U"Kw_element"        },
    };

    const std::vector<Lexeme_with_code> delim_info = {
        {UR"~(!)~",     U"Logical_not"                }, {UR"~(!&&)~",   U"Logical_and_not"            },
        {UR"~(!&&.)~",  U"Logical_and_not_full"       }, {UR"~(!&&.=)~", U"Logical_and_not_full_assign"},
        {UR"~(!&&=)~",  U"Logical_and_not_assign"     }, {UR"~(!=)~",    U"NEQ"                        },
        {UR"~(!||)~",   U"Logical_or_not"             }, {UR"~(!||.)~",  U"Logical_or_not_full"        },
        {UR"~(!||.=)~", U"Logical_or_not_full_assign" }, {UR"~(!||=)~",  U"Logical_or_not_assign"      },
        {UR"~(#)~",     U"Sharp"                      }, {UR"~(##)~",    U"Data_size"                  },
        {UR"~(%)~",     U"Remainder"                  }, {UR"~(%.)~",    U"Float_remainder"            },
        {UR"~(%.=)~",   U"Float_remainder_assign"     }, {UR"~(%=)~",    U"Remainder_assign"           },
        {UR"~(&)~",     U"Bitwise_and"                }, {UR"~(&&)~",    U"Logical_and"                },
        {UR"~(&&.)~",   U"Logical_and_full"           }, {UR"~(&&.=)~",  U"Logical_and_full_assign"    },
        {UR"~(&&=)~",   U"Logical_and_assign"         }, {UR"~(&=)~",    U"Bitwise_and_assign"         },
        {UR"~(()~",     U"Round_bracket_opened"       }, {UR"~((:)~",    U"Tuple_begin"                },
        {UR"~())~",     U"Round_bracket_closed"       }, {UR"~(*)~",     U"Mul"                        },
        {UR"~(**)~",    U"Power"                      }, {UR"~(**.)~",   U"Float_power"                },
        {UR"~(**.=)~",  U"Float_power_assign"         }, {UR"~(**=)~",   U"Power_assign"               },
        {UR"~(*/)~",    U"Comment_end"                }, {UR"~(*=)~",    U"Mul_assign"                 },
        {UR"~(+)~",     U"Plus"                       }, {UR"~(++)~",    U"Inc"                        },
        {UR"~(++<)~",   U"Inc_with_wrap"              }, {UR"~(+=)~",    U"Plus_assign"                },
        {UR"~(,)~",     U"Comma"                      }, {UR"~(-)~",     U"Minus"                      },
        {UR"~(--)~",    U"Dec"                        }, {UR"~(--<)~",   U"Dec_with_wrap"              },
        {UR"~(-=)~",    U"Minus_assign"               }, {UR"~(->)~",    U"Right_arrow"                },
        {UR"~(.)~",     U"Dot"                        }, {UR"~(..)~",    U"Range"                      },
        {UR"~(...)~",   U"Range_excluded_end"         }, {UR"~(.|.)~",   U"Algebraic_separator"        },
        {UR"~(.|.=)~",  U"Algebraic_separator_assign" }, {UR"~(/)~",     U"Div"                        },
        {UR"~(/*)~",    U"Comment_begin"              }, {UR"~(/=)~",    U"Div_assign"                 },
        {UR"~(/\)~",    U"Symmetric_difference"       }, {UR"~(/\=)~",   U"Symmetric_difference_assign"},
        {UR"~(:)~",     U"Colon"                      }, {UR"~(:))~",    U"Tuple_end"                  },
        {UR"~(::)~",    U"Scope_resolution"           }, {UR"~(:=)~",    U"Copy"                       },
        {UR"~(:>)~",    U"Meta_bracket_closed"        }, {UR"~(:])~",    U"Array_literal_end"          },
        {UR"~(:})~",    U"Set_literal_end"            }, {UR"~(;)~",     U"Semicolon"                  },
        {UR"~(<)~",     U"LT"                         }, {UR"~(<!!>)~",  U"Common_template_type"       },
        {UR"~(<!>)~",   U"Template_type"              }, {UR"~(<&&>)~",  U"Data_address"               },
        {UR"~(<&>)~",   U"Address"                    }, {UR"~(<+>)~",   U"Type_add"                   },
        {UR"~(<+>=)~",  U"Type_add_assign"            }, {UR"~(<-)~",    U"Left_arrow"                 },
        {UR"~(<:)~",    U"Meta_bracket_opened"        }, {UR"~(<:>)~",   U"Common_type"                },
        {UR"~(<<)~",    U"Left_shift"                 }, {UR"~(<<<)~",   U"Cyclic_left_shift"          },
        {UR"~(<<<=)~",  U"Cyclic_left_shift_assign"   }, {UR"~(<<=)~",   U"Left_shift_assign"          },
        {UR"~(<=)~",    U"LEQ"                        }, {UR"~(<?>)~",   U"Get_expr_type"              },
        {UR"~(<??>)~",  U"Get_elem_type"              }, {UR"~(<|)~",    U"Label_prefix"               },
        {UR"~(=)~",     U"Assign"                     }, {UR"~(==)~",    U"EQ"                         },
        {UR"~(>)~",     U"GT"                         }, {UR"~(>=)~",    U"GEQ"                        },
        {UR"~(>>)~",    U"Right_shift"                }, {UR"~(>>=)~",   U"Right_shift_assign"         },
        {UR"~(>>>)~",   U"Cyclic_right_shift"         }, {UR"~(>>>=)~",  U"Cyclic_right_shift_assign"  },
        {UR"~(?)~",     U"Cond_op"                    }, {UR"~(?.)~",    U"Cond_op_full"               },
        {UR"~(@)~",     U"At"                         }, {UR"~([)~",     U"Square_bracket_opened"      },
        {UR"~([:)~",    U"Array_literal_begin"        }, {UR"~(\)~",     U"Set_difference"             },
        {UR"~(\=)~",    U"Set_difference_assign"      }, {UR"~(])~",     U"Square_bracket_closed"      },
        {UR"~(^)~",     U"Bitwise_xor"                }, {UR"~(^=)~",    U"Bitwise_xor_assign"         },
        {UR"~(^^)~",    U"Logical_xor"                }, {UR"~(^^=)~",   U"Logical_xor_assign"         },
        {UR"~({)~",     U"Figure_bracket_opened"      }, {UR"~({..})~",  U"Pattern"                    },
        {UR"~({:)~",    U"Set_literal_begin"          }, {UR"~(|)~",     U"Bitwise_or"                 },
        {UR"~(|#|)~",   U"Cardinality"                }, {UR"~(|=)~",    U"Bitwise_or_assign"          },
        {UR"~(||)~",    U"Logical_or"                 }, {UR"~(||.)~",   U"Logical_or_full"            },
        {UR"~(||.=)~",  U"Logical_or_full_assign"     }, {UR"~(||=)~",   U"Logical_or_assign"          },
        {UR"~(})~",     U"Figure_bracket_closed"      }, {UR"~(~)~",     U"Bitwise_not"                },
        {UR"~(~&)~",    U"Bitwise_and_not"            }, {UR"~(~&=)~",   U"Bitwise_and_not_assign"     },
        {UR"~(~|)~",    U"Bitwise_or_not"             }, {UR"~(~|=)~",   U"Bitwise_or_not_assign"      },
        {UR"~(|:)~",    U"Bitscale_literal_begin"     }, {UR"~(:|)~",    U"Bitscale_literal_end"       },
        {UR"~(<#)~",    U"Minimal_value_in_collection"}, {UR"~(#>)~",    U"Maximal_value_in_collection"},
        {UR"~(<<#)~",   U"Minimal_value_of_type"      }, {UR"~(#>>)~",   U"Maximal_value_of_type"      },
    };

    std::u32string get_first_chars_of_lexemes(const std::vector<Lexeme_with_code>& info)
    {
        std::set<char32_t> fst_chars_set;
        for(auto&& [lexeme_str, _] : info)
        {
            fst_chars_set.insert(lexeme_str[0]);
        }

        std::u32string result;
        for(char32_t c : fst_chars_set)
        {
            result += c;
        }
        return result;
    }

    Transition_table build_keyword_table()
    {
        Info_for_transition_table info;

        info.lexemes_and_codes_    = kw_info;
        info.additions_for_header_ = UR"~(#include "../include/lexeme.hpp")~";
        info.additions_for_impl_   = U"";
        info.kind_                 = Table_kind::Keyword;
        info.namespace_name_       = U"fst_level_scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                        U"gavvs1977@yandex.ru"
                                     };
        info.licence_id_           = U"GPLv3";
        info.lexem_info_type_name_ = U"LexemeInfo";

        Transition_table_builder  table_builder;

        return table_builder(info);
    }

    Transition_table build_delimiter_table()
    {
        Info_for_transition_table info;

        info.lexemes_and_codes_    = delim_info;
        info.additions_for_header_ = UR"~(#include "../include/lexeme.hpp")~";
        info.additions_for_impl_   = U"";
        info.kind_                 = Table_kind::Delimiter;
        info.namespace_name_       = U"fst_level_scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                        U"gavvs1977@yandex.ru"
                                     };
        info.licence_id_           = U"GPLv3";
        info.lexem_info_type_name_ = U"LexemeInfo";

        Transition_table_builder  table_builder;

        return table_builder(info);
    }

    Auxiliary_files_content build_aux_files_content()
    {
        Transition_table_builder  table_builder;
        return table_builder.build_auxiliary_files_content("fst_level_scanner");
    }

    std::string build_enum_header_for_keyword_kinds()
    {
        std::vector<std::u32string> codes;
        for(const auto& [_, code] : kw_info)
        {
            codes.push_back(code);
        }

        Info_for_enum_creating info;
        info.codes_                = codes;
        info.kind_                 = Table_kind::Keyword;
        info.namespace_name_       = U"fst_level_scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                        U"gavvs1977@yandex.ru"
                                     };
        info.licence_id_           = U"GPLv3";
        info.underlying_type_name_ = U"uint8_t";

        Transition_table_builder  table_builder;

        return table_builder.build_enum_for_codes(info);
    }

    std::string build_enum_header_for_delimiter_kinds(const std::set<std::u32string>& additional_codes)
    {
        std::vector<std::u32string> codes;
        for(const auto& [_, code] : delim_info)
        {
            codes.push_back(code);
        }

        codes.insert(codes.end(), additional_codes.begin(), additional_codes.end());

        Info_for_enum_creating info;
        info.codes_                = codes;
        info.kind_                 = Table_kind::Delimiter;
        info.namespace_name_       = U"fst_level_scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                        U"gavvs1977@yandex.ru"
                                     };
        info.licence_id_           = U"GPLv3";
        info.underlying_type_name_ = U"uint8_t";

        Transition_table_builder  table_builder;

        return table_builder.build_enum_for_codes(info);
    }

    To_string_info build_keyword_kind_to_string_conversion()
    {
        std::vector<std::u32string> codes;
        for(const auto& [_, code] : kw_info)
        {
            codes.push_back(code);
        }

        Info_for_enum_creating info;
        info.codes_                = codes;
        info.kind_                 = Table_kind::Keyword;
        info.namespace_name_       = U"fst_level_scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                        U"gavvs1977@yandex.ru"
                                     };
        info.licence_id_           = U"GPLv3";
        info.underlying_type_name_ = U"uint8_t";

        Transition_table_builder  table_builder;

        return table_builder.build_enum_to_string_conversion_function(info);
    }

    To_string_info build_delimiter_kind_to_string_conversion(const std::set<std::u32string>& additional_codes)
    {
        std::vector<std::u32string> codes;
        for(const auto& [_, code] : delim_info)
        {
            codes.push_back(code);
        }

        codes.insert(codes.end(), additional_codes.begin(), additional_codes.end());

        Info_for_enum_creating info;
        info.codes_                = codes;
        info.kind_                 = Table_kind::Delimiter;
        info.namespace_name_       = U"fst_level_scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                        U"gavvs1977@yandex.ru"
                                     };
        info.licence_id_           = U"GPLv3";
        info.underlying_type_name_ = U"uint8_t";

        Transition_table_builder  table_builder;

        return table_builder.build_enum_to_string_conversion_function(info);
    }
};

Keyword_and_delimiters_info build_kw_delim_tables()
{
    Keyword_and_delimiters_info result;

    const auto kw_table                                       = build_keyword_table();
    result.includes_[kw_table.main_header_name_]              = kw_table.main_header_content_;
    result.srcs_[kw_table.main_impl_name_]                    = kw_table.main_impl_content_;

    const auto aux_files                                      = build_aux_files_content();
    result.includes_.insert(aux_files.includes_.begin(), aux_files.includes_.end());
    result.srcs_.insert(aux_files.srcs_.begin(), aux_files.srcs_.end());

    result.includes_["keyword_kind.hpp"]                      = build_enum_header_for_keyword_kinds();
    result.keyword_begin_chars_                               = get_first_chars_of_lexemes(kw_info);

    const auto delim_table                                    = build_delimiter_table();
    result.includes_[delim_table.main_header_name_]           = delim_table.main_header_content_;
    result.srcs_[delim_table.main_impl_name_]                 = delim_table.main_impl_content_;

    result.includes_["delimiter_kind.hpp"]                    = build_enum_header_for_delimiter_kinds(delim_table.additional_codes_);
    result.delimiter_begin_chars_                             = get_first_chars_of_lexemes(delim_info);

    const auto keyword_kind_to_str_func                       = build_keyword_kind_to_string_conversion();
    result.includes_[keyword_kind_to_str_func.header_name_]   = keyword_kind_to_str_func.header_content_;
    result.srcs_[keyword_kind_to_str_func.impl_name_]         = keyword_kind_to_str_func.impl_content_;

    const auto delimiter_kind_to_str_func                     = build_delimiter_kind_to_string_conversion(delim_table.additional_codes_);
    result.includes_[delimiter_kind_to_str_func.header_name_] = delimiter_kind_to_str_func.header_content_;
    result.srcs_[delimiter_kind_to_str_func.impl_name_]       = delimiter_kind_to_str_func.impl_content_;

    return result;
}