/*
    File:    classification_table_gen.hpp
    Created: 03 June 2021 at 11:57 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef CLASSIFICATION_TABLE_GEN_H
#define CLASSIFICATION_TABLE_GEN_H
#   include <string>
struct Classification_table_info
{
    std::string category_header_;
    std::string table_;
};

Classification_table_info build_classification_table(const std::u32string& delimiter_begin_chars,
                                                     const std::u32string& keyword_begin_chars);
#endif