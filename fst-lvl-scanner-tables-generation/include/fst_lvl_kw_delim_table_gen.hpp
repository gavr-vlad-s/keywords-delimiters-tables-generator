/*
    File:    fst_lvl_kw_delim_table_gen.hpp
    Created: 03 June 2021 at 11:42 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef FST_LVL_KW_DELIM_TABLE_GEN_H
#define FST_LVL_KW_DELIM_TABLE_GEN_H
#   include <map>
#   include <string>
struct Keyword_and_delimiters_info
{
    std::map<std::string, std::string> includes_;
    std::map<std::string, std::string> srcs_;
    std::u32string                     delimiter_begin_chars_;
    std::u32string                     keyword_begin_chars_;
};

Keyword_and_delimiters_info build_kw_delim_tables();
#endif