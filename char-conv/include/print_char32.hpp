/*
    File:    print_char32.hpp
    Created: 20 March 2021 at 11:40 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef PRINT_CHAR32_H
#define PRINT_CHAR32_H
#include <string>
std::string show_char32(const char32_t c);
void print_char32(const char32_t c);
#endif