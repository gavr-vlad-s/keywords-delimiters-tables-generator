/*
    File:    minimization_test.hpp
    Created: 11 October 2021 at 20:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef MINIMIZATION_TEST_H
#define MINIMIZATION_TEST_H
void minimization_test();
#endif