/*
    File:    print_min_dfa_test.hpp
    Created: 02 October 2021 at 16:24 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef PRINT_MIN_DFA_TEST_H
#define PRINT_MIN_DFA_TEST_H
void print_min_dfa_test();
#endif