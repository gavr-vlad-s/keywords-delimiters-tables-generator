/*
    File:    ndfa_to_dfa_test.hpp
    Created: 26 September 2021 at 11:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef NDFA_TO_DFA_TEST_H
#define NDFA_TO_DFA_TEST_H
void ndfa_to_dfa_test();
#endif