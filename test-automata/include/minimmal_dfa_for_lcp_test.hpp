/*
    File:    minimmal_dfa_for_lcp_test.hpp
    Created: 01 November 2021 at 20:49 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef MINIMMAL_DFA_FOR_LCP_TEST_H
#define MINIMMAL_DFA_FOR_LCP_TEST_H
void minimmal_dfa_for_lcp_test();
#endif