/*
    File:    print_dfa_test.hpp
    Created: 11 September 2021 at 15:46 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef PRINT_DFA_TEST_H
#define PRINT_DFA_TEST_H
void print_dfa_test();
#endif