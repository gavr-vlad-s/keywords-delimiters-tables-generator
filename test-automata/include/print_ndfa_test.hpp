/*
    File:    print_ndfa_test.hpp
    Created: 05 September 2021 at 14:15 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef PRINT_NDFA_TEST_H
#define PRINT_NDFA_TEST_H
void print_ndfa_test();
#endif