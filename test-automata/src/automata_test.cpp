/*
    File:    automata_test.cpp
    Created: 05 September 2021 at 14:19 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#include <cstdio>
#include "../include/print_ndfa_test.hpp"
#include "../include/print_dfa_test.hpp"
#include "../include/ndfa_to_dfa_test.hpp"
#include "../include/print_min_dfa_test.hpp"
#include "../include/minimization_test.hpp"
#include "../include/minimmal_dfa_for_lcp_test.hpp"

int main()
{
    puts("Testing for converting NDFA to std::string...");
    print_ndfa_test();
    puts("*********************************************");
    puts("Testing for converting DFA to std::string...");
    print_dfa_test();
    puts("*********************************************");
    puts("Testing for converting NDFA to DFA...");
    ndfa_to_dfa_test();
    puts("*********************************************");
    puts("Testing for converting minimal DFA to std::string...");
    print_min_dfa_test();
    puts("*********************************************");
    puts("Testing for minimizing DFA...");
    minimization_test();
    puts("*********************************************");
    minimmal_dfa_for_lcp_test();
}