/*
    File:    print_dfa_test.cpp
    Created: 11 September 2021 at 15:49 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#include <algorithm>
#include <cstddef>
#include <iostream>
#include <limits>
#include <string>
#include <utility>
#include "../include/print_dfa_test.hpp"
#include "../../automata/include/dfa.hpp"


using Action_type = std::size_t;
using Char_type   = char;

template<>
struct Generalized_char_traits<Char_type>
{
    static bool lt(Char_type a, Char_type b)
    {
        return a < b;
    }

    static bool eq(Char_type a, Char_type b)
    {
        return a == b;
    }

    static std::string to_string(Char_type a)
    {
        return std::string{a};
    }
};

template<>
struct Action_traits<Action_type>
{
    static std::string to_string(Action_type a)
    {
        return std::to_string(a);
    }

    static Action_type common(Action_type a, Action_type b)
    {
        return std::min(a, b);
    }

    static Action_type neutral()
    {
        return std::numeric_limits<Action_type>::max();
    }
};

using DFA_type = DFA<Char_type, Action_type, Generalized_char_traits<Char_type>>;

using Test_func = std::pair<DFA_type, std::string> (*)();

namespace{
    const auto eps = epsilon<Char_type, Generalized_char_traits<Char_type>>();

    DFA_type dfas[] = {
        // DFA for regexp reke|peve
        {
            // jumps:
            {
                {{0, ordinary_char('r')}, {1, 8}},
                {{0, ordinary_char('p')}, {2, 22}},
                {{1, ordinary_char('e')}, {3, 12}},
                {{2, ordinary_char('e')}, {4, 47}},
                {{3, ordinary_char('k')}, {5, 16}},
                {{4, ordinary_char('v')}, {6, 83}},
                {{5, ordinary_char('e')}, {7, 517}},
                {{6, ordinary_char('e')}, {8, 620}},
            },

            // start state:
            0,

            // number of states:
            9,

            // final states:
            {
                7, 8
            }
        },

        // Other DFA for regexp reke|peve
        {
            // jumps:
            {
                {{0, ordinary_char('r')}, {1, 8}},
                {{0, ordinary_char('p')}, {2, 22}},
                {{1, ordinary_char('e')}, {3, 12}},
                {{2, ordinary_char('e')}, {4, 47}},
                {{3, ordinary_char('k')}, {5, 16}},
                {{4, ordinary_char('v')}, {5, 83}},
                {{5, ordinary_char('e')}, {6, 517}},
            },

            // start state:
            0,

            // number of states:
            7,

            // final states:
            {
                6
            }
        },

        // DFA for regexp l(e(ce)*)?r
        {
            // jumps:
            {
                {{0, ordinary_char('l')}, {1, 8}},
                {{1, ordinary_char('r')}, {3, 12}},
                {{1, ordinary_char('e')}, {2, 132}},
                {{2, ordinary_char('c')}, {4, 47}},
                {{2, ordinary_char('r')}, {3, 47}},
                {{4, ordinary_char('e')}, {5, 83}},
                {{5, ordinary_char('c')}, {4, 517}},
                {{5, ordinary_char('r')}, {3, 620}},
            },

            // start state:
            0,

            // number of states:
            6,

            // final states:
            {
                3
            }
        },

        // Other DFA for regexp l(e(ce)*)?r
        {
            // jumps:
            {
                {{0, ordinary_char('l')}, {1, 8}},
                {{1, ordinary_char('r')}, {3, 12}},
                {{1, ordinary_char('e')}, {2, 132}},
                {{2, ordinary_char('c')}, {4, 47}},
                {{2, ordinary_char('r')}, {3, 47}},
                {{4, ordinary_char('e')}, {2, 83}},
            },

            // start state:
            0,

            // number of states:
            5,

            // final states:
            {
                3
            }
        },

        // DFA for regexp le(ce)*r
        {
            // jumps:
            {
                {{0, ordinary_char('l')}, {1, 8}},
                {{1, ordinary_char('e')}, {2, 12}},
                {{2, ordinary_char('c')}, {3, 47}},
                {{2, ordinary_char('r')}, {4, 47}},
                {{3, ordinary_char('e')}, {5, 47}},
                {{5, ordinary_char('c')}, {3, 517}},
                {{5, ordinary_char('r')}, {4, 620}},
            },

            // start state:
            0,

            // number of states:
            6,

            // final states:
            {
                4
            }
        },

        // Other DFA for regexp le(ce)*r
        {
            // jumps:
            {
                {{0, ordinary_char('l')}, {1, 8}},
                {{1, ordinary_char('e')}, {2, 12}},
                {{2, ordinary_char('c')}, {1, 47}},
                {{2, ordinary_char('r')}, {3, 47}},
            },

            // start state:
            0,

            // number of states:
            4,

            // final states:
            {
                3
            }
        },

        // DFA for regexp ve|vsle(ce)*r
        {
            // jumps:
            {
                {{0, ordinary_char('v')}, {1, 8}},
                {{1, ordinary_char('s')}, {3, 12}},
                {{1, ordinary_char('e')}, {2, 12}},
                {{3, ordinary_char('l')}, {4, 47}},
                {{4, ordinary_char('e')}, {5, 47}},
                {{5, ordinary_char('r')}, {7, 12}},
                {{5, ordinary_char('c')}, {6, 12}},
                {{6, ordinary_char('e')}, {8, 12}},
                {{8, ordinary_char('r')}, {7, 12}},
                {{8, ordinary_char('c')}, {6, 12}},
            },

            // start state:
            0,

            // number of states:
            9,

            // final states:
            {
                2, 7
            }
        },

        // Other DFA for regexp ve|vsle(ce)*r
        {
            // jumps:
            {
                {{0, ordinary_char('v')}, {1, 8}},
                {{1, ordinary_char('s')}, {3, 12}},
                {{1, ordinary_char('e')}, {2, 12}},
                {{3, ordinary_char('l')}, {4, 47}},
                {{4, ordinary_char('e')}, {5, 47}},
                {{5, ordinary_char('r')}, {2, 12}},
                {{5, ordinary_char('c')}, {4, 12}},
            },

            // start state:
            0,

            // number of states:
            6,

            // final states:
            {
                2
            }
        },

        // DFA for regexp os?e
        {
            // jumps:
            {
                {{0, ordinary_char('o')}, {1, 8}},
                {{1, ordinary_char('s')}, {2, 12}},
                {{1, ordinary_char('e')}, {3, 12}},
                {{2, ordinary_char('e')}, {3, 47}},
            },

            // start state:
            0,

            // number of states:
            4,

            // final states:
            {
                3
            }
        },

        // DFA for regexp lsb
        {
            // jumps:
            {
                {{0, ordinary_char('l')}, {1, 8}},
                {{1, ordinary_char('s')}, {2, 12}},
                {{2, ordinary_char('b')}, {3, 47}},
            },

            // start state:
            0,

            // number of states:
            4,

            // final states:
            {
                3
            }
        },

        // DFA for regexp la?rce
        {
            // jumps:
            {
                {{0, ordinary_char('l')}, {1, 8}},
                {{1, ordinary_char('a')}, {2, 12}},
                {{1, ordinary_char('r')}, {3, 12}},
                {{2, ordinary_char('r')}, {3, 47}},
                {{3, ordinary_char('c')}, {4, 47}},
                {{4, ordinary_char('e')}, {5, 47}},
            },

            // start state:
            0,

            // number of states:
            6,

            // final states:
            {
                5
            }
        },

        // DFA for regexp g(cg)*
        {
            // jumps:
            {
                {{0, ordinary_char('g')}, {1, 8}},
                {{1, ordinary_char('c')}, {2, 12}},
                {{2, ordinary_char('g')}, {3, 47}},
                {{3, ordinary_char('c')}, {2, 12}},
            },

            // start state:
            0,

            // number of states:
            4,

            // final states:
            {
                1, 3
            }
        },

        // Other DFA for regexp g(cg)*
        {
            // jumps:
            {
                {{0, ordinary_char('g')}, {1, 8}},
                {{1, ordinary_char('c')}, {0, 12}},
            },

            // start state:
            0,

            // number of states:
            2,

            // final states:
            {
                1
            }
        },

        //  DFA for regexp i(ci)*de
        {
            // jumps:
            {
                {{0, ordinary_char('i')}, {1, 8}},
                {{1, ordinary_char('c')}, {2, 12}},
                {{1, ordinary_char('d')}, {3, 12}},
                {{2, ordinary_char('i')}, {4, 8}},
                {{3, ordinary_char('e')}, {5, 8}},
                {{4, ordinary_char('c')}, {2, 12}},
                {{4, ordinary_char('d')}, {3, 12}},
            },

            // start state:
            0,

            // number of states:
            6,

            // final states:
            {
                5
            }
        },

        // Other DFA for regexp i(ci)*de
        {
            // jumps:
            {
                {{0, ordinary_char('i')}, {1, 8}},
                {{1, ordinary_char('c')}, {0, 12}},
                {{1, ordinary_char('d')}, {2, 12}},
                {{2, ordinary_char('e')}, {3, 8}},
            },

            // start state:
            0,

            // number of states:
            4,

            // final states:
            {
                3
            }
        },

        //  DFA for regexp m*w|c
        {
            // jumps:
            {
                {{0, ordinary_char('m')}, {1, 8}},
                {{0, ordinary_char('w')}, {2, 8}},
                {{0, ordinary_char('c')}, {3, 8}},
                {{1, ordinary_char('m')}, {1, 8}},
                {{1, ordinary_char('w')}, {2, 8}},
            },

            // start state:
            0,

            // number of states:
            4,

            // final states:
            {
                2, 3
            }
        },

        // Other DFA for regexp m*w|c
        {
            // jumps:
            {
                {{0, ordinary_char('m')}, {1, 8}},
                {{0, ordinary_char('w')}, {2, 8}},
                {{0, ordinary_char('c')}, {2, 8}},
                {{1, ordinary_char('m')}, {1, 8}},
                {{1, ordinary_char('w')}, {2, 8}},
            },

            // start state:
            0,

            // number of states:
            3,

            // final states:
            {
                2
            }
        },

        //  DFA for regexp mle?(ce?)*re
        {
            // jumps:
            {
                {{0, ordinary_char('m')}, {1, 8}},
                {{1, ordinary_char('l')}, {2, 87}},
                {{2, ordinary_char('r')}, {5, 96}},
                {{2, ordinary_char('c')}, {4, 96}},
                {{2, ordinary_char('e')}, {3, 96}},
                {{3, ordinary_char('r')}, {5, 96}},
                {{3, ordinary_char('c')}, {4, 96}},
                {{4, ordinary_char('r')}, {5, 96}},
                {{4, ordinary_char('c')}, {4, 96}},
                {{4, ordinary_char('e')}, {6, 96}},
                {{5, ordinary_char('e')}, {7, 96}},
                {{6, ordinary_char('r')}, {5, 96}},
                {{6, ordinary_char('c')}, {4, 96}},
            },

            // start state:
            0,

            // number of states:
            8,

            // final states:
            {
                7
            }
        },

        // Other DFA for regexp mle?(ce?)*re
        {
            // jumps:
            {
                {{0, ordinary_char('m')}, {1, 8}},
                {{1, ordinary_char('l')}, {2, 87}},
                {{2, ordinary_char('r')}, {4, 96}},
                {{2, ordinary_char('c')}, {2, 96}},
                {{2, ordinary_char('e')}, {3, 96}},
                {{3, ordinary_char('r')}, {4, 96}},
                {{3, ordinary_char('c')}, {2, 96}},
                {{4, ordinary_char('e')}, {5, 96}},
            },

            // start state:
            0,

            // number of states:
            6,

            // final states:
            {
                5
            }
        },

        //  DFA for regexp tp?fs|t|a
        {
            // jumps:
            {
                {{0, ordinary_char('t')}, {1, 8}},
                {{0, ordinary_char('a')}, {2, 8}},
                {{1, ordinary_char('p')}, {3, 87}},
                {{1, ordinary_char('f')}, {4, 87}},
                {{3, ordinary_char('f')}, {4, 87}},
                {{4, ordinary_char('s')}, {5, 87}},
            },

            // start state:
            0,

            // number of states:
            6,

            // final states:
            {
                1, 2, 5
            }
        },

        // Other DFA for regexp tp?fs|t|a
        {
            // jumps:
            {
                {{0, ordinary_char('t')}, {1, 8}},
                {{0, ordinary_char('a')}, {2, 8}},
                {{1, ordinary_char('p')}, {3, 87}},
                {{1, ordinary_char('f')}, {4, 87}},
                {{3, ordinary_char('f')}, {4, 87}},
                {{4, ordinary_char('s')}, {2, 87}},
            },

            // start state:
            0,

            // number of states:
            5,

            // final states:
            {
                1, 2
            }
        },

        //  DFA for regexp a((b|c)a|d(n(hn)*)?e|f(n(hn)*)?g|in(hn)*j)*(km?l)?|dne|o
        {
            // jumps:
            {
                {{0, ordinary_char('a')}, {1, 3}},
                {{0, ordinary_char('d')}, {2, 4}},
                {{0, ordinary_char('o')}, {3, 5}},

                {{1, ordinary_char('b')}, {4, 6}},
                {{1, ordinary_char('c')}, {5, 7}},
                {{1, ordinary_char('d')}, {6, 8}},
                {{1, ordinary_char('f')}, {7, 9}},
                {{1, ordinary_char('i')}, {8, 10}},
                {{1, ordinary_char('k')}, {9, 11}},

                {{2, ordinary_char('n')}, {10, 12}},

                {{4, ordinary_char('a')}, {11, 13}},
                {{5, ordinary_char('a')}, {11, 13}},

                {{6, ordinary_char('e')}, {13, 14}},
                {{6, ordinary_char('n')}, {12, 15}},

                {{7, ordinary_char('g')}, {15, 17}},
                {{7, ordinary_char('n')}, {14, 16}},

                {{8, ordinary_char('n')}, {16, 18}},

                {{9, ordinary_char('l')}, {18, 20}},
                {{9, ordinary_char('m')}, {17, 19}},

                {{10, ordinary_char('e')}, {19, 21}},

                {{11, ordinary_char('b')}, {4, 6}},
                {{11, ordinary_char('c')}, {5, 7}},
                {{11, ordinary_char('d')}, {6, 8}},
                {{11, ordinary_char('f')}, {7, 9}},
                {{11, ordinary_char('i')}, {8, 10}},
                {{11, ordinary_char('k')}, {9, 11}},

                {{12, ordinary_char('e')}, {13, 14}},
                {{12, ordinary_char('h')}, {20, 22}},

                {{13, ordinary_char('b')}, {4, 6}},
                {{13, ordinary_char('c')}, {5, 7}},
                {{13, ordinary_char('d')}, {6, 8}},
                {{13, ordinary_char('f')}, {7, 9}},
                {{13, ordinary_char('i')}, {8, 10}},
                {{13, ordinary_char('k')}, {9, 11}},

                {{14, ordinary_char('g')}, {15, 17}},
                {{14, ordinary_char('h')}, {21, 23}},

                {{15, ordinary_char('b')}, {4, 6}},
                {{15, ordinary_char('c')}, {5, 7}},
                {{15, ordinary_char('d')}, {6, 8}},
                {{15, ordinary_char('f')}, {7, 9}},
                {{15, ordinary_char('i')}, {8, 10}},
                {{15, ordinary_char('k')}, {9, 11}},

                {{16, ordinary_char('h')}, {22, 24}},
                {{16, ordinary_char('j')}, {23, 25}},

                {{17, ordinary_char('l')}, {18, 20}},

                {{20, ordinary_char('n')}, {24, 26}},

                {{21, ordinary_char('n')}, {25, 27}},

                {{22, ordinary_char('n')}, {26, 28}},

                {{23, ordinary_char('b')}, {4, 6}},
                {{23, ordinary_char('c')}, {5, 7}},
                {{23, ordinary_char('d')}, {6, 8}},
                {{23, ordinary_char('f')}, {7, 9}},
                {{23, ordinary_char('i')}, {8, 10}},
                {{23, ordinary_char('k')}, {9, 11}},

                {{24, ordinary_char('e')}, {13, 14}},
                {{24, ordinary_char('h')}, {20, 22}},

                {{25, ordinary_char('g')}, {15, 17}},
                {{25, ordinary_char('h')}, {21, 23}},

                {{26, ordinary_char('h')}, {22, 24}},
                {{26, ordinary_char('j')}, {23, 25}},
            },

            // start state:
            0,

            // number of states:
            27,

            // final states:
            {
                1, 3, 11, 13, 15, 18, 19, 23
            }
        },

        // Other DFA for regexp a((b|c)a|d(n(hn)*)?e|f(n(hn)*)?g|in(hn)*j)*(km?l)?|dne|o
        {
            // jumps:
            {
                {{0, ordinary_char('a')}, {1, 3}},
                {{0, ordinary_char('d')}, {2, 4}},
                {{0, ordinary_char('o')}, {3, 5}},

                {{1, ordinary_char('b')}, {4, 6}},
                {{1, ordinary_char('c')}, {4, 6}},
                {{1, ordinary_char('d')}, {5, 8}},
                {{1, ordinary_char('f')}, {6, 9}},
                {{1, ordinary_char('i')}, {7, 10}},
                {{1, ordinary_char('k')}, {8, 11}},

                {{2, ordinary_char('n')}, {9, 12}},

                {{4, ordinary_char('a')}, {1, 13}},

                {{5, ordinary_char('e')}, {1, 14}},
                {{5, ordinary_char('n')}, {10, 15}},

                {{6, ordinary_char('g')}, {1, 17}},
                {{6, ordinary_char('n')}, {11, 16}},

                {{7, ordinary_char('n')}, {12, 18}},

                {{8, ordinary_char('l')}, {3, 20}},
                {{8, ordinary_char('m')}, {13, 19}},

                {{9, ordinary_char('e')}, {3, 21}},

                {{10, ordinary_char('e')}, {1, 14}},
                {{10, ordinary_char('h')}, {14, 22}},

                {{11, ordinary_char('g')}, {1, 17}},
                {{11, ordinary_char('h')}, {15, 23}},

                {{12, ordinary_char('h')}, {7, 24}},
                {{12, ordinary_char('j')}, {1, 25}},

                {{13, ordinary_char('l')}, {3, 20}},

                {{14, ordinary_char('n')}, {10, 26}},

                {{15, ordinary_char('n')}, {11, 27}},
            },

            // start state:
            0,

            // number of states:
            16,

            // final states:
            {
                1, 3
            }
        },

        // DFA for regexp tire(cire)*
        {
            // jumps:
            {
                {{0, ordinary_char('t')}, {1, 3}},
                {{1, ordinary_char('i')}, {2, 6}},
                {{2, ordinary_char('r')}, {3, 12}},
                {{3, ordinary_char('e')}, {4, 13}},
                {{4, ordinary_char('c')}, {5, 14}},

                {{5, ordinary_char('i')}, {6, 6}},
                {{6, ordinary_char('r')}, {7, 12}},
                {{7, ordinary_char('e')}, {8, 13}},
                {{8, ordinary_char('c')}, {5, 14}},
            },

            // start state:
            0,

            // number of states:
            9,

            // final states:
            {
                4, 8
            }
        },

        // Other DFA for regexp tire(cire)*
        {
            // jumps:
            {
                {{0, ordinary_char('t')}, {1, 3}},
                {{1, ordinary_char('i')}, {2, 6}},
                {{2, ordinary_char('r')}, {3, 12}},
                {{3, ordinary_char('e')}, {4, 13}},
                {{4, ordinary_char('c')}, {1, 14}},
            },

            // start state:
            0,

            // number of states:
            5,

            // final states:
            {
                4
            }
        },

        // DFA for regexp e|(xd)?(v|c|t)
        {
            // jumps:
            {
                {{0, ordinary_char('e')}, {5, 88}},
                {{0, ordinary_char('x')}, {1, 4}},
                {{0, ordinary_char('v')}, {2, 3}},
                {{0, ordinary_char('c')}, {3, 3}},
                {{0, ordinary_char('t')}, {4, 3}},

                {{1, ordinary_char('d')}, {6, 6}},

                {{6, ordinary_char('v')}, {2, 3}},
                {{6, ordinary_char('c')}, {3, 3}},
                {{6, ordinary_char('t')}, {4, 3}},
            },

            // start state:
            0,

            // number of states:
            7,

            // final states:
            {
                2, 3, 4, 5
            }
        },

        // Other DFA for regexp e|(xd)?(v|c|t)
        {
            // jumps:
            {
                {{0, ordinary_char('e')}, {2, 88}},
                {{0, ordinary_char('x')}, {1, 4}},
                {{0, ordinary_char('v')}, {2, 3}},
                {{0, ordinary_char('c')}, {2, 3}},
                {{0, ordinary_char('t')}, {2, 3}},

                {{1, ordinary_char('d')}, {3, 6}},

                {{3, ordinary_char('v')}, {2, 3}},
                {{3, ordinary_char('c')}, {2, 3}},
                {{3, ordinary_char('t')}, {2, 3}},
            },

            // start state:
            0,

            // number of states:
            4,

            // final states:
            {
                2
            }
        },

        // DFA for regexp lb?(cb?)*r
        {
            // jumps:
            {
                {{0, ordinary_char('l')}, {1, 88}},

                {{1, ordinary_char('r')}, {4, 4}},
                {{1, ordinary_char('c')}, {3, 5}},
                {{1, ordinary_char('b')}, {2, 6}},

                {{2, ordinary_char('r')}, {4, 4}},
                {{2, ordinary_char('c')}, {3, 5}},

                {{3, ordinary_char('r')}, {4, 4}},
                {{3, ordinary_char('c')}, {3, 5}},
                {{3, ordinary_char('b')}, {5, 6}},

                {{5, ordinary_char('r')}, {4, 4}},
                {{5, ordinary_char('c')}, {3, 5}},
            },

            // start state:
            0,

            // number of states:
            6,

            // final states:
            {
                4
            }
        },

        // Other DFA for regexp lb?(cb?)*r
        {
            // jumps:
            {
                {{0, ordinary_char('l')}, {1, 88}},

                {{1, ordinary_char('r')}, {3, 4}},
                {{1, ordinary_char('c')}, {1, 5}},
                {{1, ordinary_char('b')}, {2, 6}},

                {{2, ordinary_char('r')}, {3, 4}},
                {{2, ordinary_char('c')}, {1, 5}},
            },

            // start state:
            0,

            // number of states:
            4,

            // final states:
            {
                3
            }
        },

    };


    enum Test_name_enum{
        TEST_CAST_EXPR_REGEXP_DFA,                TEST_CAST_EXPR_REGEXP_OTHER_DFA,
        TEST_TUPLE_EXPR_REGEXP_DFA,               TEST_TUPLE_EXPR_REGEXP_OTHER_DFA,
        TEST_ARRAY_EXPR_REGEXP_DFA,               TEST_ARRAY_EXPR_REGEXP_OTHER_DFA,
        TEST_ALLOCATION_EXPR_REGEXP_DFA,          TEST_ALLOCATION_EXPR_REGEXP__OTHER_DFA,
        TEST_FREE_EXPR_REGEXP_DFA,                TEST_LAMBDA_EXPR_REGEXP_DFA,
        TEST_FUNC_SIGNATURE_REGEXP_DFA,           TEST_FUNC_FORMAL_ARGS_REGEXP_DFA,
        TEST_FUNC_FORMAL_ARGS_REGEXP_OTHER_DFA,   TEST_GROUP_OF_FUNC_ARGS_REGEXP_DFA,
        TEST_GROUP_OF_FUNC_ARGS_REGEXP_OTHER_DFA, TEST_BASE_TYPE_REGEXP_DFA,
        TEST_BASE_TYPE_REGEXP_OTHER_DFA,          TEST_ARRAY_TYPE_REGEXP_DFA,
        TEST_ARRAY_TYPE_REGEXP_OTHER_DFA,         TEST_OTHER_TYPE_REGEXP_DFA,
        TEST_OTHER_TYPE_REGEXP_OTHER_DFA,         TEST_EXPR21_REGEXP_DFA,
        TEST_EXPR21_REGEXP_OTHER_DFA,             TEST_TYPES_REGEXP_DFA,
        TEST_TYPES_REGEXP_OTHER_DFA,              TEST_BLOCK_ENTRY_REGEXP_DFA,
        TEST_BLOCK_ENTRY_REGEXP_OTHER_DFA,        TEST_BLOCK_REGEXP_DFA,
        TEST_BLOCK_REGEXP_OTHER_DFA
    };

    std::pair<DFA_type, std::string> test_cast_expr_regexp_dfa()
    {
        return {dfas[TEST_CAST_EXPR_REGEXP_DFA], std::string{"test_cast_expr_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_cast_expr_regexp_other_dfa()
    {
        return {dfas[TEST_CAST_EXPR_REGEXP_OTHER_DFA], std::string{"test_cast_expr_regexp_other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_tuple_expr_regexp_dfa()
    {
        return {dfas[TEST_TUPLE_EXPR_REGEXP_DFA], std::string{"test_tuple_expr_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_tuple_expr_regexp_other_dfa()
    {
        return {dfas[TEST_TUPLE_EXPR_REGEXP_OTHER_DFA], std::string{"test_tuple_expr_regexp_other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_array_expr_regexp_dfa()
    {
        return {dfas[TEST_ARRAY_EXPR_REGEXP_DFA], std::string{"test_array_expr_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_array_expr_regexp_other_dfa()
    {
        return {dfas[TEST_ARRAY_EXPR_REGEXP_OTHER_DFA], std::string{"test_array_expr_regexp_other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_allocation_expr_regexp_dfa()
    {
        return {dfas[TEST_ALLOCATION_EXPR_REGEXP_DFA], std::string{"test_allocation_expr_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_allocation_expr_regexp__other_dfa()
    {
        return {dfas[TEST_ALLOCATION_EXPR_REGEXP__OTHER_DFA], std::string{"test_allocation_expr_regexp__other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_free_expr_regexp_dfa()
    {
        return {dfas[TEST_FREE_EXPR_REGEXP_DFA], std::string{"test_free_expr_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_lambda_expr_regexp_dfa()
    {
        return {dfas[TEST_LAMBDA_EXPR_REGEXP_DFA], std::string{"test_lambda_expr_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_func_signature_regexp_dfa()
    {
        return {dfas[TEST_FUNC_SIGNATURE_REGEXP_DFA], std::string{"test_func_signature_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_func_formal_args_regexp_dfa()
    {
        return {dfas[TEST_FUNC_FORMAL_ARGS_REGEXP_DFA], std::string{"test_func_formal_args_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_func_formal_args_regexp_other_dfa()
    {
        return {dfas[TEST_FUNC_FORMAL_ARGS_REGEXP_OTHER_DFA], std::string{"test_func_formal_args_regexp_other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_group_of_func_args_regexp_dfa()
    {
        return {dfas[TEST_GROUP_OF_FUNC_ARGS_REGEXP_DFA], std::string{"test_group_of_func_args_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_group_of_func_args_regexp_other_dfa()
    {
        return {dfas[TEST_GROUP_OF_FUNC_ARGS_REGEXP_OTHER_DFA], std::string{"test_group_of_func_args_regexp_other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_base_type_regexp_dfa()
    {
        return {dfas[TEST_BASE_TYPE_REGEXP_DFA], std::string{"test_base_type_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_base_type_regexp_other_dfa()
    {
        return {dfas[TEST_BASE_TYPE_REGEXP_OTHER_DFA], std::string{"test_base_type_regexp_other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_array_type_regexp_dfa()
    {
        return {dfas[TEST_ARRAY_TYPE_REGEXP_DFA], std::string{"test_array_type_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_array_type_regexp_other_dfa()
    {
        return {dfas[TEST_ARRAY_TYPE_REGEXP_OTHER_DFA], std::string{"test_array_type_regexp_other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_other_type_regexp_dfa()
    {
        return {dfas[TEST_OTHER_TYPE_REGEXP_DFA], std::string{"test_other_type_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_other_type_regexp_other_dfa()
    {
        return {dfas[TEST_OTHER_TYPE_REGEXP_OTHER_DFA], std::string{"test_other_type_regexp_other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_expr21_regexp_dfa()
    {
        return {dfas[TEST_EXPR21_REGEXP_DFA], std::string{"test_expr21_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_expr21_regexp_other_dfa()
    {
        return {dfas[TEST_EXPR21_REGEXP_OTHER_DFA], std::string{"test_expr21_regexp_other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_types_regexp_dfa()
    {
        return {dfas[TEST_TYPES_REGEXP_DFA], std::string{"test_types_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_types_regexp_other_dfa()
    {
        return {dfas[TEST_TYPES_REGEXP_OTHER_DFA], std::string{"test_types_regexp_other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_block_entry_regexp_dfa()
    {
        return {dfas[TEST_BLOCK_ENTRY_REGEXP_DFA], std::string{"test_block_entry_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_block_entry_regexp_other_dfa()
    {
        return {dfas[TEST_BLOCK_ENTRY_REGEXP_OTHER_DFA], std::string{"test_block_entry_regexp_other_dfa"}};
    }

    std::pair<DFA_type, std::string> test_block_regexp_dfa()
    {
        return {dfas[TEST_BLOCK_REGEXP_DFA], std::string{"test_block_regexp_dfa"}};
    }

    std::pair<DFA_type, std::string> test_block_regexp_other_dfa()
    {
        return {dfas[TEST_BLOCK_REGEXP_OTHER_DFA], std::string{"test_block_regexp_other_dfa"}};
    }

    const Test_func test_func_array[] = {
        test_cast_expr_regexp_dfa,                test_cast_expr_regexp_other_dfa,
        test_tuple_expr_regexp_dfa,               test_tuple_expr_regexp_other_dfa,
        test_array_expr_regexp_dfa,               test_array_expr_regexp_other_dfa,
        test_allocation_expr_regexp_dfa,          test_allocation_expr_regexp__other_dfa,
        test_free_expr_regexp_dfa,                test_lambda_expr_regexp_dfa,
        test_func_signature_regexp_dfa,           test_func_formal_args_regexp_dfa,
        test_func_formal_args_regexp_other_dfa,   test_group_of_func_args_regexp_dfa,
        test_group_of_func_args_regexp_other_dfa, test_base_type_regexp_dfa,
        test_base_type_regexp_other_dfa,          test_array_type_regexp_dfa,
        test_array_type_regexp_other_dfa,         test_other_type_regexp_dfa,
        test_other_type_regexp_other_dfa,         test_expr21_regexp_dfa,
        test_expr21_regexp_other_dfa,             test_types_regexp_dfa,
        test_types_regexp_other_dfa,              test_block_entry_regexp_dfa,
        test_block_entry_regexp_other_dfa,        test_block_regexp_dfa,
        test_block_regexp_other_dfa,
    };
};

void print_dfa_test()
{
    for(const auto func : test_func_array){
        const auto& [dfa, name] = func();
        std::cout << "Test name: " << name << "\n";
        const auto  automata_str = dfa_to_string(dfa);
        std::cout << "String representation of the corresponding DFA:\n";
        std::cout << automata_str << "\n\n";
    }
}