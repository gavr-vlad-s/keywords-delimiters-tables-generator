/*
    File:    print_min_dfa_test.cpp
    Created: 02 October 2021 at 16:25 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/


#include <algorithm>
#include <cstddef>
#include <iostream>
#include <limits>
#include <string>
#include <utility>
#include "../include/print_min_dfa_test.hpp"
#include "../../automata/include/minimal_dfa.hpp"


using Action_type = std::size_t;
using Char_type   = char;

template<>
struct Generalized_char_traits<Char_type>
{
    static bool lt(Char_type a, Char_type b)
    {
        return a < b;
    }

    static bool eq(Char_type a, Char_type b)
    {
        return a == b;
    }

    static std::string to_string(Char_type a)
    {
        return std::string{a};
    }
};

template<>
struct Action_traits<Action_type>
{
    static std::string to_string(Action_type a)
    {
        return std::to_string(a);
    }

    static Action_type common(Action_type a, Action_type b)
    {
        return std::min(a, b);
    }

    static Action_type neutral()
    {
        return std::numeric_limits<Action_type>::max();
    }
};

using DFA_type = Min_DFA<Char_type, Action_type, Generalized_char_traits<Char_type>>;

using Test_func = std::pair<DFA_type, std::string> (*)();

namespace{
    const auto eps = epsilon<Char_type, Generalized_char_traits<Char_type>>();

    const DFA_type min_dfas[] = {
        // Minimal DFA for regexp reke|peve
        {
            // jumps:
            {
                // State 0 transitions:
                {
                    {ordinary_char('r'), {1, 16}},
                    {ordinary_char('p'), {2, 17}},
                },

                // State 1 transitions:
                {
                    {ordinary_char('e'), {3, 18}},
                },

                // State 2 transitions:
                {
                    {ordinary_char('e'), {4, 19}},
                },

                // State 3 transitions:
                {
                    {ordinary_char('k'), {5, 21}},
                },

                // State 4 transitions:
                {
                    {ordinary_char('k'), {5, 22}},
                },

                // State 5 transitions:
                {
                    {ordinary_char('e'), {6, 517}},
                },

                // State 6 transitions:
                {
                },
            },

            // start state:
            0,

            // final states:
            {
                6
            }
        },

        // Minimal DFA for regexp l(e(ce)*)?r
        {
            // jumps:
            {
                // State 0 transitions:
                {
                    {ordinary_char('l'), {1, 16}},
                },

                // State 1 transitions:
                {
                    {ordinary_char('r'), {3, 18}},
                    {ordinary_char('e'), {2, 17}},
                },

                // State 2 transitions:
                {
                    {ordinary_char('c'), {4, 19}},
                    {ordinary_char('r'), {3, 20}},
                },

                // State 3 transitions:
                {
                },

                // State 4 transitions:
                {
                    {ordinary_char('e'), {2, 22}},
                },
            },

            // start state:
            0,

            // final states:
            {
                3
            }
        },

        // Minimal DFA for regexp le(ce)*r
        {
            // jumps:
            {
                // State 0 transitions:
                {
                    {ordinary_char('l'), {1, 16}},
                },

                // State 1 transitions:
                {
                    {ordinary_char('e'), {2, 17}},
                },

                // State 2 transitions:
                {
                    {ordinary_char('c'), {1, 19}},
                    {ordinary_char('r'), {3, 20}},
                },

                // State 3 transitions:
                {
                },
            },

            // start state:
            0,

            // final states:
            {
                3
            }
        },

        // Minimal DFA for regexp ve|vsle(ce)*r
        {
            // jumps:
            {
                // State 0 transitions:
                {
                    {ordinary_char('v'), {1, 16}},
                },

                // State 1 transitions:
                {
                    {ordinary_char('s'), {3, 17}},
                    {ordinary_char('e'), {2, 318}},
                },

                // State 2 transitions:
                {
                },

                // State 3 transitions:
                {
                    {ordinary_char('l'), {4, 45}},
                },

                // State 4 transitions:
                {
                    {ordinary_char('e'), {5, 4}},
                },

                // State 5 transitions:
                {
                    {ordinary_char('r'), {2, 77}},
                    {ordinary_char('c'), {4, 651}},
                },
            },

            // start state:
            0,

            // final states:
            {
                3
            }
        },


    };


    enum Test_name_enum{
        TEST_CAST_EXPR_REGEXP_MINIMAL_DFA,  TEST_TUPLE_EXPR_REGEXP_MINIMAL_DFA,
        TEST_ARRAY_EXPR_REGEXP_MINIMAL_DFA, TEST_ALLOCATION_EXPR_REGEXP_MINIMAL_DFA,
    };

    std::pair<DFA_type, std::string> test_cast_expr_regexp_minimal_dfa()
    {
        return {min_dfas[TEST_CAST_EXPR_REGEXP_MINIMAL_DFA], std::string{"test_cast_expr_regexp_minimal_dfa"}};
    }

    std::pair<DFA_type, std::string> test_tuple_expr_regexp_minimal_dfa()
    {
        return {min_dfas[TEST_TUPLE_EXPR_REGEXP_MINIMAL_DFA], std::string{"test_tuple_expr_regexp_minimal_dfa"}};
    }

    std::pair<DFA_type, std::string> test_array_expr_regexp_minimal_dfa()
    {
        return {min_dfas[TEST_ARRAY_EXPR_REGEXP_MINIMAL_DFA], std::string{"test_array_expr_regexp_minimal_dfa"}};
    }

    std::pair<DFA_type, std::string> test_allocation_expr_regexp_minimal_dfa()
    {
        return {min_dfas[TEST_ALLOCATION_EXPR_REGEXP_MINIMAL_DFA], std::string{"test_allocation_expr_regexp_minimal_dfa"}};
    }

    const Test_func test_func_array[] = {
        test_cast_expr_regexp_minimal_dfa,  test_tuple_expr_regexp_minimal_dfa,
        test_array_expr_regexp_minimal_dfa, test_allocation_expr_regexp_minimal_dfa,
    };
};

void print_min_dfa_test()
{
    for(const auto func : test_func_array){
        const auto& [min_dfa, name] = func();
        std::cout << "Test name: " << name << "\n";
        const auto  automata_str = minimal_dfa_to_string(min_dfa);
        std::cout << "String representation of the corresponding minimal DFA:\n";
        std::cout << automata_str << "\n\n";
    }
}