/*
    File:    print_ndfa_test.cpp
    Created: 05 September 2021 at 14:17 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#include <algorithm>
#include <cstddef>
#include <iostream>
#include <limits>
#include <string>
#include <utility>
#include "../include/print_ndfa_test.hpp"
#include "../../automata/include/ndfa.hpp"


using Action_type = std::size_t;
using Char_type   = char;

template<>
struct Generalized_char_traits<Char_type>
{
    static bool lt(Char_type a, Char_type b)
    {
        return a < b;
    }

    static bool eq(Char_type a, Char_type b)
    {
        return a == b;
    }

    static std::string to_string(Char_type a)
    {
        return std::string{a};
    }
};

template<>
struct Action_traits<Action_type>
{
    static std::string to_string(Action_type a)
    {
        return std::to_string(a);
    }

    static Action_type common(Action_type a, Action_type b)
    {
        return std::min(a, b);
    }

    static Action_type neutral()
    {
        return std::numeric_limits<Action_type>::max();
    }
};

using NDFA_type = NDFA<Char_type, Action_type, Generalized_char_traits<Char_type>>;

using Test_func = std::pair<NDFA_type, std::string> (*)();

namespace{
    const auto eps = epsilon<Char_type, Generalized_char_traits<Char_type>>();

    NDFA_type ndfas[] = {
        // NDFA for regexp l(e(ce)*)?r
        {
            // Transitions:
            {
                // State 0 transitions:
                {
                    {ordinary_char('l'), {Set_of_states{1}, Action_traits<Action_type>::neutral()}},
                },
                // State 1 transitions:
                {
                    {eps, {Set_of_states{2, 8}, Action_traits<Action_type>::neutral()}},
                },
                // State 2 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{3}, Action_traits<Action_type>::neutral()}},
                },
                // State 3 transitions:
                {
                    {eps, {Set_of_states{4, 7}, Action_traits<Action_type>::neutral()}},
                },
                // State 4 transitions:
                {
                    {ordinary_char('c'), {Set_of_states{5}, Action_traits<Action_type>::neutral()}},
                },
                // State 5 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{6}, Action_traits<Action_type>::neutral()}},
                },
                // State 6 transitions:
                {
                    {eps, {Set_of_states{4, 7}, Action_traits<Action_type>::neutral()}},
                },
                // State 7 transitions:
                {
                    {eps, {Set_of_states{8}, Action_traits<Action_type>::neutral()}},
                },
                // State 8 transitions:
                {
                    {ordinary_char('r'), {Set_of_states{9}, 16}},
                },
                // State 9 stransitions:
                {
                }
            },
            0, // start state
            9  // final state
        },

        // NDFA for regexp le(ce)*r
        {
            // Transitions:
            {
                // State 0 transitions:
                {
                    {ordinary_char('l'), {Set_of_states{1}, Action_traits<Action_type>::neutral()}},
                },
                // State 1 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{2}, Action_traits<Action_type>::neutral()}},
                },
                // State 2 transitions:
                {
                    {eps, {Set_of_states{3, 6}, Action_traits<Action_type>::neutral()}},
                },
                // State 3 transitions:
                {
                    {ordinary_char('c'), {Set_of_states{4}, Action_traits<Action_type>::neutral()}},
                },
                // State 4 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{5}, Action_traits<Action_type>::neutral()}},
                },
                // State 5 transitions:
                {
                    {eps, {Set_of_states{3, 6}, Action_traits<Action_type>::neutral()}},
                },
                // State 6 transitions:
                {
                    {ordinary_char('r'), {Set_of_states{7}, 3}},
                },
                // State 7 stransitions:
                {
                }
            },
            0, // start state
            7  // final state
        },

        // NDFA for regexp ve|vsle(ce)*r
        {
            // Transitions:
            {
                // State 0 transitions:
                {
                    {eps, {Set_of_states{1, 4}, Action_traits<Action_type>::neutral()}},
                },
                // State 1 transitions:
                {
                    {ordinary_char('v'), {Set_of_states{2}, Action_traits<Action_type>::neutral()}},
                },
                // State 2 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{3}, 88}},
                },
                // State 3 transitions:
                {
                    {eps, {Set_of_states{14}, Action_traits<Action_type>::neutral()}},
                },
                // State 4 transitions:
                {
                    {ordinary_char('v'), {Set_of_states{5}, Action_traits<Action_type>::neutral()}},
                },
                // State 5 transitions:
                {
                    {ordinary_char('s'), {Set_of_states{6}, Action_traits<Action_type>::neutral()}},
                },
                // State 6 transitions:
                {
                    {ordinary_char('l'), {Set_of_states{7}, Action_traits<Action_type>::neutral()}},
                },
                // State 7 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{8}, Action_traits<Action_type>::neutral()}},
                },
                // State 8 transitions:
                {
                    {eps, {Set_of_states{9, 12}, Action_traits<Action_type>::neutral()}},
                },
                // State 9 transitions:
                {
                    {ordinary_char('c'), {Set_of_states{10}, Action_traits<Action_type>::neutral()}},
                },
                // State 10 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{11}, Action_traits<Action_type>::neutral()}},
                },
                // State 11 transitions:
                {
                    {eps, {Set_of_states{9, 12}, Action_traits<Action_type>::neutral()}},
                },
                // State 12 transitions:
                {
                    {ordinary_char('r'), {Set_of_states{13}, 57}},
                },
                // State 13 transitions:
                {
                    {eps, {Set_of_states{14}, Action_traits<Action_type>::neutral()}},
                },
                // State 14 stransitions:
                {
                }
            },
            0,  // start state
            14  // final state
        },

        // NDFA for regexp la?rce
        {
            // Transitions:
            {
                // State 0 transitions:
                {
                    {ordinary_char('l'), {Set_of_states{1}, Action_traits<Action_type>::neutral()}},
                },
                // State 1 transitions:
                {
                    {eps, {Set_of_states{2, 4}, Action_traits<Action_type>::neutral()}},
                },
                // State 2 transitions:
                {
                    {ordinary_char('a'), {Set_of_states{3}, Action_traits<Action_type>::neutral()}},
                },
                // State 3 transitions:
                {
                    {eps, {Set_of_states{4}, Action_traits<Action_type>::neutral()}},
                },
                // State 4 transitions:
                {
                    {ordinary_char('r'), {Set_of_states{5}, Action_traits<Action_type>::neutral()}},
                },
                // State 5 transitions:
                {
                    {ordinary_char('c'), {Set_of_states{6}, Action_traits<Action_type>::neutral()}},
                },
                // State 6 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{7}, 33}},
                },
                // State 7 stransitions:
                {
                }
            },
            0, // start state
            7  // final state
        },

        // NDFA for regexp m*w|c
        {
            // Transitions:
            {
                // State 0 transitions:
                {
                    {eps, {Set_of_states{1, 6}, Action_traits<Action_type>::neutral()}},
                },
                // State 1 transitions:
                {
                    {eps, {Set_of_states{2, 4}, Action_traits<Action_type>::neutral()}},
                },
                // State 2 transitions:
                {
                    {ordinary_char('m'), {Set_of_states{3}, Action_traits<Action_type>::neutral()}},
                },
                // State 3 transitions:
                {
                    {eps, {Set_of_states{2, 4}, Action_traits<Action_type>::neutral()}},
                },
                // State 4 transitions:
                {
                    {ordinary_char('w'), {Set_of_states{5}, 28}},
                },
                // State 5 transitions:
                {
                    {eps, {Set_of_states{8}, Action_traits<Action_type>::neutral()}},
                },
                // State 6 transitions:
                {
                    {ordinary_char('c'), {Set_of_states{7}, 16}},
                },
                // State 7 transitions:
                {
                    {eps, {Set_of_states{8}, Action_traits<Action_type>::neutral()}},
                },
                // State 8 stransitions:
                {
                }
            },
            0, // start state
            8  // final state
        },

        // NDFA for regexp mle?(ce?)*re
        {
            // Transitions:
            {
                // State 0 transitions:
                {
                    {ordinary_char('m'), {Set_of_states{1}, Action_traits<Action_type>::neutral()}},
                },
                // State 1 transitions:
                {
                    {ordinary_char('l'), {Set_of_states{2}, Action_traits<Action_type>::neutral()}},
                },
                // State 2 transitions:
                {
                    {eps, {Set_of_states{3, 5}, Action_traits<Action_type>::neutral()}},
                },
                // State 3 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{4}, Action_traits<Action_type>::neutral()}},
                },
                // State 4 transitions:
                {
                    {eps, {Set_of_states{5}, Action_traits<Action_type>::neutral()}},
                },
                // State 5 transitions:
                {
                    {eps, {Set_of_states{6, 11}, Action_traits<Action_type>::neutral()}},
                },
                // State 6 transitions:
                {
                    {ordinary_char('c'), {Set_of_states{7}, Action_traits<Action_type>::neutral()}},
                },
                // State 7 transitions:
                {
                    {eps, {Set_of_states{8, 10}, Action_traits<Action_type>::neutral()}},
                },
                // State 8 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{9}, Action_traits<Action_type>::neutral()}},
                },
                // State 9 transitions:
                {
                    {eps, {Set_of_states{10}, Action_traits<Action_type>::neutral()}},
                },
                // State 10 transitions:
                {
                    {eps, {Set_of_states{6, 11}, Action_traits<Action_type>::neutral()}},
                },
                // State 11 transitions:
                {
                    {ordinary_char('r'), {Set_of_states{12}, Action_traits<Action_type>::neutral()}},
                },
                // State 12 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{13}, 342}},
                },
                // State 13 stransitions:
                {
                }
            },
            0, // start state
            13  // final state
        },

        // NDFA for regexp ol(e?(ce?)*)?r
        {
            // Transitions:
            {
                // State 0 transitions:
                {
                    {ordinary_char('o'), {Set_of_states{1}, Action_traits<Action_type>::neutral()}},
                },
                // State 1 transitions:
                {
                    {ordinary_char('l'), {Set_of_states{2}, Action_traits<Action_type>::neutral()}},
                },
                // State 2 transitions:
                {
                    {eps, {Set_of_states{3, 13}, Action_traits<Action_type>::neutral()}},
                },
                // State 3 transitions:
                {
                    {eps, {Set_of_states{4, 6}, Action_traits<Action_type>::neutral()}},
                },
                // State 4 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{5}, Action_traits<Action_type>::neutral()}},
                },
                // State 5 transitions:
                {
                    {eps, {Set_of_states{6}, Action_traits<Action_type>::neutral()}},
                },
                // State 6 transitions:
                {
                    {eps, {Set_of_states{7, 12}, Action_traits<Action_type>::neutral()}},
                },
                // State 7 transitions:
                {
                    {ordinary_char('c'), {Set_of_states{8}, Action_traits<Action_type>::neutral()}},
                },
                // State 8 transitions:
                {
                    {eps, {Set_of_states{9, 11}, Action_traits<Action_type>::neutral()}},
                },
                // State 9 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{10}, Action_traits<Action_type>::neutral()}},
                },
                // State 10 transitions:
                {
                    {eps, {Set_of_states{11}, Action_traits<Action_type>::neutral()}},
                },
                // State 11 transitions:
                {
                    {eps, {Set_of_states{7, 12}, Action_traits<Action_type>::neutral()}},
                },
                // State 12 transitions:
                {
                    {eps, {Set_of_states{13}, Action_traits<Action_type>::neutral()}},
                },
                // State 13 transitions:
                {
                    {ordinary_char('r'), {Set_of_states{14}, 45}},
                },
                // State 14 stransitions:
                {
                }
            },
            0, // start state
            14  // final state
        },

        // NDFA for regexp tp?fs|t|a
        {
            // Transitions:
            {
                // State 0 transitions:
                {
                    {eps, {Set_of_states{1, 8, 10}, Action_traits<Action_type>::neutral()}},
                },
                // State 1 transitions:
                {
                    {ordinary_char('t'), {Set_of_states{2}, Action_traits<Action_type>::neutral()}},
                },
                // State 2 transitions:
                {
                    {eps, {Set_of_states{3, 5}, Action_traits<Action_type>::neutral()}},
                },
                // State 3 transitions:
                {
                    {ordinary_char('p'), {Set_of_states{4}, Action_traits<Action_type>::neutral()}},
                },
                // State 4 transitions:
                {
                    {eps, {Set_of_states{5}, Action_traits<Action_type>::neutral()}},
                },
                // State 5 transitions:
                {
                    {ordinary_char('f'), {Set_of_states{6}, Action_traits<Action_type>::neutral()}},
                },
                // State 6 transitions:
                {
                    {ordinary_char('s'), {Set_of_states{7}, 13}},
                },
                // State 7 transitions:
                {
                    {eps, {Set_of_states{12}, Action_traits<Action_type>::neutral()}},
                },
                // State 8 transitions:
                {
                    {ordinary_char('t'), {Set_of_states{9}, 615}},
                },
                // State 9 transitions:
                {
                    {eps, {Set_of_states{12}, Action_traits<Action_type>::neutral()}},
                },
                // State 10 transitions:
                {
                    {ordinary_char('a'), {Set_of_states{11}, 439}},
                },
                // State 11 transitions:
                {
                    {eps, {Set_of_states{12}, Action_traits<Action_type>::neutral()}},
                },
                // State 12 stransitions:
                {
                }
            },
            0,  // start state
            12  // final state
        },

        // NDFA for regexp a((b|c)a|d(n(hn)*)?e|f(n(hn)*)?g|in(hn)*j)*(kml)?|dne|o
        {
            // Transitions:
            {
                // State 0 transitions:
                {
                    {eps, {Set_of_states{1, 46, 50}, Action_traits<Action_type>::neutral()}},
                },
                // State 1 transitions:
                {
                    {ordinary_char('a'), {Set_of_states{2}, Action_traits<Action_type>::neutral()}},
                },
                // State 2 transitions:
                {
                    {eps, {Set_of_states{3, 40}, Action_traits<Action_type>::neutral()}},
                },
                // State 3 transitions:
                {
                    {eps, {Set_of_states{4, 5, 21, 31}, Action_traits<Action_type>::neutral()}},
                },
                // State 4 transitions:
                {
                    {ordinary_char('d'), {Set_of_states{12}, Action_traits<Action_type>::neutral()}},
                },
                // State 5 transitions:
                {
                    {eps, {Set_of_states{6, 8}, Action_traits<Action_type>::neutral()}},
                },
                // State 6 transitions:
                {
                    {ordinary_char('b'), {Set_of_states{7}, Action_traits<Action_type>::neutral()}},
                },
                // State 7 transitions:
                {
                    {eps, {Set_of_states{10}, Action_traits<Action_type>::neutral()}},
                },
                // State 8 transitions:
                {
                    {ordinary_char('c'), {Set_of_states{9}, Action_traits<Action_type>::neutral()}},
                },
                // State 9 transitions:
                {
                    {eps, {Set_of_states{10}, Action_traits<Action_type>::neutral()}},
                },
                // State 10 transitions:
                {
                    {ordinary_char('a'), {Set_of_states{11}, Action_traits<Action_type>::neutral()}},
                },
                // State 11 transitions:
                {
                    {eps, {Set_of_states{39}, Action_traits<Action_type>::neutral()}},
                },
                // State 12 transitions:
                {
                    {eps, {Set_of_states{13, 19}, Action_traits<Action_type>::neutral()}},
                },
                // State 13 transitions:
                {
                    {ordinary_char('n'), {Set_of_states{14}, Action_traits<Action_type>::neutral()}},
                },
                // State 14 transitions:
                {
                    {eps, {Set_of_states{15, 18}, Action_traits<Action_type>::neutral()}},
                },
                // State 15 transitions:
                {
                    {ordinary_char('h'), {Set_of_states{16}, Action_traits<Action_type>::neutral()}},
                },
                // State 16 transitions:
                {
                    {ordinary_char('n'), {Set_of_states{17}, Action_traits<Action_type>::neutral()}},
                },
                // State 17 transitions:
                {
                    {eps, {Set_of_states{15, 18}, Action_traits<Action_type>::neutral()}},
                },
                // State 18 transitions:
                {
                    {eps, {Set_of_states{19}, Action_traits<Action_type>::neutral()}},
                },
                // State 19 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{20}, Action_traits<Action_type>::neutral()}},
                },
                // State 20 transitions:
                {
                    {eps, {Set_of_states{39}, Action_traits<Action_type>::neutral()}},
                },
                // State 21 transitions:
                {
                    {ordinary_char('f'), {Set_of_states{22}, Action_traits<Action_type>::neutral()}},
                },
                // State 22 transitions:
                {
                    {eps, {Set_of_states{23, 29}, Action_traits<Action_type>::neutral()}},
                },
                // State 23 transitions:
                {
                    {ordinary_char('n'), {Set_of_states{24}, Action_traits<Action_type>::neutral()}},
                },
                // State 24 transitions:
                {
                    {eps, {Set_of_states{25, 28}, Action_traits<Action_type>::neutral()}},
                },
                // State 25 transitions:
                {
                    {ordinary_char('h'), {Set_of_states{26}, Action_traits<Action_type>::neutral()}},
                },
                // State 26 transitions:
                {
                    {ordinary_char('n'), {Set_of_states{27}, Action_traits<Action_type>::neutral()}},
                },
                // State 27 transitions:
                {
                    {eps, {Set_of_states{25, 28}, Action_traits<Action_type>::neutral()}},
                },
                // State 28 transitions:
                {
                    {eps, {Set_of_states{29}, Action_traits<Action_type>::neutral()}},
                },
                // State 29 transitions:
                {
                    {ordinary_char('g'), {Set_of_states{30}, Action_traits<Action_type>::neutral()}},
                },
                // State 30 transitions:
                {
                    {eps, {Set_of_states{39}, Action_traits<Action_type>::neutral()}},
                },
                // State 31 transitions:
                {
                    {ordinary_char('i'), {Set_of_states{32}, Action_traits<Action_type>::neutral()}},
                },
                // State 32 transitions:
                {
                    {ordinary_char('n'), {Set_of_states{33}, Action_traits<Action_type>::neutral()}},
                },
                // State 33 transitions:
                {
                    {eps, {Set_of_states{34, 37}, Action_traits<Action_type>::neutral()}},
                },
                // State 34 transitions:
                {
                    {ordinary_char('h'), {Set_of_states{35}, Action_traits<Action_type>::neutral()}},
                },
                // State 35 transitions:
                {
                    {ordinary_char('n'), {Set_of_states{36}, Action_traits<Action_type>::neutral()}},
                },
                // State 36 transitions:
                {
                    {eps, {Set_of_states{34, 37}, Action_traits<Action_type>::neutral()}},
                },
                // State 37 transitions:
                {
                    {ordinary_char('j'), {Set_of_states{38}, Action_traits<Action_type>::neutral()}},
                },
                // State 38 transitions:
                {
                    {eps, {Set_of_states{39}, Action_traits<Action_type>::neutral()}},
                },
                // State 39 transitions:
                {
                    {eps, {Set_of_states{3, 40}, Action_traits<Action_type>::neutral()}},
                },
                // State 40 transitions:
                {
                    {eps, {Set_of_states{41, 45}, Action_traits<Action_type>::neutral()}},
                },
                // State 41 transitions:
                {
                    {ordinary_char('k'), {Set_of_states{42}, Action_traits<Action_type>::neutral()}},
                },
                // State 42 transitions:
                {
                    {ordinary_char('m'), {Set_of_states{43}, Action_traits<Action_type>::neutral()}},
                },
                // State 43 transitions:
                {
                    {ordinary_char('l'), {Set_of_states{44}, Action_traits<Action_type>::neutral()}},
                },
                // State 44 transitions:
                {
                    {eps, {Set_of_states{45}, Action_traits<Action_type>::neutral()}},
                },
                // State 45 transitions:
                {
                    {eps, {Set_of_states{52}, Action_traits<Action_type>::neutral()}},
                },
                // State 46 transitions:
                {
                    {ordinary_char('d'), {Set_of_states{47}, Action_traits<Action_type>::neutral()}},
                },
                // State 47 transitions:
                {
                    {ordinary_char('n'), {Set_of_states{48}, Action_traits<Action_type>::neutral()}},
                },
                // State 48 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{49}, Action_traits<Action_type>::neutral()}},
                },
                // State 49 transitions:
                {
                    {eps, {Set_of_states{52}, Action_traits<Action_type>::neutral()}},
                },
                // State 50 transitions:
                {
                    {ordinary_char('o'), {Set_of_states{51}, Action_traits<Action_type>::neutral()}},
                },
                // State 51 transitions:
                {
                    {eps, {Set_of_states{52}, Action_traits<Action_type>::neutral()}},
                },
                // State 52 stransitions:
                {
                }
            },
            0, // start state
            52  // final state
        },

        // NDFA for regexp e|(xd)?(v|c|t)
        {
            // Transitions:
            {
                // State 0 transitions:
                {
                    {eps, {Set_of_states{1, 13}, Action_traits<Action_type>::neutral()}},
                },
                // State 1 transitions:
                {
                    {eps, {Set_of_states{2, 5}, Action_traits<Action_type>::neutral()}},
                },
                // State 2 transitions:
                {
                    {ordinary_char('x'), {Set_of_states{3}, Action_traits<Action_type>::neutral()}},
                },
                // State 3 transitions:
                {
                    {ordinary_char('d'), {Set_of_states{4}, Action_traits<Action_type>::neutral()}},
                },
                // State 4 transitions:
                {
                    {eps, {Set_of_states{5}, Action_traits<Action_type>::neutral()}},
                },
                // State 5 transitions:
                {
                    {eps, {Set_of_states{6, 8, 10}, Action_traits<Action_type>::neutral()}},
                },
                // State 6 transitions:
                {
                    {ordinary_char('v'), {Set_of_states{7}, Action_traits<Action_type>::neutral()}},
                },
                // State 7 transitions:
                {
                    {eps, {Set_of_states{12}, Action_traits<Action_type>::neutral()}},
                },
                // State 8 transitions:
                {
                    {ordinary_char('c'), {Set_of_states{9}, Action_traits<Action_type>::neutral()}},
                },
                // State 9 transitions:
                {
                    {eps, {Set_of_states{12}, Action_traits<Action_type>::neutral()}},
                },
                // State 10 transitions:
                {
                    {ordinary_char('t'), {Set_of_states{11}, Action_traits<Action_type>::neutral()}},
                },
                // State 11 transitions:
                {
                    {eps, {Set_of_states{12}, Action_traits<Action_type>::neutral()}},
                },
                // State 12 transitions:
                {
                    {eps, {Set_of_states{15}, Action_traits<Action_type>::neutral()}},
                },
                // State 13 transitions:
                {
                    {ordinary_char('e'), {Set_of_states{11}, Action_traits<Action_type>::neutral()}},
                },
                // State 14 transitions:
                {
                    {eps, {Set_of_states{15}, Action_traits<Action_type>::neutral()}},
                },
                // State 15 stransitions:
                {
                }
            },
            0, // start state
            15  // final state
        },
    };

    enum Test_name_enum{
        TEST_TUPLE_EXPR_REGEXP_NDFA, TEST_ARRAY_EXPR_REGEXP_NDFA,
        TEST_ALLOC_EXPR_REGEXP_NDFA, TEST_FUNC_SIGNATURE_REGEXP_NDFA,
        TEST_BASE_TYPE_REGEXP_NDFA,  TEST_ARRAY_TYPE_REGEXP_NDFA,
        TEST_GEN_TYPE_REGEXP_NDFA,   TEST_OTHER_TYPE_REGEXP_NDFA,
        TEST_EXPR21_REGEXP_NDFA,     TEST_BLOCK_ENTRY_REGEXP_NDFA
    };

    std::pair<NDFA_type, std::string> test_tuple_expr_regexp_ndfa()
    {
        return {ndfas[TEST_TUPLE_EXPR_REGEXP_NDFA], std::string{"test_tuple_expr_regexp_ndfa"}};
    }

    std::pair<NDFA_type, std::string> test_array_expr_regexp_ndfa()
    {
        return {ndfas[TEST_ARRAY_EXPR_REGEXP_NDFA], std::string{"test_array_expr_regexp_ndfa"}};
    }

    std::pair<NDFA_type, std::string> test_alloc_expr_regexp_ndfa()
    {
        return {ndfas[TEST_ALLOC_EXPR_REGEXP_NDFA], std::string{"test_alloc_expr_regexp_ndfa"}};
    }

    std::pair<NDFA_type, std::string> test_func_signature_regexp_ndfa()
    {
        return {ndfas[TEST_FUNC_SIGNATURE_REGEXP_NDFA], std::string{"test_func_signature_regexp_ndfa"}};
    }

    std::pair<NDFA_type, std::string> test_base_type_regexp_ndfa()
    {
        return {ndfas[TEST_BASE_TYPE_REGEXP_NDFA], std::string{"test_base_type_regexp_ndfa"}};
    }

    std::pair<NDFA_type, std::string> test_array_type_regexp_ndfa()
    {
        return {ndfas[TEST_ARRAY_TYPE_REGEXP_NDFA], std::string{"test_array_type_regexp_ndfa"}};
    }

    std::pair<NDFA_type, std::string> test_gen_type_regexp_ndfa()
    {
        return {ndfas[TEST_GEN_TYPE_REGEXP_NDFA], std::string{"test_gen_type_regexp_ndfa"}};
    }

    std::pair<NDFA_type, std::string> test_other_type_regexp_ndfa()
    {
        return {ndfas[TEST_OTHER_TYPE_REGEXP_NDFA], std::string{"test_other_type_regexp_ndfa"}};
    }

    std::pair<NDFA_type, std::string> test_expr21_regexp_ndfa()
    {
        return {ndfas[TEST_EXPR21_REGEXP_NDFA], std::string{"test_expr21_regexp_ndfa"}};
    }

    std::pair<NDFA_type, std::string> test_block_entry_regexp_ndfa()
    {
        return {ndfas[TEST_BLOCK_ENTRY_REGEXP_NDFA], std::string{"test_block_entry_regexp_ndfa"}};
    }

    const Test_func test_func_array[] = {
        test_tuple_expr_regexp_ndfa, test_array_expr_regexp_ndfa,
        test_alloc_expr_regexp_ndfa, test_func_signature_regexp_ndfa,
        test_base_type_regexp_ndfa,  test_array_type_regexp_ndfa,
        test_gen_type_regexp_ndfa,   test_other_type_regexp_ndfa,
        test_expr21_regexp_ndfa,     test_block_entry_regexp_ndfa
    };
};

void print_ndfa_test()
{
    for(const auto func : test_func_array){
        const auto& [ndfa, name] = func();
        std::cout << "Test name: " << name << "\n";
        const auto  automata_str = ndfa_to_string(ndfa);
        std::cout << "String representation of the corresponding NDFA:\n";
        std::cout << automata_str << "\n\n";
    }
}