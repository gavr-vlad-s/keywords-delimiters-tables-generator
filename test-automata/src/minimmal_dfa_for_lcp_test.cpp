/*
    File:    minimmal_dfa_for_lcp_test.cpp
    Created: 01 November 2021 at 20:49 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#include <iostream>
#include <string_view>
#include <vector>

#include "../include/minimmal_dfa_for_lcp_test.hpp"
#include "../../automata/include/minimal_dfa_for_lcp.hpp"
#include "../../automata/include/execute_minimal_dfa.hpp"

/*
 * F    Файлы
 * S    Строки
 * e    Кодировки
 * f    Форматирование
 * i    Ввод_вывод
 * p    Преамбула
 * s    Система
 */

namespace{
    const std::vector<std::string_view> standard_modules_sv = {
        "p", "si", "Se", "Sf", "siF"
    };

    const std::vector<std::string_view> checked_svs = {
        "p", "qw", "ssi", "si", "sie", "Su", "Se", "Set", "Sf", "Sfr", "sif", "sifg", "pR", "", "siF", "siFQ"
    };
};

void minimmal_dfa_for_lcp_test()
{
    auto min_dfa        = minimal_dfa_for_lcp(standard_modules_sv);
    auto min_dfa_as_str = minimal_dfa_to_string(min_dfa);
    std::cout << R"~(Minimal DFA for finding prefixes in the list ["p", "si", "Se", "Sf", "siF"])~" << "\n";
    std::cout << min_dfa_as_str << "\n\n";
    std::cout << "\n\nTesting prefixes\n";
    for(const auto& sv : checked_svs)
    {
        auto action = execute_min_dfa(sv, min_dfa);
        std::cout << "Checking the string \'"      << sv
                  << "\'. Index of found prefix: " << action
                  << ". Prefix: "                  << ((action == Action_traits<Action_type>::neutral()) ? "" : standard_modules_sv[action])
                  << "\n";

    }
}