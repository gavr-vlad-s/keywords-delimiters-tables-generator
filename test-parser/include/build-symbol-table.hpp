/*
    File:    build-symbol-table.hpp
    Created: 08 August 2021 at 13:30 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef BUILD_SYMBOL_TABLE_H
#define BUILD_SYMBOL_TABLE_H
#   include <memory>
#   include "../../tries/include/errors_and_tries.hpp"
#   include "../../astlib/include/symbol_table.hpp"
std::shared_ptr<symbol_table::Symbol_table> build_symbol_table(const std::shared_ptr<Char_trie>& strs_trie,
                                                               const std::shared_ptr<Char_trie>& ids_trie);
#endif