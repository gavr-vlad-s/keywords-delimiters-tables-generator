/*
    File:    testing-parser.cpp
    Created: 05 August 2021 at 20:57 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/usage.hpp"
#include "../../file_utils/include/get_processed_text.hpp"
#include "../../iscanner/include/location.hpp"
#include "../../tries/include/errors_and_tries.hpp"
#include "../../tries/include/error_count.hpp"
#include "../../tries/include/warning_count.hpp"
#include "../../tries/include/char_trie.hpp"
#include "../../fst-lvl-scanner/include/arkona-fst-lvl-scanner.hpp"
#include "../include/build-symbol-table.hpp"
#include "../../parserlib/include/arkona_parser.hpp"


enum Exit_codes{
    Success, No_args, Parsing_error, File_processing_error
};

int main(int argc, char* argv[])
{
    if(1 == argc){
        usage(argv[0]);
        return No_args;
    }

    auto              text      = file_utils::get_processed_text(argv[1]);
    if(text.empty()){
        return File_processing_error;
    }

    char32_t*         p          = const_cast<char32_t*>(text.c_str());
    auto              loc        = std::make_shared<iscaner::Location>(p);
    Errors_and_tries  et;
    et.ec_                       = std::make_shared<Error_count>();
    et.wc_                       = std::make_shared<Warning_count>();
    et.ids_trie_                 = std::make_shared<Char_trie>();
    et.strs_trie_                = std::make_shared<Char_trie>();
    auto              scanner    = std::make_shared<fst_level_scanner::Scanner>(loc, et);
    auto              sym_table  = build_symbol_table(et.strs_trie_, et.ids_trie_);
    auto              parser     = std::make_shared<arkona_parser::Parser>(scanner, et, sym_table);

    printf("Pointer to text:             %p\n", p);
    printf("Pointer to location info:    %p\n", loc.get());
    printf("Pointer to error counter:    %p\n", et.ec_.get());
    printf("Pointer to warning counter:  %p\n", et.wc_.get());
    printf("Pointer to identifiers trie: %p\n", et.ids_trie_.get());
    printf("Pointer to strings trie:     %p\n", et.strs_trie_.get());
    printf("Pointer to scanner:          %p\n", scanner.get());
    printf("Pointer to symbol table:     %p\n", sym_table.get());
    printf("Pointer to parser:           %p\n", parser.get());

    auto              amodule    = parser->parse();
    printf("Pointer to amodule:          %p\n", amodule.get());

    if(et.ec_->get_number_of_errors()){
        et.ec_->print();
        return Parsing_error;
    }

    auto              module_str = amodule->to_string();
    printf("String representation of the AST of the parsed module:\n%s\n",
           module_str.c_str());

    return Success;
}