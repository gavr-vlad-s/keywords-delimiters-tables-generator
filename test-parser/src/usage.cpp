/*
    File:    usage.cpp
    Created: 08 August 2021 at 09:54 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/usage.hpp"

static const char* usage_str =
    R"~(test-parserlib
Copyright (c) Gavrilov V.S., 2021
test-parserlib is a program for a testing of parsing of the programming language Arkona.

This program is free sofwtware, and it is licensed under the GPLv3 license.
There is NO warranty, not even MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Usage: %s test_file
)~";

void usage(const char* program_name)
{
    printf(usage_str, program_name);
}