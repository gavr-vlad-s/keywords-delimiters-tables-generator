/*
    File:    build-symbol-table.cpp
    Created: 08 August 2021 at 13:34 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/build-symbol-table.hpp"

std::shared_ptr<symbol_table::Symbol_table> build_symbol_table(const std::shared_ptr<Char_trie>& strs_trie,
                                                               const std::shared_ptr<Char_trie>& ids_trie)
{
    return std::make_shared<symbol_table::Symbol_table>(strs_trie, ids_trie);
}