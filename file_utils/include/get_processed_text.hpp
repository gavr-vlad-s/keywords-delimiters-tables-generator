/*
    File:    get_processed_text.hpp
    Created: 12 June 2021 at 13:24 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef GET_PROCESSED_TEXT_H
#define GET_PROCESSED_TEXT_H
#   include <string>
namespace file_utils{
    /* Function that opens a file with text. Returns a string with text if the file was
    * opened and the file size is not zero, and an empty string otherwise. */
    std::u32string get_processed_text(const char* name);
};
#endif