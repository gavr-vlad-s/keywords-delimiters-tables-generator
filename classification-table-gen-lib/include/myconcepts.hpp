/*
    File:    myconcepts.hpp
    Created: 16 июля 2017г.
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef MYCONCEPTS_H
#define MYCONCEPTS_H

#define RandomAccessIterator typename
#define Callable             typename
#define Integral             typename

#endif