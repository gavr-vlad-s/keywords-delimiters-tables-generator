/*
    File:    create_permutation.hpp
    Created: 08 May 2021 at 08:52 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef CREATE_PERMUTATION_H
#define CREATE_PERMUTATION_H
#include "../include/permutation.hpp"
Permutation create_permutation(size_t n);
#endif