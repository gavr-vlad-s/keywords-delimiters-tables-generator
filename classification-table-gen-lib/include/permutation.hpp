/*
    File:    permutation.hpp
    Created: 08 May 2021 at 08:52 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/
#ifndef PERMUTATION_H
#define PERMUTATION_H
#   include <vector>
#   include <cstddef>
using Permutation = std::vector<size_t>;
#endif