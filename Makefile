LINKER          = g++
LINKERFLAGS     = -s
CXX             = g++
CXXFLAGS        = -O3 -Wall -std=gnu++17 -Wextra
SRC_EXT         = cpp
CXXFLAGS        = -Wall -std=gnu++17 -Wextra


CHAR_CONV_SRC_PATH   = char-conv/src
CHAR_CONV_BUILD_PATH = char-conv/build
CHAR_CONV_LIB_NAME   = libcharconv.a
CHAR_CONV_SRCS       = $(shell find $(CHAR_CONV_SRC_PATH) -name '*.$(SRC_EXT)')
CHAR_CONV_OBJECTS    = $(CHAR_CONV_SRCS:$(CHAR_CONV_SRC_PATH)/%.$(SRC_EXT)=$(CHAR_CONV_BUILD_PATH)/%.o)

TRIES_SRC_PATH   = tries/src
TRIES_BUILD_PATH = tries/build
TRIES_LIB_NAME   = libtries.a
TRIES_SRCS       = $(shell find $(TRIES_SRC_PATH) -name '*.$(SRC_EXT)')
TRIES_OBJECTS    = $(TRIES_SRCS:$(TRIES_SRC_PATH)/%.$(SRC_EXT)=$(TRIES_BUILD_PATH)/%.o)

ISCANNER_SRC_PATH   = iscanner/src
ISCANNER_BUILD_PATH = iscanner/build
ISCANNER_LIB_NAME   = libiscanner.a
ISCANNER_SRCS       = $(shell find $(ISCANNER_SRC_PATH) -name '*.$(SRC_EXT)')
ISCANNER_OBJECTS    = $(ISCANNER_SRCS:$(ISCANNER_SRC_PATH)/%.$(SRC_EXT)=$(ISCANNER_BUILD_PATH)/%.o)

FMT_SRC_PATH   = thirdparty/fmtlib/src
FMT_BUILD_PATH = thirdparty/fmtlib/build
FMT_LIB_NAME   = libformatting.a
FMT_SRCS       = $(shell find $(FMT_SRC_PATH) -name '*.$(SRC_EXT)')
FMT_OBJECTS    = $(FMT_SRCS:$(FMT_SRC_PATH)/%.$(SRC_EXT)=$(FMT_BUILD_PATH)/%.o)

CLASSIFICATION_TGEN_SRC_PATH   = classification-table-gen-lib/src
CLASSIFICATION_TGEN_BUILD_PATH = classification-table-gen-lib/build
CLASSIFICATION_TGEN_LIB_NAME   = libclassifytgen.a
CLASSIFICATION_TGEN_SRCS       = $(shell find $(CLASSIFICATION_TGEN_SRC_PATH) -name '*.$(SRC_EXT)')
CLASSIFICATION_TGEN_OBJECTS    = $(CLASSIFICATION_TGEN_SRCS:$(CLASSIFICATION_TGEN_SRC_PATH)/%.$(SRC_EXT)=$(CLASSIFICATION_TGEN_BUILD_PATH)/%.o)

TESTING_KWDELIM_TABLES_SRC_PATH   = test-kw-delim-tables-generating/src
TESTING_KWDELIM_TABLES_BUILD_PATH = test-kw-delim-tables-generating/build
TESTING_KWDELIM_TABLES_BIN_NAME   = testing-kwdelim
TESTING_KWDELIM_TABLES_SRCS       = $(shell find $(TESTING_KWDELIM_TABLES_SRC_PATH) -name '*.$(SRC_EXT)')
TESTING_KWDELIM_TABLES_OBJECTS    = $(TESTING_KWDELIM_TABLES_SRCS:$(TESTING_KWDELIM_TABLES_SRC_PATH)/%.$(SRC_EXT)=$(TESTING_KWDELIM_TABLES_BUILD_PATH)/%.o)

TESTING_CLASSIFICATION_TGEN_SRC_PATH   = test-classification-table-gen-lib/src
TESTING_CLASSIFICATION_TGEN_BUILD_PATH = test-classification-table-gen-lib/build
TESTING_CLASSIFICATION_TGEN_BIN_NAME   = testing-tgen
TESTING_CLASSIFICATION_TGEN_SRCS       = $(shell find $(TESTING_CLASSIFICATION_TGEN_SRC_PATH) -name '*.$(SRC_EXT)')
TESTING_CLASSIFICATION_TGEN_OBJECTS    = $(TESTING_CLASSIFICATION_TGEN_SRCS:$(TESTING_CLASSIFICATION_TGEN_SRC_PATH)/%.$(SRC_EXT)=$(TESTING_CLASSIFICATION_TGEN_BUILD_PATH)/%.o)

FST_LVL_TGEN_SRC_PATH   = fst-lvl-scanner-tables-generation/src
FST_LVL_TGEN_BUILD_PATH = fst-lvl-scanner-tables-generation/build
FST_LVL_TGEN_BIN_NAME   = tgen
FST_LVL_TGEN_SRCS       = $(shell find $(FST_LVL_TGEN_SRC_PATH) -name '*.$(SRC_EXT)')
FST_LVL_TGEN_OBJECTS    = $(FST_LVL_TGEN_SRCS:$(FST_LVL_TGEN_SRC_PATH)/%.$(SRC_EXT)=$(FST_LVL_TGEN_BUILD_PATH)/%.o)

NUMBERS_SRC_PATH   = numbers/src
NUMBERS_BUILD_PATH = numbers/build
NUMBERS_LIB_NAME   = libnumberslib.a
NUMBERS_SRCS       = $(shell find $(NUMBERS_SRC_PATH) -name '*.$(SRC_EXT)')
NUMBERS_OBJECTS    = $(NUMBERS_SRCS:$(NUMBERS_SRC_PATH)/%.$(SRC_EXT)=$(NUMBERS_BUILD_PATH)/%.o)

FST_LVL_SCANNER_SRC_PATH   = fst-lvl-scanner/src
FST_LVL_SCANNER_BUILD_PATH = fst-lvl-scanner/build
FST_LVL_SCANNER_LIB_NAME   = libfstlvlscanner.a
FST_LVL_SCANNER_SRCS       = $(shell find $(FST_LVL_SCANNER_SRC_PATH) -name '*.$(SRC_EXT)')
FST_LVL_SCANNER_OBJECTS    = $(FST_LVL_SCANNER_SRCS:$(FST_LVL_SCANNER_SRC_PATH)/%.$(SRC_EXT)=$(FST_LVL_SCANNER_BUILD_PATH)/%.o)

TESTING_FST_LVL_SCANNER_SRC_PATH   = test-fst-lvl-scanner/src
TESTING_FST_LVL_SCANNER_BUILD_PATH = test-fst-lvl-scanner/build
TESTING_FST_LVL_SCANNER_BIN_NAME   = testing-fst-scanner
TESTING_FST_LVL_SCANNER_SRCS       = $(shell find $(TESTING_FST_LVL_SCANNER_SRC_PATH) -name '*.$(SRC_EXT)')
TESTING_FST_LVL_SCANNER_OBJECTS    = $(TESTING_FST_LVL_SCANNER_SRCS:$(TESTING_FST_LVL_SCANNER_SRC_PATH)/%.$(SRC_EXT)=$(TESTING_FST_LVL_SCANNER_BUILD_PATH)/%.o)

LFILES_SRC_PATH   = file_utils/src
LFILES_BUILD_PATH = file_utils/build
LFILES_LIB_NAME   = libfiles.a
LFILES_SRCS       = $(shell find $(LFILES_SRC_PATH) -name '*.$(SRC_EXT)')
LFILES_OBJECTS    = $(LFILES_SRCS:$(LFILES_SRC_PATH)/%.$(SRC_EXT)=$(LFILES_BUILD_PATH)/%.o)

SND_LVL_SCANNER_SRC_PATH   = snd-lvl-scanner/src
SND_LVL_SCANNER_BUILD_PATH = snd-lvl-scanner/build
SND_LVL_SCANNER_LIB_NAME   = libsndlvlscanner.a
SND_LVL_SCANNER_SRCS       = $(shell find $(SND_LVL_SCANNER_SRC_PATH) -name '*.$(SRC_EXT)')
SND_LVL_SCANNER_OBJECTS    = $(SND_LVL_SCANNER_SRCS:$(SND_LVL_SCANNER_SRC_PATH)/%.$(SRC_EXT)=$(SND_LVL_SCANNER_BUILD_PATH)/%.o)

TESTING_SND_LVL_SCANNER_SRC_PATH   = test-snd-lvl-scanner/src
TESTING_SND_LVL_SCANNER_BUILD_PATH = test-snd-lvl-scanner/build
TESTING_SND_LVL_SCANNER_BIN_NAME   = test-snd-scanner
TESTING_SND_LVL_SCANNER_SRCS       = $(shell find $(TESTING_SND_LVL_SCANNER_SRC_PATH) -name '*.$(SRC_EXT)')
TESTING_SND_LVL_SCANNER_OBJECTS    = $(TESTING_SND_LVL_SCANNER_SRCS:$(TESTING_SND_LVL_SCANNER_SRC_PATH)/%.$(SRC_EXT)=$(TESTING_SND_LVL_SCANNER_BUILD_PATH)/%.o)

AST_LIB_SRC_PATH   = astlib/src
AST_LIB_BUILD_PATH = astlib/build
AST_LIB_LIB_NAME   = libastl.a
AST_LIB_SRCS       = $(shell find $(AST_LIB_SRC_PATH) -name '*.$(SRC_EXT)')
AST_LIB_OBJECTS    = $(AST_LIB_SRCS:$(AST_LIB_SRC_PATH)/%.$(SRC_EXT)=$(AST_LIB_BUILD_PATH)/%.o)

BITSET_SRC_PATH   = bitset/src
BITSET_BUILD_PATH = bitset/build
BITSET_LIB_NAME   = libbitsetlib.a
BITSET_SRCS       = $(shell find $(BITSET_SRC_PATH) -name '*.$(SRC_EXT)')
BITSET_OBJECTS    = $(BITSET_SRCS:$(BITSET_SRC_PATH)/%.$(SRC_EXT)=$(BITSET_BUILD_PATH)/%.o)

PARSER_SRC_PATH   = parserlib/src
PARSER_BUILD_PATH = parserlib/build
PARSER_LIB_NAME   = libparserlib.a
PARSER_SRCS       = $(shell find $(PARSER_SRC_PATH) -name '*.$(SRC_EXT)')
PARSER_OBJECTS    = $(PARSER_SRCS:$(PARSER_SRC_PATH)/%.$(SRC_EXT)=$(PARSER_BUILD_PATH)/%.o)

TESTING_PARSER_SRC_PATH   = test-parser/src
TESTING_PARSER_BUILD_PATH = test-parser/build
TESTING_PARSER_BIN_NAME   = test-parserlib
TESTING_PARSER_SRCS       = $(shell find $(TESTING_PARSER_SRC_PATH) -name '*.$(SRC_EXT)')
TESTING_PARSER_OBJECTS    = $(TESTING_PARSER_SRCS:$(TESTING_PARSER_SRC_PATH)/%.$(SRC_EXT)=$(TESTING_PARSER_BUILD_PATH)/%.o)

TESTING_AUTOMATA_SRC_PATH   = test-automata/src
TESTING_AUTOMATA_BUILD_PATH = test-automata/build
TESTING_AUTOMATA_BIN_NAME   = test-aut
TESTING_AUTOMATA_SRCS       = $(shell find $(TESTING_AUTOMATA_SRC_PATH) -name '*.$(SRC_EXT)')
TESTING_AUTOMATA_OBJECTS    = $(TESTING_AUTOMATA_SRCS:$(TESTING_AUTOMATA_SRC_PATH)/%.$(SRC_EXT)=$(TESTING_AUTOMATA_BUILD_PATH)/%.o)


CHAR_CONV_LIB           = -L$(CHAR_CONV_BUILD_PATH) -lcharconv
TRIES_LIB               = -L$(TRIES_BUILD_PATH) -ltries
FMT_LIB                 = -L$(FMT_BUILD_PATH) -lformatting 
ISCANNER_LIB            = -L$(ISCANNER_BUILD_PATH) -liscanner 
CLASSIFICATION_TGEN_LIB = -L$(CLASSIFICATION_TGEN_BUILD_PATH) -lclassifytgen
NUMBERS_LIB             = -L$(NUMBERS_BUILD_PATH) -lnumberslib
FST_LVL_SCAN_LIB        = -L$(FST_LVL_SCANNER_BUILD_PATH) -lfstlvlscanner
FS_LIB                  = -lstdc++fs
QUAD_LIB                = -lquadmath
LFILE_LIB               = -L$(LFILES_BUILD_PATH) -lfiles
SND_LVL_SCAN_LIB        = -L$(SND_LVL_SCANNER_BUILD_PATH) -lsndlvlscanner
AST_LIB                 = -L$(AST_LIB_BUILD_PATH) -lastl
BITSET_LIB              = -L$(BITSET_BUILD_PATH) -lbitsetlib
PARSER_LIB              = -L$(PARSER_BUILD_PATH) -lparserlib

TESTING_KWDELIM_TABLES_LIBS      = $(TRIES_LIB) $(CHAR_CONV_LIB) $(FMT_LIB)
TESTING_CLASSIFICATION_TGEN_LIBS = $(CLASSIFICATION_TGEN_LIB) $(CHAR_CONV_LIB) $(FMT_LIB)
TGEN_LIBS                        = $(CLASSIFICATION_TGEN_LIB) $(TRIES_LIB) $(CHAR_CONV_LIB) $(FMT_LIB) 
TESTING_FST_LVL_SCANNER_LIBS     = $(LFILE_LIB) $(FST_LVL_SCAN_LIB) $(ISCANNER_LIB) $(NUMBERS_LIB) $(TRIES_LIB) $(CHAR_CONV_LIB) $(FS_LIB) $(QUAD_LIB)
TESTING_SND_LVL_SCANNER_LIBS     = $(LFILE_LIB) $(SND_LVL_SCAN_LIB) $(FST_LVL_SCAN_LIB) $(ISCANNER_LIB) $(NUMBERS_LIB) $(TRIES_LIB) $(CHAR_CONV_LIB) $(FS_LIB) $(QUAD_LIB)
TESTING_PARSER_LIBS              = $(LFILE_LIB) $(PARSER_LIB) $(BITSET_LIB) $(AST_LIB) $(FST_LVL_SCAN_LIB) $(ISCANNER_LIB) $(NUMBERS_LIB) $(TRIES_LIB) $(CHAR_CONV_LIB) $(FS_LIB) $(QUAD_LIB) $(FMT_LIB)
TESTING_AUTOMATA_LIBS            = $(TRIES_LIB) $(FMT_LIB)

BUILD_MSG_PRINT = @tput bold; tput setaf 6; echo "$(1)"; tput sgr0

.PHONY: all clean charconv triesl formatting scanner-prj iscanner testing-kw-delim-tables lclassify classify-test fst-tgen libnum libfstlvlsc lfiles fst-lvl-testing libsndlvlsc snd-lvl-testing parser-prj ast-lib lbitset lparser parser-testing automata-testing

all: charconv triesl formatting scanner-prj parser-prj

charconv: $(CHAR_CONV_BUILD_PATH)/$(CHAR_CONV_LIB_NAME)

$(CHAR_CONV_BUILD_PATH)/$(CHAR_CONV_LIB_NAME): $(CHAR_CONV_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking character convertation library.")
	ar cr $@ $(CHAR_CONV_OBJECTS)
	@echo "             "

$(CHAR_CONV_BUILD_PATH)/%.o: $(CHAR_CONV_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

triesl: $(TRIES_BUILD_PATH)/$(TRIES_LIB_NAME)

$(TRIES_BUILD_PATH)/$(TRIES_LIB_NAME): $(TRIES_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for tries.")
	ar cr $@ $(TRIES_OBJECTS)
	@echo "             "

$(TRIES_BUILD_PATH)/%.o: $(TRIES_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

formatting: $(FMT_BUILD_PATH)/$(FMT_LIB_NAME)

$(FMT_BUILD_PATH)/$(FMT_LIB_NAME): $(FMT_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking fmtlib library.")
	ar cr $@ $(FMT_OBJECTS)
	@echo "             "

$(FMT_BUILD_PATH)/%.o: $(FMT_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

scanner-prj: iscanner testing-kw-delim-tables lclassify classify-test fst-tgen libnum libfstlvlsc lfiles fst-lvl-testing libsndlvlsc snd-lvl-testing

iscanner: $(ISCANNER_BUILD_PATH)/$(ISCANNER_LIB_NAME)

$(ISCANNER_BUILD_PATH)/$(ISCANNER_LIB_NAME): $(ISCANNER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for abstract scanner.")
	ar cr $@ $(ISCANNER_OBJECTS)
	@echo "             "

$(ISCANNER_BUILD_PATH)/%.o: $(ISCANNER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

testing-kw-delim-tables: $(TESTING_KWDELIM_TABLES_BUILD_PATH)/$(TESTING_KWDELIM_TABLES_BIN_NAME)

$(TESTING_KWDELIM_TABLES_BUILD_PATH)/$(TESTING_KWDELIM_TABLES_BIN_NAME): $(TESTING_KWDELIM_TABLES_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for testing keywords and delimiters transition tables.")
	$(LINKER) -o $@ $(TESTING_KWDELIM_TABLES_OBJECTS) $(TESTING_KWDELIM_TABLES_LIBS) $(LINKERFLAGS)
	@echo "             "

$(TESTING_KWDELIM_TABLES_BUILD_PATH)/%.o: $(TESTING_KWDELIM_TABLES_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

lclassify: $(CLASSIFICATION_TGEN_BUILD_PATH)/$(CLASSIFICATION_TGEN_LIB_NAME)

$(CLASSIFICATION_TGEN_BUILD_PATH)/$(CLASSIFICATION_TGEN_LIB_NAME): $(CLASSIFICATION_TGEN_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for generating character classification table.")
	ar cr $@ $(CLASSIFICATION_TGEN_OBJECTS)
	@echo "             "

$(CLASSIFICATION_TGEN_BUILD_PATH)/%.o: $(CLASSIFICATION_TGEN_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

classify-test: $(TESTING_CLASSIFICATION_TGEN_BUILD_PATH)/$(TESTING_CLASSIFICATION_TGEN_BIN_NAME)

$(TESTING_CLASSIFICATION_TGEN_BUILD_PATH)/$(TESTING_CLASSIFICATION_TGEN_BIN_NAME): $(TESTING_CLASSIFICATION_TGEN_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for testing character classification table generating.")
	$(LINKER) -o $@ $(TESTING_CLASSIFICATION_TGEN_OBJECTS) $(TESTING_CLASSIFICATION_TGEN_LIBS) $(LINKERFLAGS)
	@echo "             "

$(TESTING_CLASSIFICATION_TGEN_BUILD_PATH)/%.o: $(TESTING_CLASSIFICATION_TGEN_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

fst-tgen: $(FST_LVL_TGEN_BUILD_PATH)/$(FST_LVL_TGEN_BIN_NAME)
	$(call BUILD_MSG_PRINT,"Generating tables for the first level of the scanner.")
	$(FST_LVL_TGEN_BUILD_PATH)/$(FST_LVL_TGEN_BIN_NAME)

$(FST_LVL_TGEN_BUILD_PATH)/$(FST_LVL_TGEN_BIN_NAME): $(FST_LVL_TGEN_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for generating of character classification table and transition tables.")
	$(LINKER) -o $@ $(FST_LVL_TGEN_OBJECTS) $(TGEN_LIBS) $(LINKERFLAGS)
	@echo "             "

$(FST_LVL_TGEN_BUILD_PATH)/%.o: $(FST_LVL_TGEN_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

libnum: $(NUMBERS_BUILD_PATH)/$(NUMBERS_LIB_NAME)

$(NUMBERS_BUILD_PATH)/$(NUMBERS_LIB_NAME): $(NUMBERS_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library numberslib.")
	ar cr $@ $(NUMBERS_OBJECTS)
	@echo "             "

$(NUMBERS_BUILD_PATH)/%.o: $(NUMBERS_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

libfstlvlsc: $(FST_LVL_SCANNER_BUILD_PATH)/$(FST_LVL_SCANNER_LIB_NAME)

$(FST_LVL_SCANNER_BUILD_PATH)/$(FST_LVL_SCANNER_LIB_NAME): $(FST_LVL_SCANNER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for the first level of the scanner")
	ar cr $@ $(FST_LVL_SCANNER_OBJECTS)
	@echo "             "

$(FST_LVL_SCANNER_BUILD_PATH)/%.o: $(FST_LVL_SCANNER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

lfiles: $(LFILES_BUILD_PATH)/$(LFILES_LIB_NAME)

$(LFILES_BUILD_PATH)/$(LFILES_LIB_NAME): $(LFILES_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking file utils library.")
	ar cr $@ $(LFILES_OBJECTS)
	@echo "             "

$(LFILES_BUILD_PATH)/%.o: $(LFILES_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

fst-lvl-testing: $(TESTING_FST_LVL_SCANNER_BUILD_PATH)/$(TESTING_FST_LVL_SCANNER_BIN_NAME)

$(TESTING_FST_LVL_SCANNER_BUILD_PATH)/$(TESTING_FST_LVL_SCANNER_BIN_NAME): $(TESTING_FST_LVL_SCANNER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for testing the first level scanner.")
	$(LINKER) -o $@ $(TESTING_FST_LVL_SCANNER_OBJECTS) $(TESTING_FST_LVL_SCANNER_LIBS) $(LINKERFLAGS)
	@echo "             "

$(TESTING_FST_LVL_SCANNER_BUILD_PATH)/%.o: $(TESTING_FST_LVL_SCANNER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

libsndlvlsc: $(SND_LVL_SCANNER_BUILD_PATH)/$(SND_LVL_SCANNER_LIB_NAME)

$(SND_LVL_SCANNER_BUILD_PATH)/$(SND_LVL_SCANNER_LIB_NAME): $(SND_LVL_SCANNER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for the second level of the scanner")
	ar cr $@ $(SND_LVL_SCANNER_OBJECTS)
	@echo "             "

$(SND_LVL_SCANNER_BUILD_PATH)/%.o: $(SND_LVL_SCANNER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

snd-lvl-testing: $(TESTING_SND_LVL_SCANNER_BUILD_PATH)/$(TESTING_SND_LVL_SCANNER_BIN_NAME)

$(TESTING_SND_LVL_SCANNER_BUILD_PATH)/$(TESTING_SND_LVL_SCANNER_BIN_NAME): $(TESTING_SND_LVL_SCANNER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for testing the second level scanner.")
	$(LINKER) -o $@ $(TESTING_SND_LVL_SCANNER_OBJECTS) $(TESTING_SND_LVL_SCANNER_LIBS) $(LINKERFLAGS)
	@echo "             "

$(TESTING_SND_LVL_SCANNER_BUILD_PATH)/%.o: $(TESTING_SND_LVL_SCANNER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

parser-prj: ast-lib lbitset lparser parser-testing automata-testing

ast-lib: $(AST_LIB_BUILD_PATH)/$(AST_LIB_LIB_NAME)

$(AST_LIB_BUILD_PATH)/$(AST_LIB_LIB_NAME): $(AST_LIB_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for the AST processing")
	ar cr $@ $(AST_LIB_OBJECTS)
	@echo "             "

$(AST_LIB_BUILD_PATH)/%.o: $(AST_LIB_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

lbitset: $(BITSET_BUILD_PATH)/$(BITSET_LIB_NAME)

$(BITSET_BUILD_PATH)/$(BITSET_LIB_NAME): $(BITSET_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for the Pascal style sets")
	ar cr $@ $(BITSET_OBJECTS)
	@echo "             "

$(BITSET_BUILD_PATH)/%.o: $(BITSET_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

lparser: $(PARSER_BUILD_PATH)/$(PARSER_LIB_NAME)

$(PARSER_BUILD_PATH)/$(PARSER_LIB_NAME): $(PARSER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for parsing the programming language Arkona")
	ar cr $@ $(PARSER_OBJECTS)
	@echo "             "

$(PARSER_BUILD_PATH)/%.o: $(PARSER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

parser-testing: $(TESTING_PARSER_BUILD_PATH)/$(TESTING_PARSER_BIN_NAME)

$(TESTING_PARSER_BUILD_PATH)/$(TESTING_PARSER_BIN_NAME): $(TESTING_PARSER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for testing the parser library.")
	$(LINKER) -o $@ $(TESTING_PARSER_OBJECTS) $(TESTING_PARSER_LIBS) $(LINKERFLAGS)
	@echo "             "

$(TESTING_PARSER_BUILD_PATH)/%.o: $(TESTING_PARSER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

automata-testing: $(TESTING_AUTOMATA_BUILD_PATH)/$(TESTING_AUTOMATA_BIN_NAME)

$(TESTING_AUTOMATA_BUILD_PATH)/$(TESTING_AUTOMATA_BIN_NAME): $(TESTING_AUTOMATA_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for testing the FSMs processing library.")
	$(LINKER) -o $@ $(TESTING_AUTOMATA_OBJECTS) $(TESTING_AUTOMATA_LIBS) $(LINKERFLAGS)
	@echo "             "

$(TESTING_AUTOMATA_BUILD_PATH)/%.o: $(TESTING_AUTOMATA_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	@tput bold; tput setaf 6
	@echo "Removing object files, library files, and executable files."
	@tput sgr0
	rm -f $(CHAR_CONV_OBJECTS)
	rm -f $(CHAR_CONV_BUILD_PATH)/$(CHAR_CONV_LIB_NAME)
	rm -f $(TRIES_OBJECTS)
	rm -f $(TRIES_BUILD_PATH)/$(TRIES_LIB_NAME)
	rm -f $(FMT_OBJECTS)
	rm -f $(FMT_BUILD_PATH)/$(FMT_LIB_NAME)
	rm -f $(ISCANNER_OBJECTS)
	rm -f $(ISCANNER_BUILD_PATH)/$(ISCANNER_LIB_NAME)
	rm -f $(TESTING_KWDELIM_TABLES_OBJECTS)
	rm -f $(TESTING_KWDELIM_TABLES_BUILD_PATH)/$(TESTING_KWDELIM_TABLES_BIN_NAME)
	rm -f $(CLASSIFICATION_TGEN_BUILD_PATH)/$(CLASSIFICATION_TGEN_LIB_NAME)
	rm -f $(CLASSIFICATION_TGEN_OBJECTS)
	rm -f $(TESTING_CLASSIFICATION_TGEN_OBJECTS)
	rm -f $(TESTING_CLASSIFICATION_TGEN_BUILD_PATH)/$(TESTING_CLASSIFICATION_TGEN_BIN_NAME)
	rm -f $(FST_LVL_TGEN_OBJECTS)
	rm -f $(FST_LVL_TGEN_BUILD_PATH)/$(FST_LVL_TGEN_BIN_NAME)
	rm -f $(NUMBERS_OBJECTS)
	rm -f $(NUMBERS_BUILD_PATH)/$(NUMBERS_LIB_NAME)
	rm -f $(FST_LVL_SCANNER_OBJECTS)
	rm -f $(FST_LVL_SCANNER_BUILD_PATH)/$(FST_LVL_SCANNER_LIB_NAME)
	rm -f $(TESTING_FST_LVL_SCANNER_OBJECTS)
	rm -f $(TESTING_FST_LVL_SCANNER_BUILD_PATH)/$(TESTING_FST_LVL_SCANNER_BIN_NAME)
	rm -f $(LFILES_OBJECTS)
	rm -f $(LFILES_BUILD_PATH)/$(LFILES_LIB_NAME)
	rm -f $(SND_LVL_SCANNER_OBJECTS)
	rm -f $(SND_LVL_SCANNER_BUILD_PATH)/$(SND_LVL_SCANNER_LIB_NAME)
	rm -f $(TESTING_SND_LVL_SCANNER_OBJECTS)
	rm -f $(TESTING_SND_LVL_SCANNER_BUILD_PATH)/$(TESTING_SND_LVL_SCANNER_BIN_NAME)
	rm -f $(AST_LIB_OBJECTS)
	rm -f $(AST_LIB_BUILD_PATH)/$(AST_LIB_LIB_NAME)
	rm -f $(BITSET_OBJECTS)
	rm -f $(BITSET_BUILD_PATH)/$(BITSET_LIB_NAME)
	rm -f $(PARSER_OBJECTS)
	rm -f $(PARSER_BUILD_PATH)/$(PARSER_LIB_NAME)
	rm -f $(TESTING_PARSER_OBJECTS)
	rm -f $(TESTING_PARSER_BUILD_PATH)/$(TESTING_PARSER_BIN_NAME)
	rm -f $(TESTING_AUTOMATA_OBJECTS)
	rm -f $(TESTING_AUTOMATA_BUILD_PATH)/$(TESTING_AUTOMATA_BIN_NAME)
